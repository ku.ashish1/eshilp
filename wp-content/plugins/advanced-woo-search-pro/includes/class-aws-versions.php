<?php
/**
 * Versions capability
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! class_exists( 'AWS_PRO_Versions' ) ) :

/**
 * Class for plugin search
 */
class AWS_PRO_Versions {

    /**
     * Return a singleton instance of the current class
     *
     * @return object
     */
    public static function factory() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new self();
            $instance->setup();
        }

        return $instance;
    }

    /**
     * Placeholder
     */
    public function __construct() {}

    /**
     * Setup actions and filters for all things settings
     */
    public function setup() {

        $current_version = get_option( 'aws_pro_plugin_ver' );
        $reindex_version = get_option( 'aws_pro_reindex_version' );

        if ( ! ( $reindex_version ) && current_user_can( 'manage_options' ) ) {
            add_action( 'admin_notices', array( $this, 'admin_notice_no_index' ) );
        }


        if ( $reindex_version && version_compare( $reindex_version, '1.28', '<' ) && current_user_can( 'manage_options' ) ) {
            add_action( 'admin_notices', array( $this, 'admin_notice_reindex' ) );
        }


        $stopwords = get_option( 'aws_pro_stopwords' );

        if ( $stopwords === false ) {
            $stopwords = 'a, also, am, an, and, are, as, at, be, but, by, call, can, co, con, de, do, due, eg, eight, etc, even, ever, every, for, from, full, go, had, has, hasnt, have, he, hence, her, here, his, how, ie, if, in, inc, into, is, it, its, ltd, me, my, no, none, nor, not, now, of, off, on, once, one, only, onto, or, our, ours, out, over, own, part, per, put, re, see, so, some, ten, than, that, the, their, there, these, they, this, three, thru, thus, to, too, top, un, up, us, very, via, was, we, well, were, what, when, where, who, why, will';
            update_option( 'aws_pro_stopwords', $stopwords, 'no' );
        }

        if ( $current_version ) {

            if ( version_compare( $current_version, '1.29', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );
                $options_array = AWS_Admin_Options::options_array();

                if ( $settings ) {
                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach( $search_instance_settings['filters'] as $filter_num => $filter_settings ) {

                                if ( isset( $filter_settings['search_in'] ) && is_string( $filter_settings['search_in'] ) ) {
                                    $current_search_in = explode( ',', $filter_settings['search_in'] );
                                    $new_search_in = array();
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_in' && isset( $def_option['choices'] ) ) {
                                            foreach( $def_option['choices'] as $choice_key => $choice_label ) {
                                                $new_search_in[$choice_key] = in_array( $choice_key, $current_search_in ) ? 1 : 0;
                                            }
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_in'] = $new_search_in;
                                            break;
                                        }
                                    }
                                }

                                if ( ! isset( $filter_settings['search_in_attr'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_in_attr' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_in_attr'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                                if ( ! isset( $filter_settings['search_in_tax'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_in_tax' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_in_tax'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

                AWS_Helpers::add_term_id_column();

            }

            if ( version_compare( $current_version, '1.30', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['show_more'] ) ) {
                            $settings[$search_instance_num]['show_more'] = 'false';
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.32', '<' ) ) {

                if ( ! AWS_Helpers::is_table_not_exist() ) {

                    global $wpdb;
                    $table_name =  $wpdb->prefix . AWS_INDEX_TABLE_NAME;

                    $wpdb->query("
                        ALTER TABLE {$table_name}
                        MODIFY term_source varchar(50);
                    ");

                }

                $settings = get_option( 'aws_pro_settings' );
                $options_array = AWS_Admin_Options::options_array();

                if ( $settings ) {
                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach( $search_instance_settings['filters'] as $filter_num => $filter_settings ) {

                                if ( isset( $filter_settings['search_in'] ) && is_string( $filter_settings['search_in'] ) ) {
                                    $current_search_in = explode( ',', $filter_settings['search_in'] );
                                    $new_search_in = array();
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_in' && isset( $def_option['choices'] ) ) {
                                            foreach( $def_option['choices'] as $choice_key => $choice_label ) {
                                                $new_search_in[$choice_key] = in_array( $choice_key, $current_search_in ) ? 1 : 0;
                                            }
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_in'] = $new_search_in;
                                            break;
                                        }
                                    }
                                }

                                if ( ! isset( $filter_settings['search_in_meta'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_in_meta' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_in_meta'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.36', '<' ) ) {
                $seamless = get_option( 'aws_pro_seamless' );

                if ( $seamless === false ) {
                    $seamless = 'false';
                    update_option( 'aws_pro_seamless', $seamless );
                }
            }

            if ( version_compare( $current_version, '1.37', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['show_clear'] ) ) {
                            $settings[$search_instance_num]['show_clear'] = 'false';
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.38', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['show_more_text'] ) ) {
                            $settings[$search_instance_num]['show_more_text'] = __('View all results', 'advanced-woo-search');
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.41', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {
                                if ( ! isset( $filter_settings['show_cart'] ) ) {
                                    $settings[$search_instance_num]['filters'][$filter_num]['show_cart'] = 'false';
                                }
                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.44', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {
                                if ( ! isset( $filter_settings['on_sale'] ) ) {
                                    $settings[$search_instance_num]['filters'][$filter_num]['on_sale'] = 'true';
                                }
                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

                AWS_Helpers::add_on_sale_column();

            }

            if ( version_compare( $current_version, '1.45', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {

                                if ( isset( $filter_settings['exclude_cats'] ) && $filter_settings['exclude_cats'] ) {
                                    if ( ! preg_match( "/[^\\d^,^\\s]/i", $filter_settings['exclude_cats'] ) ) {
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a1']['source'] = 'tax:product_cat';
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a1']['term'] = explode( ',', $filter_settings['exclude_cats'] );
                                        $settings[$search_instance_num]['filters'][$filter_num]['exclude_cats'] = '';
                                    }
                                }

                                if ( isset( $filter_settings['exclude_tags'] ) && $filter_settings['exclude_tags'] ) {
                                    if ( ! preg_match( "/[^\\d^,^\\s]/i", $filter_settings['exclude_tags'] ) ) {
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a2']['source'] = 'tax:product_tag';
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a2']['term'] = explode( ',', $filter_settings['exclude_tags'] );
                                        $settings[$search_instance_num]['filters'][$filter_num]['exclude_tags'] = '';
                                    }
                                }

                                if ( isset( $filter_settings['exclude_products'] ) && $filter_settings['exclude_products'] ) {
                                    if ( ! preg_match( "/[^\\d^,^\\s]/i", $filter_settings['exclude_products'] ) ) {
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a3']['source'] = 'product';
                                        $settings[$search_instance_num]['filters'][$filter_num]['adv_filters']['group_a3']['term'] = explode( ',', $filter_settings['exclude_products'] );
                                        $settings[$search_instance_num]['filters'][$filter_num]['exclude_products'] = '';
                                    }
                                }

                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.50', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {
                                if ( ! isset( $filter_settings['show_outofstock_price'] ) ) {
                                    $settings[$search_instance_num]['filters'][$filter_num]['show_outofstock_price'] = 'true';
                                }
                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.51', '<' ) ) {
                $sync = get_option( 'aws_pro_autoupdates' );

                if ( $sync === false ) {
                    $sync = 'true';
                    update_option( 'aws_pro_autoupdates', $sync, 'no' );
                }
            }

            if ( version_compare( $current_version, '1.58', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['search_exact'] ) ) {
                            $settings[$search_instance_num]['search_exact'] = 'false';
                        }
                    }

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {
                                if ( ! isset( $filter_settings['var_rules'] ) ) {
                                    $settings[$search_instance_num]['filters'][$filter_num]['var_rules'] = 'parent';
                                }
                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.60', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );
                $options_array = AWS_Admin_Options::options_array();

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {

                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach( $search_instance_settings['filters'] as $filter_num => $filter_settings ) {

                                if ( ! isset( $filter_settings['search_archives'] ) ) {

                                    $show_cats = $settings[$search_instance_num]['filters'][$filter_num]['show_cats'] === 'true' ? 1 : 0;
                                    $show_tags = $settings[$search_instance_num]['filters'][$filter_num]['show_tags'] === 'true' ? 1 : 0;

                                    $settings[$search_instance_num]['filters'][$filter_num]['search_archives']['archive_category'] = $show_cats;
                                    $settings[$search_instance_num]['filters'][$filter_num]['search_archives']['archive_tag'] = $show_tags;
                                    $settings[$search_instance_num]['filters'][$filter_num]['search_archives']['archive_tax'] = 0;

                                }

                                if ( ! isset( $filter_settings['search_archives_tax'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_archives_tax' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_archives_tax'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                                if ( ! isset( $filter_settings['search_archives_attr'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_archives_attr' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_archives_attr'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.62', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['disable_smooth'] ) ) {
                            $settings[$search_instance_num]['disable_smooth'] = 'false';
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.70', '<' ) ) {

                $synonyms = get_option( 'aws_pro_synonyms' );

                if ( $synonyms === false ) {
                    $synonyms = 'buy, pay, purchase, acquire&#13;&#10;box, housing, unit, package';
                    update_option( 'aws_pro_synonyms', $synonyms, 'no' );
                }

            }

            if ( version_compare( $current_version, '1.80', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach ($search_instance_settings['filters'] as $filter_num => $filter_settings) {
                                if ( ! isset( $filter_settings['highlight'] ) && isset( $filter_settings['mark_words'] ) ) {
                                    $mark_words = $filter_settings['mark_words'];
                                    $settings[$search_instance_num]['filters'][$filter_num]['highlight'] = $mark_words;
                                }
                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '1.87', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {
                        if ( ! isset( $search_instance_settings['mobile_overlay'] ) ) {
                            $settings[$search_instance_num]['mobile_overlay'] = 'false';
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

            if ( version_compare( $current_version, '2.04', '<' ) ) {

                $settings = get_option( 'aws_pro_settings' );
                $options_array = AWS_Admin_Options::options_array();

                if ( $settings ) {

                    foreach( $settings as $search_instance_num => $search_instance_settings ) {

                        if ( isset( $search_instance_settings['filters'] ) ) {
                            foreach( $search_instance_settings['filters'] as $filter_num => $filter_settings ) {

                                if ( isset( $filter_settings['search_archives'] ) && ! isset( $filter_settings['search_archives']['archive_users'] ) ) {
                                    $settings[$search_instance_num]['filters'][$filter_num]['search_archives']['archive_users'] = 0;
                                }

                                if ( ! isset( $filter_settings['search_archives_users'] ) ) {
                                    foreach( $options_array['results'] as $def_option ) {
                                        if ( isset( $def_option['id'] ) && $def_option['id'] === 'search_archives_users' && isset( $def_option['value'] ) ) {
                                            $settings[$search_instance_num]['filters'][$filter_num]['search_archives_users'] = $def_option['value'];
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }

                    update_option( 'aws_pro_settings', $settings );

                }

            }

        }

        if ( ! $current_version ) {

            AWS_Helpers::add_term_id_column();
            AWS_Helpers::add_on_sale_column();

        }

        update_option( 'aws_pro_plugin_ver', AWS_PRO_VERSION );

    }

    /**
     * Admin notice for table first re-index
     */
    public function admin_notice_no_index() { ?>
        <div class="updated notice is-dismissible">
            <p><?php printf( esc_html__( 'Advanced Woo Search: Please go to plugin setting page and start the indexing of your products. %s', 'advanced-woo-search' ), '<a class="button button-secondary" href="'.esc_url( admin_url('admin.php?page=aws-options') ).'">'.esc_html__( 'Go to Settings Page', 'advanced-woo-search' ).'</a>'  ); ?></p>
        </div>
    <?php }

    /**
     * Admin notice for table reindex
     */
    public function admin_notice_reindex() { ?>
        <div class="updated notice is-dismissible">
            <p><?php printf( esc_html__( 'Advanced Woo Search: Please reindex table for proper work of new plugin features. %s', 'advanced-woo-search' ), '<a class="button button-secondary" href="'.esc_url( admin_url('admin.php?page=aws-options') ).'">'.esc_html__( 'Go to Settings Page', 'advanced-woo-search' ).'</a>'  ); ?></p>
        </div>
    <?php }

}


endif;

add_action( 'admin_init', 'AWS_PRO_Versions::factory' );