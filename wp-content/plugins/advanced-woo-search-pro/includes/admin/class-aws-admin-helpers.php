<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


if ( ! class_exists( 'AWS_Admin_Helpers' ) ) :

    /**
     * Class for plugin help methods
     */
    class AWS_Admin_Helpers {

        /*
         * Get array of options terms groups
         */
        static public function get_options_terms_groups( $term_group = '' ) {

            $groups_html = '';

            $groups = array(
                'Products' => array(
                    'product' => esc_html__( 'Products', 'advanced-woo-search' )
                )
            );

            $taxonomy_objects = get_object_taxonomies( 'product', 'objects' );

            foreach( $taxonomy_objects as $taxonomy_object ) {

                if ( in_array( $taxonomy_object->name, array( 'product_type', 'product_visibility' ) ) ) {
                    continue;
                }

                if ( strpos( $taxonomy_object->name, 'pa_' ) === 0 ) {
                    continue;
                }

                $tax_name = 'tax:' . $taxonomy_object->name;

                $groups['Taxonomies'][$tax_name] = $taxonomy_object->label;

            }

            if ( function_exists( 'wc_get_attribute_taxonomies' ) ) {
                $attributes = wc_get_attribute_taxonomies();
                if ( $attributes && ! empty( $attributes ) ) {
                    foreach( $attributes as $attribute ) {
                        $attribute_name = 'tax:' . wc_attribute_taxonomy_name( $attribute->attribute_name );
                        $groups['Attributes'][$attribute_name] = $attribute->attribute_label;
                    }
                }

            }

            $term_group_def_active = $term_group ? '' : ' selected="selected"';

            $groups_html .= '<option value=""' . esc_attr( $term_group_def_active ) . ' disabled=""></option>';

            foreach ( $groups as $group_name => $group_array ) {

                $groups_html .= '<optgroup label="' . esc_attr( $group_name ) . '">';

                foreach ( $group_array as $group_title => $group_term ) {
                    $group_is_active = $term_group && $term_group == $group_title ? ' selected="selected"' : '';
                    $groups_html .= '<option' . $group_is_active. ' value="' . esc_attr( $group_title ) . '">' . esc_html( $group_term ) . '</option>';
                }

                $groups_html .= '</optgroup>';

            }

            return $groups_html;

        }

        /*
         * Get array of options terms
         */
        static public function get_options_terms( $term ) {

            $options = array();

            $term_tax_group = 'product';
            $term_tax = 'product';

            if ( preg_match( '/(\w+?):/i', $term, $matches ) ) {
                if ( isset( $matches[1] ) ) {
                    $term_tax_group = $matches[1];
                    $term_tax = str_replace( $matches[0], '', $term );
                }
            }

            switch( $term_tax_group ) {

                case 'product';

                    $args = array(
                        'posts_per_page'  => -1,
                        'post_type'       => 'product'
                    );

                    $products = get_posts( $args );

                    if ( ! empty( $products ) ) {
                        foreach ( $products as $product ) {
                            $options[$product->ID] = $product->post_title;
                        }
                    }

                    $options['all'] = esc_html__( 'All products', 'advanced-woo-search' );

                    break;

                case 'tax';

                    $tags = get_terms( $term_tax );

                    if ( ! empty( $tags ) ) {
                        foreach ( $tags as $tag ) {
                            $options[$tag->term_id] = $tag->name;
                        }
                    }

                    break;


            }

            return $options;

        }

        /**
         * Get array of allowed tags for wp_kses function
         * @param array $allowed_tags Tags that is allowed to display
         * @return array $tags
         */
        static public function get_kses( $allowed_tags = array() ) {

            $tags = array(
                'a' => array(
                    'href' => array(),
                    'title' => array()
                ),
                'br' => array(),
                'em' => array(),
                'strong' => array(),
                'b' => array(),
                'code' => array(),
                'blockquote' => array(
                    'cite' => array(),
                ),
                'p' => array(),
                'i' => array(),
                'h1' => array(),
                'h2' => array(),
                'h3' => array(),
                'h4' => array(),
                'h5' => array(),
                'h6' => array(),
                'img' => array(
                    'alt' => array(),
                    'src' => array()
                )
            );

            if ( is_array( $allowed_tags ) && ! empty( $allowed_tags ) ) {
                foreach ( $tags as $tag => $tag_arr ) {
                    if ( array_search( $tag, $allowed_tags ) === false ) {
                        unset( $tags[$tag] );
                    }
                }

            }

            return $tags;

        }

    }

endif;