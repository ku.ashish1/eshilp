<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


if ( ! class_exists( 'AWS_Admin' ) ) :

/**
 * Class for plugin admin panel
 */
class AWS_Admin {

    /*
     * Name of the plugin settings page
     */
    var $page_name = 'aws-options';

    /**
     * @var AWS_Admin The single instance of the class
     */
    protected static $_instance = null;

    /**
     * Main AWS_Admin Instance
     *
     * Ensures only one instance of AWS_Admin is loaded or can be loaded.
     *
     * @static
     * @return AWS_Admin - Main instance
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /*
    * Constructor
    */
    public function __construct() {
        add_action( 'admin_menu', array( &$this, 'add_admin_page' ) );
        add_action( 'admin_init', array( &$this, 'register_settings' ) );

        if ( ! AWS_Admin_Options::get_settings() ) {
            $default_settings = AWS_Admin_Options::get_default_settings();
            update_option( 'aws_pro_settings', array( '1' => $default_settings ) );
        }

        if ( ! get_option( 'aws_instances' ) ) {
            add_option( 'aws_instances', '1', '', 'no' );
        }

        add_action( 'admin_enqueue_scripts', array( &$this, 'admin_enqueue_scripts' ) );

    }

    /*
     * Add options page
     */
    public function add_admin_page() {
        add_menu_page( esc_html__( 'Adv. Woo Search', 'advanced-woo-search' ), esc_html__( 'Adv. Woo Search', 'advanced-woo-search' ), 'manage_options', 'aws-options', array( &$this, 'display_admin_page' ), 'dashicons-search' );
    }

    /**
     * Generate and display options page
     */
    public function display_admin_page() {

        $nonce = wp_create_nonce( 'plugin-settings' );

        $instance_id = isset( $_GET['aws_id'] ) ? (int) sanitize_text_field( $_GET['aws_id'] ) : 0;
        $filter_id   = isset( $_GET['filter'] ) ? (int) sanitize_text_field( $_GET['filter'] ) : 1;

        $settings = AWS_Admin_Options::get_settings();

        $tabs = array(
            'general' => __( 'General', 'advanced-woo-search' ),
            'form'    => __( 'Search Form', 'advanced-woo-search' ),
            'results' => __( 'Search Results', 'advanced-woo-search' )
        );

        $current_tab = empty( $_GET['tab'] ) ? 'general' : sanitize_text_field( $_GET['tab'] );

        $tabs_html = '';

        foreach ( $tabs as $name => $label ) {
            $tabs_html .= '<a href="' . esc_url( admin_url( 'admin.php?page=aws-options&tab=' . $name . '&aws_id=' . $instance_id ) ) . '" class="nav-tab ' . ( $current_tab == $name ? 'nav-tab-active' : '' ) . '">' . esc_html( $label ) . '</a>';
        }

        $tabs_html = '<h2 class="nav-tab-wrapper woo-nav-tab-wrapper">'.$tabs_html.'</h2>';



        echo '<div class="wrap">';

            echo '<h1></h1>';

            if ( ! $instance_id ) {

                if ( isset( $_POST["stopwords"] ) ) {
                    update_option( 'aws_pro_stopwords', $_POST["stopwords"], 'no' );
                }

                if ( isset( $_POST["synonyms"] ) ) {
                    update_option( 'aws_pro_synonyms', $_POST["synonyms"], 'no' );
                }

                if ( isset( $_POST["seamless"] ) ) {
                    update_option( 'aws_pro_seamless', $_POST["seamless"] );
                }

                if ( isset( $_POST["autoupdates"] ) ) {
                    update_option( 'aws_pro_autoupdates', $_POST["autoupdates"], 'no' );
                }

                $this->display_instances_table();

                $this->update_table();

            } else {

                $instance_settings = $settings[$instance_id];
                $instance_name = $instance_settings['search_instance'];

                if ( empty( $instance_settings ) ) {
                    echo esc_html__( 'No such instance!', 'advanced-woo-search' );
                    return;
                }

                if ( isset( $_POST["Submit"] ) && current_user_can( 'manage_options' ) && isset( $_POST["_wpnonce"] ) && wp_verify_nonce( $_POST["_wpnonce"], 'plugin-settings' ) ) {
                    AWS_Admin_Options::update_settings();
                }


                $plugin_options = AWS_Admin_Options::get_settings();
                $plugin_options = $plugin_options[$instance_id];


                echo '<a class="button aws-back" href="' . admin_url( 'admin.php?page=aws-options' ) . '" title="' . esc_attr__( 'Back', 'advanced-woo-search' ) . '">' . esc_html__( 'Back', 'advanced-woo-search' ) . '</a>';

                echo '<h1 data-id="' . esc_attr( $instance_id ) . '" class="aws-instance-name">' . esc_html( $instance_name ) . '</h1>';

                if ( $current_tab === 'results' && $filter_id ) {
                    $filters = $plugin_options['filters'];
                    echo '<h2 class="aws-instance-filter">"' . esc_html( $filters[$filter_id]['filter_name'] ) . '" filter</h2>';
                }

                echo '<div class="aws-instance-shortcode">[aws_search_form id="' . $instance_id . '"]</div>';

                echo $tabs_html;

                echo '<form action="" name="aws_form" id="aws_form" method="post">';

                echo '<input type="hidden" name="aws_instance" value="' . esc_attr( $instance_id ) . '" />';

                switch ($current_tab) {
                    case('results'):
                        $this->filters_tabs( $plugin_options['filters'] );
                        new AWS_Admin_Fields( 'results', $plugin_options['filters'][$filter_id] );
                        break;
                    case('form'):
                        new AWS_Admin_Fields( 'form', $plugin_options );
                        break;
                    default:
                        new AWS_Admin_Fields( 'general', $plugin_options );
                }


                echo '<input type="hidden" name="_wpnonce" value="' . esc_attr( $nonce ) . '">';

                echo '</form>';

            }


        echo '</div>';

    }

    /*
     * Generate and display table of instances
     */
    private function display_instances_table() {

        $plugin_options = AWS_Admin_Options::get_settings();

        echo '<h1>Advanced Woo Search</h1>';

        echo '<table class="aws-table aws-form-instances widefat" cellspacing="0">';


        echo '<thead>';

        echo '<tr>';
        echo '<th class="aws-name">' . esc_html__( 'Form Name', 'advanced-woo-search' ) . '</th>';
        echo '<th class="aws-shortcode">' . esc_html__( 'Shortcode', 'advanced-woo-search' ) . '</th>';
        echo '<th class="aws-actions"></th>';
        echo '</tr>';

        echo '</thead>';


        echo '<tbody>';

        foreach ( $plugin_options as $instance => $instance_options ) {

            $instance_page = admin_url( 'admin.php?page=aws-options&aws_id=' . $instance );

            echo '<tr>';

            echo '<td class="aws-name">';
            echo '<a href="' . $instance_page . '">' . $instance_options['search_instance'] . '</a>';
            echo '</td>';

            echo '<td class="aws-shortcode">';
            echo '[aws_search_form id="' . $instance . '"]';
            echo '</td>';

            echo '<td class="aws-actions">';
            echo '<a class="button alignright tips delete" title="Delete" data-id="' . esc_attr( $instance ) . '" href="#">' . esc_html__( 'Delete', 'advanced-woo-search' ) . '</a>';
            echo '<a class="button alignright tips copy" title="Copy" data-id="' . esc_attr( $instance ) . '" href="#">' . esc_html__( 'Copy', 'advanced-woo-search' ) . '</a>';
            echo '<a class="button alignright tips edit" title="Edit" href="' . $instance_page . '">' . esc_html__( 'Edit', 'advanced-woo-search' ) . '</a>';
            echo '</td>';

            echo '</tr>';

        }

        echo '</tbody>';


        echo '</table>';


        echo '<div class="aws-insert-instance">';
        echo '<button class="button aws-insert-instance">' . esc_html__( 'Add New Form', 'advanced-woo-search' ) . '</button>';
        echo '</div>';

    }

    /*
     * Reindex table
     */
    private function update_table() {

        echo '<form action="" name="aws_form" id="aws_form" method="post">';

            echo '<table class="form-table">';

            echo '<tbody>';

            echo '<tr>';

                echo '<th>' . esc_html__( 'Activation', 'advanced-woo-search' ) . '</th>';

                echo '<td>';
                    echo '<div class="description activation">';
                        echo esc_html__( 'In case you need to add plugin search form on your website, you can do it in several ways:', 'advanced-woo-search' ) . '<br>';
                        echo '<div class="list">';
                            echo '1. ' . esc_html__( "Enable a 'Seamless integration' option ( may not work with some themes )", 'advanced-woo-search' ) . '<br>';
                            echo '2. ' . sprintf( esc_html__( 'Add search form using shortcode %s', 'advanced-woo-search' ), '<code>[aws_search_form id="YOUR_FORM_ID"]</code>' ) . '<br>';
                            echo '3. ' . esc_html__( 'Add search form as widget for one of your theme widget areas. Go to Appearance -> Widgets and drag&drop AWS Widget to one of your widget areas', 'advanced-woo-search' ) . '<br>';
                            echo '4. ' . sprintf( esc_html__( 'Add PHP code to the necessary files of your theme: %s', 'advanced-woo-search' ), "<code>&lt;?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form( true, array( 'id' => YOUR_FORM_ID ) ); } ?&gt;</code>" ) . '<br>';
                            echo sprintf( esc_html__( 'Replace %s with ID of search form that you want to display', 'advanced-woo-search' ), "<code>YOUR_FORM_ID</code>" ) . '<br>';
                        echo '</div>';
                    echo '</div>';
                echo '</td>';

            echo '</tr>';


            echo '<tr>';

            echo '<th>' . esc_html__( 'Reindex table', 'advanced-woo-search' ) . '</th>';

                echo '<td>';

                    echo '<div id="aws-reindex"><input class="button" type="button" value="' . esc_attr__( 'Reindex table', 'advanced-woo-search' ) . '"><span class="loader"></span><span class="reindex-progress">0%</span></div><br><br>';
                    echo '<span class="description">' .
                        sprintf( esc_html__( 'This action only need for %s one time %s - after you activate this plugin. After this all products changes will be re-indexed automatically.', 'advanced-woo-search' ), '<strong>', '</strong>' ) . '<br>' .
                        __( 'Update all data in plugins index table. Index table - table with products data where plugin is searching all typed terms.<br>Use this button if you think that plugin not shows last actual data in its search results.<br><strong>CAUTION:</strong> this can take large amount of time.', 'advanced-woo-search' ) . '<br><br>' .
                        esc_html__( 'Products in index:', 'advanced-woo-search' ) . '<span id="aws-reindex-count"> <strong>' . AWS_Helpers::get_indexed_products_count() . '</strong></span>';
                    echo '</span>';

                echo '</td>';

            echo '</tr>';


            echo '<tr>';

                echo '<th>' . esc_html__( 'Clear cache', 'advanced-woo-search' ) . '</th>';

                echo '<td>';
                    echo '<div id="aws-clear-cache"><input class="button" type="button" value="' . esc_attr__( 'Clear cache', 'advanced-woo-search' ) . '"><span class="loader"></span></div><br>';
                    echo '<span class="description">' . esc_html__( 'Clear cache for all search results.', 'advanced-woo-search' ) . '</span>';
                echo '</td>';

            echo '</tr>';


            echo '<tr>';

                echo '<th>' . esc_html__( 'Seamless integration' ) . '</th>';

                echo '<td>';
                    $seamless = get_option( 'aws_pro_seamless' ) ? get_option( 'aws_pro_seamless' ) : 'false';
                    echo '<input class="radio" type="radio" name="seamless" id="seamlesstrue" value="true" ' . checked( $seamless, 'true', false ) . '> <label for="seamlesstrue">On</label><br>';
                    echo '<input class="radio" type="radio" name="seamless" id="seamlessfalse" value="false" ' . checked( $seamless, 'false', false ) . '> <label for="seamlessfalse">Off</label><br>';
                    echo '<br><span class="description">' . esc_html__( 'Replace all the standard search forms on your website ( may not work with some themes ).', 'advanced-woo-search' ) . '</span>';
                echo '</td>';

            echo '</tr>';


            echo '<tr>';

                echo '<th>' . esc_html__( 'Sync index table' ) . '</th>';

                echo '<td>';
                    $sync = get_option( 'aws_pro_autoupdates' ) ? get_option( 'aws_pro_autoupdates' ) : 'true';
                    echo '<input class="radio" type="radio" name="autoupdates" id="autoupdatestrue" value="true" ' . checked( $sync, 'true', false ) . '> <label for="autoupdatestrue">On</label><br>';
                    echo '<input class="radio" type="radio" name="autoupdates" id="autoupdatesfalse" value="false" ' . checked( $sync, 'false', false ) . '> <label for="autoupdatesfalse">Off</label><br>';
                    echo '<br><span class="description">' . esc_html__( 'Automatically update plugin index table when product content was changed. This means that in search there will be always latest product data.', 'advanced-woo-search' ) . '<br>';
                        echo esc_html__( 'Turn this off if you have any problems with performance.', 'advanced-woo-search' );
                    echo '</span>';
                echo '</td>';

            echo '</tr>';


            echo '<tr>';

                echo '<th>' . esc_html__( 'Stop words list' ) . '</th>';

                echo '<td>';
                    $stopwords = get_option( 'aws_pro_stopwords' ) ? get_option( 'aws_pro_stopwords' ) : '';
                    echo '<textarea id="stopwords" name="stopwords" cols="85" rows="3">' . $stopwords . '</textarea>';
                    echo '<br><span class="description">' . esc_html__( 'Comma separated list of words that will be excluded from search.', 'advanced-woo-search' ) . '<br>' . esc_html__( 'Re-index required on change.', 'advanced-woo-search' ) . '</span>';
                echo '</td>';

            echo '</tr>';

            echo '<tr>';

                echo '<th>' . esc_html__( 'Synonyms' ) . '</th>';

                echo '<td>';
                    $synonyms = get_option( 'aws_pro_synonyms' ) ? get_option( 'aws_pro_synonyms' ) : '';
                    echo '<textarea id="synonyms" name="synonyms" cols="85" rows="3">' . $synonyms . '</textarea>';
                    echo '<br><span class="description">' . esc_html__( 'Comma separated list of synonym words. Each group of synonyms must be on separated text line.', 'advanced-woo-search' ) . '<br>' . esc_html__( 'Re-index required on change.', 'advanced-woo-search' ) . '</span>';
                echo '</td>';

            echo '</tr>';


            echo '</tbody>';

            echo '</table>';

            echo '<p class="submit"><input name="Submit" type="submit" class="button-primary" value="' . esc_attr__( 'Save Changes', 'advanced-woo-search' ) . '" /></p>';

        echo '</form>';

    }

    /*
     * Generate filters tabs html
     */
    private function filters_tabs( $filters ) {

        if ( isset( $_GET['section'] ) ) {
            return;
        }

        $instance_id       = isset( $_GET['aws_id'] ) ? (int) sanitize_text_field( $_GET['aws_id'] ) : 0;
        $current_filter_id = isset( $_GET['filter'] ) ? (int) sanitize_text_field( $_GET['filter'] ) : 1;

        echo '<table class="aws-table aws-form-filters widefat" cellspacing="0">';

            echo '<thead>';

                echo '<tr>';
                    echo '<th class="aws-sort">&nbsp;</th>';
                    echo '<th class="aws-name">' . esc_html__( 'Filter Name', 'advanced-woo-search' ) . '</th>';
                    echo '<th class="aws-actions"></th>';
                echo '</tr>';

            echo '</thead>';

            echo '<tbody>';

            foreach ( $filters as $filter_id => $filter_opts ) {

                $instance_page = admin_url( 'admin.php?page=aws-options&tab=results&aws_id=' . $instance_id . '&filter=' . $filter_id );

                echo '<tr class="aws-filter-item ' . ( 1 == $filter_id ? 'disabled' : '' ) . '" data-instance="' . esc_attr( $instance_id ) . '" data-id="' . esc_attr( $filter_id ) . '">';

                    if ( $filter_id != 1 ) {
                        echo '<td class="aws-sort"></td>';
                        //echo '<input type="hidden" name="filter_order[]" value="' . $filter_id . '">';
                    } else {
                        echo '<td></td>';
                        //echo '<input type="hidden" name="filter_order[]" value="1">';
                    }

                    echo '<td class="aws-name">';
                        echo '<a href="' . $instance_page . '" class="' . ( $current_filter_id == $filter_id ? 'active' : '' ) . '">' . $filter_opts['filter_name'] . '</a>';
                        if ( $current_filter_id == $filter_id ) {
                            echo '<span class="aws-current-filter">(' . esc_html__( 'editing', 'advanced-woo-search' ) . ')</span>';
                        }
                    echo '</td>';

                    echo '<td class="aws-actions">';

                        if ( $filter_id != 1 ) {
                            echo '<a class="button alignright tips delete" title="Delete" data-instance="' . esc_attr( $instance_id ) . '" data-id="' . esc_attr( $filter_id ) . '" href="#">' . esc_html__('Delete', 'advanced-woo-search') . '</a>';
                        }

                        echo '<a class="button alignright tips copy" title="Copy" data-instance="' . esc_attr( $instance_id ) . '" data-id="' . esc_attr( $filter_id ) . '" href="#">' . esc_html__( 'Copy', 'advanced-woo-search' ) . '</a>';

                    echo '</td>';

                echo '</tr>';

            }

            echo '</tbody>';

        echo '</table>';

        echo '<div class="aws-insert-filter-box">';
            echo '<button class="button aws-insert-filter" data-instance="' . esc_attr( $instance_id ) . '">' . esc_html__( 'Add New Filter', 'advanced-woo-search' ) . '</button>';
        echo '</div>';

        echo '<ul class="aws-sub">';
            echo '<li><a href="#general">' . __( "General", "advanced-woo-search" ) . '</a> | </li>';
            echo '<li><a href="#sources">' . __( "Search Sources", "advanced-woo-search" ) . '</a> | </li>';
            echo '<li><a href="#view">' . __( "View", "advanced-woo-search" ) . '</a> | </li>';
            echo '<li><a href="#excludeinclude">' . __( "Exclude/include products", "advanced-woo-search" ) . '</a></li>';
        echo '</ul>';

    }

    /*
	 * Register plugin settings
	 */
    public function register_settings() {
        register_setting( 'aws_pro_settings', 'aws_pro_settings' );
    }

    /*
     * Enqueue admin scripts and styles
     */
    public function admin_enqueue_scripts() {

        if ( isset( $_GET['page'] ) && $_GET['page'] == 'aws-options' ) {

            $instance_id = isset( $_GET['aws_id'] ) ? (int) sanitize_text_field( $_GET['aws_id'] ) : 0;
            $filter_id   = isset( $_GET['filter'] ) ? (int) sanitize_text_field( $_GET['filter'] ) : 1;

            wp_enqueue_style( 'plugin-admin-style', AWS_PRO_URL . '/assets/css/admin.css', array(), 'pro' . AWS_PRO_VERSION );
            wp_enqueue_style( 'aws-admin-chosen', AWS_PRO_URL . '/assets/chosen/chosen.min.css' );

            wp_enqueue_script( 'jquery' );
            wp_enqueue_script( 'jquery-ui-sortable' );
            wp_enqueue_media();

            wp_enqueue_script( 'aws-admin-chosen', AWS_PRO_URL . '/assets/chosen/chosen.jquery.min.js', array('jquery') );
            wp_enqueue_script( 'aws-admin', AWS_PRO_URL . '/assets/js/admin.js', array('jquery'), 'pro' . AWS_PRO_VERSION );

            wp_localize_script( 'aws-admin', 'aws_vars', array(
                'ajaxurl'    => admin_url( 'admin-ajax.php', 'relative' ),
                'ajax_nonce' => wp_create_nonce( 'aws_pro_admin_ajax_nonce' ),
                'instance'   => $instance_id,
                'filter'     => $filter_id
            ) );

        }

    }

}

endif;


add_action( 'init', 'AWS_Admin::instance' );