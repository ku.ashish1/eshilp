<?php
add_action( 'wp_enqueue_scripts', 'get_district_list' );
add_action( 'wp_enqueue_scripts', 'get_sub_district_list' );
add_action('wp_ajax_fetch_district', 'get_district_by_state_id');
add_action('wp_ajax_sub_fetch_district', 'get_sub_district_by_district_id');
add_action('wp_ajax_nopriv_fetch_district', 'get_district_by_state_id');
add_action('wp_ajax_nopriv_sub_fetch_district', 'get_sub_district_by_district_id');
add_action('wp_enqueue_scripts', 'custom_registration_validation');
add_action('wp_enqueue_scripts', 'custom_buyer_registration_validation');
 /** end */

add_action('wcfm_order_status_updated','send_push_notification_on_update_order_status',10,2);
function send_push_notification_on_update_order_status($orderid, $order_status) {
	echo '<script>alert("ds")</script>';
}
/**
 * Function         : custom_woocommerce_states
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */
add_filter( 'woocommerce_states', 'custom_woocommerce_states' );

function custom_woocommerce_states( $states ) {
    global $wpdb;
    $stateList = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}state where country_id=1 order by state_name asc", ARRAY_A  );
    $states['IN']=array();
    if(!empty($stateList)){
        foreach($stateList AS $key=>$value){
            $states['IN'][$value['state_id']]=$value['state_name'];
        }
    }
    return $states;
}

/**
 * Function         : get_district_list 
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */

function get_district_list() {
    wp_enqueue_script( 'dic_custom_address_script', plugins_url( 'js/dic_custom_address.js?t='.time(), __FILE__ ), FALSE );
    wp_localize_script( 'dic_custom_address_script', 'district_list_by_state_id',
    array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' )
        
        )
    );
}


/**
 * Function         : custom_registration_validation 
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */

function custom_registration_validation() {
    wp_enqueue_script( 'dic_custom_validation_script', plugins_url( 'js/custom_registration_validation.js', __FILE__ ), FALSE );
}
function custom_buyer_registration_validation() {
    wp_enqueue_script( 'custom_buyer_registration_script', plugins_url( 'js/custom_buyer_registration_validation.js', __FILE__ ), FALSE );
}


/**
 * Function         : get_district_by_state_id 
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */

function getDistrictList($stateId){
    global $wpdb;
    if($stateId){
        $districtList = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}district where state_id=$stateId order by district_name asc", ARRAY_A  );
        if(!empty($districtList)){
            $results = array('status' => true, 'districtList' => $districtList);
        }else{
            $results = array('status' =>false);
        }
    }else{
        $results = array('status' =>false);
    }
    return $results;
}

function get_district_by_state_id() {
    $stateId=$_POST['state_id'];
    $results= getDistrictList($stateId);
    echo json_encode($results);
    die;
}

/**
 * Function         : get_sub_district_list 
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */

function get_sub_district_list() {
    
    wp_enqueue_script( 'dic_custom_address_script', plugins_url( 'js/dic_custom_address.js', __FILE__ ), FALSE );
    wp_localize_script( 'dic_custom_address_script', 'sub_district_list_by_district',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' )
             
        )
    );
}

/**
 * Function         : get_sub_district_by_district_id
 * Devloper         : Sateesh Babu
 * Creaate_at       : 18-09-2020
 * updated_at       : 
 * updated_by       :
 * Update_reason    :
 * Description      :
 */
function get_sub_district($districtId){
    global $wpdb;
    if($districtId){
        $subDistrictList = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}sub_district where district_id=$districtId order by sub_district_name asc", ARRAY_A  );
        if(!empty($subDistrictList)){
            $results = array('status' => true, 'subDistrictList' => $subDistrictList);
        }else{
            $results = array('status' =>false);
        }
    }else{
        $results = array('status' =>false);
    }
    return $results;
}

function get_sub_district_by_district_id() {
    $results=get_sub_district($_POST['district_id']);
    echo json_encode($results);
    die;
}


/**
 * Code for tracking or Indian Post Booking 
 * Ashish Agarwal
 */
add_action( 'wp_enqueue_scripts', 'custom_traking' );
// add_action('wp_ajax_indian_post_tracking_code', 'getNewTrackingCode');

function custom_traking() {
    wp_enqueue_script( 'custom_traking', plugins_url( 'js/custom_traking.js', __FILE__ ), FALSE );
}


add_action('wp_ajax_free_tracking_code', 'freeNotUsedCode');
function freeNotUsedCode() {
    $id=$_POST["id"];
    if($id){
        global $wpdb;
        $getData=$wpdb->query("UPDATE {$wpdb->prefix}bar_code set `status`='0' where `id`=".$id);
        if($getData){
            $update_msg = array('status' => true, 'msg' => 'Updated');
        }else{
            $update_msg= array('status' => true, 'msg' => 'get error');
        }
        echo json_encode($update_msg);
        die;
    }
    echo json_encode("already assign");
    die;
}
add_action('wp_ajax_allot_tracking_code', 'allotTrackingCode');
function allotTrackingCode() {
    $code=$_POST["code"];
    // $id=$_POST["id"];
    $order_id=$_POST["order_id"];
    $item_id=$_POST["item_id"];
    $product_id=$_POST["product_id"];
    $item = new WC_Order_Item_Product($item_id);
    $item_quantity=$item->get_quantity();
    // echo json_encode($item->get_quantity());
    // die;
   
    global $wpdb;
    $three_to_ten_digit_code=substr($code,2,8);
    $eleven_digit_code=substr($code,10,1);
    $getData=$wpdb->query("UPDATE {$wpdb->prefix}bar_code set `status`='2',`quantity`='".$item_quantity."',`used_date`=now(),`shipping_status`=1,`product_id`='".$product_id."',`order_id`='".$order_id."', `item_id`='".$item_id."' where `three_to_ten_digit_code`='".$three_to_ten_digit_code."' and `eleven_digit_code`='".$eleven_digit_code."'");
    // $getData=$wpdb->query("UPDATE {$wpdb->prefix}bar_code set `status`='2',`used_date`=now(),`product_id`='".$product_id."',`order_id`='".$item_id."', `item_id`='".$order_id."' where `id`=".$id);
    if($getData){
        $update_order_id = array('status' => true, 'msg' => 'Updated');
        /**
         * code for booking
         */
        $parsal_booking = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}bar_code where shipping_status=1", ARRAY_A  );
        $booking_xml='<?xml version="1.0" encoding="UTF-8"?>
        <BookingManifest>';
        foreach ($parsal_booking as $key => $booking) {
            $order = wc_get_order( (int)$booking['order_id'] )->get_data();
            $tracking_code=$booking['first_two_digit_code'].$booking['three_to_ten_digit_code'].$booking['eleven_digit_code'].$booking['last_two_digit_code'];
            $product = wc_get_product( $booking['product_id'] )->get_data();
            $product_weight=$product['weight'] ? $product['weight'] : 0.05; 
          
            $booking_xml=$booking_xml.'<manifestDetail>
            <consigneeAddress>
                <name>'.$order['shipping']['first_name'].' '.$order['shipping']['last_name'].'</name>
                <address1>'.$order['shipping']['address_1'].' '.$order['shipping']['address_2'].'</address1>
                <city>'.$order['shipping']['city'].'</city>
                <pincode>'.$order['shipping']['postcode'].'</pincode>
                <CountryCode>IN</CountryCode>
                <MobileNo>9568664444</MobileNo>
            </consigneeAddress>
            <shipmentPackageInfo>
                <articleNumber>'.$tracking_code.'</articleNumber>
                <referenceNumber>'.$booking['order_id'].$booking['item_id'].'</referenceNumber>
                <ShipmentMethodOfPayment>CONTRACT</ShipmentMethodOfPayment>
                <CashOnDeliveryCharge>
                    <chargeOrAllowance>CHARGE</chargeOrAllowance>
                    <monetaryAmount currencyISOCode="INR">0.00</monetaryAmount>
                </CashOnDeliveryCharge>
                <shipmentPackageActualGrossWeight>
                    <weightValue unitOfMeasure="KG">'.$product_weight*$booking['quantity'].'</weightValue>
                </shipmentPackageActualGrossWeight>
                <insuredValue>0.00</insuredValue>
                <ProofOfDelivery>N</ProofOfDelivery>
            </shipmentPackageInfo>
        </manifestDetail>';
        }
        $booking_xml=$booking_xml.'</BookingManifest>';
        $filename="PO21308110000_3000000350_0040001173_SP_".time()."_".date('Y-m-d-hi').".xml";
        $upload_dir = wp_upload_dir();
        $fp = fopen($upload_dir['basedir'] . "/bws-custom-code/".$filename,"w");
        fwrite($fp,$booking_xml);
        fclose($fp);
        
        if (file_exists($upload_dir['basedir'] . "/bws-custom-code/".$filename)) {
            global $wpdb;
            $getData=$wpdb->query("INSERT into {$wpdb->prefix}shipping_upload_file (`filename`) values ('".$filename."')");
            $curl = curl_init();
            // $file = fopen(site_url(). "/wp-content/uploads/bws-custom-code/".$filename, "wb");
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://data.cept.gov.in/customer/api/BulkCustomer/upload",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => array(''=> '@'.realpath($upload_dir['basedir'] . "/bws-custom-code/".$filename)),
                )
            );
            // remote url ->site_url(). "/wp-content/uploads/bws-custom-code/".$filename
           // echo $upload_dir['basedir'] . "/bws-custom-code/".$filename;
            // die;
            if(curl_exec($curl)){
                # code
                echo curl_exec($curl);
            }else{
                echo "error";
                var_dump(curl_error($curl));
            }
            curl_close($curl); 
        }
        die;
    }else{
        $update_order_id= array('status' => true, 'msg' => "get error");
    }
    echo json_encode($update_order_id);
    die;
}
function create_xml_file(){
    global $wpdb;
    $parsal_booking = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}bar_code where shipping_status=1", ARRAY_A  );
        // $results;
        // if(!empty($parsal_booking)){
        //     $results = array('status' => true, 'parsal_booking' => $parsal_booking);
        // }else{
        //     $results = array('status' =>false);
        // }
        echo json_encode($parsal_booking);
        die;
}
