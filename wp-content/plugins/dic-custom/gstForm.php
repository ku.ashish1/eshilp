<?php 
/* Ashish Agarwal */

add_action( 'wp_enqueue_scripts', 'gst_detail' );
add_action( 'admin_post_add_gst_form', 'add_gst_form' );
function gst_detail() {
    
    wp_enqueue_script( 'dic_custom_script', plugins_url( 'js/custom.js', __FILE__ ), FALSE );
    wp_localize_script( 'dic_custom_script', 'gst_detail_tax',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' )
             
        )
    );
}




function gstForm()
{
    if(isset($_GET['msg'])){
        echo '<div class="notice notice-warning is-dismissible">'.$_GET['msg'].'</div>';
    }
    echo '<div class="updated"><h2 class="wcfm_registration_form_heading">Add GST</h2>
    <form id="dataForm" name="dataform" method="POST" action="'.esc_url( admin_url('admin-post.php') ).'" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" id="fileToUpload">
    <div class="form-group">
      <label for="category_id" class="wcfm_title">Category Id</label>
      <select class="wcfm-select" name="cat_id" id="category_id">
      <option value="1">Handloom</option>
      <option value="2">Handcraft</option>
    </select>
      
    </div>
    <div class="form-group">
      <label for="category_name" class="wcfm_title">Category Name</label>
      <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name">
    </div>
    <div class="form-group">
      <label class="form-check-label" class="wcfm_title" for="cat_desc">Category Description</label>
      <textarea id="cat_desc" name="category_description" rows="4" cols="50"></textarea>
    </div>
    <div class="form-group">
    <label class="form-check-label" class="wcfm_title" for="cat_gst">Category Gst</label>
    <input type="hidden" name="action" value="add_gst_form">
    <input type="number" class="form-control" min="1" id="category_gst" name="category_gst" placeholder="Category Gst" >
  </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form></div>';
}
function custom_menu()
{
    // add_menu_page( string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', string $icon_url = '', int $position = null )
    add_menu_page( 'gst_details', 'gst_menu', 'manage_options', 'gst-details', 'gstForm', '', 30 );
}
function add_gst_form()
{
    // for export product_Category in json file
    // $file = time() . '_product_category.json';
    // header("Content-type: application/json; charset=utf-8");
    // header('Content-Disposition: attachment; filename="'.$file.'"');
    // echo json_encode(export_product_cat(0));
    $data = json_decode(file_get_contents($_FILES['fileToUpload']['tmp_name']));
    if($data){
        global $wpdb;
        $wpdb->query("UPDATE {$wpdb->prefix}options set `option_value`='' where `option_name`='product_cat_children'");  
        $option = new stdClass();
        insert_update($data,$parent_id=0,$option);
    }else{
        //  back masg with blank file
    }
    
    // orderBooked();
    // global $wpdb;
    // $cat_id =  $_POST['cat_id'];
    // $category_name =  $_POST['category_name'];
    // $category_description =  $_POST['category_description'];
    // $category_gst =  $_POST['category_gst'];
    // if($cat_id && $category_gst &&  $category_name){
    //     $add_gst = $wpdb->query( "INSERT into {$wpdb->prefix}gst (category_id,category_name,category_description,gst_percentage) VALUES ($cat_id,'$category_name','$category_description',$category_gst)" );
    //     if($add_gst){
    //         wp_redirect(admin_url('admin.php?page=gst-details&msg=Add Successfully'));
    //     }else{
    //         wp_redirect(admin_url('admin.php?page=gst-details&msg=Get Error!'));
    //     }
    // }else{
    //     wp_redirect(admin_url('admin.php?page=gst-details&msg=Please fill the required fields'));
    // }
}
function insert_update($data,$parent_id,$option){
    $child=[];
    foreach ($data as $value) { 
        $return_id=update_term($value,$parent_id); 
        // where return_id should be parent_id if child count > 0
        if($parent_id>0){
            $child[]=(int)$return_id;
            $option->$parent_id=$child;
        }
        if(isset($value->child) && is_array($value->child) && count($value->child)>0){
            insert_update($value->child,$return_id,$option);
        }
    }
}
function update_term($value,$parent_id){
    global $wpdb;
    $getData= $wpdb->get_results( "SELECT term_id from {$wpdb->prefix}terms where slug='".$value->slug."'", ARRAY_A  );
        if(!empty($getData) && is_array($getData)){
            $return_id=$getData[0]['term_id'];
                // Update term if exist
            $wpdb->query("UPDATE {$wpdb->prefix}terms set `name`='".$value->name."',`slug`='".$value->slug."',`term_group`=0,`testing`=1 where `term_id`='".$return_id."'");
        }else{
            $wpdb->query( "INSERT into {$wpdb->prefix}terms (`name`,`slug`,`term_group`,`testing`) VALUES ('$value->name','$value->slug','$value->term_group',2)" );
            $return_id = $wpdb->insert_id;
        }
        insert_term_texonomy($return_id,$parent_id,$value->description);
        return $return_id;
}

function insert_term_texonomy($term_id,$parent_id,$description){
    global $wpdb;
    $getData= $wpdb->get_results( "SELECT term_id from {$wpdb->prefix}term_taxonomy where term_id='".$term_id."' AND taxonomy = 'product_cat'", ARRAY_A  );
        if(!empty($getData) && is_array($getData)){
            $return_id=$getData[0]['term_id'];
                // Update term if exist
            $wpdb->query("UPDATE {$wpdb->prefix}term_taxonomy set `description`='".$description."',`parent`=$parent_id,`testing`=1 where `term_id`='".$term_id."'");
        }else{
            $wpdb->query( "INSERT into {$wpdb->prefix}term_taxonomy (`term_id`,`description`,`parent`,`testing`,`taxonomy`) VALUES ($term_id,'$description','$parent_id',2,'product_cat')" );   
        }
}
function export_product_cat($parentId){
    global $wpdb;
    $product_parent_cat = $wpdb->get_results( "SELECT wpt.term_id as id, wpt.name, wpt.slug ,term_group, (SELECT count(term_taxonomy_id) FROM {$wpdb->prefix}term_taxonomy where parent = id and taxonomy = 'product_cat') AS cat_child_count FROM {$wpdb->prefix}terms AS wpt , {$wpdb->prefix}term_taxonomy as wptt where wpt.term_id in (SELECT term_id FROM {$wpdb->prefix}term_taxonomy where parent = ".(int)$parentId." and taxonomy = 'product_cat') AND wpt.term_id= wptt.term_id order by name asc", ARRAY_A  );
    return childCategory($product_parent_cat);
}

function childCategory($product_parent_cat){
    global $wpdb;
    $category=[];
    foreach ($product_parent_cat as $key => $parent_cate) {
        $cat['id']=$parent_cate['id'];
        $cat['name']=$parent_cate['name'];
        $cat['slug']=$parent_cate['slug'];
        $cat['term_group']=$parent_cate['term_group'];
        $cat['meta_data']=get_meta_data($parent_cate['id']);
        $description=$wpdb->get_results("SELECT `description` FROM `wp_term_taxonomy` where `term_id`='".$parent_cate['id']."' AND `taxonomy`='product_cat'",ARRAY_A);
        $cat['description']=!empty($description)?$description[0]['description']:'';
        // $cat['cat_child_count']=$parent_cate['cat_child_count'];
        if((int)$parent_cate['cat_child_count']>0){
            $cat['child']=export_product_cat($parent_cate['id']);
        }else{
            $cat['child']=[];
        }
        $category[]=$cat;
    }
   return $category;
}
function get_meta_data($parent_id)
{ 
    global $wpdb;
    $metaData=$wpdb->get_results("SELECT * from wp_termmeta where term_id=".$parent_id, ARRAY_A);
    $meta_datas=[];
    if($metaData){
        foreach ($metaData as $key => $value) {
            // print_r($value->meta_id);die;
            $meta_data['meta_id']=$value['meta_id'];
            $meta_data['term_id']=$value['term_id'];
            $meta_data['meta_key']=$value['meta_key'];
            if($value['meta_key']!=='_wcfmmp_commission'){
                $meta_data['meta_value']=$value['meta_value'];
            }else{
                $meta_data['meta_value']=unserialize($value['meta_value']);
            }
            $meta_datas[]=$meta_data;
            unset($meta_data['meta_value']);
        }
    }
    return $meta_datas;
}
function orderBooked()
{
    # call in add_gst_form function
    $data = '<?xml version="1.0" encoding="UTF-8"?>
        <BookingManifest>
            <manifestDetail>
                <consigneeAddress>
                    <name>Ashish Kumar</name>
                    <address1>Avas Vikas</address1>
                    <city>Moradabad</city>
                    <pincode>244001</pincode>
                    <CountryCode>IN</CountryCode>
                    <MobileNo>9568664444</MobileNo>
                </consigneeAddress>
                <shipmentPackageInfo>
                    <articleNumber>AHi634</articleNumber>
                    <referenceNumber>Ref987</referenceNumber>
                    <ShipmentMethodOfPayment>CONTRACT</ShipmentMethodOfPayment>
                    <CashOnDeliveryCharge>
                        <chargeOrAllowance>CHARGE</chargeOrAllowance>
                        <monetaryAmount currencyISOCode="INR">0.00</monetaryAmount>
                    </CashOnDeliveryCharge>
                    <shipmentPackageActualGrossWeight>
                        <weightValue unitOfMeasure="KG">1.2</weightValue>
                    <shipmentPackageActualGrossWeight>
                    <insuredValue>0.00</insuredValue>
                    <ProofOfDelivery>N</ProofOfDelivery>
                </shipmentPackageInfo>
            </manifestDetail>
        </BookingManifest>
    </xml>';
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://api.cept.gov.in/customer/api/BulkCustomer/upload");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $headers = array();
    $headers["Content-Type"] = "text/xml";
    // $headers["HttpContent"] = "multipart/form-data";
    $headers["Accept"] = "application/xml";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    // Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $resp = curl_exec($ch);
    curl_close($ch);
    var_dump($resp);
}
add_action('admin_menu','custom_menu');

?>