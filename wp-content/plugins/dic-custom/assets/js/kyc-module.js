$wcfm_kyc_table = '';
jQuery(document).ready(function () {
    jQuery('#kyc_submit_button').click(function (event) {
        event.preventDefault();
        jQuery('.wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();
        var kyc_account_name = jQuery('[name="kyc_account_name"]');
        var kyc_account_number = jQuery('[name="kyc_account_number"]');
        var kyc_bank_name = jQuery('[name="kyc_bank_name"]');
        var kyc_ifsc_code = jQuery('[name="kyc_ifsc_code"]');
        var kyc_account_doc = jQuery('[name="kyc_account_doc"]');
        if (kyc_account_name.val().length == 0)
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_account_name).removeClass('wcfm_validation_success').addClass('wcfm_validation_failed');
            jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + kyc_module_script_object.account_name).addClass('wcfm-error').slideDown();
            wcfm_notification_sound.play();
            return false;
        }
        else
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_account_name).removeClass('wcfm_validation_failed').addClass('wcfm_validation_success');
        }
        if (kyc_account_number.val().length == 0)
        {

            jQuery('#wcfm_kyc_manage_form').find(kyc_account_number).removeClass('wcfm_validation_success').addClass('wcfm_validation_failed');
            jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + kyc_module_script_object.account_number).addClass('wcfm-error').slideDown();
            wcfm_notification_sound.play();
            return false;
        }
        else
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_account_number).removeClass('wcfm_validation_failed').addClass('wcfm_validation_success');
        }
        if (kyc_bank_name.val().length == 0)
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_bank_name).removeClass('wcfm_validation_success').addClass('wcfm_validation_failed');
            jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + kyc_module_script_object.bank_name).addClass('wcfm-error').slideDown();
            wcfm_notification_sound.play();
            return false;
        }
        else
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_bank_name).removeClass('wcfm_validation_failed').addClass('wcfm_validation_success');
        }
        if (kyc_ifsc_code.val().length == 0)
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_ifsc_code).removeClass('wcfm_validation_success').addClass('wcfm_validation_failed');
            jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + kyc_module_script_object.ifsc_code).addClass('wcfm-error').slideDown();
            wcfm_notification_sound.play();
            return false;
        }
        else
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_ifsc_code).removeClass('wcfm_validation_failed').addClass('wcfm_validation_success');
        }
        if (kyc_account_doc.val().length == 0)
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_account_doc).removeClass('wcfm_validation_success').addClass('wcfm_validation_failed');
            jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + kyc_module_script_object.account_doc).addClass('wcfm-error').slideDown();
            wcfm_notification_sound.play();
            return false;
        }
        else
        {
            jQuery('#wcfm_kyc_manage_form').find(kyc_account_doc).removeClass('wcfm_validation_failed').addClass('wcfm_validation_success');
        }

//            jQuery('#wcfm_kyc_manage_form').block({
//					message: null,
//					overlayCSS: {
//						background: '#fff',
//						opacity: 0.6
//					}
//				});
        var data = {
            action: 'kyc_submit_controller',
            wcfm_kyc_manage_form: jQuery('#wcfm_kyc_manage_form').serialize()

        }

        jQuery.post(kyc_module_script_object.ajaxurl, data, function (response) {
            if (response) {
                $response_json = jQuery.parseJSON(response);
                jQuery('.wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();
                wcfm_notification_sound.play();
                if ($response_json.status) {
                    jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-completed"></span>' + $response_json.message).addClass('wcfm-success').slideDown("slow", function () {
                        if ($response_json.redirect)
                            window.location = $response_json.redirect;
                    });
                    jQuery('#wcfm_kyc_manage_form [type="text"]').val('');
                } else {
                    jQuery('#wcfm_kyc_manage_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + $response_json.message).addClass('wcfm-error').slideDown();
                }
                
                wcfmMessageHide();
                jQuery('#wcfm_kyc_manage_form').unblock();
                jQuery('#kyc_submit_button').show();
            }
        });



    });

    jQuery('#wcfm-kyclist').DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 10, //parseInt(dataTables_config.pageLength),
        "bFilter": false,
        "responsive": true,
        "dom": 'Bfrtip',
        //"language"  : $.parseJSON(dataTables_language),
        //"buttons"   : $wcfm_datatable_button_args,
        /* "columns"   : [
         { responsivePriority: 1 },
         { responsivePriority: 1 },
         { responsivePriority: 1 },
         { responsivePriority: 4 },
         
         ],
         "columnDefs": [ { "targets": 0, "orderable" : false }, 
         { "targets": 1, "orderable" : false }, 
         { "targets": 2, "orderable" : false }, 
         { "targets": 3, "orderable" : false }, 
         { "targets": 4, "orderable" : false },
         
         ],  */
        'ajax': {
            "type": "POST",
            "url": kyc_module_script_object.ajaxurl,
            "data": function (d) {
                d.action = 'wcfm_kyc_ajax_controller',
                        d.controller = 'wcfm-kyc',
                        d.kyc_status = GetURLParameter('kyc_status')
//				d.report_vendor     = $report_vendor,
//				d.report_membership = $report_membership,
//				d.filter_date_form  = $filter_date_form,
//				d.filter_date_to    = $filter_date_to
            },
            "complete": function () {
                //initiateTip();

                // Fire wcfm-vendors table refresh complete
                //$( document.body ).trigger( 'updated_wcfm-vendors' );
            }
        }
    });

    // KYC APPROVE CODE
    jQuery('#kyc_approve_button').click(function (event) {
        event.preventDefault();
        var userid = jQuery(this).data('userid');
        var status = jQuery(this).data('status');
        var kyc_account_message = jQuery('#kyc_account_message').val();
        jQuery('.wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();


        var data = {
            action: 'kyc_approve_controller',
            userid : userid,
            status : status,
            kyc_account_message : kyc_account_message

        }

        jQuery.post(kyc_module_script_object.ajaxurl, data, function (response) {
            if (response) {
                $response_json = jQuery.parseJSON(response);
                jQuery('.wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();
                wcfm_notification_sound.play();
                if ($response_json.status) {
                    jQuery('.wcfm-message').html('<span class="wcicon-status-completed"></span>' + $response_json.message).addClass('wcfm-success').slideDown("slow", function () {
                        if ($response_json.redirect)
                            window.location = $response_json.redirect;
                    });
                } else {
                    jQuery('.wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + $response_json.message).addClass('wcfm-error').slideDown();
                }
                 
                wcfmMessageHide();
                 
            }
        });



    });


});