<?php
global $wp, $WCFM, $WCFMu;
?>
<div class="collapse wcfm-collapse" id="">

    <div class="wcfm-page-headig">
        <span class="wcfmfa fa-cube"></span>
        <span class="wcfm-page-heading-text">
            <?php _e('KYC Verification', 'wc-frontend-manager'); ?>
        </span>

    </div>
    <div class="wcfm-collapse-content">

        <?php ?>

        <div class="wcfm-container wcfm-top-element-container">

            <h2>KYC Verification</h2>





            <div class="wcfm-clearfix"></div>
        </div>
        <div class="wcfm-clearfix"></div><br />



        <div class="wcfm-container">
            <div id="" class="wcfm-content">

                <?php $kyc_data = unserialize($check_data[0]); ?>
                <?php
                $user = wp_get_current_user();
                $roles = (array) $user->roles;
                ?>
                <style>
                    .kyc-alert {
                        padding: 20px;
                        background-color: #36a5f4;
                        color: white;
                        margin-bottom: 15px;
                    }
                </style>
                <?php
            
                if ($kyc_data['kyc_status'] == 'submitted' || $kyc_data['kyc_status'] == 'change_request') {
                     
                    echo '<div class="kyc-alert">Request is under Review</div>';
                  }
                  elseif ($kyc_data['kyc_status'] == 'approve') {
                     
                    echo '<div class="kyc-alert">KYC Detail is Approved</div>';
                  }
                  elseif ($kyc_data['kyc_status'] == 'reject') {
                     
                    echo '<div class="kyc-alert">KYC Detail is Reject. Please fill right Detail</div>';
                  }
                  else
                  {
                      echo '<div class="kyc-alert">Please submit KYC Detail unless you won\'t able to withdraw amount. </div>';
                  }
                  
                  
                  
                  
                  ?>

                <?php
                if ($roles[0] == 'shop_manager') {
                    ?>
                    <p class="kyc_account_name  wcfm_title" data-wahfont="15"><strong>Seller</strong></p>
                    <label class="screen-reader-text" for="kyc_account_name">Seller</label>
                    <input type="text" class="wcfm-text  " value="<?php echo get_user_meta($kyc_data['kyc_user_id'], 'store_name', true); ?>" placeholder="" disabled="">
                <?php } ?>
                <p class="kyc_account_name  wcfm_title" data-wahfont="15"><strong>Account Holder Name</strong></p>
                <label class="screen-reader-text" for="kyc_account_name">Account Holder Name</label>
                <input type="text" id="kyc_account_name" name="kyc_account_name" class="wcfm-text  " value="<?php echo $kyc_data['kyc_account_name']; ?>" placeholder="" disabled="">
                <p class="kyc_account_number  wcfm_title" data-wahfont="15"><strong>Account Number</strong></p>
                <label class="screen-reader-text" for="kyc_account_number">Account Number</label>
                <input type="text" id="kyc_account_number" name="kyc_account_number" class="wcfm-text  " value="<?php echo $kyc_data['kyc_account_number']; ?>" placeholder="" disabled="">
                <p class="kyc_bank_name  wcfm_title" data-wahfont="15"><strong>Bank Name</strong></p>
                <label class="screen-reader-text" for="kyc_bank_name">Bank Name</label>
                <input type="text" id="kyc_bank_name" name="kyc_bank_name" class="wcfm-text  " value="<?php echo $kyc_data['kyc_bank_name']; ?>" placeholder="" disabled="">
                <p class="kyc_ifsc_code  wcfm_title" data-wahfont="15"><strong>IFSC Code</strong></p>
                <label class="screen-reader-text" for="kyc_ifsc_code">IFSC Code</label>
                <input type="text" id="kyc_ifsc_code" name="kyc_ifsc_code" class="wcfm-text  " value="<?php echo $kyc_data['kyc_ifsc_code']; ?>" placeholder="" disabled="">
                <p class="kyc_account_doc  wcfm_title" data-wahfont="15"><strong>Canceled Cheque/Passbook</strong></p>
                <label class="screen-reader-text" for="kyc_account_doc">Canceled Cheque/Passbook</label>
                <input type="file" id="kyc_account_doc" name="kyc_account_doc" class="wcfm-text  " value="" placeholder="" disabled="">                            <div class="wcfm-clearfix"></div>
                
                     <?php
                if ($roles[0] == 'administrator' ) {
                    if($kyc_data['kyc_status'] == 'submitted' || $kyc_data['kyc_status'] == 'change_request')
                        {
                    ?>
                  <p class="kyc_account_message  wcfm_title"><strong>Message</strong></p>
                <label class="screen-reader-text" for="kyc_account_message">Message</label>
                <textarea id="kyc_account_message" name="kyc_account_message" class="wcfm-text"><?php echo $kyc_data['kyc_message'] ?></textarea>
                <div class="wcfm-clearfix"></div>
                
                  <div class="text-center">

                    <input type="button" style="float: unset;"  value="Approve" id="kyc_approve_button" data-userid ="519735" data-status ="approve" class="wcfm_submit_button"> 
                    <input type="button" style="float: unset;" value="Reject" id="kyc_submit_button" data-userid ="519735" data-status ="reject" class="wcfm_submit_button">   
                </div>
                
                        <?php } } else {  ?>
                <div class="text-center">
                    <a href="?action=edit" class="wcfm_submit_button">Edit KYC Detail</a>
                    
                </div>
                 <?php }    ?>
                <div class="wcfm-clearfix"></div>
                <div class="wcfm-message" style="display: none;"></div>
            </div>
        </div> 


    </div>
</div>