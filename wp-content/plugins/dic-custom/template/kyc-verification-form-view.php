<?php
global $wp, $WCFM, $WCFMu;
?>
<div class="collapse wcfm-collapse" id="">

    <div class="wcfm-page-headig">
        <span class="wcfmfa fa-cube"></span>
        <span class="wcfm-page-heading-text">
            <?php _e('KYC Verification', 'wc-frontend-manager'); ?>
        </span>

    </div>
    <div class="wcfm-collapse-content">

        <?php ?>

        <div class="wcfm-container wcfm-top-element-container">

            <h2>KYC Verification</h2>





            <div class="wcfm-clearfix"></div>
        </div>
        <div class="wcfm-clearfix"></div><br />


         <style>
                    .kyc-alert {
                        padding: 20px;
                        background-color: #36a5f4;
                        color: white;
                        margin-bottom: 15px;
                    }
                </style>
        <div class="wcfm-container">
            <div id="" class="wcfm-content">
                <form id="wcfm_kyc_manage_form" class="wcfm">
                    <?php
                    
                    ?>
                    <div class="kyc-alert">Please submit KYC Detail unless you won't able to withdraw amount. </div>
                    <?php
                    $user = wp_get_current_user();
                    $roles = (array) $user->roles;
                    
                    if($roles[0] == 'shop_manager' )
                    {
                        $WCFM->wcfm_fields->wcfm_generate_form_field(array(
                        "kyc_user_id" => array('label' => 'Seller', 'type' => 'select', 'options' => '', 'attributes' => array('style' => 'width: 60%;'), 'class' => 'wcfm-select', 'id' => 'wcfm_associate_vendor', 'label_class' => 'wcfm_title', 'value' => ''),
                    )); 
                    }
                   
                    ?>
                    
                    <?php
                    $WCFM->wcfm_fields->wcfm_generate_form_field(array(
                        "kyc_account_name" => array('label' => 'Account Holder Name', 'type' => 'text', 'class' => 'wcfm-text  ', 'label_class' => ' wcfm_title', 'value' => $kyc_data['kyc_account_name']),
                        "kyc_account_number" => array('label' => 'Account Number', 'type' => 'text', 'class' => 'wcfm-text  ', 'label_class' => ' wcfm_title', 'value' => $kyc_data['kyc_account_number']),
                        "kyc_bank_name" => array('label' => 'Bank Name', 'type' => 'text', 'class' => 'wcfm-text  ', 'label_class' => ' wcfm_title', 'value' => $kyc_data['kyc_bank_name']),
                        "kyc_ifsc_code" => array('label' => 'IFSC Code', 'type' => 'text', 'class' => 'wcfm-text  ', 'label_class' => ' wcfm_title', 'value' => $kyc_data['kyc_ifsc_code']),
                        "kyc_account_doc" => array('label' => 'Canceled Cheque/Passbook', 'type' => 'file', 'class' => 'wcfm-text  ', 'label_class' => ' wcfm_title', 'value' => $kyc_data['kyc_account_name'])
                    ));
                    ?>

                    <div class="wcfm-clearfix"></div>
                    <div class="wcfm-message" style="display: none;"></div>
                    <div class="text-center">

                        <input type="submit" style="float: unset;" name="submit-data" value="<?php _e('Submit', 'wc-frontend-manager'); ?>" id="kyc_submit_button" class="wcfm_submit_button" />   
                    </div>

                    <div class="wcfm-clearfix"></div>
                </form>
            </div>
        </div> 


    </div>
</div>