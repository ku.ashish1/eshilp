<?php
global $WCFM;

 
?>

<div class="collapse wcfm-collapse" id="wcfm_vendors_listing">

  <div class="wcfm-page-headig">
		<span class="wcfmfa fa-user-alt"></span>
		<span class="wcfm-page-heading-text"><?php echo  __( 'KYC List', 'wc-frontend-manager' ); ?></span>
		<?php do_action( 'wcfm_page_heading' ); ?>
	</div>
	<div class="wcfm-collapse-content">
		<div id="wcfm_page_load"></div>
		<?php do_action( 'before_wcfm_vendors' ); ?>
		
		<div class="wcfm-container wcfm-top-element-container">
			<h2><?php echo  __( 'KYC List', 'wc-frontend-manager') ; ?></h2>
			 
			<div class="wcfm-clearfix"></div>
		</div>
	  <div class="wcfm-clearfix"></div><br />
		
		 
		
		<div class="wcfm-container">
			<div id="wcfm_kyc_listing_expander" class="wcfm-content">
				<table id="wcfm-kyclist" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
						  <th><span class="wcicon-status-processing text_tip" data-tip="<?php _e( 'Status', 'wc-frontend-manager' ); ?>"></span></th>
							<th><?php _e( 'Seller Name', 'wc-frontend-manager' ); ?></th>
						  <th><?php _e( 'Store Name', 'wc-frontend-manager' ); ?></th>
                                                  <th><?php _e( 'Account Details', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Verified', 'wc-frontend-manager' ); ?></th>
							 
							<th><?php _e( 'Action', 'wc-frontend-manager' ); ?></th>
						</tr>
					</thead>
					<tfoot>
						<tr>
						  <th><span class="wcicon-status-processing text_tip" data-tip="<?php _e( 'Status', 'wc-frontend-manager' ); ?>"></span></th>
						  <th><?php _e( 'Seller Name', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Store Name', 'wc-frontend-manager' ); ?></th>
                                                        <th><?php _e( 'Account Details', 'wc-frontend-manager' ); ?></th>
							<th><?php _e( 'Verified', 'wc-frontend-manager' ); ?></th>
							 
							 <th><?php _e( 'Action', 'wc-frontend-manager' ); ?></th>
						</tr>
					</tfoot>
				</table>
				<div class="wcfm-clearfix"></div>
			</div>
		</div>
		 
	</div>
</div>