<?php
add_action( 'wp_enqueue_scripts', 'load_script_file' );
add_action( 'admin_post_import_product_category', 'import_product_category' );
function custom_menu()
{
    // add_menu_page( string $page_title, string $menu_title, string $capability, string $menu_slug, callable $function = '', string $icon_url = '', int $position = null )
    add_menu_page( 'category_import', 'Product cat import', 'manage_options', 'pro-cat-imp', 'gstForm', '', 30 );
}


function gstForm()
{
    if(isset($_GET['msg'])){
        echo '<div class="notice notice-warning is-dismissible">'.$_GET['msg'].'</div>';
    }
    echo '<div class="updated"><h2 class="wcfm_registration_form_heading">Add GST</h2>
    <form id="dataForm" name="dataform" method="POST" action="'.esc_url( admin_url('admin-post.php') ).'" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="hidden" name="action" value="import_product_category">
    <button type="submit" class="btn btn-primary">Submit</button>
  </form></div>';
}

function import_product_category(){
    echo "testing";
    print_r($_FILES['fileToUpload']);
    //die;
    ini_set('allow_url_fopen', '1');
    $data = json_decode(file_get_contents($_FILES['fileToUpload']['tmp_name']));
    if($data){
        global $wpdb;
        $wpdb->query("UPDATE {$wpdb->prefix}options set `option_value`='' where `option_name`='product_cat_children'");  
        $option = new stdClass();
        insert_update($data,$parent_id=0,$option);
    }else{
        //  back masg with blank file
    }
}

function insert_update($data,$parent_id,$option){
    $child=[];
    foreach ($data as $value) { 
        $return_id=update_term($value,$parent_id); 
        // where return_id should be parent_id if child count > 0
        if($parent_id>0){
            $child[]=(int)$return_id;
            $option->$parent_id=$child;
        }
        if(isset($value->child) && is_array($value->child) && count($value->child)>0){
            insert_update($value->child,$return_id,$option);
        }
    }
}
function update_term($value,$parent_id){
    global $wpdb;
    $getData= $wpdb->get_results( "SELECT term_id from {$wpdb->prefix}terms where slug='".$value->slug."'", ARRAY_A  );
        if(!empty($getData) && is_array($getData)){
            $return_id=$getData[0]['term_id'];
                // Update term if exist
            $wpdb->query("UPDATE {$wpdb->prefix}terms set `name`='".$value->name."',`slug`='".$value->slug."',`term_group`=0 where `term_id`='".$return_id."'");
        }else{
            $wpdb->query( "INSERT into {$wpdb->prefix}terms (`name`,`slug`,`term_group`) VALUES ('$value->name','$value->slug','$value->term_group')" );
            $return_id = $wpdb->insert_id;
        }
        insert_term_texonomy($return_id,$parent_id,$value->description);
        return $return_id;
}
function insert_term_texonomy($term_id,$parent_id,$description){
    global $wpdb;
    $getData= $wpdb->get_results( "SELECT term_id from {$wpdb->prefix}term_taxonomy where term_id='".$term_id."' AND taxonomy = 'product_cat'", ARRAY_A  );
        if(!empty($getData) && is_array($getData)){
            $return_id=$getData[0]['term_id'];
                // Update term if exist
            $wpdb->query("UPDATE {$wpdb->prefix}term_taxonomy set `description`='".$description."',`parent`=$parent_id where `term_id`='".$term_id."'");
        }else{
            $wpdb->query( "INSERT into {$wpdb->prefix}term_taxonomy (`term_id`,`description`,`parent`,`taxonomy`) VALUES ($term_id,'$description','$parent_id','product_cat')" );   
        }
}
add_action('admin_menu','custom_menu');
/*
* Custom Function (Ashish Agarwal) here for backup wooCommerce Admin Order rest Api for disbursment
*/ 
    
function add_custom_field_vendor_bank_detail( $response ) {  
    $response_data                    = $response->get_data();
    if(isset($response_data['line_items']) && count($response_data['line_items'])){
        global $wpdb;
        foreach ($response_data['line_items'] as $key=>$value) {
            $bankDetails= new \stdClass();
            if(isset($value['meta_data']) && count($value['meta_data'])){	
                $vendor_id=$value['meta_data'][array_search('_vendor_id',array_column($value['meta_data'],'key'))]->value;
                if($vendor_id){
                    $getData= $wpdb->get_results( "SELECT meta_value FROM {$wpdb->prefix}usermeta WHERE user_id=$vendor_id AND meta_key='wcfmmp_profile_settings'", ARRAY_A); 
                    if(!empty($getData) && is_array($getData)){
                        $meta_value=unserialize($getData[0]['meta_value']);
                        $response_data['line_items'][$key]['vendor_bank_details']=$meta_value['payment'];
                        $response_data['line_items'][$key]['vendor_id']=$vendor_id;
                    }else{
                        $response_data['line_items'][$key]['vendor_bank_details']=$bankDetails; 
                        $response_data['line_items'][$key]['vendor_id']=$vendor_id;
                    }
                }else{
                    $response_data['line_items'][$key]['vendor_bank_details']=$bankDetails; 
                    $response_data['line_items'][$key]['vendor_id']=$vendor_id;
                }
            }else{
                $response_data['line_items'][$key]['vendor_bank_details']=$bankDetails;
                $response_data['line_items'][$key]['vendor_id']=null; 
            }
        }
    } 
    $response->set_data( $response_data );
    return $response;
}
function getNewTrackingCode(){
    global $wpdb;
    $getData= $wpdb->get_row("SELECT meta_value FROM {$wpdb->prefix}bar_code WHERE status=0");
    $data='';
    if($getData){
        $data=$getData->first_two_digit_code.$getData->three_to_ten_digit_code.$getData->eleven_digit_code.$getData->last_two_digit_code;
    } 
    return $data;
}
    
?>