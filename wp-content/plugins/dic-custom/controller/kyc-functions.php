<?php

add_filter('wcfm_menus', 'kyc_verify_menu', 2);
add_filter('wcfm_query_vars', 'kyc_verify_query_vars', 20);
add_filter('wcfm_endpoint_wcfm-rma_title', 'kyc_verify_endpoint_title');
add_action('wcfm_load_views', 'kyc_verify_load_views');


function kyc_verify_menu( $menus ) {

			return array_slice( $menus, 0, 3, true ) +
			array( 'dic-kyc' => array( 'label'    => 'KYC Verification',
				'url'      =>  wcfm_rma_endpoint_url(),
				'icon'     => 'fa-check',
				'menu_for' => 'both',
				'priority' => 75
			) ) +
			array_slice( $menus, 3, count( $menus ) - 3, true );
		}
                
 function kyc_verify_query_vars( $query_vars ) {
			$wcfm_modified_endpoints = wcfm_get_option( 'wcfm_endpoints', array() );
			$rma_qry = array( 'dic-kyc' => ! empty( $wcfm_modified_endpoints['dic-kyc'] ) ? $wcfm_modified_endpoints['dic-kyc'] : 'dic-kyc' );
			return array_merge( $query_vars, $rma_qry );
		}
                
  function kyc_verify_endpoint_title( $endpoint ) {
			return 'KYC';
		}
                
  function kyc_verify_load_views( $endpoint ) {
			if($endpoint == 'dic-kyc') {

				include_once MWB_WRFFM_DIRPATH . '../template/kyc-verification-form-view.php';
			}
		}               
                

?>