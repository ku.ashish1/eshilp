<?php

/**
 * Exit if accessed directly
 */
if (!defined('ABSPATH')) {
    exit;
}

if (!class_exists('class_kyc_controller')) {


    class class_kyc_controller {

        function __construct() {

            add_filter('wcfm_menus', array($this, 'kyc_verify_menu'), 2);
            add_filter('wcfm_query_vars', array($this, 'kyc_verify_query_vars'), 20);
            add_filter('wcfm_endpoint_wcfm-rma_title', array($this, 'kyc_verify_endpoint_title'));
            add_action('wcfm_load_views', array($this, 'kyc_verify_load_views'));
            add_action('wp_ajax_kyc_submit_controller', array($this, 'kyc_submit_ajax'));
            add_action('wp_ajax_nopriv_kyc_submit_controller', array($this, 'kyc_submit_ajax'));
            add_action('wp_ajax_kyc_approve_controller', array($this, 'kyc_approve_controller'));
            add_action('wp_ajax_wcfm_kyc_ajax_controller', array($this, 'wcfm_kyc_ajax_controller'));
        }

        public function kyc_verify_query_vars($query_vars) {
            $wcfm_modified_endpoints = wcfm_get_option('wcfm_endpoints', array());
            $rma_qry = array('dic-kyc' => !empty($wcfm_modified_endpoints['dic-kyc']) ? $wcfm_modified_endpoints['dic-kyc'] : 'dic-kyc');
            return array_merge($query_vars, $rma_qry);
        }

        public function kyc_verify_endpoint_title($endpoint) {
            return 'KYC';
        }

        public function kyc_verify_load_views($endpoint) {
            global $WCFM;
            if ($endpoint == 'dic-kyc') {
                $WCFM->library->load_datatable_lib();
                wp_enqueue_script('kyc_module_script', plugins_url('../assets/js/kyc-module.js', __FILE__), array('jquery'));
                wp_localize_script('kyc_module_script', 'kyc_module_script_object', array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'account_name' => 'Account Name Required',
                    'account_number' => 'Account Number Required',
                    'bank_name' => 'Bank Name Required',
                    'ifsc_code' => 'IFSC Code Required',
                    'account_doc' => 'ChequePassbook Copy Required',
                        )
                );

                $user_id = (isset($_GET['user-id'])) ? $_GET['user-id'] : '';


                if (current_user_can('administrator')) {
                    if($user_id)
                    {
                        $check_data = get_user_meta($user_id, 'kyc_details', FALSE);
                        $kyc_data = unserialize($check_data[0]);
                        include_once DIC_DIRPATH . '/template/kyc-verification-view.php';
                    }
                    else
                    {
                        include_once DIC_DIRPATH . '/template/kyc-vendor-list-admin.php';        
                    }
                    
                } else {
                    $current_user = get_current_user_id();
                    $check_data = get_user_meta($current_user, 'kyc_details', FALSE);
                    $kyc_data = unserialize($check_data[0]);
                    $action = (isset($_GET['action'])) ? $_GET['action'] : '';
                    if ($kyc_data['kyc_status'] != 'reject' && $action != 'edit') {
                        include_once DIC_DIRPATH . '/template/kyc-verification-view.php';
                    } else {

                        include_once DIC_DIRPATH . '/template/kyc-verification-form-view.php';
                    }
                }
            }
        }

        public function kyc_verify_endpoint_url() {
            $wcfm_page = get_wcfm_page();
            $wcfm_rma_url = wcfm_get_endpoint_url('dic-kyc', '', $wcfm_page);
            return apply_filters('wcfm_rma_endpoint_url', $wcfm_rma_url);
        }

        public function kyc_verify_menu($menus) {

            return array_slice($menus, 0, 3, true) +
                    array('dic-kyc' => array('label' => 'KYC Verification',
                            'url' => $this->kyc_verify_endpoint_url(),
                            'icon' => 'fa-check',
                            'menu_for' => array('vendor', 'admin'),
                            'priority' => 75
                        )) +
                    array_slice($menus, 3, count($menus) - 3, true);
        }

        function kyc_submit_ajax() {
            $current_user = get_current_user_id();
            $params = array();
            parse_str($_POST['wcfm_kyc_manage_form'], $postArray);
            $check_data = get_user_meta($current_user, 'kyc_details', FALSE);        
            if(!preg_match('/^[a-z .\-]+$/i', $postArray['kyc_account_name'])
                   
              
              )
            {
                $response = array('status' => FALSE,
                'message' => 'Account Holder Name must be alphabetic',
                'data' => ''
            );
                
            }
            elseif(!preg_match('/^[0-9 .\-]+$/i', $postArray['kyc_account_number'])
                   
              
              )
            {
                $response = array('status' => FALSE,
                'message' => 'Account Number must be numeric only',
                'data' => ''
            );
                
            }
            elseif(!preg_match('/^[a-z .\-]+$/i', $postArray['kyc_bank_name'])
                   
              
              )
            {
                $response = array('status' => FALSE,
                'message' => 'Bank Name must be alphabetic',
                'data' => ''
            );
                
            }
            elseif(!preg_match('/^[a-z0-9 .\-]+$/i', $postArray['kyc_ifsc_code'])
                   
              
              )
            {
                $response = array('status' => FALSE,
                'message' => 'IFSC Code must be alphanumeric',
                'data' => ''
            );
                
            }
            
                    
            elseif ($check_data) {
                $postArray['kyc_status'] = 'change_request';
                 $serializeData = serialize($postArray);
                update_user_meta($current_user, 'kyc_details', $serializeData);
                $response = array('status' => true,
                'message' => 'You KYC detail is successfully submitted for verification.',
                'data' => ''
            );
            } else {
                $postArray['kyc_status'] = 'submitted';
                 $serializeData = serialize($postArray);
                add_user_meta($current_user, 'kyc_details', $serializeData);
                $response = array('status' => true,
                'message' => 'You KYC detail is successfully submitted for verification.',
                'data' => ''
            );
            }

            
            echo json_encode($response);
            die;
        }

        function wcfm_kyc_ajax_controller() {
            global $WCFM, $WCFMu;
                    
            $plugin_path = trailingslashit(dirname(__FILE__));

            $controller = '';
            if (isset($_POST['controller'])) {
                $controller = $_POST['controller'];

                switch ($controller) {
                    case 'wcfm-kyc':
                        
                      require_once( $plugin_path . 'wcfm-kyc-ajax.php' );
                       new WCFM_Kyc_Controller();
                        break;
                }
            }
                    
  	die();
        }
        
        function kyc_approve_controller()
        {
            global $WCFM, $WCFMu, $_POST;
            $InputArray = $_POST;
            $check_data = get_user_meta($InputArray['userid'], 'kyc_details', FALSE);
            $kyc_data = unserialize($check_data[0]);
            $status = $InputArray['status'];
           
            if(trim($status) != 'approve' &&  trim($status) != 'reject' )
            {
                echo '{"status" : false, "message" : "Wrong Input Data", "redirect" : ""}';
            }
            elseif ($kyc_data['kyc_status'] == 'submitted' || $kyc_data['kyc_status'] == 'change_request') {
                $kyc_data['kyc_status'] = $status;
                $kyc_data['kyc_message'] = $InputArray['kyc_account_message'];
                $serializeData = serialize($kyc_data);
                    
                if(update_user_meta($InputArray['userid'], 'kyc_details', $serializeData))
                {
                    echo '{"status" : true, "message" : "Successfully Updated", "redirect" : ""}';
                }
                else
                {
                    echo '{"status" : false, "message" : "Something Wrong1"}';
                }
                
            } else {
                echo '{"status" : false, "message" : "Something Wrong2"}';
            }
            
            die;
            
        }
        
        

    }

    new class_kyc_controller();
}