<?php
/**
 * WCFM plugin controllers
 *
 * Plugin Products Custom Menus Build Controller
 *
 * @author 		WC Lovers
 * @package 	wcfmcsm/controllers
 * @version   1.0.0
 */

class WCFM_Kyc_Controller {
	
	public function __construct() {
		global $WCFM, $WCFMu;
		
		$this->processing();
	}
	
	public function processing() {
		global $WCFM, $WCFMu, $wpdb, $_POST;
                
                
                
		$length = wc_clean($_POST['length']);
		$offset = wc_clean($_POST['start']);
		
		$report_for = '7day';
		if( isset($_POST['report_for']) && !empty($_POST['report_for']) ) {
			$report_for = wc_clean($_POST['report_for']);
		}
		
		$filter_date_form = '';
		if( isset($_POST['filter_date_form']) && !empty($_POST['filter_date_form']) ) {
			$filter_date_form = wc_clean($_POST['filter_date_form']);
		}
		
		$filter_date_to = '';
		if( isset($_POST['filter_date_to']) && !empty($_POST['filter_date_to']) ) {
			$filter_date_to = wc_clean($_POST['filter_date_to']);
		}
		
		if( $filter_date_form && $filter_date_to ) {
			$report_for = 'custom';
		}
		
		$search_vendor = '';
		if ( ! empty( $_POST['report_vendor'] ) ) {
			$search_vendor = absint( $_POST['report_vendor'] );
		}
		
		$report_membership = '';
		if ( ! empty( $_POST['report_membership'] ) ) {
			$report_membership = absint( $_POST['report_membership'] );
			
			if( !$search_vendor ) {
				$search_vendor = (array) get_post_meta( $report_membership, 'membership_users', true );
			}
		}
		
		$vendor_search_data = array();
		if ( isset( $_POST['search_data'] ) && ! empty( $_POST['search_data'] ) ) {
			$vendor_search_data = $_POST['search_data'];
		}
		 
		$wcfm_vendors_array = $WCFM->wcfm_vendor_support->wcfm_get_vendor_list( true, $offset, $length, '', $search_vendor, true, $vendor_search_data );
		
                unset($wcfm_vendors_array[0]);
		
		// Get Vendor Count
                $wcfm_all_vendors = $WCFM->wcfm_vendor_support->wcfm_get_vendor_list( true );
		unset($wcfm_all_vendors[0]);
               
                
                 
		// Get Filtered Vendor Count
		$wcfm_filtered_vendors = $WCFM->wcfm_vendor_support->wcfm_get_vendor_list( true, '', '', '', $search_vendor, true, $vendor_search_data );
		unset($wcfm_filtered_vendors[0]);
		
		$admin_fee_mode = apply_filters( 'wcfm_is_admin_fee_mode', false );
		
		if( defined('WCFM_REST_API_CALL') ) {
			return $wcfm_vendors_array;
		}
                
		// Generate Vendors JSON
		$wcfm_vendors_json = '';
		$wcfm_vendors_json = '{
															"draw": ' . wc_clean($_POST['draw']) . ',
															"recordsTotal": ' . count( $wcfm_all_vendors ) . ',
															"recordsFiltered": ' . count( $wcfm_filtered_vendors ) . ',
															"data": ';
		if(!empty($wcfm_vendors_array)) {
			$index = 0;
			$wcfm_vendors_json_arr = array();
                        //print_r($wcfm_vendors_array); die;
			foreach($wcfm_vendors_array as $wcfm_vendors_id => $wcfm_vendors_name ) {
				
				// Status
				$disable_vendor = get_user_meta( $wcfm_vendors_id, '_disable_vendor', true );
				$is_store_offline = get_user_meta( $wcfm_vendors_id, '_wcfm_store_offline', true );
				if( $is_store_offline ) {
					$wcfm_vendors_json_arr[$index][] = '<span class="order-status tips wcfmfa fa-power-off text_tip" style="color: #ff9310;" data-tip="' . __( 'Store Off-line', 'wc-frontend-manager' ) . '"></span>';
				} else {
					if( $disable_vendor ) {
						$wcfm_vendors_json_arr[$index][] = '<span class="order-status tips wcicon-status-cancelled text_tip" data-tip="' . __( 'Disable Vendor', 'wc-frontend-manager' ) . '"></span>';
					} else {
						$wcfm_vendors_json_arr[$index][] = '<span class="order-status tips wcicon-status-completed text_tip" data-tip="' . __( 'Active Vendor', 'wc-frontend-manager' ) . '"></span>'; 
					}
				}
				
				 
				
				// Name
				 
					$vendor_display_name = '<span class="wcfm_dashboard_item_title">' . apply_filters( 'wcfm_vendors_display_name_data', $wcfm_vendors_name, $wcfm_vendors_id ) . '</span>';
				 
				$wcfm_vendors_json_arr[$index][] =  $vendor_display_name;
				
				// Shop Name
				$wcfm_vendors_json_arr[$index][] =  '<span class="wcfm_vendor_store">' . apply_filters( 'wcfm_vendors_store_name_data', wcfm_get_vendor_store( $wcfm_vendors_id ), $wcfm_vendors_id ) . '</span>';
				
				 
				$kyc_detail = unserialize(get_user_meta( $wcfm_vendors_id, 'kyc_details', TRUE));
                                $account_details = '';
                                if($kyc_detail)
                                {
                                   
                                $account_details .= '<span class="wcfm_dashboard_item_title"> Account Name : ' . $kyc_detail['kyc_account_name'].' </span></br> ';
                                $account_details .= '<span class="wcfm_dashboard_item_title"> Account Number :' . $kyc_detail['kyc_account_number'].' </span></br> ';
                                $account_details .= '<span class="wcfm_dashboard_item_title"> Bank Name :' . $kyc_detail['kyc_bank_name'].' </span></br> ';
                                $account_details .= '<span class="wcfm_dashboard_item_title"> IFSC Code :' . $kyc_detail['kyc_ifsc_code'].' </span></br> ';
                                 
                                }
                                
                                $wcfm_vendors_json_arr[$index][] =  $account_details;
				// Additional Info
				$wcfm_vendors_json_arr[$index][] =  ($kyc_detail['kyc_submitted']) ? $kyc_detail['kyc_submitted'] : 'Not Submitted';

				// Action
                                $actions = '';
                                $actions = '<a href="?user-id='.$wcfm_vendors_id.'" class="wcfm-action-icon"><span class="wcfmfa fa-eye text_tip" data-tip="' . __( 'Details', 'wc-frontend-manager' ) . '"></span></a>';
				$wcfm_vendors_json_arr[$index][] = $actions;		 
                                
                                 
				
				$index++;
			}												
		}
		if( !empty($wcfm_vendors_json_arr) ) $wcfm_vendors_json .= json_encode($wcfm_vendors_json_arr);
		else $wcfm_vendors_json .= '[]';
		$wcfm_vendors_json .= '
													}';
													
		echo $wcfm_vendors_json;
                
                
                
	 
	  
	  die;
	}
}