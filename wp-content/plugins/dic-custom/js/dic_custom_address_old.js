jQuery(document).ready(function () {

    // jQuery('#district option').remove();
   //  jQuery('#district').append('<option value="" selected>Select District</option>');
    // jQuery('#sub_district option').remove();
     //jQuery('#sub_district').append('<option value="" selected>Select Sub District</option>');
 
 
     /**
      * Function         : get_district_by_state_id 
      * Devloper         : Sateesh Babu
      * Creaate_at       : 18-09-2020
      * updated_at       : 
      * updated_by       :
      * Update_reason    :
      * Description      :
      */
 
     // console.log(userData);
     // var state_id=jQuery("#state,#billing_state").val()?jQuery("#state,#billing_state").val():'';
 
     
 jQuery.ajax({
     type: "post",
     dataType: 'json',
     url: district_list_by_state_id.ajaxurl, //this is wordpress ajax file which is already avaiable in wordpress
     data: {
         action:'get_user_details_ajax' //this value is first parameter of add_action,
     },
     success: function(res){
        console.log(res);
        jQuery('select#billing_district').val(res.billing_district_id);
        jQuery('select#billing_sub_district').val(res.billing_sub_district_id);
		var shipdist_id = res.shipping_district_id;
		var ship_subdist_id = res.shipping_sub_district_id;
		if(res.shipping_district_id == 0){
			shipdist_id = res.billing_district_id;
		}
		if(res.shipping_sub_district_id == 0){
			ship_subdist_id = res.billing_sub_district_id;
		}
        jQuery('select#shipping_district').val(shipdist_id);
        jQuery('select#shipping_sub_district').val(ship_subdist_id);
		/*if(jQuery('input#billing_first_name').val()==''){
			jQuery('input#billing_first_name').val(res.user_data.first_name[0]);
		}
		if(jQuery('input#billing_last_name').val()==''){
			jQuery('select#billing_last_name').val(res.user_data.last_name[0]);
		}
		if(jQuery('input#billing_phone').val()==''){
			jQuery('select#billing_phone').val(res.user_data.billing_phone[0]);
        }
		if(jQuery('input#billing_email').val()==''){
			jQuery('select#billing_email').val(res.user_data.billing_email[0]);
        }
		
		if(jQuery('input#shipping_first_name').val()==''){
			jQuery('input#shipping_first_name').val(res.user_data.first_name[0]);
		}
		if(jQuery('input#shipping_last_name').val()==''){
			jQuery('select#shipping_last_name').val(res.user_data.last_name[0]);
        } */
     }
 });
     
 setTimeout(function(){ 
 
         
         jQuery("#state").on('change',function () {
            var state_id = jQuery(this).val();
            jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
             getDistrct('district',state_id);
         }); 
         jQuery("#billing_state").on('change',function () {
			
            var state_id = jQuery(this).val();
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
             getDistrct('billing_district',state_id);
         });
         jQuery("#shipping_state").on('change',function () {
            var state_id = jQuery(this).val();
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
             getDistrct('shipping_district',state_id);
         });
 
         /**
          * Function         : get_sub_district_by_district_id 
          * Devloper         : Sateesh Babu
          * Creaate_at       : 18-09-2020
          * updated_at       : 
          * updated_by       :
          * Update_reason    :
          * Description      :
          */
         jQuery("#district").on('change',function () {
             var district_id = jQuery(this).val();
             if(district_id==''){
                 console.log('District should not be empty');
                 return false;
             }
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
             getSubDistrict('sub_district',district_id);
         });
         jQuery("#shipping_district").on('change',function () {
             var district_id = jQuery(this).val();
             if(district_id==''){
                 console.log('District should not be empty');
                 return false;
             }
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
             getSubDistrict('shipping_sub_district',district_id);
         });
         jQuery("#billing_district").on('change',function () {
             var district_id = jQuery(this).val();
			 var state_id = jQuery('#billing_state').val();
             if(district_id==''){
                 console.log('District should not be empty');
                 return false;
             }
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
			 if(jQuery('#ship-to-different-address-checkbox').is(":checked") == false){
				getDistrct('shipping_district',state_id,district_id);
			}
             getSubDistrict('billing_sub_district',district_id);
         });
		 jQuery("#billing_sub_district").on('change',function () {
             var sub_district_id = jQuery(this).val();
			 var district_id = jQuery('#billing_district').val();
             if(district_id==''){
                 console.log('District should not be empty');
                 return false;
             }
             jQuery('#wcfm_membership_container').block({
                 message: null,
                 overlayCSS: {
                     background: '#fff',
                     opacity: 0.6
                 }
             });
			 if(jQuery('#ship-to-different-address-checkbox').is(":checked") == false){
				getSubDistrict('shipping_sub_district',district_id,sub_district_id);
			}
         });
     },2000);
 });
 
 
 function getDistrct(field_id,state_id='',district_id=''){  
     if(state_id==''){
         console.log('state should not be empty');
         return false;
     }
     jQuery.ajax({
         url: district_list_by_state_id.ajaxurl,
         type: "post",
         async: false,
         data: {
             action: "fetch_district",
             state_id: state_id
         },
         success: function (res) {
             jQuery('#wcfm_membership_container').unblock();
             var resData=JSON.parse(res);
             if(resData.status==true){
                var districtList=resData.districtList;
                jQuery('#'+field_id+' option').remove();
                var districtOption='';
                 if(district_id==''){
                     districtOption='<option value="" selected>Select District</option>';
                 }
                 jQuery.each(districtList,function( index,value ) {
                     if(district_id==value.district_id){
                         districtOption +='<option value="'+value.district_id+'">'+value.district_name+'</option>';
                     }else{
                         districtOption +='<option value="'+value.district_id+'">'+value.district_name+'</option>';
                     }
                 });
                 jQuery('#'+field_id).append(districtOption);
                 return true;
             }else{
                 console.log(resData.status);
             }
         },
         error: function(err){
             console.log('Error :' + err.status);
         }
     });
 }
 
 function getSubDistrict(field_id,district_id,sub_district_id=''){
     jQuery.ajax({
         url: sub_district_list_by_district.ajaxurl,
         type: "post",
         async: false,
         data: {
             action: "sub_fetch_district",
             district_id: district_id
         },
         success: function (res) {
             jQuery('#wcfm_membership_container').unblock();
             var resData=JSON.parse(res);
             if(resData.status==true){
                 var subDistrictList=resData.subDistrictList;
                 jQuery('#'+field_id+' option').remove();
                 var subDistrictOption='';
                 if(sub_district_id==''){
                     subDistrictOption='<option value="" selected>Select Sub District</option>';
                 }
                 jQuery.each(subDistrictList,function( index,value ) {
                     if(sub_district_id==value.sub_district_id){
                         subDistrictOption +='<option value="'+value.sub_district_id+'" selected>'+value.sub_district_name+'</option>';
                     }else{
                         subDistrictOption +='<option value="'+value.sub_district_id+'">'+value.sub_district_name+'</option>';
                     }
                 });
                 jQuery('#'+field_id).append(subDistrictOption);
                 return true;
             }else{
                 console.log(resData.status);
             }
         },
         error: function(err){
             console.log('Error :' + err.status);
         }
     });
 }