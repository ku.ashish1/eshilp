
function custome_vender_registration_validation() {
    jQuery('.wcfm-message').empty();
    var returnType  = [];
    returnType[0]  = checkPasswordConfirmPassword();
    returnType[1]  = checkPassword();
    returnType[2]  = checkPinCode();
    // returnType[3]  = checkSubDistrict();
    // returnType[4]  = checkDistrict();
    // returnType[5]   = checkState();
    returnType[6]   = checkCountry();
    returnType[7]   = checkCity();
    returnType[8]   = checkAddressLine2();
    returnType[9]   = checkAddressLine1();
    returnType[10]   = checkBrandName();
    returnType[11]   = checkLastName();
    returnType[12]   = checkFirstName();
    returnType[13]   = checkEmailLength();
    returnType[14]   = checkPhone();
    // console.log(jQuery.inArray(false, returnType));
    // return false;
    if (jQuery.inArray(false, returnType)!==-1) {
        return false;
    }
    return true;
}
jQuery(document).ready(function () {
    jQuery('#user_phone,#billing_phone').keypress(function (e) { 
        var keyCode = e.keyCode || e.which;
        var regex = /^[0-9]+$/;
        var isNum= regex.test(String.fromCharCode(keyCode));
        if(!isNum){
           return false;
        }
        if(jQuery(this).val().length>=10){
            return false;
        }
    });

    jQuery('#user_phone,#billing_phone').keyup(function (e) { 
        if(jQuery(this).val().length==1 && jQuery(this).val()==0) {
            jQuery(this).val('');
            return false;
        }
    });
    jQuery('#user_phone,#billing_phone').on('change', function () {
        var value = parseInt(jQuery('#user_phone,#billing_phone').val());
        jQuery('#user_phone,#billing_phone').val(value ? value : '');
    });
    jQuery('#user_phone').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkPhone();
    });

    jQuery(".wcfm_sms_verified_input").keyup(function(e){
        jQuery('.sms_verification_message').attr("style", "display:none");
    });

    jQuery('#user_email').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkEmailLength();
    });
        
    jQuery('#first_name').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkFirstName();
    });
    jQuery('#last_name').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkLastName();
    });
    jQuery('#brand_name').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkBrandName();
    });

    jQuery('#zip').keypress(function (e) { 
        var keyCode = e.keyCode || e.which;
        var regex = /^[0-9]+$/;
        var isNum= regex.test(String.fromCharCode(keyCode));
        if(!isNum){
           return false;
        }
        if(jQuery(this).val().length>=6){
            return false;
        }
    });

    jQuery('#zip').keyup(function (e) { 
        if(jQuery(this).val().length==1 && jQuery(this).val()==0) {
            jQuery(this).val('');
            return false;
        }
    });
    jQuery('#zip').on('change', function () {
        var value = parseInt(jQuery('#zip').val());
        jQuery('#zip').val(value ? value : '');
    });

    jQuery('#zip').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkPinCode();
    });

    jQuery('#addr_1').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkAddressLine1();
    });

    jQuery('#addr_2').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkAddressLine2();
    });

    jQuery('#city').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkCity();
    });

    jQuery('#passoword').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkPassword();
    });

    jQuery('#confirm_pwd').on('blur', function () {
        jQuery('.wcfm-message').empty();
        checkPasswordConfirmPassword();
    });

});

function checkPhone() {
    var phone = jQuery('#user_phone,#billing_phone').val() ? jQuery('#user_phone,#billing_phone').val() : '';
    if (phone=='' || !jQuery.isNumeric(phone) > 0 || phone.length > 10 || phone.length < 10) {
        jQuery('#user_phone,#billing_phone').removeClass('wcfm_validation_success');
        jQuery('#user_phone,#billing_phone').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Length of mobile number should be numeric and contain 10 digit<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#phone,#billing_phone').val('');
    jQuery('#phone,#billing_phone').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#phone,#billing_phone').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkEmailLength() {
    var user_email = jQuery('#user_email').val() ? jQuery('#user_email').val() : '';
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!regex.test(user_email)){
        if (user_email == '' || jQuery.type(new String(user_email)) !== "string" || user_email.length > 50) {
            jQuery('#user_email').removeClass('wcfm_validation_success');
            jQuery('#user_email').addClass('wcfm_ele wcfm_validation_failed');
            jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Enter a valide email sould be contain maximum length 50 (example@domainname)<br>');
            jQuery('.wcfm-message').attr("style", "display:block");
            jQuery('.wcfm-message').addClass("wcfm-error");
            return false;
        }
        jQuery('#user_email').removeClass('wcfm_validation_success');
        jQuery('#user_email').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Email sould be string & contain maximum length 50 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#user_email').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#user_email').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkFirstName() {
    var first_name = jQuery('#first_name').val() ? jQuery('#first_name').val() : '';
    var last_name = jQuery('#last_name').val() ? jQuery('#last_name').val() : '';
    if (jQuery.type(new String(first_name)) !== "string" || first_name.length > 50) {
        jQuery('#first_name').removeClass('wcfm_validation_success');
        jQuery('#first_name').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>First name sould be string & contain maximum length 50 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#first_name').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#first_name').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    jQuery('#brand_name').val(first_name + last_name);
    return true;
}

function checkLastName() {
    var first_name = jQuery('#first_name').val() ? jQuery('#first_name').val() : '';
    var last_name = jQuery('#last_name').val() ? jQuery('#last_name').val() : '';
    if (first_name == '') {
        // jQuery('#last_name').removeClass('wcfm_validation_success');
        // jQuery('#last_name').addClass('wcfm_ele wcfm_validation_failed');
        // jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>First name sould not empty<br>');
        // jQuery('.wcfm-message').attr("style", "display:block");
        // jQuery('.wcfm-message').addClass("wcfm-error");
        // return false;
        jQuery('#last_name').removeClass('wcfm_ele wcfm_validation_failed');
        jQuery('#last_name').addClass('wcfm_validation_success');
        jQuery('.wcfm-message').attr("style", "display:none");
        jQuery('.wcfm-message').removeClass("wcfm-error");
        jQuery('#brand_name').val(first_name + last_name);
        return true;
    }else{
        if (jQuery.type(new String(last_name)) !== "string" || last_name.length > 50) {
            jQuery('#last_name').removeClass('wcfm_validation_success');
            jQuery('#last_name').addClass('wcfm_ele wcfm_validation_failed');
            jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Last name sould be string & contain maximum length 50 char<br>');
            jQuery('.wcfm-message').attr("style", "display:block");
            jQuery('.wcfm-message').addClass("wcfm-error");
            return false;
        }
        jQuery('#last_name').removeClass('wcfm_ele wcfm_validation_failed');
        jQuery('#last_name').addClass('wcfm_validation_success');
        jQuery('.wcfm-message').attr("style", "display:none");
        jQuery('.wcfm-message').removeClass("wcfm-error");
        jQuery('#brand_name').val(first_name + last_name);
        return true;
    }
}

function checkBrandName() {
    var brand_name = jQuery('#brand_name').val() ? jQuery('#brand_name').val() : '';
    if (jQuery.type(new String(brand_name)) !== "string" || brand_name.length > 100) {
        jQuery('#brand_name').removeClass('wcfm_validation_success');
        jQuery('#brand_name').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Last name sould be string & contain maximum length 100 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#brand_name').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#brand_name').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkAddressLine1() {
    var addr_1 = jQuery('#addr_1').val() ? jQuery('#addr_1').val() : '';
    if (addr_1=='' || jQuery.type(new String(addr_1)) !== "string" || addr_1.length > 50) {
        jQuery('#addr_1').removeClass('wcfm_validation_success');
        jQuery('#addr_1').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Address line 1 sould be string & contain maximum 50 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#addr_1').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#addr_1').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}
function checkAddressLine2() {
    var addr_2 = jQuery('#addr_2').val() ? jQuery('#addr_2').val() : '';
    if (jQuery.type(new String(addr_2)) !== "string" || addr_2.length > 50) {
        jQuery('#addr_2').removeClass('wcfm_validation_success');
        jQuery('#addr_2').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Address line 2 sould be string & contain maximum 50 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#addr_2').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#addr_2').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}
function checkCity() {
    var city = jQuery('#city').val() ? jQuery('#city').val() : '';
    if (jQuery.type(new String(city)) !== "string" || city.length > 50) {
        jQuery('#city').removeClass('wcfm_validation_success');
        jQuery('#city').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>City sould be string & contain maximum 50 char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#city').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#city').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkCountry(){
    var country=jQuery('#country').val();
    if(country==''){
        jQuery('#country').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#country').removeClass('wcfm_validation_success');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Select Country<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#country').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#country').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkState(){
    var state=jQuery('#state').val();
    if(state==''){        
        jQuery('#state').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#state').removeClass('wcfm_validation_success');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Select State<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#state').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#state').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkDistrict(){
    var district=jQuery('#district').val();
    if(district==''){
        jQuery('#district').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#district').removeClass('wcfm_validation_success');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Select District<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#district').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#district').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkSubDistrict(){
    var sub_district=jQuery('#sub_district').val();
    if(sub_district==''){        
        jQuery('#sub_district').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#sub_district').removeClass('wcfm_validation_success');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Select Sub District<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#sub_district').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#sub_district').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkPinCode() {
    var pincode = jQuery('#zip').val() ? jQuery('#zip').val() : '';
    if (pincode=='' || (!jQuery.isNumeric(pincode) > 0) || (pincode.length != 6)) {
        jQuery('#zip').removeClass('wcfm_validation_success');
        jQuery('#zip').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Length of pincode should be numeric and contain 6 digit<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#zip').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#zip').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkPassword() {
    var password = jQuery('#passoword').val() ? jQuery('#passoword').val() : '';
    if ( password=='' || !password.match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        jQuery('#passoword').removeClass('wcfm_validation_success');
        jQuery('#passoword').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#password_strength').addClass('short').html('Password should contain atleaset eight alpha numaric and one special char and contain one special char');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Password should contain atleaset eight alpha numaric and one special char and contain one special char<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#passoword').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#passoword').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}

function checkPasswordConfirmPassword() {
    var password = jQuery('#passoword').val() ? jQuery('#passoword').val() : '';
    var confirm_pwd = jQuery('#confirm_pwd').val() ? jQuery('#confirm_pwd').val() : '';
    if(confirm_pwd==''){
        jQuery('#confirm_pwd').removeClass('wcfm_validation_success');
        jQuery('#confirm_pwd').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Confirm password must not be empty<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    if(password==''){
        jQuery('#passoword').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#passoword').removeClass('wcfm_validation_success');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Password must not be empty<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    if (confirm_pwd !== password) {
        jQuery('#passoword').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('#passoword').removeClass('wcfm_validation_success');
        jQuery('#confirm_pwd').removeClass('wcfm_validation_success');
        jQuery('#confirm_pwd').addClass('wcfm_ele wcfm_validation_failed');
        jQuery('.wcfm-message').prepend('<span class="wcicon-status-cancelled"></span>Password & confirm password must be equel<br>');
        jQuery('.wcfm-message').attr("style", "display:block");
        jQuery('.wcfm-message').addClass("wcfm-error");
        return false;
    }
    jQuery('#confirm_pwd').removeClass('wcfm_ele wcfm_validation_failed');
    jQuery('#confirm_pwd').addClass('wcfm_validation_success');
    jQuery('.wcfm-message').attr("style", "display:none");
    jQuery('.wcfm-message').removeClass("wcfm-error");
    return true;
}


