function manageVendorShippingTracking( item ) {
    var data = {
                          action  : 'wcfmu_shipment_tracking_html',
                          orderid       : item.data('orderid'),
                          productid     : item.data('productid'),
                          orderitemid   : item.data('orderitemid'),
                        }
    jQuery.ajax({
        type    :		'POST',
        url     : wcfm_params.ajax_url,
        data    : data,
        success :	function(response) {                                     
            // Intialize colorbox
            jQuery.colorbox( { html: response, height: 400, width: $popup_width,
                onComplete:function() {
                    jQuery('#cboxClose').click(function(e){
                        free_code(jQuery('#wcfm_tracking_id').val());
                    })
                    jQuery('#wcfm_tracking_button').click(function(e) {
                        e.preventDefault();
                        
                        jQuery('#wcfm_shipping_tracking_form').block({
                                message: null,
                                overlayCSS: {
                                    background: '#fff',
                                    opacity: 0.6
                                }
                            });
                        
                        jQuery( document.body ).trigger( 'wcfm_form_validate', jQuery('#wcfm_shipping_tracking_form') );
                        if( !$wcfm_is_valid_form ) {
                            wcfm_notification_sound.play();
                            jQuery('#wcfm_shipping_tracking_form').unblock();
                        } else {
                            if(jQuery('#wcfm_tracking_id').val()){
                                mark_code_used(jQuery('#wcfm_tracking_id').val(),item.data('orderid'),item.data('orderitemid'),item.data('productid'));
                                // return false;
                                var tracking_url  ="http://";                    
                                var tracking_code = jQuery('#wcfm_tracking_code').val();
                                item.hide();
                            
                                jQuery('#wcfm_tracking_button').hide();
                                jQuery('#wcfm_shipping_tracking_form .wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();
                            
                                var data = {
                                    action        : item.data('shipped_action'),
                                    orderid       : item.data('orderid'),
                                    productid     : item.data('productid'),
                                    orderitemid   : item.data('orderitemid'),
                                    tracking_url  : tracking_url,
                                    tracking_code : tracking_code,
                                    tracking_data : jQuery('#wcfm_shipping_tracking_form').serialize()
                                }	
                                jQuery.ajax({
                                    type:		'POST',
                                    url: wcfm_params.ajax_url,
                                    data: data,
                                    success:	function(response) {
                                        wcfm_notification_sound.play();
                                        jQuery('#wcfm_shipping_tracking_form').unblock();
                                        jQuery('#wcfm_shipping_tracking_form').unblock();
                                        jQuery('#wcfm_shipping_tracking_form .wcfm-message').html( '<span class="wcicon-status-completed"></span>' + wcfm_shipping_tracking_labels.tracking_saved ).addClass('wcfm-success').slideDown();
                                        setTimeout(function() {
                                            jQuery.colorbox.remove();
                                            if( !window.location.hash ) {
                                                window.location = window.location.href + '#sm_order_shipment_options';
                                            }
                                            window.location.reload();
                                        }, 2000);
                                    }
                                });
                            }else{
                                mark_code_used(jQuery('#wcfm_tracking_code').val(),item.data('orderid'),item.data('orderitemid'),item.data('productid'));
                                // console.log(item.data('shipped_action'));
                                jQuery.colorbox.remove();
                            }
                        }
                    });
                },
                overlayClose: false,
            });
        }
    });
}

function free_code(id){
    jQuery.ajax({
        url: wcfm_params.ajax_url,
        type: "post",
        async: false,
        data: {
            id:id,
            action: "free_tracking_code",
        },
        success: function (res) {
            console.log(res);
        },
        error: function(err){
            console.log("not found");
        }
    });
}
function mark_code_used(trackcode_code,order_id,item_id,product_id){
    console.log({trackcode_code,order_id,item_id,product_id});
    jQuery.ajax({
        url: wcfm_params.ajax_url,
        type: "post",
        async: false,
        data: {
            code:trackcode_code,
            order_id:order_id,
            item_id:item_id,
            product_id:product_id,
            action: "allot_tracking_code",
        },
        success: function (res) {
            console.log(res);
        },
        error: function(err){
            console.log("not found");
        }
    });
}
jQuery(function($){

    $("body").on('blur','#vendor_price,.vendor_price',function(){ 
        // var e = document.getElementById("product_type");
        var vendor_price;
        if(jQuery("#product_type").val()=="variable"){
            vendor_price = jQuery(this).val();
            $(this).parents('.multi_input_block').find('input[id^=variations_regular_price]').val(priceForSite(vendor_price));
        }else{
            vendor_price = jQuery("#vendor_price").val();
            jQuery('#regular_price').val(priceForSite(vendor_price));
        }
       
      
        function priceForSite(price) {
	
            var gst_tax = 0;
            var shipping = 0;
            var shipping_gst = 0;
            var portal_gst = 0;
            var portal_charges = 0;
            var payment_gateway = 0;
            var payment_gateway_charges = 0;
            var category_id=jQuery('#wcfm_cat_level_2').val();
            if(category_id=="" || jQuery('#wcfm_cat_level_2').hasClass("wcfm_custom_hide")){
                var category_id=jQuery('#wcfm_cat_level_1').val();
            }
            console.log(category_id);
            jQuery.ajax({
                url:dic_custom_ajax_object.ajaxurl,
                type: "post",
                async: false,
                data: {
                    action: "fetch_gst",
                    category_id: category_id
                },
                success: function (res) {
                    var response = JSON.parse(res);
                    console.log(response);
                    if (response) {
                        if (response.gst) {
                            gst_tax = response.gst;
                        }
                        if (response.payment_gateway) {
                            payment_gateway = response.payment_gateway;
                            payment_gateway_charges = response.payment_gateway;
                        }
                        if (response.portal) {
                            var portal_charge_type= response.portal.charge_type;
                            if(portal_charge_type==0){
                                // 0 mean percentage
                                portal_charges= (parseInt(price)*response.portal.charges)/100;
                            }if(portal_charge_type==1)
                            {
                                portal_charges= response.portal.charges;
                            }
                            portal_gst= (portal_charges*response.portal.gst_rate)/100;
                            
                        }
                        if (response.shipping) {
                            var shipping_charge_type= response.shipping.shipping_type;
                            if(shipping_charge_type==0){
                                 // 0 mean percentage
                                 shipping= (response.shipping.shipping_charges * response.shipping.gst_rate)/100;
                            }
                            if(shipping_charge_type==1){
                                shipping=response.shipping.shipping_charges;
                            }
                            shipping_gst= (shipping*response.shipping.gst_rate)/100;
                        }
                    }
                },
                error: function (err) {
                    console.log("error");
                }
            });
			
			if (isNaN(parseInt(gst_tax))) {
				gst_tax = 0;
			}
			if(portal_charges == null){
				
				portal_charges =0;
			}
			if(portal_gst == null){
				
				portal_gst = 0;
			}
			if(shipping_gst == null){
				shipping_gst =0;
			
			}
			if(shipping == null){
				
				shipping =0;
			}
            
             var final_price = parseInt(price) + parseInt(price) * parseInt(gst_tax) / 100;
			 console.log(final_price);
            return final_price.toFixed(0);
        } 
    });
    
    if(jQuery('#vendor_price_variationval').length){
        var getvarvalues = jQuery('#vendor_price_variationval').val();
        var mavarr = getvarvalues.split(",");
        jQuery.each(mavarr,function(i,v){
            jQuery('#variations_vendor_price_'+i).val(v);
        }); 
    }
})