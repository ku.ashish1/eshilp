
function custome_buyer_registration_validation() {
    jQuery('.woocommerce-error').remove();
    var returnType  = [];
    returnType[0]  = checkBuyerPasswordConfirmPassword();
    returnType[1]  = checkBuyerPassword();
    returnType[2]   = checkBuyerPhone();
    returnType[3]   = checkBuyerEmailLength();
    returnType[4]   = checkBuyerLastName();
    returnType[5]   = checkBuyerFirstName();
    // console.log(jQuery.inArray(false, returnType));
    // return false;
    if (jQuery.inArray(false, returnType)!==-1) {
        return false;
    }
    return true;
}
jQuery(document).ready(function () {

    jQuery('#customerRegistration').submit(function(){
        var res=custome_buyer_registration_validation();
        return res;
    });


    jQuery('#reg_mobile').keypress(function (e) { 
        var keyCode = e.keyCode || e.which;
        var regex = /^[0-9]+$/;
        var isNum= regex.test(String.fromCharCode(keyCode));
        if(!isNum){
           return false;
        }
        if(jQuery(this).val().length>=10){
            return false;
        }
    });

    jQuery('#reg_mobile').keyup(function (e) {
        if(jQuery(this).val().length==1 && jQuery(this).val()==0) {
            jQuery(this).val('');
            return false;
        }
    });
    jQuery('#reg_mobile').on('change', function () {
        var value = parseInt(jQuery('#reg_mobile').val());
        jQuery('#reg_mobile').val(value ? value : '');
    });
    jQuery('#reg_mobile').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerPhone();
    });

    jQuery('#reg_email').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerEmailLength();
    });
        
    jQuery('#first_name').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerFirstName();
    });
    jQuery('#last_name').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerLastName();
    });
   

    jQuery('#reg_password').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerPassword();
    });

    jQuery('#reg_cnfpassword').on('blur', function () {
        jQuery('.woocommerce-error').remove();
        checkBuyerPasswordConfirmPassword();
    });

});

function checkBuyerPhone() {
    var phone = jQuery('#reg_mobile').val() ? jQuery('#reg_mobile').val() : '';
    if (phone=='' || !jQuery.isNumeric(phone) > 0 || phone.length > 10 || phone.length < 10) {
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Length of mobile number should be numeric and contain 10 digit</li></ul>');
        return false;
    }
    return true;
}

function checkBuyerEmailLength() {
    var user_email = jQuery('#reg_email').val() ? jQuery('#reg_email').val() : '';
    var regex = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!regex.test(user_email)){
        if (user_email == '' || jQuery.type(new String(user_email)) !== "string" || user_email.length > 50) {
            jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Enter a valide email sould be contain maximum length 50 (example@domainname)</li></ul>');
            return false;
        }
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Enter a valide email sould be contain maximum length 50 (example@domainname)</li></ul>');
        return false;
    }
    return true;
}

function checkBuyerFirstName() {
    var first_name = jQuery('#first_name').val() ? jQuery('#first_name').val() : '';
    if (first_name=='' || jQuery.type(new String(first_name)) !== "string" || first_name.length > 50) {
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> First name must be string & contain maximum length 50 char.</li></ul>');
        return false;
    }
    return true;
}

function checkBuyerLastName() {
    var last_name = jQuery('#last_name').val() ? jQuery('#last_name').val() : '';
    if (last_name=='' || jQuery.type(new String(last_name)) !== "string" || last_name.length > 50) {
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Last name must be string & contain maximum length 50 char.</li></ul>');
        return false;
    }
    return true;
}


function checkBuyerPassword() {
    var password = jQuery('#reg_password').val() ? jQuery('#reg_password').val() : '';
    if ( password=='' || !password.match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Password must contain at leaset eight alpha numaric and one special char and contain one special char</li></ul>');
        return false;
    }
    return true;
}

function checkBuyerPasswordConfirmPassword() {
    var password = jQuery('#reg_password').val() ? jQuery('#reg_password').val() : '';
    var confirm_pwd = jQuery('#reg_cnfpassword').val() ? jQuery('#reg_cnfpassword').val() : '';
    if(confirm_pwd==''){
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Confirm password must not be empty</li></ul>');
        return false;
    }
    if(password==''){
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Password must not be empty</li></ul>');
        return false;
    }
    if (confirm_pwd !== password) {
        jQuery('.woocommerce').append('<ul class="woocommerce-error"><li><strong>Error : </strong> Password & confirm password must be equel</li></ul>');
        return false;
    }
    return true;
}


