function manageVendorShippingTracking( item ) {
    var data = {
                          action  : 'wcfmu_shipment_tracking_html',
                          orderid       : item.data('orderid'),
                          productid     : item.data('productid'),
                          orderitemid   : item.data('orderitemid'),
                        }
    jQuery.ajax({
        type    :		'POST',
        url     : wcfm_params.ajax_url,
        data    : data,
        success :	function(response) {                                     
            // Intialize colorbox
            jQuery.colorbox( { html: response, height: 400, width: $popup_width,
                onComplete:function() {
                    jQuery('#cboxClose').click(function(e){
                        free_code(jQuery('#wcfm_tracking_id').val());
                    })
                    jQuery('#wcfm_tracking_button').click(function(e) {
                        e.preventDefault();
                        
                        jQuery('#wcfm_shipping_tracking_form').block({
                                message: null,
                                overlayCSS: {
                                    background: '#fff',
                                    opacity: 0.6
                                }
                            });
                        
                        jQuery( document.body ).trigger( 'wcfm_form_validate', jQuery('#wcfm_shipping_tracking_form') );
                        if( !$wcfm_is_valid_form ) {
                            wcfm_notification_sound.play();
                            jQuery('#wcfm_shipping_tracking_form').unblock();
                        } else {
                            if(jQuery('#wcfm_tracking_id').val()){
                                mark_code_used(jQuery('#wcfm_tracking_id').val(),item.data('orderid'),item.data('orderitemid'),item.data('productid'));
                                var tracking_url  ="http://";                    
                                var tracking_code = jQuery('#wcfm_tracking_code').val();
                                item.hide();
                            
                                jQuery('#wcfm_tracking_button').hide();
                                jQuery('#wcfm_shipping_tracking_form .wcfm-message').html('').removeClass('wcfm-error').removeClass('wcfm-success').slideUp();
                            
                                var data = {
                                    action        : item.data('shipped_action'),
                                    orderid       : item.data('orderid'),
                                    productid     : item.data('productid'),
                                    orderitemid   : item.data('orderitemid'),
                                    tracking_url  : tracking_url,
                                    tracking_code : tracking_code,
                                    tracking_data : jQuery('#wcfm_shipping_tracking_form').serialize()
                                }	
                                jQuery.ajax({
                                    type:		'POST',
                                    url: wcfm_params.ajax_url,
                                    data: data,
                                    success:	function(response) {
                                        wcfm_notification_sound.play();
                                        jQuery('#wcfm_shipping_tracking_form').unblock();
                                        jQuery('#wcfm_shipping_tracking_form').unblock();
                                        jQuery('#wcfm_shipping_tracking_form .wcfm-message').html( '<span class="wcicon-status-completed"></span>' + wcfm_shipping_tracking_labels.tracking_saved ).addClass('wcfm-success').slideDown();
                                        setTimeout(function() {
                                            jQuery.colorbox.remove();
                                            if( !window.location.hash ) {
                                                window.location = window.location.href + '#sm_order_shipment_options';
                                            }
                                            window.location.reload();
                                        }, 2000);
                                    }
                                });
                            }else{
                                console.log(item.data('shipped_action'));
                                jQuery.colorbox.remove();
                            }
                        }
                    });
                },
                overlayClose: false,
            });
        }
    });
}

function free_code(id){
    jQuery.ajax({
        url: wcfm_params.ajax_url,
        type: "post",
        async: false,
        data: {
            id:id,
            action: "free_tracking_code",
        },
        success: function (res) {
            console.log(res);
        },
        error: function(err){
            console.log("not found");
        }
    });
}
function mark_code_used(trackcode_id,order_id,item_id,product_id){
    console.log({trackcode_id,order_id,item_id,product_id});
    jQuery.ajax({
        url: wcfm_params.ajax_url,
        type: "post",
        async: false,
        data: {
            id:trackcode_id,
            order_id:order_id,
            item_id:item_id,
            product_id:product_id,
            action: "allot_tracking_code",
        },
        success: function (res) {
            console.log(res);
        },
        error: function(err){
            console.log("not found");
        }
    });
}
