jQuery(document).ready( function(){         
            jQuery('[name="wcfmvm_custom_infos[handicraft-handloom-id]"]').blur(function(e) { 
                    e.preventDefault();
        var handicraft_id = jQuery(this).val();    ; 
         if(handicraft_id == '')
         {
         var error_responce  = "";
             error_responce += "<div class=\"notice notice-warning is-dismissible\">";
             error_responce += "<p><strong>Please Enter Correct Order ID</strong></p>";
             error_responce += "<button type=\"button\" class=\"notice-dismiss\">";
             error_responce += "<span class=\"screen-reader-text\">Dismiss this notice.</span></button></div>";
		//alert('Enter Data');
	 //jQuery("#dic_order_response").html(error_responce);
           return false;
         }
         jQuery('#wcfm_membership_container').block({
			message: null,
			overlayCSS: {
				background: '#fff',
				opacity: 0.6
			}
		});
        jQuery.ajax({
            url : dic_custom_ajax_object.ajaxurl,
            type : "post",
            data : {
                action : "fetch_artisan_data",
                handicraft_id : handicraft_id
            },
            success : function( data ) {
                jQuery('#wcfm_membership_container').unblock();
                var response = JSON.parse(data);
                
                if(response.status)
                {
                     jQuery('[name="user_email"]').val(response.data.EmailID);
                     jQuery('[name="first_name"]').val(response.data.FirtsName);
                     jQuery('[name="last_name"]').val(response.data.LastName);
                      
                     jQuery('[name="wcfmvm_static_infos[phone]"]').val(response.data.MobileNo);
                     jQuery('[name="wcfmvm_static_infos[address][addr_1]"]').val(response.data.Street1);
                     jQuery('[name="wcfmvm_static_infos[address][addr_2]"]').val(response.data.Street2);
                     jQuery('[name="wcfmvm_static_infos[address][city]"]').val(response.data.City);
                     jQuery('[name="wcfmvm_static_infos[address][state]"]').val(response.data.State);
                     jQuery('[name="wcfmvm_static_infos[address][zip]"]').val(response.data.Zip);  
                     jQuery('[name="wcfmvm_static_infos[address][country]"]').val(response.data.Country);
                }
                else
                {
                    //alert('Data Does not Exist');
                }
              // alert(response);
              // jQuery("#dic_order_response").html(response);    
            }
        });
                    
    });     

	jQuery('.onsale').each(function(){
      		jQuery(this).text(jQuery(this).text().replace('-',''));
		//jQuery(this).css('display','block !important');
		jQuery(this).attr('style','display:block!important;padding-top: 16px;padding-left: 9px;');
	})
	//jQuery('.product-labels').css('display','block');
}); 
 