<?php

/*
 * Plugin Name:       DIC Custom Plugin
 * Plugin URI:        https://dic.gov.in/
 * Description:       Handle the additional function with this plugin.
 * Version:           1.0
 * Author:            DIC TEAM
 * Author URI:        https://dic.gov.in/index.php/about/team1/core-team
 * Text Domain:       dic-custom
 * Domain Path:       /languages
 */

add_action('admin_menu', 'add_menu_dashboard');
add_action('wp_ajax_dic_order_verify_method', 'dic_payment_verify_process');
add_action( 'wp_enqueue_scripts', 'dic_custom_enqueue_script' );
add_action('wp_ajax_fetch_artisan_data', 'get_detail_by_handicraftId');
add_action('wp_ajax_nopriv_fetch_artisan_data', 'get_detail_by_handicraftId');
add_action( 'login_head', 'dic_custom_login_logo' );
/* Ashish Agarwal */
add_action('wp_ajax_fetch_gst', 'get_gst');
add_action( 'wp_enqueue_scripts', 'gst_detail' );
 /** end */

 /* Ashish Agarwal */
add_action('wp_ajax_fetch_gst', 'get_gst');
function get_gst() {
    $id=$_POST['category_id'];
    $gst=get_term_meta($id, 'wh_gst', true);
    global $wpdb;
    $shipping = $wpdb->get_results( "SELECT gst_rate,shipping_type,shipping_charges FROM {$wpdb->prefix}shipping_charges where status=1", ARRAY_A  );
    $portal_charge = $wpdb->get_results( "SELECT charge_type,charges,gst_rate FROM {$wpdb->prefix}portal_charges where status=1 order by id desc" , ARRAY_A  );
    $payment_gatway = $wpdb->get_results( "SELECT charge,charge_type FROM {$wpdb->prefix}payment_gateway_chares where status=1", ARRAY_A  );
    // 0="percentage" , 1="flat"
    
    $results = array('status' => true, 'gst' => $gst , 'shipping'=>$shipping[0] , 'portal'=>$portal_charge[0],'payment_gateway'=>$payment_gatway[0] );
    echo json_encode($results);
    die;
}
 /** end */


function add_menu_dashboard() {
    add_menu_page('Verify Payment By Order Number', 'Verify Payment', 'administrator', 'dic-verify-payment', 'dic_payment_verify_form', 'dashicons-awards', 26);
}

function dic_payment_verify_form() {
    echo '<script>';


    echo 'jQuery(document).ready( function(){         
            jQuery("#order_submit_btn").click(function(e) { 
                    e.preventDefault();
        var dic_order_id = parseInt(jQuery("#dic_order_id").val()); 
         if(isNaN(dic_order_id))
         {
         var error_responce  = "";
             error_responce += "<div class=\"notice notice-warning is-dismissible\">";
             error_responce += "<p><strong>Please Enter Correct Order ID</strong></p>";
             error_responce += "<button type=\"button\" class=\"notice-dismiss\">";
             error_responce += "<span class=\"screen-reader-text\">Dismiss this notice.</span></button></div>";
		
	 jQuery("#dic_order_response").html(error_responce);
           return false;
         }
        jQuery.ajax({
            url : "admin-ajax.php",
            type : "post",
            data : {
                action : "dic_order_verify_method",
                dic_order_id : dic_order_id
            },
            success : function( response ) {
               
               jQuery("#dic_order_response").html(response);    
            }
        });
                    
    });     
});';
    echo '</script>';
    echo '<div class="wrap">';
    echo '<h1>Verify Order Payment Status</h1>';
    echo '<div id="dic_order_response"></div>';
    echo '<form action="" method="post"><table class="form-table">
                <tbody>
                <tr class="wcfm_row">
                <th scope="row">
                <label for="wc_frontend_manager_page_id">Enter Order Id :</label>
                </th>
                <td>
                <input name="dic_order_id" type="text" id="dic_order_id" value="" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="12">
                </td>
                </tr>
                </tbody>
                </table>';
    echo '<input class="button-primary" type="submit" name="order_submit_btn" id="order_submit_btn" value="Get Details">';

    echo '</form>';
    
    echo '</div>';
    
    
    
    
}

function dic_payment_verify_process() {
    $data = $_POST;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://payments.airpay.co.in/order/verify.php");
    curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS, "postvar1=value1&postvar2=value2&postvar3=value3");

// In real life you should use something like:
 curl_setopt($ch, CURLOPT_POSTFIELDS, 
          http_build_query(array('mercid' => '32977', 'merchant_txnId' => $data['dic_order_id'], 'privatekey' => 'ecc387f3c535e6b6f96a317c2843c7b5799ce169b61bfe8963a04845bc26b833')));
// Receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close($ch);
    print_r(simplexml_load_string($server_output));  
}



function dic_custom_enqueue_script() {
    
    wp_enqueue_script( 'dic_custom_script', plugins_url( 'js/custom.js', __FILE__ ), array( 'jquery' ) );
    wp_localize_script( 'dic_custom_script', 'dic_custom_ajax_object',
        array( 
            'ajaxurl' => admin_url( 'admin-ajax.php' )
             
        )
    );
}
function get_detail_by_handicraftId() {
    
    global $wpdb;
    $id = $_POST['handicraft_id'];
   // echo "SELECT * FROM {$wpdb->prefix}users  WHERE unique_id = '{$id}' ";
    $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}userinfo  WHERE HandicraftID = '{$id}' ", ARRAY_A  );
    
    $results = array('status' => true, 
                     'data' => $results[0]
                    );
    echo json_encode($results);
    die;
}

function dic_custom_login_logo() {
    $logo_url=get_post_field( 'guid', 1286 , 'raw' );
    $wp_logo_height=get_option('wp_logo_height');
    $wp_logo_width=get_option('wp_logo_width');
	if(empty($wp_logo_height))
	{
		$wp_logo_height='100px';
	}
	else
	{
		$wp_logo_height.='px';
	}
	if(empty($wp_logo_width))
	{
		$wp_logo_width='100%';
	}	
	else
	{
		$wp_logo_width.='px';
	}
	if(!empty($logo_url))
	{
		echo '<style type="text/css">'.
             'h1 a { 
				background-image:url('.$logo_url.') !important;
				height:'.$wp_logo_height.' !important;
				width:'.$wp_logo_width.' !important;
				background-size:100% !important;
				line-height:inherit !important;
				}'.
         '</style>';
	}
}
function dic_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'dic_login_logo_url' );
         
function dic_count_func( $atts ) {
           global $wpdb;
	$a = shortcode_atts( array(
		'type' => '',
		 
	), $atts );
        
        switch ($a['type']) {
            case "product":
              $count = count( get_posts( array('post_type' => 'product', 'post_status' => 'publish', 'fields' => 'ids', 'posts_per_page' => '-1') ) );
              $title = 'Products';
              break;
            case "artisan":
              $count = count($wpdb->get_results("SELECT * from $wpdb->usermeta WHERE meta_key = 'wcfmvm_custom_infos' AND meta_value like  '%\"Artisan\"%'"));
              $title = 'Artisan';
              break;
            case "weaver":
               $count = count($wpdb->get_results("SELECT * from $wpdb->usermeta WHERE meta_key = 'wcfmvm_custom_infos' AND meta_value like  '%\"Weaver\"%'"));
              $title = 'Weaver';
              break;
            case "order":
              $count = wc_orders_count( 'completed' );
              $title = 'Orders';
              break;
            case "customer":
              $count = 0;//count_users()['avail_roles']['customer'];
              $title = 'Buyer';
              break;
            default:
              echo 0;
          }     
	$responce  =   '<h1 class="dashnum">'.$count.'</h1>';
        $responce .=   '<h4 class="dashsub">Total No. of</h4>';
        $responce .=   '<h3 class="dashtitle">'.$title.'</h3>';
        
        return $responce;
}
add_shortcode( 'diccount', 'dic_count_func' ); 
            
            $hook_to = 'woocommerce_thankyou';
$what_to_hook = 'wl8OrderPlacedTriggerSomething';
$prioriy = 111;
$num_of_arg = 1;    
add_action($hook_to, $what_to_hook, $prioriy, $num_of_arg);

            
function wc_billing_field_strings( $translated_text, $text, $domain ) {
switch ( $translated_text ) {
case 'Billing details' :
$translated_text = __( 'Delivery Address', 'woocommerce' );
break;
}
return $translated_text;
}
//add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

add_filter( 'the_title', 'shorten_woo_product_title', 10, 2 );
function shorten_woo_product_title( $title, $id ) {
//echo (int)is_singular();
 //   echo get_the_ID();
    return $title;
if ( get_post_type( $id ) === 'product'  ) {
//return substr( $title, 0, 2 ); // change last number to the number of characters you want
} else {
//return $title;
}
}



/*  Start Of KYC Verification Module */
define('DIC_DIRPATH', plugin_dir_path( __FILE__ ));
include( DIC_DIRPATH . '/controller/class-kyc-controller.php' );
//include( DIC_DIRPATH . 'controller/kyc-functions.php' );

/*  End Of KYC Verification Module */

//add_filter('wcfm_menus', 'kyc_verify_menu', 2);
//add_filter('wcfm_query_vars', 'kyc_verify_query_vars', 20);
//add_filter('wcfm_endpoint_wcfm-rma_title', 'kyc_verify_endpoint_title');
//add_action('wcfm_load_views', 'kyc_verify_load_views');
      

      
/**
  * Sateesh custom plugin file
  */
  include('dic_custom_address.php');
  /**
  * Ashish Kumar file
  */
 // include 'gstForm.php';
?>