<?php
namespace PayP;
class Init{
    protected static $instance = null;

    final public static function instance() {
		if ( null === static::$instance ) {
			static::$instance = new static();
		}
		return static::$instance;
    }
    
    public function __construct() {
		// Hook in data stores.
		// add_filter( 'woocommerce_data_stores', array( __CLASS__, 'add_data_stores' ) );
		// REST API extensions init.
		add_action( 'rest_api_init', array( $this, 'paytm_checksume' ) );
    }
    
    function paytm_checksume() {
        register_rest_route('PayP','my-route', 'my-phrase', array(
                        'methods' => 'GET',
                        'callback' => 'null',
                        'args'     => array(),
                    )
                );
    }
    
    function custom_phrase() {
        return rest_ensure_response( 'Hello World! This is my first REST API' );
    }
}
?>