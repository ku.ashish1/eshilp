=== Delhivery Logistics Courier ===

Contributors: Rajlakshmi
Plugin Name: Delhivery Logistics Courier
Plugin URI: Plugins Web Page URL
Tags: delhivery logistics courier
Author URI: https://www.wpbeginner.com/
Author: WPBeginner
Donate link: https://www.wpbeginner.com/wpbeginner-needs-your-help/
Requires at least: 4.9
Tested up to: 5.5.1
Requires PHP: 5.6
Stable tag: 3.7.0

== Description ==
Delhivery is one of India's largest e-commerce logistics company that offers you access to ship to over 18000+ pin-codes.

The Delhivery Seller App becomes a natural extension for your Woocommerce storefront as it allows you to access all logistics & shipping related tasks within the same window.

Delhivery App offers the following features:

- Signup and Login - Existing Delhivery users (CL Panel Clients) can login to the plugin using their Cl Panel existing credentials and start shipping. New users can sign-up on the fly in 10 min and start shipping orders.
- Warehouse Management - Create new or integrate your existing pickup points for your business
- Order Management - Track all your Forward and Reverse shipments seamlessly
- Rate Calculator - Quickly check shipping tariff for your orders
- Pickup Request - Once your shipments are ready to be picked up, you can schedule a pickup request for the Delhivery executive to come and pick up the shipments from your warehouse or pickup point.
-Single and Bulk orders Shipping: Ship a single order at a time or ship bulk orders at one go seamlessly using this plugin


== Installation ==

### Manual Install From WordPress Dashboard


1. Login to your site's admin panel and navigate to Plugins -> Add New -> Upload.
2. Click choose file, select the plugin file and click install

== Frequently Asked Questions ==

= How do I manage my orders from the Delhivery Woo-commerce Plugin?

Go to My orders tab, you can simply edit, track or cancel orders.Print packaging slips option is also available on the same page.

= How do I search for my package status?

You can go to My Orders and select 'Track Order' from Actions tab.
The actions available on the 'Action' button will change. There would be the following "Four" fields visible (Edit, Cancel, Track and Shipping Label) in dropdown. On clicking on 'Cancel', the details of that order will no longer be visible.

= How will I know the estimated cost of my Package?

On the Rate Calculator Tab seller can enter the origin, destination pin-code, shipping mode, COD amount, weight (higher of actual or volumetric) and check the Delhivery shipping charges.

= How do I Create Warehouses?

Warehouses are the pickup points from where Delhivery would be picking up your shipments. Each seller can have one or more warehouses or pickup points.

From the menu--> Select Warehouse option, you can create new warehouses or view your existing warehouses.

Click on "Add New" button to create a new warehouse. Fields which are marked with "*" are mandatory and if you try to save without adding the mandatory fields there will be an error message shown.

= I have created an order. Will it be picked up automatically?

No,You will have to create a pickup request for Delhivery to assign an executive to pickup your shipments.You can do that through the app.

= Can I charge different shipping costs to different consignees?

Yes, you can enter shipping cost for your orders which is to be passed on to the consignee based on your business model.

== Screenshots ==

1. This is how plugin looks at back-end

== Upgrade Notice ==

nothing

== Changelog ==

nothing