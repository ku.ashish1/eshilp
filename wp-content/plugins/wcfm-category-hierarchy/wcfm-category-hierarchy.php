<?php
/**
 * Plugin Name: WCFM - Category Hierarchy
 * Plugin URI: https://wclovers.com/
 * Description: WCFM Product manager Category Selection by Hierarchy
 * Author: WC Lovers
 * Version: 1.0.4
 * Author URI: https://wclovers.com
 *
 * Text Domain: wcfm-category-hierarchy
 * Domain Path: /lang/
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 4.0.0
 *
 */

if(!defined('ABSPATH')) exit; // Exit if accessed directly

if(!defined('WCFM_TOKEN')) return;
if(!defined('WCFM_TEXT_DOMAIN')) return;

if ( ! class_exists( 'WCFMch_Dependencies' ) )
	require_once 'helpers/class-wcfm-category-hierarchy-dependencies.php';

if( !WCFMch_Dependencies::woocommerce_plugin_active_check() )
	return;

if( !WCFMch_Dependencies::wcfm_plugin_active_check() )
	return;

require_once 'helpers/wcfm-category-hierarchy-core-functions.php';
require_once 'wcfm-category-hierarchy-config.php';

if(!class_exists('WCFM_Category_Hierarchy')) {
	include_once( 'core/class-wcfm-category-hierarchy.php' );
	global $WCFM, $WCFMch, $WCFM_Query;
	$WCFMch = new WCFM_Category_Hierarchy( __FILE__ );
	$GLOBALS['WCFMch'] = $WCFMch;
}