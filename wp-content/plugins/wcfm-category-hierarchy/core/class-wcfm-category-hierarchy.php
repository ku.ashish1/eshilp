<?php

/**
 * WCFM Tuneer plugin core
 *
 * Plugin intiate
 *
 * @author 		WC Lovers
 * @package 	wcfm-category-hierarchy
 * @version   1.0.0
 */
 
class WCFM_Category_Hierarchy {
	
	public $plugin_base_name;
	public $plugin_url;
	public $plugin_path;
	public $version;
	public $token;
	public $text_domain;
	
	public function __construct($file) {

		$this->file = $file;
		$this->plugin_base_name = plugin_basename( $file );
		$this->plugin_url = trailingslashit(plugins_url('', $plugin = $file));
		$this->plugin_path = trailingslashit(dirname($file));
		$this->token = WCFMch_TOKEN;
		$this->text_domain = WCFMch_TEXT_DOMAIN;
		$this->version = WCFMch_VERSION;
		
		add_action( 'wcfm_init', array( &$this, 'init' ), 20 );
	}
	
	function init() {
		global $WCFM, $WCFMch;
		
		// Init Text Domain
		$this->load_plugin_textdomain();
		
		// Category Hoerarchy Scripts
		add_action( 'after_wcfm_load_scripts', array( &$this, 'wcbc_load_scripts' ), 40 );
		
		// Category Hoerarchy Styles
		add_action( 'after_wcfm_load_styles', array( &$this, 'wcbc_load_styles' ), 40 );
		
		// Category Hoerarchy
		add_action( 'before_wcfm_products_manage_taxonomies',array( &$this, 'wcfm_product_manage_category_hierarch' ), 50 );
		
    // Category Hoerarchy save
		add_action( 'after_wcfm_products_manage_meta_save', array( &$this, 'wcfm_product_manage_category_hierarch_save' ), 50, 2 );
		
		// Category Checlist View OFF
		add_filter( 'wcfm_is_category_checklist', array( &$this, 'wcfm_disable_category_checklist_view' ), 750 );
		
	}
	
	public function wcbc_load_scripts( $end_point ) {
	  global $WCFM, $WCFMu, $WCFMch;
    
	  wp_enqueue_script( 'wcfmch_category_hierarchy_manual_js', $this->plugin_url . 'js/wcfm-script-category-hierarchy.js', array('jquery'), $WCFMch->version, true );
	}
	
	public function wcbc_load_styles( $end_point ) {
	  global $WCFM, $WCFMu, $WCFMch;
    
	  //wp_enqueue_style( 'WCFMch_booking_customer_manual_css', $this->plugin_url . 'css/wcfm-script-category-hierarchy.css', array(), $WCFMch->version );
	}
	
	function wcfm_product_manage_category_hierarch( $product_id ) {
		global $WCFM, $WCFMu, $WCFMch;
		
		add_filter( 'wcfm_is_allow_product_category', '__return_false' );
		
		$selected_categories = array();
		if( $product_id ) {
			$pcategories = get_the_terms( $product_id, 'product_cat' );
			if( !empty($pcategories) ) {
				foreach($pcategories as $pkey => $pcategory) {
					$selected_categories[] = $pcategory->term_id;
				}
			}
		}
		
		$product_categories   = get_terms( 'product_cat', 'orderby=name&hide_empty=0&parent=0' );
		$p_category_options   = array( '' => __( 'Choose a category', 'wcfm-category-hierarchy' ) );
		$default_category_id  = absint( get_option( 'default_product_cat', 0 ) );
		
		foreach ( $product_categories as $cat ) {
			if( apply_filters( 'wcfm_is_allow_hide_uncatorized', false, $cat->term_id ) && ( ( $cat->term_id == $default_category_id ) || ( in_array( $cat->slug, array( 'uncategorized', 'uncategorised' ) ) ) ) ) continue;
			//$p_category_options[$cat->term_id] = $cat->name;
		}
		
		$ptax_custom_arrtibutes = array( 'taxonomy' => 'product_cat', 'parent' => '0', 'level' => 0, 'selected' => json_encode( $selected_categories ) );
		
		$wcfm_custom_validation_options = get_option( 'wcfm_custom_validation_options', array() );
		if( isset( $wcfm_custom_validation_options['product_cat'] ) ) {
			$ptax_custom_arrtibutes['required'] = '1';
			$ptax_custom_arrtibutes ['required_message'] = __( 'Categories', 'wc-frontend-manager' ) . ': ' . __( 'This field is required.', 'wc-frontend-manager' );
		}
		
		$WCFM->wcfm_fields->wcfm_generate_form_field( array(
																											"wcfm_cat_level_0" => array( 'label' => __( 'Categories', 'wc-frontend-manager' ), 'type' => 'select', 'name' => 'wcfm_cat_level[0][]', 'label_class' => 'wcfm_title wcfm_cat_level_label_0', 'class' => 'wcfm-select wcfm_category_hierarchy wcfm_cat_level_0', 'options' => $p_category_options, 'value' => $selected_categories, 'custom_attributes' => $ptax_custom_arrtibutes )
																											 ) );
		
		if( !empty( $p_category_options ) ) $this->showCategoryHierarchySelector( );
		
	}
	
	
	public function showCategoryHierarchySelector( $level = 1 ) {
		global $WCFM, $WCFMch;
		
		$ptax_custom_arrtibutes = apply_filters( 'wcfm_taxonomy_custom_attributes', array(), 'product_cat' );
		?>
		<p class="wcfm_title wcfm_custom_hide wcfm_cat_level_label_<?php echo $level; ?>">
		  <strong>
		    <?php 
		    _e( 'Sub-', 'wcfm-category-hierarchy' ); 
		    echo apply_filters( 'wcfm_taxonomy_custom_label', __( 'Categories', 'wc-frontend-manager' ), 'product_cat' );
		    ?>
		  </strong>
		</p>
		<label class="screen-reader-text" for="product_cats">
		  <?php 
		  echo __( 'Sub-', 'wcfm-category-hierarchy' );
		  echo apply_filters( 'wcfm_taxonomy_custom_label', __( 'Categories', 'wc-frontend-manager' ), 'product_cat' ); 
		  ?>
		</label>
	  <select id="wcfm_cat_level_<?php echo $level; ?>" name="wcfm_cat_level[<?php echo $level; ?>][]" class="wcfm-select wcfm_custom_hide wcfm_category_hierarchy wcfm_cat_level_<?php echo $level; ?>" data-level="<?php echo $level; ?>" data-taxonomy="product_cat" data-catlimit="1" <?php echo implode( ' ', $ptax_custom_arrtibutes ); ?>>
	  </select>
	  <?php
	  $level++;
	  if( $level < 8 ) $this->showCategoryHierarchySelector( $level );
	}
	
	
	public function wcfm_product_manage_category_hierarch_save( $new_product_id, $wcfm_products_manage_form_data ) {
  	global $WCFM, $WCFMch, $WCFMmp;
  	
  	$vendor_id = 0;
		if( wcfm_is_vendor( $WCFMmp->vendor_id ) ) {
			$vendor_id = $WCFMmp->vendor_id;
		} elseif( isset( $wcfm_products_manage_form_data['wcfm_associate_vendor'] ) ) {
			$vendor_id = absint( $wcfm_products_manage_form_data['wcfm_associate_vendor'] );
		}
		
		if( $vendor_id ) {
			$WCFMmp->wcfmmp_vendor->wcfmmp_reset_vendor_taxonomy( $vendor_id, $new_product_id );
		}
		
  	$is_first = true;
  	if(isset($wcfm_products_manage_form_data['wcfm_cat_level']) && !empty($wcfm_products_manage_form_data['wcfm_cat_level'])) {
			foreach($wcfm_products_manage_form_data['wcfm_cat_level'] as $level => $taxonomy_values ) {
				if( !empty( $taxonomy_values ) ) {
					foreach( $taxonomy_values as $taxonomy_value ) {
						if($is_first) {
							$is_first = false;
							wp_set_object_terms( $new_product_id, (int)$taxonomy_value, 'product_cat' );
						} else {
							wp_set_object_terms( $new_product_id, (int)$taxonomy_value, 'product_cat', true );
						}
						
						if( $vendor_id ) {
							$WCFMmp->wcfmmp_vendor->wcfmmp_save_vendor_taxonomy( $vendor_id, $new_product_id, $taxonomy_value );
						}
					}
				}
			}
		}
	}
	
	function wcfm_disable_category_checklist_view( $is_checklist ) {
		return false;
	}
	
	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present
	 *
	 * @access public
	 * @return void
	 */
	public function load_plugin_textdomain() {
		$locale = function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
		$locale = apply_filters( 'plugin_locale', $locale, 'wcfm-category-hierarchy' );
		
		//load_plugin_textdomain( 'wcfm-tuneer-orders' );
		//load_textdomain( 'wcfm-category-hierarchy', WP_LANG_DIR . "/wcfm-category-hierarchy/wcfm-category-hierarchy-$locale.mo");
		load_textdomain( 'wcfm-category-hierarchy', $this->plugin_path . "lang/wcfm-category-hierarchy-$locale.mo");
		load_textdomain( 'wcfm-category-hierarchy', ABSPATH . "wp-content/languages/plugins/wcfm-category-hierarchy-$locale.mo");
	}
	
	public function load_class($class_name = '') {
		if ('' != $class_name && '' != $this->token) {
			require_once ('class-' . esc_attr($this->token) . '-' . esc_attr($class_name) . '.php');
		} // End If Statement
	}
}