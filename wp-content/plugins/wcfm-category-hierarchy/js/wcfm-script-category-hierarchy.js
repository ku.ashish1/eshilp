jQuery(document).ready(function($) {
	if( $('#wcfm_cat_level_0').length > 0 ) { 
		$selected_cats = $('#wcfm_cat_level_0').data('selected');
		
		var data = {
			action       : 'wcfm_json_search_taxonomies',
			taxonomy     : $('#wcfm_cat_level_0').data( 'taxonomy' ),
			parent       : -1,
			term         : ''
		}	
		jQuery.ajax({
			type:		'POST',
			url: wcfm_params.ajax_url,
			data: data,
			success:	function(response) {
				if(response) {
					$next_html = '';
					$.each( response, function( $id, $val ) {
						$next_html += '<option value="'+$id+'"';
						if( $.inArray( parseInt($id), $selected_cats ) !== -1 ) {
							$next_html += ' selected';
						}
						$next_html += ' class="wcfm_cat_option_'+ $id +'">'+$val+'</option>';
					});
					if( $next_html && $next_html.length > 0 ) {
						$next_html = '<option value="">'+wcfm_dashboard_messages.search_taxonomy_select2+'</option>' + $next_html;
						$('#wcfm_cat_level_0').html($next_html);
		
						$('.wcfm_category_hierarchy').each(function() {
							$(this).change(function() {
								$level = parseInt($(this).data('level'));
								$value = $(this).val();
								if( $('.wcfm_cat_level_'+($level+1)).length > 0 ) {
									$('.wcfm_cat_level_'+($level+1)).addClass('wcfm_custom_hide');
									$('.wcfm_cat_level_label_'+($level+1)).addClass('wcfm_custom_hide');
									if($('.wcfm_cat_level_'+($level+1)).data('select2')) $('.wcfm_cat_level_'+($level+1)).select2('destroy');//.change();
									if( $value ) {
										$next_level = ($level+1);
										$next_html = '';
										var data = {
											action       : 'wcfm_json_search_taxonomies',
											taxonomy     : $(this).data( 'taxonomy' ),
											parent       : $value,
											term         : ''
										}	
										jQuery.ajax({
											type:		'POST',
											url: wcfm_params.ajax_url,
											data: data,
											success:	function(response) {
												if(response) {
													$.each( response, function( $id, $val ) {
														$next_html += '<option value="'+$id+'"';
														if( $.inArray( parseInt($id), $selected_cats ) !== -1 ) {
															$next_html += ' selected';
														}
														$next_html += '>'+$val+'</option>';
													});
													if( $next_html && $next_html.length > 0 ) {
														$next_html = '<option value="">'+wcfm_dashboard_messages.search_taxonomy_select2+'</option>' + $next_html;
														$('.wcfm_cat_level_'+$next_level).html($next_html).removeClass('wcfm_custom_hide');
														$('.wcfm_cat_level_label_'+$next_level).removeClass('wcfm_custom_hide');
														$('.wcfm_cat_level_'+$next_level).select2().change();
													}
												}
											}
										});
									}
								}
							}).change();
						});
						
						$('#wcfm_cat_level_0').select2();
						filterProductTypeWiseCategories();
					}
				}
			}
		});
		
		// Product Type Wise Categories Support
		$( document.body ).on( 'wcfm_product_type_changed', function( event ) {
			filterProductTypeWiseCategories();
		});
		
		function filterProductTypeWiseCategories() {
			$has_cat = false;
			var product_type = $('#product_type').val();
			$('#wcfm_cat_level_0').find('option').attr( 'disabled', true ).css( 'display', 'none' );
			$.each( wcfm_product_type_categories, function( product_type_cat, allowed_categories ) {
				if( product_type == product_type_cat ) {
					$.each( allowed_categories, function( index, allowed_category ) {
						$('#wcfm_cat_level_0').find('.wcfm_cat_option_'+allowed_category).attr( 'disabled', false ).css( 'display', 'block' );
						$has_cat = true;
					} );	
				}
			} );
			if( !$has_cat ) {
				$('#wcfm_cat_level_0').find('option').attr( 'disabled', false ).css( 'display', 'block' );
			} else {
				$selected_cat  = $("#wcfm_cat_level_0").val();
				//$selected_attr = $("#wcfm_cat_level_0").find('option[value="'+$selected_cat+'"]').prop( 'disabled' );
				//console.log( $selected_cat +"::" + $selected_attr );
				if( $selected_cat === null ) {
					$("#wcfm_cat_level_0").val('');
					$("#wcfm_cat_level_0").change();
				}
			}
			$('#wcfm_cat_level_0').select2('destroy');
			$("#wcfm_cat_level_0").select2();
		}
	}
});