#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: WCFM Category Hierarchy\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-11-03 10:00+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: core/class-wcfm-category-hierarchy.php:101
#: core/class-wcfm-category-hierarchy.php:108
msgid "Sub-"
msgstr ""

#: core/class-wcfm-category-hierarchy.php:113
msgid "-- Select Category --"
msgstr ""

#. Name of the plugin
msgid "WCFM Category Hierarchy"
msgstr ""

#. Description of the plugin
msgid "WCFM Product manager Category Selection by Hierarchy"
msgstr ""

#. URI of the plugin
msgid "https://wclovers.com/product/woocommerce-multivendor-membership"
msgstr ""

#. Author of the plugin
msgid "WC Lovers"
msgstr ""

#. Author URI of the plugin
msgid "https://wclovers.com"
msgstr ""
