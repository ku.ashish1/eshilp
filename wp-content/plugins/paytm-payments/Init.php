<?php
/**
 * Plugin Name: Ashish
 * Plugin URI: https://github.com/Paytm-Payments/
 * Description: This plugin allow you to accept payments using Paytm. This plugin will add a Paytm Payment option on WooCommerce checkout page, when user choses Paytm as Payment Method, he will redirected to Paytm website to complete his transaction and on completion his payment, paytm will send that user back to your website along with transactions details. This plugin uses server-to-server verification to add additional security layer for validating transactions. Admin can also see payment status for orders by navigating to WooCommerce > Orders from menu in admin.
 * Version: 2.0
 * Author: Paytm
 * Author URI: https://developer.paytm.com/
 * Tags: Paytm, Paytm Payments, PayWithPaytm, Paytm WooCommerce, Paytm Plugin, Paytm Payment Gateway
 * Requires at least: 4.0.1
 * Tested up to: 5.0
 * Requires PHP: 5.6
 * Text Domain: Paytm Payments
 * WC requires at least: 2.0.0
 * WC tested up to: 4.0
 */
    add_action( 'rest_api_init','paytm_checksume' );
    
    function paytm_checksume() {
        register_rest_route('wpptm/v1','/create-checksum/(?P<order_id>\d+)', array(
                        'methods' => 'GET',
                        'callback' => 'test',
                        'args'     => array(
                            "order_id"=>"",
                        ),
                    )
                );
    }
    function test($data)
    {
        return $data['order_id'];
        return get_posts( array(
            'author' => $data['id'],
        ) );
    }
?>