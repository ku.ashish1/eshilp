=== Return Refund and Exchange For WooCommerce ===
Contributors: makewebbetter
Donate link:  https://makewebbetter.com/
Tags: return,refund, woocommerce,exchange,RMA
Requires at least: 4.0
Tested up to: 5.4.2
WC requires at least: 3.0.0
WC tested up to: 4.3.0
Stable tag: 2.1.2
Requires PHP: 5.6
License: GPLv3 or later 
License URI: http://www.gnu.org/licenses/gpl-3.0.html


This plugin allows customers to submit product refund request or exchange request on your store.

== Description ==

***Looking for dedicated Refund System for your shopping site?***

*Make a smart move with Premium Version of [Refund and Exchange](https://makewebbetter.com/product/woocommerce-rma-return-refund-exchange/?utm_source=MWB-RMA-org&utm_medium=MWB-ORG-Page&utm_campaign=MWB-RMA1)*

**Refund and Exchange plugin for WooCommerce** gives your customers an easy and simple Refund Management system stuffed with organized Exchange, Wallet, Cancel features.

Saves time on complaints, disputes on call and the hurdles of managing stock, with all in one **Refund and Exchange plugin**.

Your customers will be able to request full or partial refunds from their accounts with a justification statement.

The whole process goes under a dedicated and streamlined mailing system which would keep both the parties on the same note.

Ultimately leads to easy returns for your customers with increased conversions.


== What makes this plugin the best Return Refund and Exchange plugin? ==

* Dedicated refund system.
* Return an order after delivery.
* Efficient tax handling.
* Allow customers to Add a reason for a refund.
* Admin can set predefined refund reason.
* User can send an attachment with a refund request.
* Admin can manage stock on each refund.
* Admin can enable refund requests for selected order statuses.
* Dedicated Mailing system.
* Shortcodes for Email content.
* Customers, Admin can communicate through messages.

== Plugin meets your requirement, So ==

&nbsp;&nbsp;&nbsp;&nbsp;[**Download Now**](https://downloads.wordpress.org/plugin/woo-refund-and-exchange-lite.zip)

&nbsp;&nbsp;&nbsp;&nbsp;Visit [**Return Refund and Exchange For WooCommerce Documentation**](http://docs.makewebbetter.com/woocommerce-refund-and-exchange-lite/?utm_source=MWB-RMA-org&utm_medium=MWB-ORG-Page&utm_campaign=MWB-doc) and take help to configure it.

==What Premium Version Offers ==

Try something out of the box and allow your customers to make a smart move with our **RMA For Return Refund & Exchange**
> Note:// Take advantage of the exclusive features for your [**RMA For Return Refund & Exchange with the Premium Version**](https://makewebbetter.com/product/woocommerce-rma-return-refund-exchange/?utm_source=MWB-RMA-org&utm_medium=MWB-ORG-Page&utm_campaign=MWB-premium_plugin)

* Refund policy feature.
* Order Product Selection feature for Refund/Exchange.
* Exchange feature.
* Cancel feature.
* Wallet feature.
* Return slip label feature. 
* Full RMA support.
* Partial refund feature.
* Auto accept refund request feature. 
* Refund exchange feature for guest users.
* Minimum order amount feature.
* Product category disable feature.
* Integration for return ship label with ShipStation.
* Compatibility with Ultimate WooCommerce Gift Cards.
* Shortcode of product table and the refundable amount for Refund Mails. 

== Premium Plugin Live Demo == 

Explore complete feature of the premium plugin, by looking at the live demo.

* [**RMA For Return Refund & Exchange Frontend demo**](https://demo.makewebbetter.com/woocommerce-rma-for-return-refund-and-exchange/my-account/orders/?visitor=true&utm_source=MWB-RMA-org&utm_medium=MWB-ORG-Page&utm_campaign=MWB-frontend_demo)
* [**RMA For Return Refund & Exchange Backend demo**](https://demo.makewebbetter.com/woocommerce-rma-for-return-refund-and-exchange/get-your-personal-demo/?utm_source=MWB-RMA-org&utm_medium=MWB-ORG-Page&utm_campaign=MWB-backend_demo)

== Don’t hesitate to reach us! ==

If you need support or have any question then kindly use our online chat window [here](https://makewebbetter.com/?utm_source=MWB-RMA-org&utm_medium=MWB-org-page&utm_campaign=MWB-RMA-org) or  connect with us then [**Generate a Ticket**](https://makewebbetter.com/submit-query/)

== Follow Us ==

* [**Our Official Website**](https://makewebbetter.com/?utm_source=MWB-RMA-org&utm_medium=MWB-org-page&utm_campaign=MWB-RMA-org) 
* [**Follow us on Facebook Page**](https://www.facebook.com/makewebbetter)
* [**Tweet us on @MakeWebBetter**](https://twitter.com/makewebbetter)
* [**Visit our LinkedIn Account**](https://www.linkedin.com/company/makewebbetter)
* [**Subscribe To Our YouTube Channel**](https://www.youtube.com/channel/UC7nYNf0JETOwW3GOD_EW2Ag)
* [**Follow Our SlideShare**](https://www.slideshare.net/MakeWebBetter)

== Installation ==

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of Return Refund and Exchange For WooCommerce, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Return Refund and Exchange For WooCommerce" and click Search Plugins. Once you've found our plugin you can view details about it such as the point release, rating and description. Most importantly of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading our Return Refund and Exchange For WooCommerce and uploading it to your webserver via your favorite FTP application. The WordPress contains [instructions on how to do this here](https://wordpress.org/support/article/managing-plugins/#manual-plugin-installation).

= Updating =

Automatic updates should work like a charm; as always though, ensure you backup your site just in case.


== Frequently Asked Questions ==

= How to setup the Refund Products? =

Please reach the woocommerce menu -> RAE Configuration setting -> Click on the Refund Product -> Enable Refund Request setting.

= How to place Refund button in Thank you and My-Account page? =

You can easily go to the Woocommerce -> Settings -> RAEsetting ->Refund Products and set the following points.

1)Enable Refund Request.
2)Set a maximum number of days.
3)Select the order status in which the order can be refunded.

= How to set the order status for the orders which need to be refunded? =

It's a very easy way, go to the Woocommerce -> Settings ->RAE setting ->Refund  Products and Select the order status in which the order can be Refund.

= How to use a mail configuration setting? =

1) RAE Configuration Setting -> Mail configuration -> Basic setting -> Set header.  
2) RAE Configuration Setting -> Mail configuration ->Refund ->and then fill  all details.


== Changelog ==

= 2.1.2 – RELEASED ON 21 JULY 2020 =

* Important: Your translation might be lost please take backup of your language translations before update.
* New: Support for WooCommerce 4.3.0
* New: Support for WordPress 5.4.2

= 2.1.1 – RELEASED ON 15 JULY 2020 =

* New: Support for WooCommerce 4.3.0
* New: Support for WordPress 5.4.2

= 2.1.0 – RELEASED ON 18 APRIL 2020 =
* New: Support for WooCommerce 4.0.1
* New: Support for WordPress 5.4
* New: Customers, Admin communication through messages.

= 2.0.0 – RELEASED ON 24 JANUARY 2020 =
* New: Compatibility with Latest WC and WP.
* Updated in plugin core and updated language
* Fix:Image save issue in Mail Configuration setting.
* Fix: Expiry date issue with gift card  for refund process.

= 1.0.0 =
* First version. 

== Screenshots ==

1. Refund setting.
2. Order detail and click the refund button.
3. submit the request.
4. Refund and manage the account by a merchant.
5. Customers can send message to admin.
6. Admin can reply to customer messages.
7. Accept and cancel the refund request by a merchant.
8. Manage the refunded product Stock.
9. Send the mail by admin to the customer.

== Upgrade Notice ==

= 2.1.2 – RELEASED ON 21 JULY 2020 =

* Important: Your translation might be lost please take backup of your language translations before update.
* New: Support for WooCommerce 4.3.0
* New: Support for WordPress 5.4.2

	
