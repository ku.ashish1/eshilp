<?php
/**
 * Plugin Name:  WooCommerce RMA For WCFM
 * Plugin URI: https://makewebbetter.com
 * Description: WooCommerce RMA for WCFM is a unique solution for WCFM Multivendor Marketplace. Using this plugin, vendors' can manage refund & exchange requests raised by their customers for the products sold.
 * Version: 1.0.0
 * Author: MakeWebBetter <webmaster@makewebbetter.com>
 * Author URI: https://makewebbetter.com
 * Requires at least: 3.5
 * Tested up to: 5.3.0
 * WC tested up to:  3.9.0
 * Text Domain: woocommerce-rma-for-wcfm
 * Domain Path: /languages
 */

/**
 * Exit if accessed directly
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$mwb_wrffm_lite_activated = false;
$activated = true;
if (function_exists('is_multisite') && is_multisite())
{
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( !is_plugin_active( 'woocommerce/woocommerce.php' ) )
	{
		$activated = false;
	}
	if ( !is_plugin_active( 'wc-frontend-manager/wc_frontend_manager.php' ) )
	{
		$activated = false;
	}
	
}
else
{
	if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
	{
		$activated = false;
	}
	if (!in_array('wc-frontend-manager/wc_frontend_manager.php', apply_filters('active_plugins', get_option('active_plugins'))))
	{
		$activated = false;
	}
}

/**
 * Check if WooCommerce is active
 **/
if ($activated) 
{
	define('MWB_WRFFM_DIRPATH', plugin_dir_path( __FILE__ ));
	define('MWB_WRFFM_URL', plugin_dir_url( __FILE__ ));
	define('MWB_WRFFM_VERSION', '1.0.0' );
	define('MWB_WRFFM_SECRET_KEY', '59f32ad2f20102.74284991');
	define('MWB_WRFFM_SERVER_URL','https://makewebbetter.com/');
	define('MWB_WRFFM_ITEM_REFRENCE','WooCommerce RMA For WCFM');


	include_once MWB_WRFFM_DIRPATH.'includes/mwb-wrffm-class.php';
	include_once MWB_WRFFM_DIRPATH.'admin/class-order-meta.php';
	include_once MWB_WRFFM_DIRPATH.'admin/class-admin-setting.php';
	include_once MWB_WRFFM_DIRPATH.'front/class-order-return.php';
	include_once MWB_WRFFM_DIRPATH.'front/class-order-exchange.php';
	include_once MWB_WRFFM_DIRPATH.'gateway/wallet-gateway.php';
	include_once MWB_WRFFM_DIRPATH.'vendor/class-vendor-functions.php';
	
	
	/**
	 * This function is used for formatting the price
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 * @param unknown $price
	 * @return string
	 */
	function mwb_wrffm_format_price($price)
	{
		$price = apply_filters( 'formatted_woocommerce_price', number_format( $price, wc_get_price_decimals(), wc_get_price_decimal_separator(), wc_get_price_thousand_separator() ), $price, wc_get_price_decimals(), wc_get_price_decimal_separator(), wc_get_price_thousand_separator() );
		$currency_symbol = get_woocommerce_currency_symbol();
		$currency_pos = get_option( 'woocommerce_currency_pos' );
		switch ( $currency_pos ) {
			case 'left' :
				$uprice = $currency_symbol.'<span class="mwb_wrffm_formatted_price">'.$price.'</span>';
				break;
			case 'right' :
				$uprice = '<span class="mwb_wrffm_formatted_price">'.$price.'</span>'.$currency_symbol;
				break;
			case 'left_space' :
				$uprice = $currency_symbol.'&nbsp;<span class="mwb_wrffm_formatted_price">'.$price.'</span>';
				break;
			case 'right_space' :
				$uprice = '<span class="mwb_wrffm_formatted_price">'.$price.'</span>&nbsp;'.$currency_symbol;
				break;
		}
		return $uprice;
	}

	/**
	 * This function is used for formatting the price seprator
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 * @param unknown $price
	 * @return price
	 */
	function mwb_wrffm_currency_seprator($price)
	{
		$price = apply_filters( 'formatted_woocommerce_price', number_format( $price, wc_get_price_decimals(), wc_get_price_decimal_separator(), wc_get_price_thousand_separator() ), $price, wc_get_price_decimals(), wc_get_price_decimal_separator(), wc_get_price_thousand_separator() );
		return $price;
	}
	
	
	/**
	 * This function is to add pages for return and exchange request form
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	
	function mwb_wrffm_add_pages()
	{
		$email = get_option('admin_email', false);
		$admin = get_user_by('email', $email);
		$admin_id = $admin->ID;
		 
		$mwb_wrffm_return_request_form = array(
				'post_author'    => $admin_id,
				'post_name'      => 'return-request-form',
				'post_title'     => 'Return Request Form',
				'post_type'      => 'page',
				'post_status'    => 'publish',
					
		);
			
		$page_id = wp_insert_post($mwb_wrffm_return_request_form);
			
		if($page_id) {
			$mwb_wrffm_pages['pages']['mwb_return_from']=$page_id;
		}
			
		$mwb_exchange_request_form = array(
				'post_author'    => $admin_id,
				'post_name'      => 'exchange-request-form',
				'post_title'     => 'Exchange Request Form',
				'post_type'      => 'page',
				'post_status'    => 'publish',
	
		);
	
		$page_id = wp_insert_post($mwb_exchange_request_form);
	
		if($page_id) {
			$mwb_wrffm_pages['pages']['mwb_exchange_from']=$page_id;
		}
		
		$mwb_return_exchange_request_form = array(
				'post_author'    => $admin_id,
				'post_name'      => 'request-form',
				'post_title'     => 'Return/Exchange Request Form',
				'post_type'      => 'page',
				'post_status'    => 'publish',
		
		);
		

		$page_id = wp_insert_post($mwb_return_exchange_request_form);
		
		if($page_id) {
			$mwb_wrffm_pages['pages']['mwb_request_from']=$page_id;
		}

		$mwb_cancel_product_request_form = array(
				'post_author'    => $admin_id,
				'post_name'      => 'product-cancel-request-form',
				'post_title'     => 'Product Cancel Request Form',
				'post_type'      => 'page',
				'post_status'    => 'publish',
		
		);
	
		$page_id = wp_insert_post($mwb_cancel_product_request_form);
		
		if($page_id) {
			$mwb_wrffm_pages['pages']['mwb_cancel_request_from']=$page_id;
		}
		
		update_option('mwb_wrffm_pages', $mwb_wrffm_pages);
	}
	register_activation_hook( __FILE__, 'mwb_wrffm_add_pages');
	
	/**
	 * This function is to remove pages for return and exchange request form
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	function mwb_wrffm_remove_pages()
	{
		$mwb_wrffm_pages =  get_option('mwb_wrffm_pages');
		$page_id = $mwb_wrffm_pages['pages']['mwb_exchange_from'];
		wp_delete_post($page_id);
		$page_id = $mwb_wrffm_pages['pages']['mwb_return_from'];
		wp_delete_post($page_id);
		$page_id = $mwb_wrffm_pages['pages']['mwb_request_from'];
		wp_delete_post($page_id);
		$page_id = $mwb_wrffm_pages['pages']['mwb_cancel_request_from'];
		wp_delete_post($page_id);
		delete_option('mwb_wrffm_pages');
	}
	register_deactivation_hook(__FILE__, 'mwb_wrffm_remove_pages');

	add_filter('body_class', 'mwb_wrffm_custom_template_body_class');
	function mwb_wrffm_custom_template_body_class( $classes ) {
		$mwb_wrffm_pages =  get_option('mwb_wrffm_pages');
		$page_id1= $mwb_wrffm_pages['pages']['mwb_exchange_from'];
		$page_id2 = $mwb_wrffm_pages['pages']['mwb_return_from'];
		$mwb_wrffm_show_sidebar_on_form = get_option('mwb_wrffm_show_sidebar_on_form','no');	

	    if ( is_page( $page_id1 ) || is_page( $page_id2 ) ){
	    	if( $mwb_wrffm_show_sidebar_on_form != 'yes' ) {
	        	$classes[] = 'mwb_wrffm_hide_form_sidebar';
	    	}
	    }
	    return $classes;
	}
	
	/**
	 * This function is used to load language'.
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	
	function mwb_wrffm_load_plugin_textdomain()
	{
		$domain = "woocommerce-rma-for-wcfm";
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );
		load_textdomain( $domain, MWB_WRFFM_DIRPATH .'languages/'.$domain.'-' . $locale . '.mo' );
		$var=load_plugin_textdomain( $domain, false, plugin_basename( dirname(__FILE__) ) . '/languages' );
		
	}
	add_action('plugins_loaded', 'mwb_wrffm_load_plugin_textdomain');
	


	/**
	 * This function checks session is set or not
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	function mwb_wrffm_set_session()
	{
		if(isset($_POST['mwb_wrffm_order_id_submit']))
		{
			$order_id = $_POST['order_id'];
			$billing_email = get_post_meta($order_id, '_billing_email', true);
			$req_email = $_POST['order_email'];
			if($req_email == $billing_email)
			{
				WC()->session->set( 'mwb_wrffm_email' , $billing_email );
				$order = new WC_Order($order_id);
				$url = $order->get_checkout_order_received_url();
				wp_redirect($url);
				die;
			}
			else
			{
				WC()->session->set( 'mwb_wrffm_notification' , __('OrderId or Email is Invalid', 'woocommerce-rma-for-wcfm') );
			}
		}
	}
	add_action('init', 'mwb_wrffm_set_session');

	/**
	 * This function set a woocommerce customer session for guest user
	 *
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	add_action( 'woocommerce_init', function() {
		if (is_user_logged_in() || is_admin())
        	return;

	    if ( isset( WC()->session ) ) {
	        if ( !WC()->session->has_session() ) {
	            WC()->session->set_customer_session_cookie( true );
	       	}
	    }
	} );
	
	/**
	 * Add settings link on plugin page
	 * @name admin_settings_for_pmr()
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	
	function mwb_wrffm_admin_settings($actions, $plugin_file) {
		static $plugin;
		if (! isset ( $plugin )) {
	
			$plugin = plugin_basename ( __FILE__ );
		}
		if ($plugin == $plugin_file) {
			$settings = array (
					'settings' => '<a href="' . home_url ( '/wp-admin/admin.php?page=wc-settings&tab=mwb_wrffm_setting' ) . '">' . __ ( 'Settings', 'woocommerce-rma-for-wcfm' ) . '</a>',
			);
			$actions = array_merge ( $settings, $actions );
		}
		return $actions;
	}
	
	//add link for settings
	add_filter ( 'plugin_action_links','mwb_wrffm_admin_settings', 10, 5 );
	
	
	/**
	 * Dynamically Generate Coupon Code
	 *
	 * @name mwb_wrffm_coupon_generator
	 * @param number $length
	 * @return string
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	function mwb_wrffm_coupon_generator($length = 10)
	{
		$password = '';
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$final_array = array_merge($alphabets,$numbers);
		while($length--)
		{
		$key = array_rand($final_array);
		$password .= $final_array[$key];
		}
	
		$wrffm_prefix = get_option("mwb_wrffm_return_coupon_prefeix", '');
		$password = $wrffm_prefix.$password;
		return $password;
	}
	
	function mwb_wrffm_wallet_feature_enable()
	{

		$enabled = false;
		$wallet_enabled = get_option('mwb_wrffm_return_wallet_enable', "no");
		if($wallet_enabled == "yes")
		{
			$enabled = true;
		}	
		return $enabled;
	}

	function mwb_wrffm_get_vendor_product()
	{
		$user_id = get_current_user_id();
		$vendor_products = get_posts( 
			array(
				'numberposts' => -1,
				'author'      =>  $user_id,
				'orderby'     => 'post_date',
				'post_type'   => array( 'product', 'product_variation' ),
				'order'       => 'DESC'
				)
			);
		$product_ids = array();

		foreach ( $vendor_products as $_product ) {
			$product_ids[] = $_product->ID;
		}
		return $product_ids ;
	}
	
	function mwb_wrffm_get_vendor_order()
	{
		$orders = array(); 
		global $wpdb;
		
		$seller_id = get_current_user_id();
		if ( current_user_can('administrator') ) {
			$sql = "SELECT do.post_id
	                FROM {$wpdb->prefix}postmeta AS do
	                LEFT JOIN $wpdb->posts p ON do.post_id = p.ID
	                WHERE
	                    p.post_author = %d AND
	                    p.post_type	= 'shop_order' AND
	                    p.post_status != 'trash'
	                    GROUP BY do.post_id
	                    ORDER BY p.post_date DESC";
		} else {
	        $sql = "SELECT do.order_id
	                FROM {$wpdb->prefix}wcfm_marketplace_orders AS do
	                LEFT JOIN $wpdb->posts p ON do.order_id = p.ID
	                WHERE
	                    do.vendor_id = %d AND
	                    p.post_status != 'trash'
	                    GROUP BY do.order_id
	                    ORDER BY p.post_date DESC";
		}
        $orders_list = $wpdb->get_results( $wpdb->prepare( $sql, $seller_id ) );
        if(is_array($orders_list) && !empty($orders_list))
        {
        	foreach ($orders_list as $order_obj) {
        		if ( current_user_can('administrator') ) {
        			$order_obj->order_id = $order_obj->post_id;
        		}
        		$mwb_wrffm_request_made = get_post_meta($order_obj->order_id,'mwb_wrffm_request_made',true);
        		$get_order = wc_get_order($order_obj->order_id);
        		$order_data = $get_order->get_data();
        		if ( $mwb_wrffm_request_made || $order_data['parent_id'] > 0)
        		{
        			if ( current_user_can('administrator') ) {
        				$items = $get_order->get_items();
        				foreach ( $items as $item ) {
        					$product_id = $item->get_product_id();
	        				$product_ids = mwb_wrffm_get_vendor_product();
	        				if( in_array( $product_id, $product_ids ) ){
	        					if( ! in_array( $order_obj->order_id, $orders ) ) {
	        						$orders[] = $order_obj->order_id;
	        					}
	        				}
        				}
        			} else {
        				if( $order_data['parent_id'] > 0 ) {
	        				$orders[] = $order_obj->order_id;
	        				$orders[] = $order_data['parent_id'];
	        			} else {
	        				$orders[] = $order_obj->order_id;
	        			}
        			}
        		} else {
        			$orders[] = $order_obj->order_id;
        		}
        	}
        }
		return $orders;
	}

	if ( ! function_exists( 'get_wcfm_rma_request_url' ) ) {
		function get_wcfm_rma_request_url( $order_id = '' ) {
			global $WCFM;
			$wcfm_page = get_wcfm_page();
			$get_wcfm_rma_request_url = wcfm_get_endpoint_url( 'rma/?orderid=', $order_id, $wcfm_page );
			$get_wcfm_rma_request_url = str_replace('orderid=/', 'orderid=', $get_wcfm_rma_request_url);
			return apply_filters( 'get_wcfm_rma_request_url', $get_wcfm_rma_request_url );
		}
	}

	function mwb_wrffm_refund_policy_price_deduction($total)
	{
		$product_total = $total;
			$mwb_wrffm_enable_price_policy = get_option( 'mwb_wrffm_enable_price_policy', 'no' );
			if ( $mwb_wrffm_enable_price_policy == 'no' ) {
				return $product_total;
			}
			$mwb_wrffm_price_policy_array=array();
			$mwb_wrffm_number_of_days = get_option( 'mwb_wrffm_number_of_days', array() );
			$mwb_wrffm_price_reduced = get_option( 'mwb_wrffm_price_reduced', array() );
			foreach ($mwb_wrffm_number_of_days as $key => $value) {
				foreach ($mwb_wrffm_price_reduced as $key1 => $value1) {
			 		if($key1===$key)
					{
						$mwb_wrffm_price_policy_array[$value]=$value1;
					}
				}
			}
			ksort($mwb_wrffm_price_policy_array);
			$order_id = sanitize_text_field($_POST['order_id']);
			if ( !empty( $mwb_wrffm_number_of_days ) ) {
				$order = wc_get_order($order_id);
				$order_date = $order->order_date;
				$order_date = strtotime( $order_date );
				$current_date = strtotime( current_time('Y-m-d h:i:s') );
				$date_dif = $current_date - $order_date;
				$date_dif = floor($date_dif/(60*60*24));
				foreach ($mwb_wrffm_price_policy_array as $key => $value) {
					if ($date_dif > $key) {
						continue;
					}else{
						$product_total = $product_total - $product_total*$value/100;
						break;
					}
				}
			}
			return $product_total;
	}

	function mwb_wrffm_wcfm_activated()
	{
		$mwb_wrffm_activated = true;
		if (function_exists('is_multisite') && is_multisite())
		{
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			if ( !is_plugin_active( 'wc-frontend-manager/wc_frontend_manager.php' ) )
			{
				$mwb_wrffm_activated = false;
			}
		}
		else
		{
			if (!in_array('wc-frontend-manager/wc_frontend_manager.php', apply_filters('active_plugins', get_option('active_plugins'))))
			{
				$mwb_wrffm_activated = false;
			}
		}

		return $mwb_wrffm_activated;
	}

	register_activation_hook( __FILE__, 'mwb_wrffm_activation_process');

	/**
	 * install function, perform all necessary operation
	 * on plugin activation.
	 * 
	 * @since 1.0.0
	 */
	function mwb_wrffm_activation_process()
	{
		$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
		if(!$mwb_wrffm_activation_date)
		{
			$today_date = current_time('timestamp');
			update_option('mwb_wrffm_activation_date',$today_date);
		}	
	}
	add_action('admin_notices','mwb_wrffm_license_notification');

	/**
	 * Licennse activation notification messege.
	 * 
	 * @since 1.0.0
	 */
	function mwb_wrffm_license_notification()
	{

		$mwb_wrffm_license_status = get_option('mwb_wrffm_license_status',false);
		$today_date = current_time('timestamp');
		$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
		
		if(!$mwb_wrffm_activation_date)
		{
			update_option('mwb_wrffm_activation_date',$today_date);
			$mwb_wrffm_activation_date = $today_date;
		}
		$mwb_wrffm_days = $today_date - $mwb_wrffm_activation_date;
		$mwb_wrffm_day_diff = floor($mwb_wrffm_days/(60*60*24));
		if(!$mwb_wrffm_license_status)
		{
			if($mwb_wrffm_day_diff < 30 && $mwb_wrffm_day_diff >=0)
			{
				$day_diff = 30 - $mwb_wrffm_day_diff;
			}
			else
			{
				$day_diff = 0;
			}
			?>
			<div class="update-nag">
		        <strong><?php _e( 'You have ','woocommerce-rma-for-wcfm');echo $day_diff; _e(' days left to verify license of WooCommerce RMA For WCFM. For License verification please ', 'woocommerce-rma-for-wcfm' ); ?> <a href="<?php echo admin_url('/').'admin.php?page=mwb-wrffm-notification&tab=mwb_wrffm_license_section' ?>"><?php _e('Click Here','woocommerce-rma-for-wcfm'); ?></a>.
		    	</strong>
		    </div>
			<?php
		}	
	}
}
else
{
	/**
	 * Show warning message if woocommerce is not install
	 * @name mwb_wrffm_plugin_error_notice()
	 * @author makewebbetter<webmaster@makewebbetter.com>
	 * @link http://www.makewebbetter.com/
	 */
	
	function mwb_wrffm_plugin_error_notice()
 	{ ?>
 		 <div class="error notice is-dismissible">
 			<p><?php _e( 'Woocommerce or WCFM - WooCommerce Frontend Manager is not activated, Please activate Woocommerce and WCFM - WooCommerce Frontend Manager first to install WooCommerce RMA For WCFM.', 'woocommerce-rma-for-wcfm' ); ?></p>
   		</div>
   		<style>
   		#message{display:none;}
   		</style>
   	<?php 
 	} 
 	add_action( 'admin_init', 'mwb_wrffm_plugin_deactivate' );  
 
 	
 	/**
 	 * Call Admin notices
 	 * @name mwb_wrffm_plugin_deactivate()
 	 * @author makewebbetter<webmaster@makewebbetter.com>
 	 * @link http://www.makewebbetter.com/
 	 */
 	
  	function mwb_wrffm_plugin_deactivate()
	{
	   deactivate_plugins( plugin_basename( __FILE__ ) );do_action( 'woocommerce_product_options_stock_fields' );
	   add_action( 'admin_notices', 'mwb_wrffm_plugin_error_notice' );
	}
}
mwb_wrffm_auto_update();
// Plugin Auto Update.
function mwb_wrffm_auto_update() {
	$mwb_wrffm_license_key = get_option('mwb_wrffm_license_key');
	define( 'MWB_WRFFM_LICENSE_KEY', $mwb_wrffm_license_key );
	define( 'MWB_WRFFM_FILE', __FILE__ );
	$mwb_wrffm_update_check = "https://makewebbetter.com/pluginupdates/woocommerce-rma-for-wcfm/update.php";
	require_once('mwb-wrffm-update.php');
}
?>
