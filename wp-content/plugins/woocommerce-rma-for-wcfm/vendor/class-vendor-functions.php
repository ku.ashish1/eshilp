<?php
/**
 * Exit if accessed directly
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if( !class_exists( 'class_vendor_functions' ) )
{
	/**
	 * This is class for managing order status and other functionalities for vendor.
	 *
	 * @name    class_vendor_functions
	 * @category Class
	 * @author   makewebbetter <webmaster@makewebbetter.com>
	 */
	
	class class_vendor_functions{
		
		/**
		 * This is construct of class
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function __construct() 
		{
			$mwb_wrffm_license_status = get_option('mwb_wrffm_license_key',false);
			$today_date = current_time('timestamp');
			$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
			$mwb_wrffm_days = $today_date - $mwb_wrffm_activation_date;
			$mwb_wrffm_day_diff = floor( $mwb_wrffm_days / ( 60*60*24 ) );
			if( $mwb_wrffm_license_status || $mwb_wrffm_day_diff <= 30 )
			{
				//////////////////// admin hooks ////////////////////////////
				add_action( 'mwb_wrffm_notification_setting_tab_before_addon_section', array($this ,'mwb_wrffm_notification_setting_tab_callback') );	
				add_action( 'mwb_wrffm_custom_setting_section_for_wcfm', array($this , 'mwb_wrffm_custom_setting_section_callback' ));	

				//////////////////// public hooks ////////////////////////////
				add_action( 'wp_enqueue_scripts', array($this, 'mwb_wrffm_vendor_scripts'), 10);


				add_filter( 'wcfm_menus', array( $this, 'wcfm_rma_menus' ), 10 );
				add_filter( 'wcfm_query_vars', array( $this, 'wcfm_rma_query_vars' ), 20 );
				add_filter( 'wcfm_endpoint_wcfm-rma_title', array( $this, 'wcfm_rma_endpoint_title' ) );
				add_action( 'wcfm_load_views', array( $this, 'wcfm_rma_load_views' ) );

				//////////////////// ajax requests /////////////////////////////
				add_action( 'wp_ajax_mwb_wrffm_return_fee_add', array($this, 'mwb_wrffm_return_fee_add_callback'));
				add_action( 'wp_ajax_nopriv_mwb_wrffm_return_fee_add', array($this, 'mwb_wrffm_return_fee_add_callback'));
				add_action('wp_ajax_mwb_wrffm_vendor_return_req_approve',array($this,'mwb_wrffm_vendor_return_req_approve_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_vendor_return_req_approve',array($this,'mwb_wrffm_vendor_return_req_approve_callback'));
				add_action('wp_ajax_mwb_wrffm_vendor_manage_stock',array($this,'mwb_wrffm_vendor_manage_stock_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_vendor_manage_stock',array($this,'mwb_wrffm_vendor_manage_stock_callback'));
				add_action('wp_ajax_mwb_wrffm_cancel_return_request_vendor',array($this,'mwb_wrffm_cancel_return_request_vendor_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_cancel_return_request_vendor',array($this,'mwb_wrffm_cancel_return_request_vendor_callback'));

				add_action( 'wp_ajax_mwb_wrffm_exchange_fee_add', array($this, 'mwb_wrffm_exchange_fee_add_callback'));
				add_action( 'wp_ajax_nopriv_mwb_wrffm_exchange_fee_add', array($this, 'mwb_wrffm_exchange_fee_add_callback'));
				add_action('wp_ajax_mwb_wrffm_vendor_exchange_req_approve',array($this,'mwb_wrffm_vendor_exchange_req_approve_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_vendor_exchange_req_approve',array($this,'mwb_wrffm_vendor_exchange_req_approve_callback'));
				add_action('wp_ajax_mwb_wrffm_cancel_exchange_request_vendor',array($this,'mwb_wrffm_cancel_return_exchange_vendor_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_cancel_exchange_request_vendor',array($this,'mwb_wrffm_cancel_return_exchange_vendor_callback'));
				add_action('wp_ajax_mwb_wrffm_vendor_exchange_req_approve_refund',array($this,'mwb_wrffm_vendor_exchange_req_approve_refund_callback'));
				add_action('wp_ajax_nopriv_mwb_wrffm_vendor_exchange_req_approve_refund',array($this,'mwb_wrffm_vendor_exchange_req_approve_refund_callback'));

				add_action( 'mwb_wrffm_customer_refund_request_mail_for_vendor', array($this, 'mwb_wrffm_customer_refund_request_mail_for_vendor_callback'),10,5);

				add_action( 'mwb_wrffm_customer_refund_request_approve_mail_for_vendor', array($this, 'mwb_wrffm_customer_refund_request_action_mail_for_vendor_callback'),10,4);

				add_action( 'mwb_wrffm_customer_refund_request_cancel_mail_for_vendor', array($this, 'mwb_wrffm_customer_refund_request_action_mail_for_vendor_callback'),10,4);

				add_action( 'mwb_wrffm_customer_exchange_request_approve_mail_for_vendor', array($this, 'mwb_wrffm_customer_refund_request_action_mail_for_vendor_callback'),10,5);
				add_action( 'mwb_wrffm_customer_exchange_request_cancel_mail_for_vendor', array($this, 'mwb_wrffm_customer_refund_request_action_mail_for_vendor_callback'),10,5);

			}
			
		}

		/**
		 * This function is used for creating Setting tab in RMA Configuration pannel.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link https://www.makewebbetter.com/
		 *
		 * @since    1.0.0
		 */
		public function mwb_wrffm_notification_setting_tab_callback()
		{
			$wrffm_tab_active = '';
			if(isset($_GET['tab']) && $_GET['tab'] == 'wrffm_tab')
			{
				$wrffm_tab_active = 'nav-tab-active';
			}
			?>
			<a class="nav-tab <?php echo $wrffm_tab_active;?>" href="<?php echo admin_url()?>/admin.php?page=mwb-wrffm-notification&amp;tab=wrffm_tab"><?php _e('RMA WCFM', 'woocommerce-rma-for-wcfm' ); ?></a>
			<?php
		}

		/**
		 * This function is used for creating settings on RMA Pannel.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link https://www.makewebbetter.com/
		 *
		 * @since    1.0.0
		*/
		public function mwb_wrffm_custom_setting_section_callback()
		{
			if(isset($_POST['mwb_wrffm_addon_tab']))
			{
				?>
				<div class="notice notice-success is-dismissible">
					<p><strong><?php _e('Settings saved.','woocommerce-rma-for-wcfm'); ?></strong></p>
					<button type="button" class="notice-dismiss">
						<span class="screen-reader-text"><?php _e('Dismiss this notices.','woocommerce-rma-for-wcfm'); ?></span>
					</button>
				</div><?php
				unset($_POST['mwb_wrffm_addon_tab']);
				$post = $_POST;
				foreach($post as $k=>$val)
				{
					update_option($k, sanitize_text_field($val));
				}
				if(!isset($post['mwb_wrffm_enable_vendor_manage_stock']))
				{
					update_option( 'mwb_wrffm_enable_vendor_manage_stock' , 'no' );
				}
				if(!isset($post['mwb_wrffm_enable_vendor_refund']))
				{
					update_option( 'mwb_wrffm_enable_vendor_refund' , 'no' );
				}
				if(!isset($post['mwb_wrffm_enable_vendor_addon']))
				{
					update_option( 'mwb_wrffm_enable_vendor_addon' , 'no' );
				}
				if(!isset($post['mwb_wrffm_enable_vendor_ship_fee']))
				{
					update_option( 'mwb_wrffm_enable_vendor_ship_fee' , 'no' );
				}
			}
			$mwb_wrffm_license_status = get_option('mwb_wrffm_license_status');
			$today_date = current_time('timestamp');
			$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
			$mwb_wrffm_days = $today_date - $mwb_wrffm_activation_date;
			$mwb_wrffm_day_diff = floor($mwb_wrffm_days/(60*60*24));
			if((isset($_GET['section']) && $_GET['section'] == 'license') || ((!$mwb_wrffm_license_status && $mwb_wrffm_day_diff >= 30) && isset($_GET['tab']) && $_GET['tab'] == 'wrffm_tab'))
			{
				?><div class="mwb_wrffm_table">
					<h3><?php _e('Woocommerce Refund & Exchange RMA For WCFM','woocommerce-rma-for-wcfm');?></h3>
					<hr/>
					<div style="text-align: justify; float: left;  font-size: 16px; line-height: 25px; padding-right: 4%;">
						<?php 
					 	echo sprintf(__('This is the License Activation Panel. After purchasing extension from <strong>MakeWebBetter</strong> you will get the purchase code of this extension. Please verify your purchase code below so that you can use feature of this plugin.','woocommerce-rma-for-wcfm'));
					 	?>
					 	
					 </div>
					<table class="form-table">
						<tbody>
							<tr valign="top">
								<th class="titledesc" scope="row">
									<label><?php _e('Enter Purchase Code','woocommerce-rma-for-wcfm');?></label>
								</th>
								<td class="forminp">
									<fieldset>
										<input type="text" id="mwb_dokan_license_key" class="input-text regular-input" placeholder="<?php _e('Enter your Purchase code here...','woocommerce-rma-for-wcfm'); ?>">
										<input type="button" value="<?php _e('Validate','woocommerce-rma-for-wcfm'); ?>" class="button-primary" id="mwb_wcffm_license_save">
										<img class="loading_image" src="<?php echo MWB_WRFFM_URL;?>assets/images/loader.gif" style="height: 28px;vertical-align: middle;display:none;">
										<b class="licennse_notification"></b>
									</fieldset>
								</td>
							</tr>
						</tbody>
					</table>
				</div><?php
			}
			else if(isset($_GET['tab']) && $_GET['tab'] == 'wrffm_tab')
			{
				?>
				<div class="mwb_wrffm_table">
					<form enctype="multipart/form-data" action="" id="mainform" method="post">
						<div id="mwb_wrffm_accordion">
							<div class="mwb_wrffm_accord_sec_wrap">
								<h2 id="wrffm_mail_setting" class="mwb_wrffm_basic_setting mwb_wrffm_slide_active"><?php _e('Vendor Setting For Product Refund/Exchange', 'woocommerce-rma-for-wcfm' ); ?></h2>
								<div id="wrffm_mail_setting_wrapper">
									<table class="form-table mwb_wrffm_notification_section">
										<tbody>
											<?php $mwb_wrffm_enable_vendor_addon = get_option( 'mwb_wrffm_enable_vendor_addon', 'no' ); ?>
											<tr valign="top">
												<th class="titledesc" scope="row">
													<label for="mwb_wrffm_enable_vendor_addon"><?php _e('Allow Vendor to Manage Refund/Exchange Request', 'woocommerce-rma-for-wcfm' ); ?></label>
												</th>
												<td>
													<span class="mwb_wrffm_checkbox_wrrapper">
														<input type="checkbox" id="mwb_wrffm_enable_vendor_addon" name="mwb_wrffm_enable_vendor_addon" <?php if ($mwb_wrffm_enable_vendor_addon == 'on') {
															?>checked="checked"<?php
														} ?>></input><label for="mwb_wrffm_enable_vendor_addon"></label>
													</span>
													<span class="description"><?php _e( 'Enable to Allow Vendor to Manage Refund/Exchange Request.', 'woocommerce-rma-for-wcfm' ); ?></span>
												</td>
											</tr>
											<?php $mwb_wrffm_enable_vendor_ship_fee = get_option( 'mwb_wrffm_enable_vendor_ship_fee', 'no' ); ?>
											<tr valign="top">
												<th class="titledesc" scope="row">
													<label for="mwb_wrffm_enable_vendor_ship_fee"><?php _e('Allow Vendor to Manage Shipping Cost', 'woocommerce-rma-for-wcfm' ); ?></label>
												</th>
												<td>
													<span class="mwb_wrffm_checkbox_wrrapper">
														<input type="checkbox" id="mwb_wrffm_enable_vendor_ship_fee" name="mwb_wrffm_enable_vendor_ship_fee" <?php if ($mwb_wrffm_enable_vendor_ship_fee == 'on') {
															?>checked="checked"<?php
														} ?>></input><label for="mwb_wrffm_enable_vendor_ship_fee"></label>
													</span>
													<span class="description"><?php _e( 'Enable to Allow Vendor to apply Shipping fee.', 'woocommerce-rma-for-wcfm' ); ?></span>
												</td>
											</tr>
											<?php $mwb_wrffm_enable_vendor_refund = get_option( 'mwb_wrffm_enable_vendor_refund', 'no' ); ?>
											<tr valign="top">
												<th class="titledesc" scope="row">
													<label for="mwb_wrffm_enable_vendor_refund"><?php _e('Allow Vendor to Refund Money in Customer Wallet', 'woocommerce-rma-for-wcfm' ); ?></label>
												</th>
												<td>
													<span class="mwb_wrffm_checkbox_wrrapper">
														<input type="checkbox" id="mwb_wrffm_enable_vendor_refund" name="mwb_wrffm_enable_vendor_refund" <?php if ($mwb_wrffm_enable_vendor_refund == 'on') {
															?>checked="checked"<?php
														} ?>></input><label for="mwb_wrffm_enable_vendor_refund"></label>
													</span>
													<span class="description"><?php _e( 'Enable to provide permission to vendor for refund money in customer wallet.', 'woocommerce-rma-for-wcfm' ); ?></span>
												</td>
											</tr>
											<?php $mwb_wrffm_enable_vendor_manage_stock = get_option( 'mwb_wrffm_enable_vendor_manage_stock', 'no' ); ?>
											<tr valign="top">
												<th class="titledesc" scope="row">
													<label for="mwb_wrffm_enable_vendor_manage_stock"><?php _e('Allow Vendor to Manage Refunded Product Stock', 'woocommerce-rma-for-wcfm' ); ?></label>
												</th>
												<td>
													<span class="mwb_wrffm_checkbox_wrrapper">
														<input type="checkbox" id="mwb_wrffm_enable_vendor_manage_stock" name="mwb_wrffm_enable_vendor_manage_stock" <?php if ($mwb_wrffm_enable_vendor_manage_stock == 'on') {
															?>checked="checked"<?php
														} ?>></input><label for="mwb_wrffm_enable_vendor_manage_stock"></label>
													</span>
													<span class="description"><?php _e( 'Enable to provide permission to vendor for manage stock of refunded product.', 'woocommerce-rma-for-wcfm' ); ?></span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<p class="submit">
							<input type="submit" value="<?php _e('Save changes', 'woocommerce-rma-for-wcfm' ); ?>" class="mwb-wrffm-save-button button-primary woocommerce-save-button" name="mwb_wrffm_addon_tab"> 
						</p>
					</form>
				</div>
				<?php 
			}
		}

		public function mwb_wrffm_vendor_scripts() {
			wp_enqueue_style( 'jquery-dataTables-css', MWB_WRFFM_URL.'vendor/css/jquery.dataTables.css' );
			wp_enqueue_style( 'mwb-wrffm-vendor-public-css', MWB_WRFFM_URL.'vendor/css/mwb-wrffm-vendor-public.css' );
			wp_enqueue_script( 'jquery-dataTables-js', MWB_WRFFM_URL.'vendor/js/jquery.dataTables.js', array( 'jquery' ) );
			wp_register_script( 'mwb-wrffm-vendor-public-js', MWB_WRFFM_URL.'vendor/js/mwb-wrffm-vendor-public.js', array( 'jquery' ) );
			$translation_array = array(
				'ajaxurl'           => admin_url( 'admin-ajax.php' ),
				'mwb_wrffm_nonce'	=> wp_create_nonce( "mwb-wrffm-ajax-security-string" ),
			);
			wp_localize_script( 'mwb-wrffm-vendor-public-js', 'global_wrffm_addon', $translation_array );
			wp_enqueue_script( 'mwb-wrffm-vendor-public-js' );
		}

		function wcfm_rma_query_vars( $query_vars ) {
			$wcfm_modified_endpoints = wcfm_get_option( 'wcfm_endpoints', array() );
			$rma_qry = array( 'wcfm-rma' => ! empty( $wcfm_modified_endpoints['wcfm-rma'] ) ? $wcfm_modified_endpoints['wcfm-rma'] : 'rma' );
			return array_merge( $query_vars, $rma_qry );
		}

		function wcfm_rma_endpoint_title( $endpoint ) {
			return 'RMA';
		}

		function wcfm_rma_load_views( $endpoint ) {
			if($endpoint == 'wcfm-rma') {
				include_once MWB_WRFFM_DIRPATH . 'template/mwb-vendor-rma-list.php';
			}
		}

		function wcfm_rma_endpoint_url() {
			$wcfm_page = get_wcfm_page();
			$wcfm_rma_url = wcfm_get_endpoint_url( 'wcfm-rma', '', $wcfm_page );
			return apply_filters( 'wcfm_rma_endpoint_url', $wcfm_rma_url );
		}

		function wcfm_rma_menus( $menus ) {

			return array_slice( $menus, 0, 3, true ) +
			array( 'wcfm-rma' => array( 'label'    => 'RMA Request',
				'url'      =>  $this->wcfm_rma_endpoint_url(),
				'icon'     => 'fa-retweet',
				'menu_for' => 'both',
				'priority' => 75
			) ) +
			array_slice( $menus, 3, count( $menus ) - 3, true );
		}

		/**
		 * This function is add extra fee to Refund product by Vendor
		 * 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public function mwb_wrffm_return_fee_add_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) {

				$orderid = $_POST['orderid'];
				$pending_date = $_POST['date'];
				$fees = $_POST['fees'];
				
				$current_user = get_current_user_id();
				if(isset($fees))
				{
					foreach($fees as $k=>$fee)
					{
						if($fee['text'] == '' || $fee['val'] == '')
						{
							unset($fees[$k]);
						}
					}
				}
				$added_fees = get_post_meta($orderid, 'mwb_wrffm_return_added_fee'.$current_user, true);
				$exist = true;
				if(isset($added_fees) && !empty($added_fees))
				{
					foreach($added_fees as $date=>$added_fee)
					{
						if($date == $pending_date)
						{
							$added_fees[$pending_date] = $fees;
							$exist = false;
							break;
						}
					}
				}
		
				if($exist)
				{
					$added_fees[$pending_date] = $fees;
				}
		
				update_post_meta($orderid, 'mwb_wrffm_return_added_fee'.$current_user, $added_fees);
				$response['response'] = 'success';
				echo json_encode($response);
				die;
			}
		}


		/**
		 * This function is approving refund request.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 * @since    1.0.0
		*/
		public function mwb_wrffm_vendor_return_req_approve_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) 
			{
				global $WCFM;
				$order_id  = isset($_POST['order_id']) ? sanitize_text_field($_POST['order_id']) : 0 ;
				$mwb_wrffm_vendor_id = get_current_user_id();
				if($order_id > 0)
				{
					$return_datas = get_post_meta($order_id, 'mwb_wrffm_return_product', true);
					if(is_array($return_datas) && !empty($return_datas))
					{
						foreach ($return_datas as $date => $return_data) {
							if(isset($return_data['products']) && is_array($return_data['products']) && !empty($return_data['products']))
							{
								$request_date = $date;
								$product_datas = $return_data['products'];
								foreach ($return_data['products']  as $key => $prod_data) {
									if(in_array($prod_data['product_id'], mwb_wrffm_get_vendor_product()) || in_array($prod_data['variation_id'], mwb_wrffm_get_vendor_product()))
									{
										$return_data['products'][$key]['approved'] = 1;
										$return_data['products'][$key]['approve_date'] = date( get_option( 'date_format' ) );
									}
								}
							}
							$return_datas[$date]['products'] = $return_data['products']; 
						}
					}

					update_post_meta($order_id, 'mwb_wrffm_return_product', $return_datas);
					update_post_meta($order_id, 'mwb_wrffm_request_approve'.$mwb_wrffm_vendor_id ,'1');

					$request_files = get_post_meta($order_id, 'mwb_wrffm_return_attachment', true);

					if(isset($request_files) && !empty($request_files))
					{
						foreach($request_files as $date=>$request_file)
						{
							if($request_file['status'] == 'pending')
							{
								$request_files[$date]['status'] = 'complete';
								break;
							}
						}
					}
					update_post_meta($order_id, 'mwb_wrffm_return_attachment', $request_files);

					$order = wc_get_order($order_id);

					$fname = get_option('mwb_wrffm_notification_from_name');
					$fmail = get_option('mwb_wrffm_notification_from_mail');

					$headers = array();
					$headers[] = "From: $fname <$fmail>";
					$headers[] = "Content-Type: text/html; charset=UTF-8";

					$to = get_post_meta($order_id, '_billing_email', true);

					$subject = get_option('mwb_wrffm_notification_return_approve_subject', false);
					$approve = get_option('mwb_wrffm_notification_return_approve', false);
					$wallet_enable = get_option('mwb_wrffm_return_wallet_enable', "no");

					if($wallet_enable == 'yes')
					{
						if( WC()->version < "3.0.0" )
						{
							$order_id=$order->id;
						}
						else
						{
							$order_id=$order->get_id();
						}
						$customer_id = ( $value = get_post_meta( $order_id, '_customer_user', true ) ) ? absint( $value ) : '';
						if($customer_id > 0)
						{
							$approve = get_option('mwb_wrffm_notification_return_approve_wallet', false);
						}
					}	

					$fname = get_post_meta($order_id, '_billing_first_name', true);
					$lname = get_post_meta($order_id, '_billing_last_name', true);

					$fullname = $fname." ".$lname;

					$approve = str_replace('[username]', $fullname, $approve);
					$approve = str_replace('[order]', "#".$order_id, $approve);
					$approve = str_replace('[siteurl]', home_url(), $approve);

					$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
					$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));

					$message = '<html>
					<body>
						<style>
							body {
								box-shadow: 2px 2px 10px #ccc;
								color: #767676;
								font-family: Arial,sans-serif;
								margin: 80px auto;
								max-width: 700px;
								padding-bottom: 30px;
								width: 100%;
							}

							h2 {
								font-size: 30px;
								margin-top: 0;
								color: #fff;
								padding: 40px;
								background-color: #557da1;
							}

							h4 {
								color: #557da1;
								font-size: 20px;
								margin-bottom: 10px;
							}

							.content {
								padding: 0 40px;
							}

							.Customer-detail ul li p {
								margin: 0;
							}

							.details .Shipping-detail {
								width: 40%;
								float: right;
							}

							.details .Billing-detail {
								width: 60%;
								float: left;
							}

							.details .Shipping-detail ul li,.details .Billing-detail ul li {
								list-style-type: none;
								margin: 0;
							}

							.details .Billing-detail ul,.details .Shipping-detail ul {
								margin: 0;
								padding: 0;
							}

							.clear {
								clear: both;
							}

							table,td,th {
								border: 2px solid #ccc;
								padding: 15px;
								text-align: left;
							}

							table {
								border-collapse: collapse;
								width: 100%;
							}

							.info {
								display: inline-block;
							}

							.bold {
								font-weight: bold;
							}

							.footer {
								margin-top: 30px;
								text-align: center;
								color: #99B1D8;
								font-size: 12px;
							}
							dl.variation dd {
								font-size: 12px;
								margin: 0;
							}
						</style>

						<div style="text-align: center; padding: 10px;" class="header">
							'.$mail_header.'
						</div>		

						<div class="header">
							<h2>'.__('Your Refund Request is Approved', 'woocommerce-rma-for-wcfm').'</h2>
						</div>
						<div class="content">
							<div class="reason">
								<p>'.$approve.'</p>
							</div>
							<div class="Order">
								<h4>Order #'.$order_id.'</h4>
								<table>
									<tbody>
										<tr>
											<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
										</tr>';
										$order = wc_get_order($order_id);
										$requested_products = $return_datas[$request_date]['products'];
										if(isset($requested_products) && !empty($requested_products))
										{
											$total = 0;
											$mwb_get_refnd = get_post_meta( $order_id, 'mwb_wrffm_return_product',true );
											if( !empty( $mwb_get_refnd ) )
											{
												foreach ($mwb_get_refnd as $key => $value) 
												{
													if( isset( $value['amount'] ) )
													{
														$total_price = $value['amount'];
														break;
													}
												}
											}
											foreach( $order->get_items() as $item_id => $item )
											{
												$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
												foreach($requested_products as $requested_product)
												{
													if($item_id == $requested_product['item_id'])
													{

														if(isset($requested_product['variation_id']) && $requested_product['variation_id'] > 0)
														{
															$prod = wc_get_product($requested_product['variation_id']);

														}
														else
														{
															$prod = wc_get_product($requested_product['product_id']);
														}
														if(in_array($prod->get_id(), mwb_wrffm_get_vendor_product()))
														{

															$prod_price = wc_get_price_excluding_tax($prod, array( 'qty' => 1 ));
															$subtotal = $requested_product['price']*$requested_product['qty'];
															$total += $subtotal;
															if( WC()->version < "3.1.0" )
															{
																$item_meta      = new WC_Order_Item_Meta( $item, $_product );
																$item_meta_html = $item_meta->display( true, true );
															}
															else
															{
																$item_meta      = new WC_Order_Item_Product( $item, $product );
																$item_meta_html = wc_display_item_meta($item_meta,array('echo'=> false));
															}
															$message .= '<tr>
															<td>'.$item['name'].'<br>';
																$message .= '<small>'.$item_meta_html.'</small>
																<td>'.$requested_product['qty'].'</td>
																<td>'.mwb_wrffm_format_price($requested_product['price']*$requested_product['qty']).'</td>
															</tr>';
															update_post_meta($order_id,'refund_amount_vendor'.$mwb_wrffm_vendor_id.$item_id,$requested_product['price']*$requested_product['qty']);
														}
													}
												}
											}	
											$total = mwb_wrffm_refund_policy_price_deduction($total);		
											$message .= '<tr>
											<th colspan="2">Total:</th>
											<td>'.mwb_wrffm_format_price($total).'</td>
										</tr>
										<tr>
											<th colspan="3">Extra:</th>
										</tr>';
									}
									if( WC()->version < "3.0.0" )
									{
										$order_id=$order->id;
									}
									else
									{
										$order_id=$order->get_id();
									}
									$added_fees = get_post_meta($order_id, 'mwb_wrffm_return_added_fee'.$mwb_wrffm_vendor_id, true);
									if(isset($added_fees) && !empty($added_fees))
									{
										foreach( $added_fees as $da=>$added_fee )
										{
											if($date == $da)
											{ 
												foreach($added_fee as $fee)
												{
													$total -= $fee['val'];

													$message .= ' <tr>
													<th colspan="2">'.$fee['text'].':</th>
													<td>'.mwb_wrffm_format_price($fee['val']).'</td>
												</tr>';
											}
										}
									}
								}
								if( WC()->version < "3.0.0" )
								{
									$order_id=$order->id;
								}
								else
								{
									$order_id=$order->get_id();
								}
								$message .= ' <tr>
								<th colspan="2">'.__('Refund Total', 'woocommerce-rma-for-wcfm').':</th>
											<td>'.mwb_wrffm_format_price($total).'</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="Customer-detail">
								<h4>'.__('Customer details', 'woocommerce-rma-for-wcfm').'</h4>
								<ul>
									<li>
										<p class="info">
											<span class="bold">'.__('Email','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_email', true).'
										</p>
									</li>
									<li>
										<p class="info">
											<span class="bold">'.__('Tel','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_phone', true).'
										</p>
									</li>
								</ul>
							</div>
							<div class="details">
								<div class="Shipping-detail">
									<h4>'.__('Shipping Address', 'woocommerce-rma-for-wcfm').'</h4>
									'.$order->get_formatted_shipping_address().'
								</div>
								<div class="Billing-detail">
									<h4>'.__('Billing Address', 'woocommerce-rma-for-wcfm').'</h4>
									'.$order->get_formatted_billing_address().'
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div style="text-align: center; padding: 10px;" class="footer">
							'.$mail_footer.'
						</div>
					</body>
					</html>';

				
					$template = stripslashes(get_option('mwb_wrffm_notification_return_approve_template', 'no'));

					if(isset($template) && $template == 'on')
					{
						$refund_approve_template = stripslashes(get_option('mwb_wrffm_notification_return_approve',false));
						if($wallet_enable == 'yes')
						{
							$wallet_template = stripslashes(get_option('mwb_wrffm_notification_return_approve_wallet_template', 'no'));
							if(isset($wallet_template) && $wallet_template == 'on')
							{
								$refund_approve_template = stripslashes(get_option('mwb_wrffm_notification_return_approve_wallet', false));
							}
						}

					}

					////////////////shortcode replace variable start//////////////////////

					$fname = get_post_meta($order_id, '_billing_first_name', true);
					$lname = get_post_meta($order_id, '_billing_last_name', true);
					$billing_company = get_post_meta($order_id, '_billing_company', true);
					$billing_email = get_post_meta($order_id, '_billing_email', true);
					$billing_phone = get_post_meta($order_id, '_billing_phone', true);
					$billing_country = get_post_meta($order_id, '_billing_country', true);
					$billing_address_1 = get_post_meta($order_id, '_billing_address_1', true);
					$billing_address_2 = get_post_meta($order_id, '_billing_address_2', true);
					$billing_state = get_post_meta($order_id, '_billing_state', true);
					$billing_postcode = get_post_meta($order_id, '_billing_postcode', true);
					$shipping_first_name = get_post_meta($order_id, '_shipping_first_name', true);
					$shipping_last_name = get_post_meta($order_id, '_shipping_last_name', true);
					$shipping_company = get_post_meta($order_id, '_shipping_company', true);
					$shipping_country = get_post_meta($order_id, '_shipping_country', true);
					$shipping_address_1 = get_post_meta($order_id, '_shipping_address_1', true);
					$shipping_address_2 = get_post_meta($order_id, '_shipping_address_2', true);
					$shipping_city = get_post_meta($order_id, '_shipping_city', true);
					$shipping_state = get_post_meta($order_id, '_shipping_state', true);
					$shipping_postcode = get_post_meta($order_id, '_shipping_postcode', true);
					$payment_method_tittle = get_post_meta($order_id, '_payment_method_title', true);
					$order_shipping = get_post_meta($order_id, '_order_shipping', true);
					$order_total = get_post_meta($order_id, '_order_total', true);
					$refundable_amount = get_post_meta($order_id, 'refundable_amount', true);

					/////////////////////shortcode replace variable end///////////////////

					$fullname = $fname." ".$lname;

					$message = str_replace('[username]', $fullname, $message);
					$message = str_replace('[order]', "#".$order_id, $message);
					$message = str_replace('[siteurl]', home_url(), $message);
					$message = str_replace('[_billing_company]', $billing_company, $message);
					$message = str_replace('[_billing_email]', $billing_email, $message);
					$message = str_replace('[_billing_phone]', $billing_phone, $message);
					$message = str_replace('[_billing_country]', $billing_country, $message);
					$message = str_replace('[_billing_address_1]', $billing_address_1, $message);
					$message = str_replace('[_billing_address_2]', $billing_address_2, $message);
					$message = str_replace('[_billing_state]', $billing_state, $message);
					$message = str_replace('[_billing_postcode]', $billing_postcode, $message);
					$message = str_replace('[_shipping_first_name]', $shipping_first_name, $message);
					$message = str_replace('[_shipping_last_name]', $shipping_last_name, $message);
					$message = str_replace('[_shipping_company]', $shipping_company, $message);
					$message = str_replace('[_shipping_country]', $shipping_country, $message);
					$message = str_replace('[_shipping_address_1]', $shipping_address_1, $message);
					$message = str_replace('[_shipping_address_2]', $shipping_address_2, $message);
					$message = str_replace('[_shipping_city]', $shipping_city, $message);
					$message = str_replace('[_shipping_state]', $shipping_state, $message);
					$message = str_replace('[_shipping_postcode]', $shipping_postcode, $message);
					$message = str_replace('[_payment_method_tittle]', $payment_method_tittle, $message);
					$message = str_replace('[_order_shipping]', $order_shipping, $message);
					$message = str_replace('[_order_total]', $order_total, $message);
					$message = str_replace('[_refundable_amount]', $refundable_amount, $message);
					$message = str_replace('[formatted_shipping_address]', $order->get_formatted_shipping_address(), $message);
					$message = str_replace('[formatted_billing_address]', $order->get_formatted_billing_address(), $message);

					if(isset($refund_approve_template) && $refund_approve_template != '')
					{
						$template = $refund_approve_template;
						$template = str_replace('[username]', $fullname, $template);
						$template = str_replace('[order]', "#".$order_id, $template);
						$template = str_replace('[siteurl]', home_url(), $template);
						$template = str_replace('[_billing_company]', $billing_company, $template);
						$template = str_replace('[_billing_email]', $billing_email, $template);
						$template = str_replace('[_billing_phone]', $billing_phone, $template);
						$template = str_replace('[_billing_country]', $billing_country, $template);
						$template = str_replace('[_billing_address_1]', $billing_address_1, $template);
						$template = str_replace('[_billing_address_2]', $billing_address_2, $template);
						$template = str_replace('[_billing_state]', $billing_state, $template);
						$template = str_replace('[_billing_postcode]', $billing_postcode, $template);
						$template = str_replace('[_shipping_first_name]', $shipping_first_name, $template);
						$template = str_replace('[_shipping_last_name]', $shipping_last_name, $template);
						$template = str_replace('[_shipping_company]', $shipping_company, $template);
						$template = str_replace('[_shipping_country]', $shipping_country, $template);
						$template = str_replace('[_shipping_address_1]', $shipping_address_1, $template);
						$template = str_replace('[_shipping_address_2]', $shipping_address_2, $template);
						$template = str_replace('[_shipping_city]', $shipping_city, $template);
						$template = str_replace('[_shipping_state]', $shipping_state, $template);
						$template = str_replace('[_shipping_postcode]', $shipping_postcode, $template);
						$template = str_replace('[_payment_method_tittle]', $payment_method_tittle, $template);
						$template = str_replace('[_order_shipping]', $order_shipping, $template);
						$template = str_replace('[_order_total]', $order_total, $template);
						$template = str_replace('[_refundable_amount]', $refundable_amount, $template);
						$template = str_replace('[formatted_shipping_address]', $order->get_formatted_shipping_address(), $template);
						$template = str_replace('[formatted_billing_address]', $order->get_formatted_billing_address(), $template);
						$html_content = $template;
					}
					else
					{
						$html_content = $message;
					}
					wc_mail($to, $subject, $html_content, $headers);

					$headers = array();
					$headers[] = "Content-Type: text/html; charset=UTF-8";
					$to = get_option('mwb_wrffm_notification_from_mail');
					$subject= __('Refund Request Approved By Vendor','woocommerce-rma-for-wcfm');
					wc_mail( $to, $subject, $html_content, $headers );

											// Wallet for customer 

					$wallet_enable = get_option('mwb_wrffm_return_wallet_enable', "no");
					$mwb_wrffm_select_refund_method_enable = get_option('mwb_wrffm_select_refund_method_enable', "no");
					$mwb_wrffm_refund_method = '';
					$mwb_wrffm_refund_method = get_post_meta($order_id,'mwb_wrffm_refund_method',true);
					$response['refund_method'] = '';
					if($mwb_wrffm_refund_method != '')
						$response['refund_method'] = $mwb_wrffm_refund_method;

					if(($wallet_enable == 'yes' && $mwb_wrffm_select_refund_method_enable == 'yes' && $mwb_wrffm_refund_method == 'manual_method') || ($wallet_enable != 'yes') )
					{
						update_post_meta($order_id,'refundable_amount_'.$mwb_wrffm_vendor_id,$total);
					}
					else
					{

						update_post_meta($order_id,'refundable_amount_'.$mwb_wrffm_vendor_id,$total);
					}
					$final_stotal = 0;
					$get_items = $order->get_items();
					$lastElement = end( $get_items );
					foreach( $order->get_items() as $item_id => $item )
					{
						if($item != $lastElement) {
							$final_stotal += $item['subtotal'];
						}
					}

					update_post_meta($order_id,'discount',0);

					if($final_stotal > 0)
					{
						$mwb_wrffm_obj = wc_get_order( $order_id );
						$tax_rate = 0;
						$tax = new WC_Tax();
						$country_code = $mwb_wrffm_obj->get_billing_country(); // or populate from order to get applicable rates
						$rates = $tax->find_rates( array( 'country' => $country_code ) );
						foreach( $rates as $rate ){
							$tax_rate = $rate['rate'];
						}

						$total_ptax = $final_stotal*$tax_rate/100;
						$orderval = $final_stotal+$total_ptax;
						$orderval = round($orderval,2);
						if( WC()->version < "3.7.0" ) {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_used_coupons();
						} else {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_coupon_codes();
						}
						$finaldiscount = 0;
						// Coupons used in the order LOOP (as they can be multiple)
						foreach( $mwb_ord_cpn as $coupon_name ){
							$coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
							$coupon_id = $coupon_post_obj->ID;
							$coupons_obj = new WC_Coupon($coupon_id);

							$coupons_amount = $coupons_obj->get_amount();
							$coupons_type = $coupons_obj->get_discount_type();
							if($coupons_type == 'percent'){
								$finaldiscount = $orderval*$coupons_amount/100;
							}

						}
						$discount = $finaldiscount*100/(100+$tax_rate);


						if($discount > 0)
						{
							update_post_meta($order_id,'discount',$discount);
						}
						else
						{
							update_post_meta($order_id,'_cart_discount_tax',0.00);
							update_post_meta($order_id,'discount',0.00);
						}
					}

		
					//Auto accept return request
					if(isset($_POST['autoaccept']))
					{
						if( WC()->version < "3.0.0" )
						{
							$order_id=$order->id;
						}
						else
						{
							$order_id=$order->get_id();
						}
						$headers = array();
						$headers[] = "Content-Type: text/html; charset=UTF-8";
						$to = get_option('mwb_wrffm_notification_from_mail');
						$subject = get_option('mwb_wrffm_notification_auto_accept_return_subject');
						$subject = str_replace('[order]', "#".$order_id, $subject);

						$message = get_option('mwb_wrffm_notification_auto_accept_return_rcv');
						$message = str_replace('[username]', $fullname, $message);
						$message = str_replace('[order]', "#".$order_id, $message);
						$message = str_replace('[siteurl]', home_url(), $message);
						$message = str_replace('[formatted_shipping_address]', $order->get_formatted_shipping_address(), $message);
						$message = str_replace('[formatted_billing_address]', $order->get_formatted_billing_address(), $message);
						$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
						$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));

						$html_content = '<html>
						<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
						</head>
						<body>
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="text-align: center; margin-top: 30px; padding: 10px; color: #99B1D8; font-size: 12px;">
										'.$mail_header.'
									</td>
								</tr>	
								<tr>
									<td>
										<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; font-family:Open Sans; max-width: 600px; width: 100%;">
											<tr>
												<td style="padding: 36px 48px; width: 100%; background-color:#557DA1;color: #fff; font-size: 30px; font-weight: 300; font-family:helvetica;">'.$subject.'</td>
											</tr>
											<tr>
												<td style="width:100%; padding: 36px 48px 10px; background-color:#fdfdfd; font-size: 14px; color: #737373;">'.$message.'</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="text-align: center; margin-top: 30px; color: #99B1D8; font-size: 12px;"> 
										'.$mail_footer.'
									</td>
								</tr>
							</table>

						</body>
						</html>';
					
						wc_mail( $to, $subject, $html_content, $headers );
						$mwb_wrffm_vendor_data = get_userdata( $mwb_wrffm_vendor_id );
						$mwb_wrffm_vendor_email = $mwb_vendor_data->data->user_email;
						wc_mail($mwb_wrffm_vendor_email, $subject, $html_content , $headers);
					}

					$mwb_wrffm_enable_return_ship_label =get_option('mwb_wrffm_enable_return_ship_label', 'no');
					if($mwb_wrffm_enable_return_ship_label == 'on')
					{
						if( WC()->version < "3.0.0" )
						{
							$order_id=$order->id;
						}
						else
						{
							$order_id=$order->get_id();
						}
						$mwb_wrffm_shiping_address = $order->get_formatted_shipping_address();
						if($mwb_wrffm_shiping_address == '')
						{
							$mwb_wrffm_shiping_address = $order->get_formatted_billing_address();
						}

						$headers = array();
						$headers[] = "Content-Type: text/html; charset=UTF-8";
						$to = get_post_meta($order_id, '_billing_email', true);
						$subject = get_option('mwb_wrffm_return_slip_mail_subject');
						$subject = str_replace('[order]', "#".$order_id, $subject);
						
						$message = get_option('mwb_wrffm_return_ship_template');
						$message = str_replace('[username]', $fullname, $message);
						$message = str_replace('[order]', "#".$order_id, $message);
						$message = str_replace('[siteurl]', home_url(), $message);
						$message = str_replace('[Tracking_Id]', "ID#".$order_id, $message);
						$message = str_replace('[Order_shipping_address]', $mwb_wrffm_shiping_address, $message);
						$message = str_replace('[formatted_shipping_address]', $order->get_formatted_shipping_address(), $message);
						$message = str_replace('[formatted_billing_address]', $order->get_formatted_billing_address(), $message);
						
						$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
						$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));

						if($message == '')
						{

						}
						
						$mwb_wrffm_targetPath = get_post_meta($order_id,'mwb_return_label_attachment_path',true);
						wc_mail( $to, $subject, $message, $headers, $mwb_wrffm_targetPath);
					}
					
					$store_name = $WCFM->wcfm_vendor_support->wcfm_get_vendor_store_name_by_vendor( absint($mwb_wrffm_vendor_id) );
					$status = $order->update_status('wc-return-approved', __('User Request of Refund Product is approved by vendor store : '.$store_name.'. ','woocommerce-rma-for-wcfm'));
					$order->update_status('wc-return-approved', __('User Request of Refund Product is approved by vendor store : '.$store_name.'. ','woocommerce-rma-for-wcfm'));
					$response['response'] = 'success';
					echo json_encode($response);
					die;
				}
			}
		}

		/**
		 * This function is managing refund approved product stock.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 * @since    1.0.0
		*/
		public function mwb_wrffm_vendor_manage_stock_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) 
			{
				$order_id  = isset($_POST['order_id']) ? sanitize_text_field($_POST['order_id']) : 0 ;
				$mwb_wrffm_vendor_id = get_current_user_id();

				if($order_id > 0)
				{
					$mwb_wrffm_type = isset($_POST['type']) ? sanitize_text_field($_POST['type']) : '' ;

					if($mwb_wrffm_type != '')
					{
						if($mwb_wrffm_type == 'mwb_wrffm_return_stock')
						{ 
							$manage_stock = get_option('mwb_wrffm_return_request_manage_stock');
							if($manage_stock == "yes")
							{
								$mwb_wrffm_return_data = get_post_meta($order_id, 'mwb_wrffm_return_product', true);
								if(is_array($mwb_wrffm_return_data) && !empty($mwb_wrffm_return_data))
								{
									foreach ($mwb_wrffm_return_data as $date => $requested_data) {
										$mwb_wrffm_returned_products = $requested_data['products'];
										if(is_array($mwb_wrffm_returned_products) && !empty($mwb_wrffm_returned_products))
										{
											foreach ($mwb_wrffm_returned_products as $key => $product_data) 
											{
												if(in_array($product_data['product_id'], mwb_wrffm_get_vendor_product()) || in_array($product_data['variation_id'], mwb_wrffm_get_vendor_product()))
												{
													if($product_data['variation_id'] > 0)
													{
														$product =wc_get_product($product_data['variation_id']);
													}
													else
													{
														$product =wc_get_product($product_data['product_id']);
													}
													if($product->managing_stock())
													{
														$avaliable_qty = $product_data['qty'];
														if( WC()->version < '3.0.0' )
														{
															$product->set_stock( $avaliable_qty, 'add' );						
														}
														else
														{
															if($product_data['variation_id'] > 0)
															{
																$total_stock = get_post_meta($product_data['variation_id'],'_stock',true);
																$total_stock = $total_stock + $avaliable_qty;
																wc_update_product_stock( $product_data['variation_id'],$total_stock, 'set' );
															}
															else
															{
																$total_stock = get_post_meta($product_data['product_id'],'_stock',true);
																$total_stock = $total_stock + $avaliable_qty;
																wc_update_product_stock( $product_data['product_id'],$total_stock, 'set' );
															}
														}
														update_post_meta($order_id,'mwb_wrffm_manage_stock_for_return_'.$mwb_wrffm_vendor_id,'no');
														$response['result'] = 'success';
														$response['msg'] = __('Product Stock is updated Succesfully.','woocommerce-rma-for-wcfm');
													}
													else
													{
														$response['result'] = false;
														$response['msg'] = __('Product Stock is not updated as manage stock setting of product is disable.','woocommerce-rma-for-wcfm');
													}
												}
											}
										}
									}
								}
							}
						}
						else
						{
							$manage_stock = get_option('mwb_wrffm_exchange_request_manage_stock');
							if($manage_stock == "yes")
							{
								$mwb_wrffm_exchange_deta = get_post_meta($order_id, 'mwb_wrffm_exchange_product', true);
								if(is_array($mwb_wrffm_exchange_deta) && !empty($mwb_wrffm_exchange_deta))
								{
									foreach ($mwb_wrffm_exchange_deta as $date => $requested_data) 
									{
										$mwb_wrffm_exchanged_products = $requested_data['from'];
										if(is_array($mwb_wrffm_exchanged_products) && !empty($mwb_wrffm_exchanged_products))
										{
											foreach ($mwb_wrffm_exchanged_products as $key => $product_data) 
											{
												if(in_array($product_data['product_id'], mwb_wrffm_get_vendor_product()) || in_array($product_data['variation_id'], mwb_wrffm_get_vendor_product()))
												{
													if($product_data['variation_id'] > 0)
													{
														$product =wc_get_product($product_data['variation_id']);
													}
													else
													{
														$product =wc_get_product($product_data['product_id']);
													}
													if($product->managing_stock())
													{
														$avaliable_qty = $product_data['qty'];
														if( WC()->version < '3.0.0' )
														{
															$product->set_stock( $avaliable_qty, 'add' );						
														}
														else
														{
															if($product_data['variation_id'] > 0)
															{
																$total_stock = get_post_meta($product_data['variation_id'],'_stock',true);
																$total_stock = $total_stock + $avaliable_qty;
																wc_update_product_stock( $product_data['variation_id'],$total_stock, 'set' );
															}
															else
															{
																$total_stock = get_post_meta($product_data['product_id'],'_stock',true);
																$total_stock = $total_stock + $avaliable_qty;
																wc_update_product_stock( $product_data['product_id'],$total_stock, 'set' );
															}
														}
														update_post_meta($order_id,'mwb_wrffm_manage_stock_for_exchange_','no');
														$response['result'] = true;
														$response['msg'] = __('Product Stock is updated Succesfully.','woocommerce-rma-for-wcfm');
													}
													else
													{
														$response['result'] = false;
														$response['msg'] = __('Product Stock is not updated as manage stock setting of product is disable.','woocommerce-rma-for-wcfm');
													}
												}
											}
										}
									}
								}
							}
						}	
					}

				}
				echo json_encode($response);
				die();
			}
		}

		/**
		 * This function is add extra fee to exchange product by Vendor
		 * 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_exchange_fee_add_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) {
				$orderid = $_POST['orderid'];
				$pending_date = $_POST['date'];
				$fees = array();
				if(isset( $_POST['fees']))
				{	
					$fees = $_POST['fees'];
					if(isset($fees))
					{
						foreach($fees as $k=>$fee)
						{
							if($fee['text'] == '' || $fee['val'] == '')
							{
								unset($fees[$k]);
							}
						}
					}
				}	
				$exchange_details = get_post_meta($orderid, 'mwb_wrffm_exchange_product', true);
				if(isset($exchange_details[$pending_date]))
				{
					if(isset($exchange_details[$pending_date]['fee']))
					{
						$added_fees = $exchange_details[$pending_date]['fee'];
					}	
					else
					{
						$added_fees = array();
					}	 
				}
				$exist = true;
				if(isset($added_fees) && !empty($added_fees))
				{
					foreach($added_fees as $date=>$added_fee)
					{
						if($date == $pending_date)
						{
							$exchange_details[$pending_date]['fee'] = $fees;
							$exist = false;
							break;
						}
					}
				}
			
				if($exist)
				{
					$exchange_details[$pending_date]['fee'] = $fees;
				}
				
				update_post_meta($orderid, 'mwb_wrffm_exchange_product', $exchange_details);
				$response['response'] = 'success';
				echo json_encode($response);
				die;
			}
		}

		

		/**
		 * This function is used for cancel refund request.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 * @since    1.0.0
		*/
		public function mwb_wrffm_cancel_return_request_vendor_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) 
			{
				$orderid = intval($_POST['orderid']);
				$date = sanitize_text_field($_POST['date']);
				$mwb_wrffm_vendor_id = get_current_user_id();
				$products = get_post_meta($orderid, 'mwb_wrffm_return_product', true);

						//Fetch the return request product
				if(isset($products) && !empty($products))
				{
					foreach($products as $date=>$product)
					{
						if($product['status'] == 'pending')
						{
							$product_datas = $product['products'];
							$product['status'] ='wc-return-cancelled';
							break;
						}
					}
				}
			
						//Update the status
				update_post_meta($orderid, 'mwb_wrffm_request_rejected'.$mwb_wrffm_vendor_id,'1');

				$order = wc_get_order($orderid);
				$fname = get_option('mwb_wrffm_notification_from_name');
				$fmail = get_option('mwb_wrffm_notification_from_mail');

				$headers[] = "From: $fname <$fmail>";
				$headers[] = "Content-Type: text/html; charset=UTF-8";
				$to = get_post_meta($orderid, '_billing_email', true);
				$subject = get_option('mwb_wrffm_notification_return_cancel_subject', false);
				$message = stripslashes(get_option('mwb_wrffm_notification_return_cancel', false));
			
				$order_id = $orderid;
				$fname = get_post_meta($orderid, '_billing_first_name', true);
				$lname = get_post_meta($orderid, '_billing_last_name', true);
				$billing_company = get_post_meta($order_id, '_billing_company', true);
				$billing_email = get_post_meta($order_id, '_billing_email', true);
				$billing_phone = get_post_meta($order_id, '_billing_phone', true);
				$billing_country = get_post_meta($order_id, '_billing_country', true);
				$billing_address_1 = get_post_meta($order_id, '_billing_address_1', true);
				$billing_address_2 = get_post_meta($order_id, '_billing_address_2', true);
				$billing_state = get_post_meta($order_id, '_billing_state', true);
				$billing_postcode = get_post_meta($order_id, '_billing_postcode', true);
				$shipping_first_name = get_post_meta($order_id, '_shipping_first_name', true);
				$shipping_last_name = get_post_meta($order_id, '_shipping_last_name', true);
				$shipping_company = get_post_meta($order_id, '_shipping_company', true);
				$shipping_country = get_post_meta($order_id, '_shipping_country', true);
				$shipping_address_1 = get_post_meta($order_id, '_shipping_address_1', true);
				$shipping_address_2 = get_post_meta($order_id, '_shipping_address_2', true);
				$shipping_city = get_post_meta($order_id, '_shipping_city', true);
				$shipping_state = get_post_meta($order_id, '_shipping_state', true);
				$shipping_postcode = get_post_meta($order_id, '_shipping_postcode', true);
				$payment_method_tittle = get_post_meta($order_id, '_payment_method_title', true);
				$order_shipping = get_post_meta($order_id, '_order_shipping', true);
				$order_total = get_post_meta($order_id, '_order_total', true);
				$refundable_amount = get_post_meta($order_id, 'refundable_amount', true);

				$fullname = $fname." ".$lname;

				$message = str_replace('[username]', $fullname, $message);
				$message = str_replace('[order]', "#".$orderid, $message);
				$message = str_replace('[siteurl]', home_url(), $message);
				$message = str_replace('[_billing_company]', $billing_company, $message);
				$message = str_replace('[_billing_email]', $billing_email, $message);
				$message = str_replace('[_billing_phone]', $billing_phone, $message);
				$message = str_replace('[_billing_country]', $billing_country, $message);
				$message = str_replace('[_billing_address_1]', $billing_address_1, $message);
				$message = str_replace('[_billing_address_2]', $billing_address_2, $message);
				$message = str_replace('[_billing_state]', $billing_state, $message);
				$message = str_replace('[_billing_postcode]', $billing_postcode, $message);
				$message = str_replace('[_shipping_first_name]', $shipping_first_name, $message);
				$message = str_replace('[_shipping_last_name]', $shipping_last_name, $message);
				$message = str_replace('[_shipping_company]', $shipping_company, $message);
				$message = str_replace('[_shipping_country]', $shipping_country, $message);
				$message = str_replace('[_shipping_address_1]', $shipping_address_1, $message);
				$message = str_replace('[_shipping_address_2]', $shipping_address_2, $message);
				$message = str_replace('[_shipping_city]', $shipping_city, $message);
				$message = str_replace('[_shipping_state]', $shipping_state, $message);
				$message = str_replace('[_shipping_postcode]', $shipping_postcode, $message);
				$message = str_replace('[_payment_method_tittle]', $payment_method_tittle, $message);
				$message = str_replace('[_order_shipping]', $order_shipping, $message);
				$message = str_replace('[_order_total]', $order_total, $message);
				$message = str_replace('[_refundable_amount]', $refundable_amount, $message);

				$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
				$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));

				$template = get_option('mwb_wrffm_notification_return_cancel_template','no');

				if(isset($template) && $template == 'on')
				{

					$html_content = $message;
				}
				else
				{
					$html_content = '<html>
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
					</head>
					<body>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td style="text-align: center; margin-top: 30px; padding: 10px; color: #99B1D8; font-size: 12px;">
									'.$mail_header.'
								</td>
							</tr>
							<tr>
								<td>
									<table align="center" cellpadding="0" cellspacing="0" style="border-collapse: collapse; font-family:Open Sans; max-width: 600px; width: 100%;">
										<tr>
											<td style="padding: 36px 48px; width: 100%; background-color:#557DA1;color: #fff; font-size: 30px; font-weight: 300; font-family:helvetica;">'.$subject.'</td>
										</tr>
										<tr>
											<td style="width:100%; padding: 36px 48px 10px; background-color:#fdfdfd; font-size: 14px; color: #737373;">'.$message.'</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td style="text-align: center; margin-top: 30px; color: #99B1D8; font-size: 12px;">
									'.$mail_footer.'
								</td>
							</tr>
						</table>
					</body>
					</html>';
				}

				wc_mail($to, $subject, $html_content, $headers);

				$headers = array();
				$headers[] = "Content-Type: text/html; charset=UTF-8";
				$to = get_option('mwb_wrffm_notification_from_mail');

				$message1 = '<html>
					<body>
						<style>
							body {
								box-shadow: 2px 2px 10px #ccc;
								color: #767676;
								font-family: Arial,sans-serif;
								margin: 80px auto;
								max-width: 700px;
								padding-bottom: 30px;
								width: 100%;
							}

							h2 {
								font-size: 30px;
								margin-top: 0;
								color: #fff;
								padding: 40px;
								background-color: #557da1;
							}

							h4 {
								color: #557da1;
								font-size: 20px;
								margin-bottom: 10px;
							}

							.content {
								padding: 0 40px;
							}

							.Customer-detail ul li p {
								margin: 0;
							}

							.details .Shipping-detail {
								width: 40%;
								float: right;
							}

							.details .Billing-detail {
								width: 60%;
								float: left;
							}

							.details .Shipping-detail ul li,.details .Billing-detail ul li {
								list-style-type: none;
								margin: 0;
							}

							.details .Billing-detail ul,.details .Shipping-detail ul {
								margin: 0;
								padding: 0;
							}

							.clear {
								clear: both;
							}

							table,td,th {
								border: 2px solid #ccc;
								padding: 15px;
								text-align: left;
							}

							table {
								border-collapse: collapse;
								width: 100%;
							}

							.info {
								display: inline-block;
							}

							.bold {
								font-weight: bold;
							}

							.footer {
								margin-top: 30px;
								text-align: center;
								color: #99B1D8;
								font-size: 12px;
							}
							dl.variation dd {
								font-size: 12px;
								margin: 0;
							}
						</style>

						<div style="text-align: center; padding: 10px;" class="header">
							'.$mail_header.'
						</div>		

						<div class="header">
							<h2>'.__('Your Refund Request is Cancelled', 'woocommerce-rma-for-wcfm').'</h2>
						</div>
						<div class="content">
							
							<div class="Order">
								<h4>Order #'.$orderid.'</h4>
								<table>
									<tbody>
										<tr>
											<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
										</tr>';
									
										$order = wc_get_order($orderid);
									
										$requested_products = $products[$date]['products'];
								
										if(isset($requested_products) && !empty($requested_products))
										{
											$total = 0;
											$mwb_get_refnd = get_post_meta( $orderid, 'mwb_wrffm_return_product',true );
											if( !empty( $mwb_get_refnd ) )
											{
												foreach ($mwb_get_refnd as $key => $value) 
												{
													if( isset( $value['amount'] ) )
													{
														$total_price = $value['amount'];
														break;
													}
												}
											}
											foreach( $order->get_items() as $item_id => $item )
											{
												$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
												foreach($requested_products as $requested_product)
												{
													if($item_id == $requested_product['item_id'])
													{

														if(isset($requested_product['variation_id']) && $requested_product['variation_id'] > 0)
														{
															$prod = wc_get_product($requested_product['variation_id']);

														}
														else
														{
															$prod = wc_get_product($requested_product['product_id']);
														}
														if(in_array($prod->get_id(), mwb_wrffm_get_vendor_product()))
														{

															$prod_price = wc_get_price_excluding_tax($prod, array( 'qty' => 1 ));
															$subtotal = $requested_product['price']*$requested_product['qty'];
															$total += $subtotal;
															if( WC()->version < "3.1.0" )
															{
																$item_meta      = new WC_Order_Item_Meta( $item, $product );
																$item_meta_html = $item_meta->display( true, true );
															}
															else
															{
																$item_meta      = new WC_Order_Item_Product( $item, $product );
																$item_meta_html = wc_display_item_meta($item_meta,array('echo'=> false));
															}

															$message1 .= '<tr>
															<td>'.$item['name'].'<br>';
																$message1 .= '<small>'.$item_meta_html.'</small>
																<td>'.$requested_product['qty'].'</td>
																<td>'.mwb_wrffm_format_price($requested_product['price']*$requested_product['qty']).'</td>
															</tr>';
														}
													}
												}
											}	
											$total = mwb_wrffm_refund_policy_price_deduction($total);		
											$message1 .= '<tr>
											<th colspan="2">Total:</th>
											<td>'.mwb_wrffm_format_price($total).'</td>
										</tr>
										<tr>
											<th colspan="3">Extra:</th>
										</tr>';
									}
									if( WC()->version < "3.0.0" )
									{
										$orderid=$order->id;
									}
									else
									{
										$orderid=$order->get_id();
									}
									$added_fees = get_post_meta($orderid, 'mwb_wrffm_return_added_fee', true);
									if(isset($added_fees) && !empty($added_fees))
									{
										foreach( $added_fees as $da=>$added_fee )
										{
											if($date == $da)
											{ 
												foreach($added_fee as $fee)
												{
													$total -= $fee['val'];

													$message .= ' <tr>
													<th colspan="2">'.$fee['text'].':</th>
													<td>'.mwb_wrffm_format_price($fee['val']).'</td>
												</tr>';
											}
										}
									}
								}
								if( WC()->version < "3.0.0" )
								{
									$orderid=$order->id;
								}
								else
								{
									$orderid=$order->get_id();
								}
								$message1 .= ' <tr>
								<th colspan="2">'.__('Refund Total', 'woocommerce-rma-for-wcfm').':</th>
											<td>'.mwb_wrffm_format_price($total).'</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="Customer-detail">
								<h4>'.__('Customer details', 'woocommerce-rma-for-wcfm').'</h4>
								<ul>
									<li>
										<p class="info">
											<span class="bold">'.__('Email','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($orderid, '_billing_email', true).'
										</p>
									</li>
									<li>
										<p class="info">
											<span class="bold">'.__('Tel','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($orderid, '_billing_phone', true).'
										</p>
									</li>
								</ul>
							</div>
							<div class="details">
								<div class="Shipping-detail">
									<h4>'.__('Shipping Address', 'woocommerce-rma-for-wcfm').'</h4>
									'.$order->get_formatted_shipping_address().'
								</div>
								<div class="Billing-detail">
									<h4>'.__('Billing Address', 'woocommerce-rma-for-wcfm').'</h4>
									'.$order->get_formatted_billing_address().'
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div style="text-align: center; padding: 10px;" class="footer">
							'.$mail_footer.'
						</div>
					</body>
					</html>';

					
				$subject= __('Refund Request Cancelled By Vendor','woocommerce-rma-for-wcfm');
				wc_mail( $to, $subject, $message1, $headers );
				$order->update_status('partial-cancel', __('User Request of Refund Product is cancelled by vendor(s)','woocommerce-rma-for-wcfm'));
				$response['response'] = 'success';
				echo json_encode($response);
				die;
			}
		}

		/**
		 * This function is approving exchange request.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 * @since    1.0.0
		*/
		public function mwb_wrffm_vendor_exchange_req_approve_callback()
		{
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) 
			{
				$orderid = intval($_POST['orderid']);
				$checkdate = sanitize_text_field($_POST['date']);

				$exchange_details = get_post_meta($orderid, 'mwb_wrffm_exchange_product', true);

				if(isset($exchange_details) && !empty($exchange_details))
				{
					foreach($exchange_details as $date=>$exchange_detail)
					{
						if($exchange_detail['status'] == 'pending')
						{
							$exchanged_products = $exchange_detail['to'];
							$exchanged_from_products = $exchange_detail['from'];
							if(isset($exchange_detail['fee']))
								$added_fee = $exchange_detail['fee'];
							$exchange_details[$date]['status'] = 'complete';
							$exchange_details[$date]['approve'] = date('d-m-Y');
							break;
						}
					}
				}
				$order_detail = wc_get_order($orderid);

				$includeTax = isset($order_detail->prices_include_tax) ? $order_detail->prices_include_tax : false;
				$user_id = $order_detail->user_id;

				$order_data = array(
						'post_name'     => 'order-' . date('M-d-Y-hi-a'), //'order-jun-19-2014-0648-pm'
						'post_type'     => 'shop_order',
						'post_title'    => 'Order &ndash; ' . date('F d, Y @ h:i A'), //'June 19, 2014 @ 07:19 PM'
						'post_status'   => 'wc-exchange-approve',
						'ping_status'   => 'closed',
						'post_excerpt'  => 'requested',
						'post_author'   => $user_id,
						'post_password' => uniqid( 'order_' ),
						'post_date'     => date('Y-m-d H:i:s e'), 
						'comment_status' => 'open'
						);

				$order_id = wp_insert_post( $order_data, true );


				$approve = get_option('mwb_wrffm_notification_exchange_approve');
				$fname = get_post_meta($orderid, '_billing_first_name', true);
				$lname = get_post_meta($orderid, '_billing_last_name', true);

				$fullname = $fname." ".$lname;

				$approve = str_replace('[username]', $fullname, $approve);
				$approve = str_replace('[order]', "#".$orderid, $approve);
				$approve = str_replace('[siteurl]', home_url(), $approve);
				$message = stripslashes(get_option('mwb_wrffm_notification_exchange_approve', false));
				$fname = get_post_meta($orderid, '_billing_first_name', true);
				$lname = get_post_meta($orderid, '_billing_last_name', true);
				$billing_company = get_post_meta($orderid, '_billing_company', true);
				$billing_email = get_post_meta($orderid, '_billing_email', true);
				$billing_phone = get_post_meta($orderid, '_billing_phone', true);
				$billing_country = get_post_meta($orderid, '_billing_country', true);
				$billing_address_1 = get_post_meta($orderid, '_billing_address_1', true);
				$billing_address_2 = get_post_meta($orderid, '_billing_address_2', true);
				$billing_state = get_post_meta($orderid, '_billing_state', true);
				$billing_postcode = get_post_meta($orderid, '_billing_postcode', true);
				$shipping_first_name = get_post_meta($orderid, '_shipping_first_name', true);
				$shipping_last_name = get_post_meta($orderid, '_shipping_last_name', true);
				$shipping_company = get_post_meta($orderid, '_shipping_company', true);
				$shipping_country = get_post_meta($orderid, '_shipping_country', true);
				$shipping_address_1 = get_post_meta($orderid, '_shipping_address_1', true);
				$shipping_address_2 = get_post_meta($orderid, '_shipping_address_2', true);
				$shipping_city = get_post_meta($orderid, '_shipping_city', true);
				$shipping_state = get_post_meta($orderid, '_shipping_state', true);
				$shipping_postcode = get_post_meta($orderid, '_shipping_postcode', true);
				$payment_method_tittle = get_post_meta($orderid, '_payment_method_title', true);
				$order_shipping = get_post_meta($orderid, '_order_shipping', true);
				$order_total = get_post_meta($orderid, '_order_total', true);
				$refundable_amount = get_post_meta($orderid, 'refundable_amount', true);

				$fullname = $fname." ".$lname;

				$message = str_replace('[username]', $fullname, $message);
				$message = str_replace('[order]', "#".$orderid, $message);
				$message = str_replace('[siteurl]', home_url(), $message);
				$message = str_replace('[_billing_company]', $billing_company, $message);
				$message = str_replace('[_billing_email]', $billing_email, $message);
				$message = str_replace('[_billing_phone]', $billing_phone, $message);
				$message = str_replace('[_billing_country]', $billing_country, $message);
				$message = str_replace('[_billing_address_1]', $billing_address_1, $message);
				$message = str_replace('[_billing_address_2]', $billing_address_2, $message);
				$message = str_replace('[_billing_state]', $billing_state, $message);
				$message = str_replace('[_billing_postcode]', $billing_postcode, $message);
				$message = str_replace('[_shipping_first_name]', $shipping_first_name, $message);
				$message = str_replace('[_shipping_last_name]', $shipping_last_name, $message);
				$message = str_replace('[_shipping_company]', $shipping_company, $message);
				$message = str_replace('[_shipping_country]', $shipping_country, $message);
				$message = str_replace('[_shipping_address_1]', $shipping_address_1, $message);
				$message = str_replace('[_shipping_address_2]', $shipping_address_2, $message);
				$message = str_replace('[_shipping_city]', $shipping_city, $message);
				$message = str_replace('[_shipping_state]', $shipping_state, $message);
				$message = str_replace('[_shipping_postcode]', $shipping_postcode, $message);
				$message = str_replace('[_payment_method_tittle]', $payment_method_tittle, $message);
				$message = str_replace('[_order_shipping]', $order_shipping, $message);
				$message = str_replace('[_order_total]', $order_total, $message);
				$message = str_replace('[_refundable_amount]', $refundable_amount, $message);
				$mwb_wrffm_odr = wc_get_order($orderid);
				$message = str_replace('[formatted_shipping_address]', $mwb_wrffm_odr->get_formatted_shipping_address(), $message);
				$message = str_replace('[formatted_billing_address]', $mwb_wrffm_odr->get_formatted_billing_address(), $message);

				$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
				$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));
				$mwb_wrffm_notification_exchange_approve_template =get_option('mwb_wrffm_notification_exchange_approve_template','no');
				$mwb_dis_tot = 0;
				if(isset($mwb_wrffm_notification_exchange_approve_template) && $mwb_wrffm_notification_exchange_approve_template == 'on')
				{
					$html_content = $message;
				}
				else
				{
					$html_content = '<html>
					<body>
						<style>
							body {
								box-shadow: 2px 2px 10px #ccc;
								color: #767676;
								font-family: Arial,sans-serif;
								margin: 80px auto;
								max-width: 700px;
								padding-bottom: 30px;
								width: 100%;
							}

							h2 {
								font-size: 30px;
								margin-top: 0;
								color: #fff;
								padding: 40px;
								background-color: #557da1;
							}

							h4 {
								color: #557da1;
								font-size: 20px;
								margin-bottom: 10px;
							}

							.content {
								padding: 0 40px;
							}

							.Customer-detail ul li p {
								margin: 0;
							}

							.details .Shipping-detail {
								width: 40%;
								float: right;
							}

							.details .Billing-detail {
								width: 60%;
								float: left;
							}

							.details .Shipping-detail ul li,.details .Billing-detail ul li {
								list-style-type: none;
								margin: 0;
							}

							.details .Billing-detail ul,.details .Shipping-detail ul {
								margin: 0;
								padding: 0;
							}

							.clear {
								clear: both;
							}

							table,td,th {
								border: 2px solid #ccc;
								padding: 15px;
								text-align: left;
							}

							table {
								border-collapse: collapse;
								width: 100%;
							}

							.info {
								display: inline-block;
							}

							.bold {
								font-weight: bold;
							}

							.footer {
								margin-top: 30px;
								text-align: center;
								color: #99B1D8;
								font-size: 12px;
							}
							dl.variation dd {
								font-size: 12px;
								margin: 0;
							}
						</style>
						<div class="header" style="text-align: center; padding: 10px;">
							'.$mail_header.'
						</div>

						<div class="header">
							<h2>'.__('Your Exchange Request is Accepted.', 'woocommerce-rma-for-wcfm').'</h2>
						</div>

						<div class="content">
							<div class="reason">
								<p>'.$html_content.'</p>
							</div>
							<div class="Order">
								<h4>Order #'.$orderid.'</h4>
								<h4>'.__('Exchanged From', 'woocommerce-rma-for-wcfm').'</h4>
								<table>
									<tbody>
										<tr>
											<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
										</tr>';
										$order = wc_get_order($orderid);
										$requested_products = $exchange_details[$date]['from'];

										if(isset($requested_products) && !empty($requested_products))
										{
											$total = 0;
											foreach( $order->get_items() as $item_id => $item )
											{
												$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
												foreach($requested_products as $requested_product)
												{
													if($item_id == $requested_product['item_id'])
													{	
														if(isset($requested_product['variation_id']) && $requested_product['variation_id'] >0)
														{
															$requested_product_obj = wc_get_product($requested_product['variation_id']);
														}
														else
														{
															$requested_product_obj = wc_get_product($requested_product['product_id']);
														}
														$subtotal = $requested_product['price']*$requested_product['qty'];
														$total += $subtotal;
														if( WC()->version < "3.1.0" )
														{
															$item_meta      = new WC_Order_Item_Meta( $item, $product );
															$item_meta_html = $item_meta->display( true, true );
														}
														else
														{
															$item_meta      = new WC_Order_Item_Product( $item, $product );
															$item_meta_html = wc_display_item_meta($item_meta,array('echo'=> false));
														}

														$html_content .= '<tr><td>'.$item['name'].'<br>';
														$html_content .= '<small>'.$item_meta_html.'</small><td>'.$requested_product['qty'].'</td><td>'.mwb_wrffm_format_price($requested_product['price']*$requested_product['qty']).'</td></tr>';
													}
												}
											}
										}
										$html_content .= '
										<tr>
											<th colspan="2">'.__('Total', 'woocommerce-rma-for-wcfm').':</th>
											<td>'.mwb_wrffm_format_price($total).'</td>
										</tr>
									</tbody>
								</table>
								<h4>'.__('Exchanged To', 'woocommerce-rma-for-wcfm').'</h4>
								<table>
									<tbody>
										<tr>
											<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
											<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
										</tr>';

										$exchanged_to_products = $exchange_details[$date]['to'];

										$total_price = 0;
										if(isset($exchanged_to_products) && !empty($exchanged_to_products))
										{
											foreach($exchanged_to_products as $key=>$exchanged_product)
											{
												$variation_attributes = array();
												if(isset($exchanged_product['variation_id']))
												{
													if($exchanged_product['variation_id'])
													{
														$variation_product = new WC_Product_Variation($exchanged_product['variation_id']);
														$variation_attributes = $variation_product->get_variation_attributes();
														$variation_labels = array();
														foreach ($variation_attributes as $label => $value){
															if(is_null($value) || $value == ''){
																$variation_labels[] = $label;
															}
														}

														if(isset($exchanged_product['variations']) && !empty($exchanged_product['variations']))
														{
															$variation_attributes = $exchanged_product['variations'];
														}
													}
												}

												if(isset($exchanged_product['p_id']))
												{
													if($exchanged_product['p_id'])
													{
														$grouped_product = new WC_Product_Grouped($exchanged_product['p_id']);
														$grouped_product_title = $grouped_product->get_title();
													}
												}

												if(isset($exchanged_product['variation_id']))
												{

													$product = wc_get_product($exchanged_product['variation_id']);
												}
												else
												{
													$product = wc_get_product($exchanged_product['id']);
												}
												$pro_price = $exchanged_product['qty']*$exchanged_product['price'];
												$total_price += $pro_price;
												$title = "";
												if(isset($exchanged_product['p_id']))
												{
													$title .= $grouped_product_title.' -> ';
												}
												$title .=$product->get_title();											

												if(isset($variation_attributes) && !empty($variation_attributes))
												{
													$title .= wc_get_formatted_variation( $variation_attributes );
												}

												$html_content .= '<tr>
												<td>'.$title.'</td>
												<td>'.$exchanged_product['qty'].'</td>
												<td>'.mwb_wrffm_format_price($pro_price).'</td>
											</tr>';

										}
									}
									$html_content .= '<tr>
									<th colspan="2">'.__('Sub-Total', 'woocommerce-rma-for-wcfm').':</th>
									<td>'.mwb_wrffm_format_price($total_price).'</td>
								</tr>';
								if(isset($added_fee) && !empty($added_fee))
								{
									if(is_array($added_fee))
									{
										foreach($added_fee as $fee)
										{
											$total_price += $fee['val'];
											$html_content .= '<tr>
											<th colspan="2">'.$fee['text'].'</th>
											<td>'.mwb_wrffm_format_price($fee['val']).'</td>
										</tr>';
									}
								}
							}
							$html_content .= '<tr>
							<th colspan="2">'.__('Grand Total', 'woocommerce-rma-for-wcfm').'</th>
							<td>'.mwb_wrffm_format_price($total_price).'</td>
						</tr>';

						$html_content .= '</tbody>
					</table>
				</div>';
				$mwb_cpn_dis = $order->get_discount_total();
				$mwb_cpn_tax = $order->get_discount_tax();

				$mwb_dis_tot = 0;
				if($total_price -  ( $total + $mwb_dis_tot ) > 0)
				{
					$extra_amount = $total_price - ( $total + $mwb_dis_tot );
					$html_content .= '<h2>'.__('Extra Amount : ', 'woocommerce-rma-for-wcfm').mwb_wrffm_format_price($extra_amount).'</h2>';
				}
				else
				{
					if( $mwb_dis_tot > $total_price )
					{
						$total_price = 0;
					}
					else
					{
						$total_price = $total_price - $mwb_dis_tot;
					}
					$left_amount = $total - $total_price;
					update_post_meta($orderid,"mwb_wrffm_left_amount",$left_amount);

					$html_content .= '<h2><i>'.__('Left Amount After Exchange : ', 'woocommerce-rma-for-wcfm').'</i>'.mwb_wrffm_format_price($left_amount).'</h2>';
				}
				$html_content .= ' <div class="Customer-detail">
						<h4>'.__('Customer details', 'woocommerce-rma-for-wcfm').'</h4>
						<ul>
							<li><p class="info">
								<span class="bold">'.__('Email','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($orderid, '_billing_email', true).'
							</p></li>
							<li><p class="info">
								<span class="bold">'.__('Tel','woocommerce-rma-for-wcfm').': </span>'.get_post_meta($orderid, '_billing_phone', true).'
							</p></li>
						</ul>
					</div>
					<div class="details">
						<div class="Shipping-detail">
							<h4>'.__('Shipping Address', 'woocommerce-rma-for-wcfm').'</h4>
							'.$order->get_formatted_shipping_address().'
						</div>
						<div class="Billing-detail">
							<h4>'.__('Billing Address', 'woocommerce-rma-for-wcfm').'</h4>
							'.$order->get_formatted_billing_address().'
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div style="text-align: center; padding: 10px;" class="footer">
					'.$mail_footer.'
				</div>
				</body>
				</html>';
				}												

				$fname = get_option('mwb_wrffm_notification_from_name');
				$fmail = get_option('mwb_wrffm_notification_from_mail');

				$headers = array();
				$headers[] = "From: $fname <$fmail>";
				$headers[] = "Content-Type: text/html; charset=UTF-8";
				$subject = get_option('mwb_wrffm_notification_exchange_approve_subject');

				$to = get_post_meta($orderid, '_billing_email', true);

				wc_mail( $to, $subject, $html_content, $headers );

				$mwb_wrffm_vendor_id = get_current_user_id();
				$mwb_wrffm_vendor_data = get_userdata( $mwb_wrffm_vendor_id );
				$mwb_wrffm_vendor_email = $mwb_wrffm_vendor_data->data->user_email;
				$mwb_wrffm_vendor_name = $mwb_wrffm_vendor_data->data->user_nicename;
				
				$headers1 = array();
				$headers1[] = "From: $mwb_wrffm_vendor_name <$mwb_wrffm_vendor_email>";
				$headers1[] = "Content-Type: text/html; charset=UTF-8";
				$to = get_option('mwb_wrffm_notification_from_mail');
				$subject= __('Exchange Request Approved By Vendor','woocommerce-rma-for-wcfm');
				
				wc_mail( $to, $subject, $html_content, $headers1 );
				
				$mwb_wrffm_enable_return_ship_label = get_option('mwb_wrffm_enable_return_ship_label', 'no');
				if($mwb_wrffm_enable_return_ship_label == 'on')
				{
					$headers = array();
					$headers[] = "Content-Type: text/html; charset=UTF-8";
					$to = get_post_meta($orderid, '_billing_email', true);
					$subject = get_option('mwb_wrffm_return_slip_mail_subject');
					$subject = str_replace('[order]', "#".$orderid, $subject);
					$mwb_wrffm_order_for_label = wc_get_order($orderid);
					$mwb_wrffm_shiping_address = $mwb_wrffm_order_for_label->get_formatted_shipping_address();
					if($mwb_wrffm_shiping_address == '')
					{
						$mwb_wrffm_shiping_address = $mwb_wrffm_order_for_label->get_formatted_billing_address();
					}
					$message = get_option('mwb_wrffm_return_ship_template');
					$message = str_replace('[username]', $fullname, $message);
					$message = str_replace('[order]', "#".$orderid, $message);
					$message = str_replace('[siteurl]', home_url(), $message);
					$message = str_replace('[Tracking_Id]', "ID#".$orderid, $message);
					$message = str_replace('[Order_shipping_address]', $mwb_wrffm_shiping_address, $message);

					$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
					$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));

					if($message == '')
					{

					}

					wc_mail( $to, $subject, $message, $headers );
				}

				update_post_meta($order_id, "mwb_wrffm_exchange_order", $orderid); 
				update_post_meta($orderid, "date-$date", $order_id);
				update_post_meta( $orderid, 'mwb_wrffm_status_exchanged', $mwb_dis_tot );
				if($left_amount > 0)
				{
					$item = new WC_Order_Item_Fee();
					$item->set_props( array(
						'name'      => esc_attr( "Refundable Amount" ),
						'tax_class' => '',
						'total'     => (float) esc_attr( $left_amount ),
						'total_tax' => $totalProducttax,
						'taxes'     => array(
							'total' => array(),
						),
						'order_id'  => $orderid,
					) );
					$item->save();
					$item_id = $order_detail->add_item( $item );
				}
				foreach( $order_detail->get_items() as $item_id => $item )
				{
					if(isset($exchanged_from_products) && !empty($exchanged_from_products))
					{	
						foreach($exchanged_from_products as $k=>$product_data)
						{
							if($item['product_id'] == $product_data['product_id'] && $item['variation_id'] == $product_data['variation_id'])
							{
								$product = apply_filters( 'woocommerce_order_item_product', $order_detail->get_product_from_item( $item ), $item );
								$item['qty'] = $item['qty'] - $product_data['qty'];
								$args['qty'] = $item['qty'];
								if( WC()->version < '3.0.0' ){
									$order->update_product( $item_id, $product, $args );
								}
								else{
									wc_update_order_item_meta( $item_id, '_qty' , $item['qty'] );

									if ( $product->backorders_require_notification() && $product->is_on_backorder( $args['qty'] ) ) {
										$item->add_meta_data( apply_filters( 'woocommerce_backordered_item_meta_name', __( 'Backordered', 'woocommerce' ) ), $args['qty'] - max( 0, $product->get_stock_quantity() ), true );
									}
									$item_data = $item->get_data();

									$price_excluded_tax = wc_get_price_excluding_tax($product, array( 'qty' => 1 ));
									$price_tax_excluded = $item_data['total']/$item_data['quantity'];

									$args['subtotal'] =$price_excluded_tax*$args['qty'];
									$args['total']	= $price_tax_excluded*$args['qty'];

									$item->set_order_id( $orderid );
									$item->set_props( $args );
									$item->save();
								}

								break;
							}
						}
					}
				}
								// $order = wc_get_order($orderid);
				$order_detail->calculate_totals();
				$order_detail->update_status("wc-completed");
				$order = (object)$order_detail->get_address('shipping');

								// Shipping info

				update_post_meta($order_id, '_customer_user', $user_id);
				update_post_meta($order_id, '_shipping_address_1', $order->address_1);
				update_post_meta($order_id, '_shipping_address_2', $order->address_2);
				update_post_meta($order_id, '_shipping_city', $order->city);
				update_post_meta($order_id, '_shipping_state', $order->state);
				update_post_meta($order_id, '_shipping_postcode', $order->postcode);
				update_post_meta($order_id, '_shipping_country', $order->country);
				update_post_meta($order_id, '_shipping_company', $order->company);
				update_post_meta($order_id, '_shipping_first_name', $order->first_name);
				update_post_meta($order_id, '_shipping_last_name', $order->last_name);

								// billing info

				$order_detail = wc_get_order($orderid);
				$order_detail->calculate_totals();
				$order = (object)$order_detail->get_address('billing');

				add_post_meta($order_id, '_billing_first_name', $order->first_name, true);
				add_post_meta($order_id, '_billing_last_name', $order->last_name, true);
				add_post_meta($order_id, '_billing_company', $order->company, true);
				add_post_meta($order_id, '_billing_address_1', $order->address_1, true);
				add_post_meta($order_id, '_billing_address_2', $order->address_2, true);
				add_post_meta($order_id, '_billing_city', $order->city, true);
				add_post_meta($order_id, '_billing_state', $order->state, true);
				add_post_meta($order_id, '_billing_postcode', $order->postcode, true);
				add_post_meta($order_id, '_billing_country', $order->country, true);
				add_post_meta($order_id, '_billing_email', $order->email, true);
				add_post_meta($order_id, '_billing_phone', $order->phone, true);

								//$exchanged_products

				$order = wc_get_order($order_id);
				if( WC()->version >= '3.0.0' )
				{
					if( !$order->get_order_key() )
					{
						update_post_meta( $order_id , '_order_key' , 'wc-'.uniqid('order_') );
					}
				}

				if(isset($exchanged_products) && !empty($exchanged_products))
				{
					foreach($exchanged_products as $exchanged_product)
					{
						if(isset($exchanged_product['variation_id']))
						{
							$product = wc_get_product($exchanged_product['variation_id']);
							$variation_product = new WC_Product_Variation($exchanged_product['variation_id']);
							$variation_attributes = $variation_product->get_variation_attributes();
							if(isset($exchanged_product['variations']) && !empty($exchanged_product['variations']))
							{
								$variation_attributes = $exchanged_product['variations'];
							}

							$variation_product_price = wc_get_price_excluding_tax($variation_product, array( 'qty' => 1 ));	

							$variation_att['variation'] = $variation_attributes;

							$variation_att['totals']['subtotal'] = $exchanged_product['qty'] * $variation_product_price;
							$variation_att['totals']['total'] = $exchanged_product['qty'] * $variation_product_price;
							$variation_att['totals']['subtotal'] = $exchanged_product['qty'] * $variation_product_price;
							$variation_att['totals']['total'] = $exchanged_product['qty'] * $variation_product_price;

							$item_id = $order->add_product($variation_product, $exchanged_product['qty'], $variation_att);

							if($product->managing_stock())
							{
								$qty       = $exchanged_product['qty'];
							}

						}
						elseif(isset($exchanged_product['id']))
						{
							$product = wc_get_product($exchanged_product['id']);
							$item_id = $order->add_product($product, $exchanged_product['qty']);

							if($product->managing_stock())
							{
								$qty       = $exchanged_product['qty'];
							}
						}
						else
						{
							$product = wc_get_product($exchanged_product['id']);
							$item_id = $order->add_product($product, $exchanged_product['qty']);
							if($product->managing_stock())
							{
								$qty       = $exchanged_product['qty'];
								
							}
						}
					}
				}

				if(isset($added_fee) && !empty($added_fee))
				{
					if(is_array($added_fee))
					{
						foreach($added_fee as $fee)
						{
							$item = new WC_Order_Item_Fee();
							$item->set_props( array(
								'name'      => esc_attr( $fee['text'] ),
								'tax_class' => '',
								'total'     => (float) esc_attr( $fee['val'] ),
								'total_tax' => $new_fee->tax_class !== '0',
								'taxes'     => array(
									'total' => array(),
								),
								'order_id'  => $order_id,
							) );
							$item->save();
							$item_id = $order->add_item( $item );
						}
					}
				}
				$discount = 0;

				if(isset($exchanged_from_products) && !empty($exchanged_from_products))
				{
					$totalProducttax = '';
					$exchanged_from_products_count = count($exchanged_from_products);
					$l_amount = $left_amount/$exchanged_from_products_count;
					foreach($exchanged_from_products as $exchanged_product)
					{
						if(isset($exchanged_product['variation_id']) && $exchanged_product['variation_id'] > 0)
						{
							$p = wc_get_product($exchanged_product['variation_id']);
						}
						else
						{
							$p = wc_get_product($exchanged_product['product_id']);
						}
						if($includeTax){
							$_tax = new WC_Tax();

							$prePrice = $p->get_price_excluding_tax();
							$pTax = $exchanged_product['qty'] * ($p->get_price()-$prePrice);
							$totalProducttax += $pTax;
							$item_rate = round(array_shift($rates));
							$price = $exchanged_product['qty'] * $prePrice;
							$discount += $price;
						}else{
							$price = $exchanged_product['qty'] * $exchanged_product['price'];
							$discount += $price;
						}
					}
				}

				if($left_amount > 0)
				{
					$mwb_wrffm_obj = $order;
					$amount_discount = $mwb_wrffm_obj->calculate_totals();
					$total_ptax = $mwb_wrffm_obj->get_total_tax();
					$amount_discount = $amount_discount - $total_ptax;

					$new_fee  = new WC_Order_Item_Fee();
					$new_fee->set_name(esc_attr( "Discount" )) ;
					$new_fee->set_total(-$amount_discount);
					$new_fee->set_tax_class('');
					$new_fee->set_tax_status('none');
					$new_fee->set_total_tax(0);
					$new_fee->save();
					$item_id = $order->add_item($new_fee);

				}
				else
				{
					if($discount > 0)
					{

						$mwb_wrffm_obj = wc_get_order( $orderid );
						$tax_rate = 0;
						$tax = new WC_Tax();
						$country_code = $mwb_wrffm_obj->get_billing_country(); // or populate from order to get applicable rates
						$rates = $tax->find_rates( array( 'country' => $country_code ) );
						foreach( $rates as $rate ){
							$tax_rate = $rate['rate'];
						}

						$total_ptax = $discount*$tax_rate/100;
						$mwb_wrffm_woo_tax_enable = get_option('woocommerce_calc_taxes');
						$mwb_wrffm_woo_tax_option = get_option('woocommerce_prices_include_tax');
						$mwb_exchange_tax_include = get_option('mwb_wrffm_exchange_tax_enable');
						if($mwb_wrffm_woo_tax_option == 'no' && $mwb_wrffm_woo_tax_enable == "yes")
						{
							$orderval = $discount;
						}
						else
						{
							if($mwb_exchange_tax_include == 'no' && $mwb_wrffm_woo_tax_option == 'yes')
							{
								$orderval = $discount;
							}
							else
							{
								$orderval = $discount+$total_ptax;
							}
						}
						$orderval = round($orderval,2);
						$finaldiscount = 0;
						if( WC()->version < "3.7.0" ) {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_used_coupons();
						} else {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_coupon_codes();
						}
						// Coupons used in the order LOOP (as they can be multiple)
						foreach( $mwb_ord_cpn as $coupon_name ){
							$coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
							$coupon_id = $coupon_post_obj->ID;
							$coupons_obj = new WC_Coupon($coupon_id);

							$coupons_amount = $coupons_obj->get_amount();
							$coupons_type = $coupons_obj->get_discount_type();
							if($coupons_type == 'percent'){
								$finaldiscount = $orderval*$coupons_amount/100;
							}

						}


						$discount = $orderval - $finaldiscount;
						$discount = $discount*100/(100+$tax_rate);

						$new_fee  = new WC_Order_Item_Fee();
						$new_fee->set_name(esc_attr( "Discount" )) ;
						$new_fee->set_total(-$discount);
						$new_fee->set_tax_class('');
						$new_fee->set_tax_status('none');
						$new_fee->set_total_tax(0);
						$new_fee->save();
						$order->add_item($new_fee);
						$items_key = $new_fee->get_id();
					}
				}

				$order_total = $order->calculate_totals();
				$order->set_total($order_total, 'total');


				if($order_total == 0)
				{
					$order->update_status("wc-processing");
					$manage_stock = get_option('mwb_wrffm_exchange_request_manage_stock');
					if($manage_stock == "yes")
					{
						if(isset($exchanged_products) && !empty($exchanged_products))
						{
							foreach($exchanged_products as $key=>$prod_data)
							{
								if($prod_data['variation_id'] > 0)
								{
									$product =wc_get_product($prod_data['variation_id']);
								}
								else
								{
									$product =wc_get_product($prod_data['id']);
								}
								if($product->managing_stock())
								{
									$avaliable_qty = $prod_data['qty'];
									if($prod_data['variation_id'] > 0)
									{
										$total_stock = get_post_meta($prod_data['variation_id'],'_stock',true);
										$total_stock = $total_stock - $avaliable_qty;
										wc_update_product_stock( $prod_data['variation_id'],$total_stock, 'set' );
									}
									else
									{
										$total_stock = get_post_meta($prod_data['id'],'_stock',true);
										$total_stock = $total_stock - $avaliable_qty;
										wc_update_product_stock( $prod_data['id'],$total_stock, 'set' );
									}

								}
							}
						}
					}
				}
				if($includeTax){
					$order_total = $order_total - $totalProducttax;
				}	
				$order->calculate_totals();
				update_post_meta($orderid, 'mwb_wrffm_exchange_product', $exchange_details);

				$order = wc_get_order($orderid);
				$order->calculate_totals();	
			}
		}

		/**
		 * This function is used refund money after exchange request.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 * @since    1.0.0
		*/
		public function mwb_wrffm_vendor_exchange_req_approve_refund_callback()
		{	
			$check_ajax = check_ajax_referer( 'mwb-wrffm-ajax-security-string', 'security_check' );
			if ( $check_ajax ) 
			{
				global $WCFM;
				$mwb_wrffm_vendor_id = get_current_user_id();
				$order_id = isset($_POST['orderid']) ? intval($_POST['orderid']) : 0;
				$mwb_wrffm_amount_for_refund = isset($_POST['amount']) ? sanitize_text_field($_POST['amount']) : 0;
				$request_type = isset($_POST['request_type']) ? sanitize_text_field($_POST['request_type']) : '';
				$wallet_enable = get_option('mwb_wrffm_return_wallet_enable', "no");
				if($wallet_enable == 'yes' && $order_id > 0)
				{
					$customer_id = ( $value = get_post_meta( $order_id, '_customer_user', true ) ) ? absint( $value ) : '';
					if($customer_id > 0)
					{
						$walletcoupon = get_user_meta( $customer_id, 'mwb_wrffm_refund_wallet_coupon', true );
						if(empty($walletcoupon))
						{
							$coupon_code = mwb_wrffm_coupon_generator(5); // Code
							$amount = $mwb_wrffm_amount_for_refund; // Amount
							$discount_type = 'fixed_cart';
							$coupon_description = "REFUND ACCEPTED - ORDER #$order_id";

							$coupon = array(
								'post_title' => $coupon_code,
								'post_content' => $coupon_description,
								'post_excerpt' => $coupon_description,
								'post_status' => 'publish',
								'post_author' => get_current_user_id(),
								'post_type'		=> 'shop_coupon'
								);
							
							$new_coupon_id = wp_insert_post( $coupon );
							$discount_type = 'fixed_cart';
							update_post_meta( $new_coupon_id, 'discount_type', $discount_type );
							update_post_meta( $new_coupon_id, 'wrffmwallet', true );
							update_user_meta( $customer_id, 'mwb_wrffm_refund_wallet_coupon', $coupon_code );
							update_post_meta( $new_coupon_id, 'coupon_amount', $amount );
						}
						else 
						{
							$the_coupon = new WC_Coupon( $walletcoupon );
							$coupon_id = $the_coupon->get_id();
							if(isset($coupon_id))
							{
								$amount = get_post_meta( $coupon_id, 'coupon_amount', true );
								$remaining_amount = $amount + $mwb_wrffm_amount_for_refund;
								update_user_meta( $customer_id, 'mwb_wrffm_refund_wallet_coupon', $walletcoupon );
								update_post_meta( $coupon_id, 'wrffmwallet', true );
								update_post_meta( $coupon_id, 'coupon_amount', $remaining_amount );
							}
						}
						if($request_type == 'refund')
						{
							update_post_meta($order_id,'refund_amount_by_vendor'.$mwb_wrffm_vendor_id,1);
							update_post_meta($order_id,'refundable_amount_'.$mwb_wrffm_vendor_id,0);
						}
						else
						{
							update_post_meta($order_id,"mwb_wrffm_left_amount_".$mwb_wrffm_vendor_id,0);
						}
						$order = wc_get_order($order_id);
						$order->calculate_totals();
						$mwb_wrffm_obj = wc_get_order( $order_id );
						$tax_rate = 0;
						$tax = new WC_Tax();
						$country_code = $mwb_wrffm_obj->get_billing_country(); // or populate from order to get applicable rates
						$rates = $tax->find_rates( array( 'country' => $country_code ) );
						foreach( $rates as $rate ){
							$tax_rate = $rate['rate'];
						}
						$total_ptax = $mwb_wrffm_amount_for_refund*$tax_rate/100;
						$mwb_wrffm_woo_tax_enable = get_option('woocommerce_calc_taxes');
						$mwb_wrffm_woo_tax_option = get_option('woocommerce_prices_include_tax');
						$mwb_exchange_tax_include = get_option('mwb_wrffm_exchange_tax_enable');
						if($mwb_wrffm_woo_tax_option == 'no' && $mwb_wrffm_woo_tax_enable == "yes")
						{
							$orderval = $mwb_wrffm_amount_for_refund;
						}
						else
						{
							if($mwb_exchange_tax_include == 'no' && $mwb_wrffm_woo_tax_option == 'yes')
							{
								$orderval = $mwb_wrffm_amount_for_refund;
							}
							else
							{
								$orderval = $mwb_wrffm_amount_for_refund+$total_ptax;
							}
						}
						$orderval = round($orderval,2);
						$finaldiscount = 0;
						if( WC()->version < "3.7.0" ) {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_used_coupons();
						} else {
							$mwb_ord_cpn = $mwb_wrffm_obj->get_coupon_codes();
						}
						// Coupons used in the order LOOP (as they can be multiple)
						foreach( $mwb_ord_cpn as $coupon_name ){
							$coupon_post_obj = get_page_by_title($coupon_name, OBJECT, 'shop_coupon');
							$coupon_id = $coupon_post_obj->ID;
							$coupons_obj = new WC_Coupon($coupon_id);

							$coupons_amount = $coupons_obj->get_amount();
							$coupons_type = $coupons_obj->get_discount_type();
							if($coupons_type == 'percent'){
								$finaldiscount = $orderval*$coupons_amount/100;
							}

						}

						$mwb_wrffm_amount_for_refund = $orderval - $finaldiscount;
						$mwb_wrffm_amount_for_refund = $mwb_wrffm_amount_for_refund*100/(100+$tax_rate);

						$item = new WC_Order_Item_Fee();
						$item->set_props( array(
							'name'      => esc_attr( "Amount Refunded in wallet" ),
							'tax_class' => '',
							'total'     => (float) esc_attr( -$mwb_wrffm_amount_for_refund ),
							'total_tax' => false,
							'taxes'     => array(
								'total' => array(),
							),
							'order_id'  => $order_id,
						) );
						$item->save();
						$item_id = $order->add_item( $item );

						$order->calculate_totals();
						$store_name = $WCFM->wcfm_vendor_support->wcfm_get_vendor_store_name_by_vendor( absint($mwb_wrffm_vendor_id) );
						$status = $order->update_status('wc-wcfm-refunded', __('Order is Refunded by vendor store : '.$store_name.'. ','woocommerce-rma-for-wcfm'));
						$order->update_status('wc-wcfm-refunded', __('Order is Refunded by vendor store : '.$store_name.'. ','woocommerce-rma-for-wcfm'));
						$response['result'] = true;
						$response['msg'] = __('Amount is added in customer wallet.','woocommerce-rma-for-wcfm');
						echo json_encode($response);
						die();	
					}
					else{
						$response['result'] = false;
						$response['msg'] = __('Amount cannot be refunded by vendor.','woocommerce-rma-for-wcfm');
						echo json_encode($response);
						die();	
					}	
				}
				else
				{
					$response['result'] = false;
					$response['msg'] = __('Wallet is not Enable ,Please Enable wallet to add amount in customer wallet.','woocommerce-rma-for-wcfm');
					echo json_encode($response);
					die();
				}

			}
		}

		/**
		 * This function is used for sending refund request mail to related vendor.
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link https://www.makewebbetter.com/
		 *
		 * @since    1.0.0
		*/
		function mwb_wrffm_customer_refund_request_mail_for_vendor_callback($mwb_vendor_emails,$order_id,$reason ,$reason_subject, $requested_products)
		{ 
			

			$order = wc_get_order($order_id);
			$headers = array();
			$headers[] = "Content-Type: text/html; charset=UTF-8";
			$subject = get_option('mwb_wrffm_notification_merchant_return_subject');
			$subject = str_replace('[order]', "#".$order_id, $subject);
			$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
			$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));
			foreach ($mwb_vendor_emails as $vendor_email) {

				$vendor_email_message = '<html>
				<body>
					'.do_action('wwrffm_return_request_before_mail_content', $order_id).'
					<style>
						body {
							box-shadow: 2px 2px 10px #ccc;
							color: #767676;
							font-family: Arial,sans-serif;
							margin: 80px auto;
							max-width: 700px;
							padding-bottom: 30px;
							width: 100%;
						}

						h2 {
							font-size: 30px;
							margin-top: 0;
							color: #fff;
							padding: 40px;
							background-color: #557da1;
						}

						h4 {
							color: #557da1;
							font-size: 20px;
							margin-bottom: 10px;
						}

						.content {
							padding: 0 40px;
						}

						.Customer-detail ul li p {
							margin: 0;
						}

						.details .Shipping-detail {
							width: 40%;
							float: right;
						}

						.details .Billing-detail {
							width: 60%;
							float: left;
						}

						.details .Shipping-detail ul li,.details .Billing-detail ul li {
							list-style-type: none;
							margin: 0;
						}

						.details .Billing-detail ul,.details .Shipping-detail ul {
							margin: 0;
							padding: 0;
						}

						.clear {
							clear: both;
						}

						table,td,th {
							border: 2px solid #ccc;
							padding: 15px;
							text-align: left;
						}

						table {
							border-collapse: collapse;
							width: 100%;
						}

						.info {
							display: inline-block;
						}

						.bold {
							font-weight: bold;
						}

						.footer {
							margin-top: 30px;
							text-align: center;
							color: #99B1D8;
							font-size: 12px;
						}
						dl.variation dd {
							font-size: 12px;
							margin: 0;
						}
					</style>
					<div class="header" style="text-align:center;padding: 10px;">
						'.$mail_header.'
					</div>	
					<div class="header">
						<h2>'.$reason_subject.'</h2>
					</div>
					<div class="content">

						<div class="reason">
							<h4>'.__('Reason of Refund', 'woocommerce-rma-for-wcfm').'</h4>
							<p>'.$reason.'</p>
						</div>
						<div class="Order">
							<h4>Order #'.$order_id.'</h4>
							<table>
								<tbody>
									<tr>
										<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
										<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
										<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
									</tr>';
									

									if(isset($requested_products) && !empty($requested_products))
									{
										$total = 0;
										foreach( $order->get_items() as $item_id => $item )
										{
											$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
											foreach($requested_products as $requested_product)
											{
												if(isset($requested_product['item_id']))
												{	
													if($item_id == $requested_product['item_id'])
													{
														if(isset($requested_product['variation_id']) && $requested_product['variation_id'] > 0)
														{
															$prod = wc_get_product($requested_product['variation_id']);
														}
														else
														{
															$prod = wc_get_product($requested_product['product_id']);
														}
														$vendor_post 	= get_post( $prod->get_id()); 
														$mwb_vendor_id 		= $vendor_post->post_author; 
														$mwb_dokan_vendor_data = get_userdata( $mwb_vendor_id );
														if($mwb_dokan_vendor_data->data->user_email == $vendor_email)
														{

															$subtotal = $requested_product['price']*$requested_product['qty'];
															$total += $subtotal;
															if( WC()->version < "3.1.0" )
															{
																$item_meta      = new WC_Order_Item_Meta( $item, $product );
																$item_meta_html = $item_meta->display( true, true );
															}
															else
															{
																$item_meta      = new WC_Order_Item_Product( $item, $product );
																$item_meta_html = wc_display_item_meta($item_meta,array('echo'=> false));
															}
															
															$vendor_email_message .= '<tr>
															<td>'.$item['name'].'<br>';
																$vendor_email_message .= '<small>'.$item_meta_html.'</small>
																<td>'.$requested_product['qty'].'</td>
																<td>'.mwb_wrffm_format_price($requested_product['price']*$requested_product['qty']).'</td>
															</tr>';
														}
													}
												}
											}	
										}	
									}
									$vendor_email_message .= '<tr>
										<th colspan="2">'.__('Refund Total', 'woocommerce-rma-for-wcfm').':</th>
										<td>'.mwb_wrffm_format_price($total).'</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="Customer-detail">
							<h4>'.__('Customer details', 'woocommerce-rma-for-wcfm').'</h4>
							<ul>
								<li><p class="info">
									<span class="bold">'.__('Email', 'woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_email', true).'
								</p></li>
								<li><p class="info">
									<span class="bold">'.__('Tel', 'woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_phone', true).'
								</p></li>
							</ul>
						</div>
						<div class="details">
							<div class="Shipping-detail">
								<h4>'.__('Shipping Address', 'woocommerce-rma-for-wcfm').'</h4>
								'.$order->get_formatted_shipping_address().'
							</div>
							<div class="Billing-detail">
								<h4>'.__('Billing Address', 'woocommerce-rma-for-wcfm').'</h4>
								'.$order->get_formatted_billing_address().'
							</div>
							<div class="clear"></div>
						</div>

					</div>
					<div class="footer" style="text-align:center;padding: 10px;">
						'.$mail_footer.'
					</div>

				</body>
				</html>';

				$status = wc_mail( $vendor_email, $subject, $vendor_email_message, $headers );
			}
		}

		function mwb_wrffm_customer_refund_request_action_mail_for_vendor_callback($mwb_vendor_emails,$order_id,$requested_products,$request,$exchanged_to_products='')
		{ 
			
			$order = wc_get_order($order_id);
			$headers = array();
			$headers[] = "Content-Type: text/html; charset=UTF-8";
			
			$mail_header = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
			$mail_footer = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));
			if($request == "return_req_approve"){
				$request_subject= __('Refund Request Approved By Admin','woocommerce-rma-for-wcfm');
				$subject = get_option('mwb_wrffm_notification_return_approve_subject','Refund Request Approved');
			}
			else if($request == 'refund_req_cancel'){
				$request_subject= __('Refund Request Cancelled By Admin','woocommerce-rma-for-wcfm');
				$subject = get_option('mwb_wrffm_notification_return_cancel_subject','Refund Request Cancelled');
			}
			else if($request == 'exchange_req_appr'){
				$request_subject= __('Exchange Request Approved By Admin','woocommerce-rma-for-wcfm');
				$subject = get_option('mwb_wrffm_notification_exchange_approve_subject','Exchange Request Approved');
			}
			else if($request == 'exchange_req_cancel'){
				$request_subject= __('Exchange Request Cancelled By Admin','woocommerce-rma-for-wcfm');
				$subject = get_option('mwb_wrffm_notification_exchange_cancel_subject','Exchange Request Cancelled');
			}
			$subject = str_replace('[order]', "#".$order_id, $subject);
			foreach ($mwb_vendor_emails as $vendor_email) {

				$vendor_email_message = '<html>
				<body>
					'.do_action('wwrffm_return_request_before_mail_content', $order_id).'
					<style>
						body {
							box-shadow: 2px 2px 10px #ccc;
							color: #767676;
							font-family: Arial,sans-serif;
							margin: 80px auto;
							max-width: 700px;
							padding-bottom: 30px;
							width: 100%;
						}

						h2 {
							font-size: 30px;
							margin-top: 0;
							color: #fff;
							padding: 40px;
							background-color: #557da1;
						}

						h4 {
							color: #557da1;
							font-size: 20px;
							margin-bottom: 10px;
						}

						.content {
							padding: 0 40px;
						}

						.Customer-detail ul li p {
							margin: 0;
						}

						.details .Shipping-detail {
							width: 40%;
							float: right;
						}

						.details .Billing-detail {
							width: 60%;
							float: left;
						}

						.details .Shipping-detail ul li,.details .Billing-detail ul li {
							list-style-type: none;
							margin: 0;
						}

						.details .Billing-detail ul,.details .Shipping-detail ul {
							margin: 0;
							padding: 0;
						}

						.clear {
							clear: both;
						}

						table,td,th {
							border: 2px solid #ccc;
							padding: 15px;
							text-align: left;
						}

						table {
							border-collapse: collapse;
							width: 100%;
						}

						.info {
							display: inline-block;
						}

						.bold {
							font-weight: bold;
						}

						.footer {
							margin-top: 30px;
							text-align: center;
							color: #99B1D8;
							font-size: 12px;
						}
						dl.variation dd {
							font-size: 12px;
							margin: 0;
						}
					</style>
					<div class="header" style="text-align:center;padding: 10px;">
						'.$mail_header.'
					</div>	
					<div class="header">
						<h2>'.$request_subject.'</h2>
					</div>
					<div class="content">

						<div class="Order">
							<h4>Order #'.$order_id.'</h4>
							<table>
								<tbody>
									<tr>
										<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
										<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
										<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
									</tr>';
									
								
									if(isset($requested_products) && !empty($requested_products))
									{
										$total = 0;
										foreach( $order->get_items() as $item_id => $item )
										{
											$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
											$_product  = $order->get_product_from_item( $item );
											foreach($requested_products as $requested_product)
											{
												if(isset($requested_product['item_id']))
												{	
													if($item_id == $requested_product['item_id'])
													{
														if(isset($requested_product['variation_id']) && $requested_product['variation_id'] > 0)
														{
															$prod = wc_get_product($requested_product['variation_id']);
														}
														else
														{
															$prod = wc_get_product($requested_product['product_id']);
														}
														$vendor_post 	= get_post( $prod->get_id()); 
														$mwb_vendor_id 		= $vendor_post->post_author; 
														$mwb_dokan_vendor_data = get_userdata( $mwb_vendor_id );
														if($mwb_dokan_vendor_data->data->user_email == $vendor_email)
														{

															$subtotal = $requested_product['price']*$requested_product['qty'];
															$total += $subtotal;
															if( WC()->version < "3.1.0" )
															{
																$item_meta      = new WC_Order_Item_Meta( $item, $_product );
																$item_meta_html = $item_meta->display( true, true );
															}
															else
															{
																$item_meta      = new WC_Order_Item_Product( $item, $_product );
																$item_meta_html = wc_display_item_meta($item_meta,array('echo'=> false));
															}
															
															$vendor_email_message .= '<tr>
															<td>'.$item['name'].'<br>';
																$vendor_email_message .= '<small>'.$item_meta_html.'</small>
																<td>'.$requested_product['qty'].'</td>
																<td>'.mwb_wrffm_format_price($requested_product['price']*$requested_product['qty']).'</td>
															</tr>';
														}
													}
												}
											}	
										}	
									}
									if($request == 'exchange_req_appr' || $request== 'exchange_req_cancel'){
										$vendor_email_message .= '
												<tr>
													<th colspan="2">'.__('Total', 'woocommerce-rma-for-wcfm').':</th>
													<td>'.mwb_wrffm_format_price($total).'</td>
												</tr>
											</tbody>
										</table>
										<h4>'.__('Exchanged To', 'woocommerce-rma-for-wcfm').'</h4>
										<table>
											<tbody>
												<tr>
													<th>'.__('Product', 'woocommerce-rma-for-wcfm').'</th>
													<th>'.__('Quantity', 'woocommerce-rma-for-wcfm').'</th>
													<th>'.__('Price', 'woocommerce-rma-for-wcfm').'</th>
												</tr>';
					
											$exchanged_to_products = $exchanged_to_products;
											
											$total_price = 0;
											if(isset($exchanged_to_products) && !empty($exchanged_to_products))
											{
												foreach($exchanged_to_products as $key=>$exchanged_product)
												{
													$variation_attributes = array();
													if(isset($exchanged_product['variation_id']))
													{
														if($exchanged_product['variation_id'])
														{
															$variation_product = new WC_Product_Variation($exchanged_product['variation_id']);
															$variation_attributes = $variation_product->get_variation_attributes();
															$variation_labels = array();
															foreach ($variation_attributes as $label => $value){
																if(is_null($value) || $value == ''){
																	$variation_labels[] = $label;
																}
															}
											
															if(isset($exchanged_product['variations']) && !empty($exchanged_product['variations']))
															{
																$variation_attributes = $exchanged_product['variations'];
															}
														}
													}
														
													if(isset($exchanged_product['p_id']))
													{
														if($exchanged_product['p_id'])
														{
															$grouped_product = new WC_Product_Grouped($exchanged_product['p_id']);
															$grouped_product_title = $grouped_product->get_title();
														}
													}
														
													if(isset($exchanged_product['variation_id']))
													{

													$product = wc_get_product($exchanged_product['variation_id']);
													}
													else
													{
														$product = wc_get_product($exchanged_product['id']);
													}
													$pro_price = $exchanged_product['qty']*$exchanged_product['price'];
													$total_price += $pro_price;
													$title = "";
													if(isset($exchanged_product['p_id']))
													{
														$title .= $grouped_product_title.' -> ';
													}
													$title .=$product->get_title();											
											
													if(isset($variation_attributes) && !empty($variation_attributes))
													{
														$title .= wc_get_formatted_variation( $variation_attributes );
													}
											
													$vendor_email_message .= '<tr>
																	<td>'.$title.'</td>
																	<td>'.$exchanged_product['qty'].'</td>
																	<td>'.mwb_wrffm_format_price($pro_price).'</td>
																</tr>';
											
												}
											}
											$vendor_email_message .= '<tr>
															<th colspan="2">'.__('Sub-Total', 'woocommerce-rma-for-wcfm').':</th>
															<td>'.mwb_wrffm_format_price($total_price).'</td>
														</tr>';
											if(isset($added_fee) && !empty($added_fee))
											{
												if(is_array($added_fee))
												{
													foreach($added_fee as $fee)
													{
														$total_price += $fee['val'];
														$html_content .= '<tr>
																		<th colspan="2">'.$fee['text'].'</th>
																		<td>'.mwb_wrffm_format_price($fee['val']).'</td>
																	</tr>';
													}
												}
											}
											$vendor_email_message .= '<tr>
															<th colspan="2">'.__('Grand Total', 'woocommerce-rma-for-wcfm').'</th>
																<td>'.mwb_wrffm_format_price($total_price).'</td>
															</tr>';
											
													$vendor_email_message .= '</tbody>
												</table>
											</div>';
											$mwb_cpn_dis = $order->get_discount_total();
											$mwb_cpn_tax = $order->get_discount_tax();
											
											$mwb_dis_tot = 0;
											if($total_price -  ( $total + $mwb_dis_tot ) > 0)
											{
												$extra_amount = $total_price - ( $total + $mwb_dis_tot );
												$html_content .= '<h2>'.__('Extra Amount', 'woocommerce-rma-for-wcfm').mwb_wrffm_format_price($extra_amount).'</h2>';
											}
											else
											{
												if( $mwb_dis_tot > $total_price )
												{
													$total_price = 0;
												}
												else
												{
													$total_price = $total_price - $mwb_dis_tot;
												}
												$left_amount = $total - $total_price;
												update_post_meta($orderid,"mwb_wrffm_left_amount",$left_amount);

												$vendor_email_message .= '<h2><i>'.__('Left Amount After Exchange:', 'woocommerce-rma-for-wcfm').'</i> '.mwb_wrffm_format_price($left_amount).'</h2>';
											}
									} else {
									$vendor_email_message .= '<tr>
										<th colspan="2">'.__('Refund Total', 'woocommerce-rma-for-wcfm').':</th>
										<td>'.mwb_wrffm_format_price($total).'</td>
									</tr>
								</tbody>
							</table>
						</div>';
					}
						$vendor_email_message .= '<div class="Customer-detail">
							<h4>'.__('Customer details', 'woocommerce-rma-for-wcfm').'</h4>
							<ul>
								<li><p class="info">
									<span class="bold">'.__('Email', 'woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_email', true).'
								</p></li>
								<li><p class="info">
									<span class="bold">'.__('Tel', 'woocommerce-rma-for-wcfm').': </span>'.get_post_meta($order_id, '_billing_phone', true).'
								</p></li>
							</ul>
						</div>
						<div class="details">
							<div class="Shipping-detail">
								<h4>'.__('Shipping Address', 'woocommerce-rma-for-wcfm').'</h4>
								'.$order->get_formatted_shipping_address().'
							</div>
							<div class="Billing-detail">
								<h4>'.__('Billing Address', 'woocommerce-rma-for-wcfm').'</h4>
								'.$order->get_formatted_billing_address().'
							</div>
							<div class="clear"></div>
						</div>

					</div>
					<div class="footer" style="text-align:center;padding: 10px;">
						'.$mail_footer.'
					</div>

				</body>
				</html>';
				
				$status = wc_mail( $vendor_email, $subject, $vendor_email_message, $headers );
			}
		}
	}
	new class_vendor_functions();
}
?>