(function( $ ) {
	'use strict';

/**
* All of the code for your public-facing JavaScript source
* should reside in this file.
*
* Note: It has been assumed you will write $ code here, so the
* $ function reference has been prepared for usage within the scope
* of this function.
*
* This enables you to define handlers, for when the DOM is ready:
*
* $(function() {
*
* });
*
* When the window is loaded:
*
* $( window ).load(function() {
*
* });
*
* ...and/or other possibilities.
*
* Ideally, it is not considered best practise to attach more than a
* single DOM-ready or window-load handler for a particular page.
* Although scripts in the WordPress core, Plugins and Themes may be
* practising this, we should strive to set a better example in our own work.
*/

$(document).ready(function(){

	$(".mwb_wrffm_add-return-product-fee").click(function(){
		var html = '<div class="mwb_wrffm_add_fee">';
		html += '<input type="text" class="mwb_return_fee_txt" name="mwb_return_fee_txt[]" value="" placeholder="Fee Name">';
		html += '<input type="text" class="mwb_return_fee_value wc_input_price" value="" placeholder="0" name="">';
		html += '<input type="button" class="button mwb_wrffm_remove-return-product-fee" value="Remove">';
		html += '</div>';
		$('#mwb_wrffm_add_fee').append(html);
	});

	$(".mwb_wrffm_save-return-product-fee").click(function(){
		$(".mwb_wrffm_return_loader").show();
		var added_fee = {};
		var count = 0;
		var orderid = $(this).data('orderid');
		var date = $(this).data('date');
		$(".mwb_wrffm_add_fee").each(function(){
			var fee = {};
			var fee_txt = $(this).children().val();
			var fee_val = $(this).children().next().val();
			fee['text'] = fee_txt;
			fee['val']  = fee_val;
			added_fee[count] = fee;
			count++;
		});
		var data = {
					action:'mwb_wrffm_return_fee_add',
					fees:added_fee,
					orderid : orderid,
					date : date,
					security_check	: global_wrffm_addon.mwb_wrffm_nonce	
				   };
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data,
			dataType :'json',	
			success: function(response) 
			{
				$(".mwb_wrffm_return_loader").hide();
				location.reload();
			}
		});
		
	});


	jQuery("#mwb_wrffm_add_fee").on('click','.mwb_wrffm_remove-return-product-fee', function(){
		jQuery(this).parent().remove();
	});
	
	$('#mwb_wrffm_accept_return_vendor').on('click',function(){

		$(this).attr('disabled',true);
		$(".mwb_wrffm_return_loader").show();
		var order_id = $(this).data('orderid');
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			dataType :'json',	
			data 	: { 
				action:'mwb_wrffm_vendor_return_req_approve', 
				order_id:order_id,
				security_check : global_wrffm_addon.mwb_wrffm_nonce
			},
			success: function(response) 
			{	
				$(".mwb_wrffm_return_loader").hide();
				window.location.reload();
			}
		});
	});

	$(document).on('click','#mwb_wrffm_manage_stock',function(){
		$(this).attr('disabled',true);
		var order_id = $(this).data('orderid');
		var type = $(this).data('type');
		var data = { 
			action   : 'mwb_wrffm_vendor_manage_stock' ,
			order_id : order_id ,
			type     : type,
			security_check : global_wrffm_addon.mwb_wrffm_nonce
		};
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",             
			data: data,
			dataType :'json',
			success: function(response)   
			{
				if(response.result)
				{
					$("#mwb-return-alert").html(response.msg);
					$("#mwb-return-alert").removeClass('woocommerce-error');
					$("#mwb-return-alert").addClass("woocommerce-message");
					$("#mwb-return-alert").css("color", "#8FAE1B");
					$("#mwb-return-alert").show();
					$('html, body').animate({
						scrollTop: $("#mwb_wrffm_vendor_form_wrapper").offset().top
					}, 800);
					window.setTimeout(function() {
						window.location.reload();
					}, 1000);
				}
				else
				{
					$("#mwb-return-alert").show();
					$("#mwb-return-alert").html(response.msg);
					$('html, body').animate({
						scrollTop: $("#mwb_wrffm_vendor_form_wrapper").parents('.woocommerce-account').offset().top
					}, 800);
				}
			}
		});
	});	

	$(".mwb_wrffm_add-exchange-product-fee").click(function(){
		var html = '<div class="mwb_wrffm_exchange_add_fee">';
		html += '<input type="text" class="mwb_exchange_fee_txt" name="mwb_exchange_fee_txt[]" value="" placeholder="Fee Name">';
		html += '<input type="text" class="mwb_exchange_fee_value wc_input_price" value="" placeholder="0" name="">';
		html += '<input type="button" class="button mwb_wrffm_remove-exchange-product-fee" value="Remove">';
		html += '</div>';
		$('#mwb_wrffm_exchange_add_fee').append(html);
	});
	
	$(".mwb_wrffm_save-exchange-product-fee").click(function(){
		$(".mwb_wrffm_exchange_loader").show();
		var added_fee = {};
		var count = 0;
		var orderid = $(this).data('orderid');
		var date = $(this).data('date');
		$(".mwb_wrffm_exchange_add_fee").each(function(){
			var fee = {};
			var fee_txt = $(this).children().val();
			var fee_val = $(this).children().next().val();
			fee['text'] = fee_txt;
			fee['val'] = fee_val;
			added_fee[count] = fee;
			count++;
		});
		var data = {
					action:'mwb_wrffm_exchange_fee_add',
					fees:added_fee,
					orderid : orderid,
					date : date,
					security_check	:	global_wrffm_addon.mwb_wrffm_nonce	
				   };
		
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data,
			dataType :'json',	
			success: function(response) 
			{
				$(".mwb_wrffm_exchange_loader").hide();
				location.reload();
			}
		});
		
	});
	
	$("#mwb_wrffm_exchange_add_fee").on('click','.mwb_wrffm_remove-exchange-product-fee', function(){
		$(this).parent().remove();
	});

	$("#mwb_wrffm_accept_exchange").click(function(){
		$(this).attr('disabled',true);
		$(".mwb_wrffm_exchange_loader").show();
		var orderid = $(this).data('orderid');
		var date = $(this).data('date');
		var data = {
			action:'mwb_wrffm_vendor_exchange_req_approve',
			orderid:orderid,
			date:date,
			security_check	:global_wrffm_addon.mwb_wrffm_nonce	
		};
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data,
			dataType :'json',	
			success: function(response) 
			{
				$(".mwb_wrffm_exchange_loader").hide();
				location.reload();
			}
		});
	});

	$("#mwb_wrffm_cancel_return_request_vendor").click(function(){
		$(this).attr('disabled',true);
		$(".mwb_wrffm_return_loader").show();
		var orderid = $(this).data('orderid');
		var date = $(this).data('date');
		var data = {
			action:'mwb_wrffm_cancel_return_request_vendor',
			orderid:orderid,
			date:date,
			security_check	:global_wrffm_addon.mwb_wrffm_nonce	
		};
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data,
			dataType :'json',	
			success: function(response) 
			{
				$(".mwb_wrffm_return_loader").hide();
				location.reload();
			}
		});
	});

	$("#mwb_wrffm_cancel_exchange").click(function(){
		$(this).attr('disabled',true);
		$(".mwb_wrffm_exchange_loader").show();
		var orderid = $(this).data('orderid');
		var date = $(this).data('date');
		var data = {
			action:'mwb_wrffm_cancel_exchange_request_vendor',
			orderid:orderid,
			date:date,
			security_check	:	global_wrffm_addon.mwb_wrffm_nonce	
		};
		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data,
			dataType :'json',	
			success: function(response) 
			{
				$(".mwb_wrffm_exchange_loader").hide();
				location.reload();
			}
		});
	});

	$("#mwb_wrffm_left_amount").click(function(){
		$(this).attr('disabled',true);
		var result = false;

		if($('#left_amount').val() > 0)
			result = true;
	
		var refund_amount = 0;
		var type = '';
		if(result)
		{
			refund_amount = $("#left_amount").val();
			type = 'exchange';
			result=false;
		}
		else
		{
			refund_amount = $(".mwb_wrffm_total_amount_for_refund").val();
			type = 'refund';
		}
		var order_id = $(this).data('orderid');
		var data1 = {
			action:'mwb_wrffm_vendor_exchange_req_approve_refund',
			orderid:order_id,
			request_type : type,
			amount : refund_amount,
			security_check	:global_wrffm_addon.mwb_wrffm_nonce	
		};

		$.ajax({
			url: global_wrffm_addon.ajaxurl, 
			type: "POST",  
			data: data1,
			dataType :'json',	
			success: function(response) 
			{
				if(response.result)
				{

					$("#mwb-return-alert").html(response.msg);
					$("#mwb-return-alert").removeClass('woocommerce-error');
					$("#mwb-return-alert").addClass("woocommerce-message");
					$("#mwb-return-alert").css("color", "#8FAE1B");
					$("#mwb-return-alert").show();
					$('html, body').animate({
						scrollTop: $("#mwb_wrffm_vendor_form_wrapper").offset().top
					}, 800);
					window.setTimeout(function() {
						window.location.reload();
					}, 1000);
				}
				else
				{
					$("#mwb-return-alert").show();
					$("#mwb-return-alert").html(response.msg);
					$('html, body').animate({
						scrollTop: $("#mwb_wrffm_vendor_form_wrapper").parents('.woocommerce-account').offset().top
					}, 800);
				}
			}
		});
	});

	var mwb_vendor_class_exist = $('#mwb_wrffm_rma_request_list_table').hasClass('mwb_wrffm_rma_vendor_table');
	if (mwb_vendor_class_exist) {
		$(document).find('.mwb_wrffm_rma_vendor_table').dataTable( {
	        "order": [[ 1, "desc" ]]
	    });
	}

	$('#mwb_wrffm_vendor_form_wrapper h2').on('click', function(e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(this).next()
      .stop()
      .slideUp(300);
    } else {
      $(this).addClass('active');
      $(this).next()
      .stop()
      .slideDown(300);
    }
  });
});

})( jQuery );
