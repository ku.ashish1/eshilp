<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( ! class_exists( 'MWB_WCFFM_Update' ) ) 
{
	class MWB_WCFFM_Update 
	{
		public function __construct() {
			register_activation_hook(MWB_WRFFM_FILE, array($this, 'mwb_wrffm_check_activation'));
			add_action( 'mwb_wrffm_check_event', array($this, 'mwb_wrffm_check_update'));
			add_filter( 'http_request_args', array($this, 'mwb_wrffm_updates_exclude'), 5, 2 );
			register_deactivation_hook(MWB_WRFFM_FILE, array($this, 'mwb_wrffm_check_deactivation'));
			add_action( 'install_plugins_pre_plugin-information', array($this,'mwb_wrffm_plugin_details'));
		}	

		public function mwb_wrffm_check_deactivation() 
		{
			wp_clear_scheduled_hook('mwb_wrffm_check_event');
		}

		public function mwb_wrffm_check_activation() 
		{
			wp_schedule_event(time(), 'daily', 'mwb_wrffm_check_event');
		}

		public function mwb_wrffm_check_update() 
		{
			global $wp_version;
			global $mwb_wrffm_update_check;
			$mwb_wrffm_update_check = 'https://makewebbetter.com/pluginupdates/woocommerce-rma-for-wcfm/update.php';
			$plugin_folder = plugin_basename( dirname( MWB_WRFFM_FILE ) );
			$plugin_file = basename( ( MWB_WRFFM_FILE ) );

			if ( defined( 'WP_INSTALLING' ) )
			{
				return false;
			} 
			$postdata = array(
				'action' => 'check_update',
				'license_key' => MWB_WRFFM_LICENSE_KEY
			);
			
			$args = array(
				'method' => 'POST',
				'body' => $postdata,
			);
			
			$response = wp_remote_post( $mwb_wrffm_update_check, $args );
			if(empty($response['body'])) {
				return false;
			}
			if(!isset($response['body'])) 
			{
				return false;
			}
			list($version, $url) = explode('~', $response['body']);

			if($this->mwb_wrffm_plugin_get("Version") <= $version) 
			{
				return false;
			}
			
			$plugin_transient = get_site_transient('update_plugins');
			$a = array(
				'slug' => $plugin_folder,
				'new_version' => $version,
				'url' => $this->mwb_wrffm_plugin_get("AuthorURI"),
				'package' => $url
			);
			$o = (object) $a;
			$plugin_transient->response[$plugin_folder.'/'.$plugin_file] = $o;
			set_site_transient('update_plugins', $plugin_transient);
		}

		public function mwb_wrffm_updates_exclude( $r, $url ) 
		{
			if ( 0 !== strpos( $url, 'http://api.wordpress.org/plugins/update-check' ) )
			{
				return $r; 
			}	
			$plugins = unserialize( $r['body']['plugins'] );
			unset( $plugins->plugins[ plugin_basename( __FILE__ ) ] );
			unset( $plugins->active[ array_search( plugin_basename( __FILE__ ), $plugins->active ) ] );
			$r['body']['plugins'] = serialize( $plugins );
			return $r;
		}

		//Returns current plugin info.
		public function mwb_wrffm_plugin_get($i) 
		{
			if ( ! function_exists( 'get_plugins' ) )
			{
				require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			}	
			$plugin_folder = get_plugins( '/' . plugin_basename( dirname( MWB_WRFFM_FILE ) ) );
			$plugin_file = basename( ( MWB_WRFFM_FILE ) );
			return $plugin_folder[$plugin_file][$i];
		}


		public function mwb_wrffm_plugin_details(){
			global $tab;
			if($tab == 'plugin-information' && $_REQUEST['plugin'] == 'woocommerce-rma-for-wcfm'){

				$url = 'https://makewebbetter.com/pluginupdates/woocommerce-rma-for-wcfm/update.php';

				$postdata = array(
					'action' => 'check_update',
					'license_code' => MWB_WRFFM_LICENSE_KEY
					);
				
				$args = array(
					'method' => 'POST',
					'body' => $postdata,
					);
				
				$data = wp_remote_post( $url, $args );
				if(is_wp_error($data)){
					return;
				}
				
				if(isset($data['body'])){
					$all_data = json_decode($data['body'],true);

					if(is_array($all_data) && !empty($all_data)){
						$this->create_html_data($all_data);

						wp_die();
					}
				}
			}
		}

		public function create_html_data($all_data){
			?>
			<style>
				#TB_window{
					top : 4% !important;
				}
				.mwb_wrffm_plugin_banner > img {
					height: 55%;
					width: 100%;
					border: 1px solid;
					border-radius: 7px;
				}
				.mwb_wrffm_plugin_description > h4 {
					background-color: #3779B5;
					padding: 5px;
					color: #ffffff;
					border-radius: 5px;
				}
				.mwb_wrffm_plugin_requirement > h4 {
					background-color: #3779B5;
					padding: 5px;
					color: #ffffff;
					border-radius: 5px;
				}
				#error-page > p {
					display: none;
				}
			</style>
			<div class="mwb_wrffm_plugin_details_wrapper">
				<div class="mwb_wrffm_plugin_banner">
					<img src="<?php echo $all_data['banners']['banner'];?>">	
				</div>
				<div class="mwb_wrffm_plugin_description">
					<h4><?php _e('Plugin Description','notify-user'); ?></h4>
					<span><?php echo $all_data['sections']['description']; ?></span>
				</div>
				<div class="mwb_wrffm_plugin_requirement">
					<h4><?php _e('Plugin Change Log','notify-user'); ?></h4>
					<span><?php echo $all_data['sections']['changelog']; ?></span>
				</div> 
			</div>
			<?php
		}
	}
	new MWB_WCFFM_Update();
}		
?>