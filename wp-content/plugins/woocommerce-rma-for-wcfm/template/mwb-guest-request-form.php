<?php
$current_user_id = get_current_user_id();
if($current_user_id > 0)
{
	$myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
	$myaccount_page_url = get_permalink( $myaccount_page );
	wp_redirect($myaccount_page_url);
	exit;
}	

get_header( 'shop' );

/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
*/
do_action( 'woocommerce_before_main_content' );

/**
 * woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */

$mwb_main_wrapper_class = get_option('mwb_wrffm_return_exchange_class');
$mwb_child_wrapper_class = get_option('mwb_wrffm_return_exchange_child_class');
?>
<div class="woocommerce woocommerce-account mwb-wrffm_ref_ex_container <?php echo $mwb_main_wrapper_class;?>">
	<div class="<?php echo $mwb_child_wrapper_class;?>">
		<div id="mwb_wrffm_guest_request_form_wrapper">
			<h2><?php 
			$page_head = get_option( 'mwb_wrffm_return_exchange_page_heading_text' , 'Refund/Exchange Request Form' );
			if ($page_head == '') {
				$page_head =  __('Refund/Exchange Request Form','woocommerce-rma-for-wcfm' );
			}
			$return_product_form = $page_head;
			echo apply_filters('mwb_wrffm_return_product_form', $return_product_form);
			?>
			</h2>
			<?php
			if(null != WC()->session->get( 'mwb_wrffm_notification' ) && '' != WC()->session->get( 'mwb_wrffm_notification' ))
			{ 
				?>
				<ul class="woocommerce-error">
						<li><strong><?php _e('ERROR','woocommerce-rma-for-wcfm'); ?></strong>: <?php echo WC()->session->get( 'mwb_wrffm_notification' );?></li>
				</ul>
				<?php 
				WC()->session->__unset( 'mwb_wrffm_notification' );
			}
			?>
			<form class="login mwb_wrffm_guest_form" method="post">
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					<label for="username"><?php _e('Enter Order Id','woocommerce-rma-for-wcfm' );?><span class="required"> *</span></label>
					<input type="text" id="order_id" name="order_id" class="woocommerce-Input woocommerce-Input--text input-text">
				</p>
				
				<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
					<label for="username"><?php _e('Enter Order Email','woocommerce-rma-for-wcfm' );?><span class="required"> *</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="order_email" id="order_email" value="">
				</p>
				
				<p class="form-row">
					<input type="submit" value="<?php _e('Submit','woocommerce-rma-for-wcfm'); ?>" name="mwb_wrffm_order_id_submit" class="woocommerce-Button button">
				</p>
			</form>
		</div>
	</div>
</div>
<?php 
do_action( 'woocommerce_after_main_content' );

/**
 * woocommerce_sidebar hook.
 *
 * @hooked woocommerce_get_sidebar - 10
*/
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
?>