<?php
/**
 * Exit if accessed directly
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce;
$allowed = true;


//Product Exchange request form

$current_user_id = get_current_user_id();   //check user is logged in or not

if($allowed)
{
	$subject = "";
	$reason = "";
	if(isset($_POST['order_id']))
	{
		$order_id = $_POST['order_id'];
		
	}
	elseif (isset($_GET['order_id'])) {
		$order_id = $_GET['order_id'];
	}
	else 
	{
		
		$url = strtok($_SERVER['REQUEST_URI'], '?');
		$link_array = explode('/',$url);
		if(empty($link_array[count($link_array)-1]))
		{
			$order_id = $link_array[count($link_array)-2];
		}	
		else
		{
			$order_id = $link_array[count($link_array)-1];
		}	
		
	}	
	
	//check order id is valid
	
	if(!is_numeric($order_id))
	{
		if(get_current_user_id() > 0)
		{
			$myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
			$myaccount_page_url = get_permalink( $myaccount_page );
		}
		else
		{
			$mwb_wrffm_pages= get_option('mwb_wrffm_pages');
			$page_id = $mwb_wrffm_pages['pages']['mwb_request_from'];
			$myaccount_page_url = get_permalink( $page_id );
		}
		$allowed = false;
		$reason = __('Please choose an Order.','woocommerce-rma-for-wcfm').'<a href="'.$myaccount_page_url.'">'.__('Click Here','woocommerce-rma-for-wcfm').'</a>';
		$reason = apply_filters('mwb_wrffm_exchange_choose_order', $reason);
	}
	else
	{
		$order_customer_id = get_post_meta($order_id, '_customer_user', true);
		if($current_user_id > 0)    // check order associated to customer account or not for registered user
		{
			if(!(current_user_can('administrator')))
			{
				if($order_customer_id != $current_user_id)
				{
					$myaccount_page = get_option( 'woocommerce_myaccount_page_id' );
					$myaccount_page_url = get_permalink( $myaccount_page );
					$allowed = false;
					$reason = __("This order #$order_id is not associated to your account. <a href='$myaccount_page_url'>Click Here</a>",'woocommerce-rma-for-wcfm' );
					$reason = apply_filters('mwb_wrffm_exchange_choose_order', $reason);
				}
			}
		}
		else						// check order associated to customer account or not for guest user
		{
			if(null != WC()->session->get( 'mwb_wrffm_email' ))
			{
				$user_email = WC()->session->get( 'mwb_wrffm_email' );
				$order_email = get_post_meta($order_id, '_billing_email', true);
				if(!(current_user_can('administrator')))
				{
					if($user_email != $order_email)
					{
						$allowed = false;
						$mwb_wrffm_pages= get_option('mwb_wrffm_pages');
						$page_id = $mwb_wrffm_pages['pages']['mwb_request_from'];
						$myaccount_page_url = get_permalink( $page_id );
						$reason = __("This order #$order_id is not associated to your account. <a href='$myaccount_page_url'>Click Here</a>",'woocommerce-rma-for-wcfm' );
						$reason = apply_filters('mwb_wrffm_exchange_choose_order', $reason);
					}
				}
			}
			else 
			{
				$allowed = false;
			}	
			
		}
	}
	
	if($allowed)
	{	
		$mwb_wrffm_next_return = true;
		$mwb_wrffm_enable = get_option('mwb_wrffm_return_exchange_enable', false);
		if($mwb_wrffm_enable == 'yes')
		{
			$mwb_wrffm_made = get_post_meta($order_id, "mwb_wrffm_request_made", true);
			if(isset($mwb_wrffm_made) && !empty($mwb_wrffm_made))
			{
				$mwb_wrffm_next_return = false;
			}
		}
		if($mwb_wrffm_next_return)
		{
			$allowed = true;
		}
		else
		{
			$allowed = false;
		}
		
		if($allowed)
		{
			$order = wc_get_order($order_id);
		
			//Check enable exchange
			$exchange_enable = get_option('mwb_wrffm_exchange_enable', false);
			if(isset($exchange_enable) && !empty($exchange_enable))
			{
				if($exchange_enable == 'yes')
				{
					$allowed = true;
				}
				else
				{
					$allowed = false;
					$reason = __('Exchange request is disabled.','woocommerce-rma-for-wcfm' );
					$reason = apply_filters('mwb_wrffm_exchange_order_amount', $reason);
				}
			}
			else
			{
				$allowed = false;
				$reason = __('Exchange request is disabled.','woocommerce-rma-for-wcfm' );
				$reason = apply_filters('mwb_wrffm_exchange_order_amount', $reason);
			}
			
			
			
			if($allowed)
			{
				if(null != WC()->session->get( 'exchange_requset' ))
				{

					$exchange_details = WC()->session->get( 'exchange_requset' );
				}	
				else
				{
					
					$exchange_details = get_post_meta($order_id, 'mwb_wrffm_exchange_product', true);
				}	
				
				WC()->session->set( 'exchange_requset' , $exchange_details );
				
				//Get pending exchange request
				
				if(isset($exchange_details) && !empty($exchange_details))
				{
					foreach($exchange_details as $date=>$exchange_detail)
					{
						if($exchange_detail['status'] == 'pending')
						{
							if(isset($exchange_detail['subject']))
							{
								$subject = $exchange_details[$date]['subject'];
							}
							if(isset($exchange_detail['reason']))
							{
								$reason = $exchange_details[$date]['reason'];
							}
							if(isset($exchange_detail['from']))
							{
								$exchange_products = $exchange_detail['from'];
							}
							else
							{
								$exchange_products = array();
							}
							if(isset($exchange_detail['to']))
							{
								$exchange_to_products = $exchange_detail['to'];
							}
							else
							{
								$exchange_to_products = array();
							}
						}
					}
				}
				$order = new WC_Order( $order );
				$items = $order->get_items();
				$mwb_wrffm_catalog=get_option('catalog',array());
				if(is_array($mwb_wrffm_catalog) && !empty($mwb_wrffm_catalog) )
				{	
					$mwb_wrffm_catalog_exchange=array();
					foreach ( $items as $item ) {
					    $product_id = $item['product_id'];
					    if(is_array($mwb_wrffm_catalog) && !empty($mwb_wrffm_catalog) )
						{
							foreach ($mwb_wrffm_catalog as $key => $value) {
								if(is_array($value['products']))
								{
									if(in_array($product_id, $value['products']))	
									{
										$mwb_wrffm_catalog_exchange[]=$value['exchange'];
									}
									
								}
							}
						}
					}
					if(is_array($mwb_wrffm_catalog_exchange) && !empty($mwb_wrffm_catalog_exchange))
					{
						$mwb_wrffm_catalog_exchange_days=max($mwb_wrffm_catalog_exchange);
					}
				}
				if( WC()->version < "3.0.0" )
				{
					$order_date = date_i18n( 'F j, Y', strtotime( $order->order_date  ) );
			    }
			    else
			    {
			    	$order_date = date_i18n( 'F j, Y', strtotime( $order->get_date_created()  ) );
			    }
			    $today_date = date_i18n( 'F j, Y' );
	    		$order_date = strtotime($order_date);
	    		$today_date = strtotime($today_date);
				$days = $today_date - $order_date;
				$day_diff = floor($days/(60*60*24));
				$day_allowed = get_option('mwb_wrffm_exchange_days', true); //Check allowed days
				if(isset($mwb_wrffm_catalog_exchange_days)&& $mwb_wrffm_catalog_exchange_days != 0)
				{
					if($mwb_wrffm_catalog_exchange_days >= $day_diff)
					{
						$allowed = true;
					}
					else
					{
						$allowed = false;
						$reason = __('Days exceed.', 'woocommerce-rma-for-wcfm' );
						$reason = apply_filters('mwb_wrffm_exchange_day_exceed', $reason);
					}
				}
				else
				{
					if($day_allowed >= $day_diff && $day_allowed != 0)
					{
						$allowed = true;
					}
					else
					{
						$allowed = false;
						$reason = __('Days exceed.', 'woocommerce-rma-for-wcfm' );
						$reason = apply_filters('mwb_wrffm_exchange_day_exceed', $reason);
					}
				}
				if($allowed)
				{
					$order = wc_get_order( $order_id );
					$order_total = $order->get_total();
					$exchange_min_amount = get_option('mwb_wrffm_exchange_minimum_amount', false);
						
					//Check minimum amount
						
					if(isset($exchange_min_amount) && !empty($exchange_min_amount))
					{
						if($exchange_min_amount <= $order_total)
						{
							$allowed = true;
						}
						else
						{
							$allowed = false;
							$reason = __('For Exchange request Order amount must be greater of equal to ', 'woocommerce-rma-for-wcfm' ).$exchange_min_amount.'.';
							$reason = apply_filters('mwb_wrffm_exchange_order_amount', $reason);
						}
					}
					if($allowed)
					{
						$statuses = get_option('mwb_wrffm_exchange_order_status', array());
						$order_status ="wc-".$order->get_status();
						if(!in_array($order_status, $statuses))
						{
							$allowed = false;
							$reason =  __('Exchange request is disabled.','woocommerce-rma-for-wcfm' );
							$reason = apply_filters('mwb_wrffm_return_order_amount', $reason);
						}
					}
				}
			}	
		}
	}
}	
get_header( 'shop' );

/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action( 'woocommerce_before_main_content' );
if($allowed)
{
	$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
	$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
	
	$mwb_main_wrapper_class = get_option('mwb_wrffm_return_exchange_class');
	$mwb_child_wrapper_class = get_option('mwb_wrffm_return_exchange_child_class');
	$mwb_exchange_css = get_option('mwb_wrffm_exchange_custom_css');
	?>
	<style>	<?php echo $mwb_exchange_css;?>	</style>
	<div class="woocommerce woocommerce-account mwb_wcffm_exchange_form_class <?php echo $mwb_main_wrapper_class;?>">
		<div class="<?php echo $mwb_child_wrapper_class;?>" id="mwb_wrffm_exchange_request_form_wrapper">
			<div id="mwb_wrffm_exchange_request_container">
				<h1>
				<?php 
					$exchange_product_form = __( 'Product Exchange Request Form', 'woocommerce-rma-for-wcfm' );
					echo apply_filters('mwb_wrffm_exchange_product_form', $exchange_product_form);
				?>
				</h1>
				<p>
				<?php 
					$select_product_text = __( 'Select Product to Exchange', 'woocommerce-rma-for-wcfm' );
					echo apply_filters('mwb_wrffm_select_exchange_text', $select_product_text);
				?>
				</p>
			</div>
			<ul class="woocommerce-error" id="mwb-exchange-alert">
			</ul>
			<input type="hidden" id="mwb_wrffm_exchange_request_order" value="<?php echo $order_id;?>">
			<div class="mwb_wrffm_product_table_wrapper">
				<table class="shop_table order_details mwb_wrffm_product_table">
					<thead>
						<tr>
							<th class="product-check"><input type="checkbox" name="mwb_wrffm_exchange_product_all" class="mwb_wrffm_exchange_product_all"> <?php _e( 'Check All', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th class="product-name"><?php _e( 'Product', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th class="product-qty"><?php _e( 'Quantity', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th class="product-total"><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
							$mwb_wrffm_sale = get_option('mwb_wrffm_exchange_sale_enable', false);
							$mwb_wrffm_ex_cats = get_option('mwb_wrffm_exchange_ex_cats', array());
						
							$sale_enable = false;
							if($mwb_wrffm_sale == 'yes')
							{
								$sale_enable = true;
							}
							
							$mwb_wrffm_in_tax = get_option('mwb_wrffm_exchange_tax_enable', false);
							$in_tax = false;
							if($mwb_wrffm_in_tax == 'yes')
							{
								$in_tax = true;
							}
							foreach( $order->get_items() as $item_id => $item ) 
							{
									if($item['qty'] > 0)
									{
										if(isset($item['variation_id']) && $item['variation_id'] >0)
									{
										$variation_id = $item['variation_id'];
										$product_id = $item['product_id'];
									}
									else
									{
										$product_id = $item['product_id'];
									}
									$mwb_wrffm_catalog_detail=get_option('catalog',array());
									$day_allowed = get_option('mwb_wrffm_exchange_days', true);
									if(isset($mwb_wrffm_catalog_detail) && !empty($mwb_wrffm_catalog_detail))
									{
										$mwb_wrffm_catalog_exchange=array();
										foreach ($mwb_wrffm_catalog_detail as $key => $value) 
										{
											if(is_array($mwb_wrffm_catalog_detail[$key]['products']) && !empty($mwb_wrffm_catalog_detail[$key]['products']))
											{
												if(in_array( $product_id, $mwb_wrffm_catalog_detail[$key]['products']))
												{
													$mwb_wrffm_pro=$product_id;
													$mwb_wrffm_catalog_exchange[]=$mwb_wrffm_catalog_detail[$key]['exchange'];
													if( WC()->version < "3.0.0" )
													{
														$order_date = date_i18n( 'F j, Y', strtotime( $order->order_date  ) );
													}
													else
													{
														
														$order_date = date_i18n( 'F j, Y', strtotime( $order->get_date_created()  ) );
													}
													$today_date = date_i18n( 'F j, Y' );
										    		$order_date = strtotime($order_date);
										    		$today_date = strtotime($today_date);
													$days = $today_date - $order_date;
													$day_diff = floor($days/(60*60*24));
												}
											}
										}
										if(is_array($mwb_wrffm_catalog_exchange) && !empty($mwb_wrffm_catalog_exchange))
										{
											$mwb_wrffm_catalog_exchange_day=min($mwb_wrffm_catalog_exchange);
										}
									}
									if(isset($product_id)&&isset($mwb_wrffm_pro) &&$product_id==$mwb_wrffm_pro)
									{
										if($mwb_wrffm_catalog_exchange_day >= $day_diff && $mwb_wrffm_catalog_exchange_day != 0)
										{
											$show = true;
										}
										else
										{
											$show = false;
										}
									}
									else
									{
										if($day_allowed >= $day_diff && $day_allowed != 0)
										{
											$show = true;
										}
										else
										{
											$show = false;
										}
									}
									$product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
									
									$thumbnail     = wp_get_attachment_image($product->get_image_id(),'thumbnail');
									
									
									$pro_categories = get_the_terms( $product_id, 'product_cat' );
									$productdata = wc_get_product($product_id);
									
									$disable_product = get_post_meta($product_id, 'mwb_wrffm_disable_exchange', true);
									if(isset($disable_product) && !empty($disable_product))
									{
										if($disable_product == "open")
										{
											$show = false;
										}
									}
									
									if(isset($pro_categories) && !empty($pro_categories))
									{
										foreach($pro_categories as $k=>$cat)
										{
											$cat = (array)$cat;
												
											if(in_array($cat['term_id'], $mwb_wrffm_ex_cats))
											{
												$show = false;
											}
										}
									}
									
									if($show)
									{
										if($sale_enable)
										{
											$show = true;
										}
										else
										{
											if($productdata->is_on_sale())
											{
												$show = false;
											}
										}
									}
									
									$mwb_product_total = $order->get_line_subtotal( $item, $in_tax );
									$mwb_product_qty = $item['qty'];
									if($mwb_product_qty > 0)
									{
										$mwb_per_product_price = $mwb_product_total / $mwb_product_qty;
									}
									$purchase_note = get_post_meta( $product_id, '_purchase_note', true );
			
									$checked = "";
									$qty = 1;
									if(isset($exchange_products) && !empty($exchange_products))
									{
										foreach($exchange_products as $exchange_product)
										{
											if($item['product_id'] == $exchange_product['product_id'] && $item['variation_id'] == $exchange_product['variation_id'])
											{
												$checked = 'checked="checked"';
												$qty = $exchange_product['qty'];
												break;
											}
										}
									}
									
									
									?>
									<tr class="mwb_wrffm_exchange_column" data-productid="<?php echo $product_id?>" data-variationid="<?php echo $item['variation_id']?>" data-itemid="<?php echo $item_id?>">
										<td class="product-select">
											<?php 
											if($show)
											{
												if( WC()->version < "3.7.0" ) {
													$mwb_ord_cpn = $order->get_used_coupons();
												} else {
													$mwb_ord_cpn = $order->get_coupon_codes();
												}
												$mwb_wlt_status = false;
												if( !empty( $mwb_ord_cpn ) )
												{
													foreach ($mwb_ord_cpn as $k_cpn => $v_cpn) 
													{
														$mwb_cpn_obj = new WC_Coupon( $v_cpn );
														$mwb_cpn_id = $mwb_cpn_obj->get_id();
														$mwb_wlt = get_post_meta( $mwb_cpn_id, 'wrffmwallet',true );
														if( $mwb_wlt )
														{
															$mwb_wlt_status = true;
														}
													}
												}
												if( $mwb_wlt_status )
												{
													$mwb_actual_price = $mwb_per_product_price;
												}
												else
												{
													$mwb_actual_price = $order->get_item_total( $item, $in_tax );
												}
												?>
												<input type="checkbox" <?php echo $checked?> class="mwb_wrffm_exchange_product" value="<?php echo $mwb_actual_price;?>">
											<?php 
											}
											else
											{?>
												<img src="<?php echo MWB_WRFFM_URL?>/assets/images/exchange-disable.png" width="20px">

											<?php $mwb_actual_price = $order->get_item_total( $item, $in_tax );
											}
											?>
										</td>
										<td class="product-name">
										<?php
											$is_visible        = $product && $product->is_visible();
											$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );
								
											echo  '<div class="mwb_wrffm_prod_img">'.wp_kses_post( $thumbnail ).'</div>';
											?>
											<div class="mwb_wrffm_product_title">
											<?php
											echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
											echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );
											?><input type="hidden" class="quanty" value="<?php echo $item['qty']; ?>"> <?php
											do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );
											if( WC()->version < "3.0.0" )
											{
												$order->display_item_meta( $item );
												$order->display_item_downloads( $item );
											}
											else
											{
												wc_display_item_meta( $item );
												wc_display_item_downloads( $item );
											}
											do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
										?>
										<p>
											<b><?php _e( 'Price', 'woocommerce-rma-for-wcfm' ); ?> : </b><?php 
											 echo mwb_wrffm_format_price( $mwb_actual_price ); ?>
											<?php 
												if($in_tax == true)
												{	
												?>
													<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
												<?php 
												}	
											?>
										</p>
										</div>
										</td>
										<td class="product-quantity">
											<?php echo sprintf( '<input type="number" max="%s" min="1" value="%s" class="mwb_wrffm_exchange_product_qty form-control" name="mwb_wrffm_exchange_product_qty">', $item['qty'], $qty );?>
										</td>
										<td class="product-total">
											<?php
											echo mwb_wrffm_format_price( $mwb_actual_price ); ?>
											<?php 
												if($in_tax == true)
												{	
												?>
													<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
												<?php 
												}	
											?>
								  			<input type="hidden" id="quanty" value="<?php echo $item['qty']; ?>"> 
										</td>
									</tr>
									<?php 
								}
							}?>
						<tr>
							<th scope="row" colspan="3"><?php _e('Total Amount', 'woocommerce-rma-for-wcfm') ?></th>
							<td class="mwb_wrffm_total_amount_wrap"><span id="mwb_wrffm_total_exchange_amount"><?php echo mwb_wrffm_format_price(0);?></span><?php 
								if($in_tax == true)
								{	
								?>
									<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
								<?php 
								}
							?>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="mwb_wrffm_return_notification_checkbox"><img src="<?php echo MWB_WRFFM_URL?>/assets/images/loading.gif" width="40px"></div>
			</div>
			<p class="form-row form-row form-row-wide mwb_wrffm_exchange_note">
				<?php do_action('mwb_wrffm_exchange_after_order_item_table'); ?>
			</p>
			<p class="form-row form-row form-row-wide mwb_wrffm_exchange_note">
				<i><img src="<?php echo MWB_WRFFM_URL?>/assets/images/return-disable.png" width="20px"> : <?php _e('It means product can\'t be exchanged.', 'woocommerce-rma-for-wcfm');?></i>
			</p>
			<p id="mwb_wrffm_variation_list"></p>
			<p class="form-row form-row form-row-wide">
				<?php 
				$choose_product_button ='<input type="button" class="button btn mwb_wrffm_exhange_shop" name="mwb_wrffm_exhange_shop"  id="mwb_wrffm_exhange_shop" value="'. __('CHOOSE PRODUCTS', 'woocommerce-rma-for-wcfm').'" class="input-text">';
				?>
				<a class="mwb_wrffm_exhange_shop" href="javascript:void(0);">
					
					<?php echo apply_filters('mwb_wrffm_exchange_choose_product_button', $choose_product_button); ?>
					<span class="mwb_wrffm_exchange_notification_choose_product "><img src="<?php echo MWB_WRFFM_URL?>/assets/images/loading.gif" width="20px"></span>
				</a>
				
			</p>
			<?php 
			
			$total_price = 0;
			if(isset($exchange_to_products) && !empty($exchange_to_products))
			{
			?>
			<div class="mwb_wrffm_product_table_container">
				<table class="shop_table order_details  mwb_wrffm_exchanged_products mwb_wrffm_product_table">
					<thead>
						<tr>
							<th><?php _e( 'Product', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th><?php _e( 'Quantity', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th><?php _e( 'Price', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
							<th><?php _e( 'Remove', 'woocommerce-rma-for-wcfm' ); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach($exchange_to_products as $key=>$exchange_to_product)
						{
							$props = array();
							$variation_attributes = array();
							$mwb_woo_tax_enable_setting = get_option('woocommerce_calc_taxes');
							$mwb_woo_tax_display_shop_setting = get_option('woocommerce_tax_display_shop');
							$mwb_wrffm_tax_test = false;
							if(isset($exchange_to_product['variation_id']))
							{
								if($exchange_to_product['variation_id'])
								{
									$variation_product = wc_get_product($exchange_to_product['variation_id']);
									$variation_attributes = $variation_product->get_variation_attributes();
									$variation_attributes = isset($exchange_to_product['variations']) ? $exchange_to_product['variations'] : $variation_product->get_variation_attributes();
									$variation_labels = array();
									foreach ($variation_attributes as $label => $value){
										if(is_null($value) || $value == ''){
											$variation_labels[] = $label;
										}
									}
									if(count($variation_labels)){
										$all_line_items = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );
										$var_attr_info = array();
										foreach($all_line_items as $ear_item){
											$variationID = isset($ear_item['item_meta']['_variation_id']) ? $ear_item['item_meta']['_variation_id'][0] : 0;
												
											if($variationID && $variationID == $exchange_to_product['variation_id']){
												$itemMeta = isset($ear_item['item_meta']) ? $ear_item['item_meta'] : array();
													
												foreach($itemMeta as $metaKey=>$metaInfo){
													$metaName = 'attribute_'. sanitize_title( $metaKey );
													if(in_array($metaName, $variation_labels)){
														$variation_attributes[$metaName] = isset( $term->name ) ? $term->name : $metaInfo[0];
													}
												}
											}
										}
									}
									$mwb_wrffm_thumbnail     = wp_get_attachment_image_src($variation_product->get_image_id(),'thumbnail');
									$mwb_wrffm_thumbnail = $mwb_wrffm_thumbnail[0];
									
									if($mwb_woo_tax_enable_setting == 'yes')
									{	
										$mwb_wrffm_tax_test = true;
										$mwb_wrffm_exchange_to_product_price =wc_get_price_including_tax($variation_product);
									}
									else
									{
										$mwb_wrffm_exchange_to_product_price = $exchange_to_product['price'];
									}
									$product=wc_get_product($exchange_to_product['variation_id']);

								}	
							}
							else
							{
								$product=wc_get_product($exchange_to_product['id']);
								if($mwb_woo_tax_enable_setting == 'yes')
								{
									$mwb_wrffm_tax_test = true;
									$mwb_wrffm_exchange_to_product_price =wc_get_price_including_tax($product);
								}
								else
								{
									$mwb_wrffm_exchange_to_product_price = $exchange_to_product['price'];
								}

								$mwb_wrffm_thumbnail = wp_get_attachment_image_src($product->get_image_id(),'thumbnail');
								$mwb_wrffm_thumbnail = $mwb_wrffm_thumbnail[0];
							
							}

							
							if(isset($exchange_to_product['p_id']))
							{
								if($exchange_to_product['p_id'])
								{
									$grouped_product = new WC_Product_Grouped($exchange_to_product['p_id']);
									$grouped_product_title = $grouped_product->get_title();
									$props = wc_get_product_attachment_props( get_post_thumbnail_id($exchange_to_product['p_id']), $grouped_product );

								}
							}

							
							$pro_price = $exchange_to_product['qty']*$mwb_wrffm_exchange_to_product_price;
							$total_price += $pro_price;
							?>
							<tr>
								<td><?php 
								if(isset($mwb_wrffm_thumbnail) && !is_null($mwb_wrffm_thumbnail))
								{
									?>
										<div class="mwb_wrffm_prod_img"><img width="100" height="100" title="<?php if(isset($props['title']) && !empty($props['title'])){echo $props['title'];}?>" alt="<?php ?>" class="attachment-thumbnail size-thumbnail wp-post-image" src="<?php echo $mwb_wrffm_thumbnail; ?>"></div>
									<?php 
								}	
								else
								{
									?>
										<div class="mwb_wrffm_prod_img"><img width="100" height="100" title="<?php if(isset($props['title']) && !empty($props['title'])){echo $props['title'];}?>" alt="<?php ?>" class="attachment-thumbnail size-thumbnail wp-post-image" src="<?php echo home_url('/wp-content/plugins/woocommerce/assets/images/placeholder.png');?>"></div>
									<?php 
								}	

								?>
									<div class="mwb_wrffm_product_title">
									<?php 
									if(isset($exchange_to_product['p_id']))
									{
										echo $grouped_product_title.' -> ';
									}
									echo $product->get_title(); 
									if(isset($variation_attributes) && !empty($variation_attributes)) 
									{
										echo wc_get_formatted_variation( $variation_attributes );
									}	
									?>
								</div></td>
								<td ><?php echo $exchange_to_product['qty']; ?></td>
								<td ><?php echo mwb_wrffm_format_price($mwb_wrffm_exchange_to_product_price);
									if($mwb_wrffm_tax_test == true)
									{	
									?>
										<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
									<?php 
									}	
								 ?></div></td>
								<td ><?php echo mwb_wrffm_format_price($pro_price);
									if($mwb_wrffm_tax_test == true)
									{	
									?>
										<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
									<?php 
									}
								?></td>
								<td data-key="<?php echo $key;?>" class="mwb_wrffm_exchnaged_product_remove"><a class="remove" href="javascript:void(0)">×</a></td>
							</tr>
						<?php 
						}
						?>
					</tbody>
					<tfoot class="exchange_product_table_footer">
						<th colspan="3"><?php _e('Total Amount', 'woocommerce-rma-for-wcfm' );?></th>
						<th colspan="2" id="mwb_wrffm_exchanged_total_show"> <?php echo mwb_wrffm_format_price($total_price);
						if($mwb_wrffm_tax_test == true)
						{	
							?>
							<small class="tax_label"><?php _e('(incl. tax)','woocommerce-rma-for-wcfm'); ?></small>
							<?php 
						}
						?></th>
					</tfoot>
				</table>
			</div>	
			<?php 
			}
			?>
			<div class="mwb_wrffm_note_tag_wrapper">
			<input type="text" value="<?php echo $total_price;?>" id="mwb_wrffm_exchanged_total" style="display:none;">
			<p class="form-row form-row form-row-wide" id="mwb_wrffm_exchange_extra_amount">
				<label>
					<b>
						<?php
							$reason_exchange_amount = __('<i>Extra Amount Need to Pay</i>', 'woocommerce-rma-for-wcfm');;
							echo apply_filters('mwb_wrffm_exchange_extra_amount', $reason_exchange_amount);
						?> : 
						<span class="mwb_wrffm_exchange_extra_amount">
							<?php echo mwb_wrffm_format_price(0);?>
						</span>
					</b>
				</label>
			</p>
			<p class="form-row form-row form-row-wide">
				<?php do_action('mwb_wrffm_exchange_before_exchange_subject'); ?>
			</p>
			<p class="form-row form-row form-row-wide">
				<label>
					<b>
						<?php 
							$reason_exchange_request = __('Subject of Exchange Request', 'woocommerce-rma-for-wcfm');
							echo apply_filters('mwb_wrffm_exchange_request_subject', $reason_exchange_request);
						?>
					</b>
				</label>
				<?php 
				$predefined_exchange_reason = get_option('mwb_wrffm_exchange_predefined_reason', false);
				if(isset($predefined_exchange_reason) && !empty($predefined_exchange_reason))
				{	
				?>
					<div class="mwb_wrffm_subject_dropdown">
						<select name="mwb_wrffm_exchange_request_subject" id="mwb_wrffm_exchange_request_subject">
							<?php 
							foreach($predefined_exchange_reason as $predefine_reason)
							{
								if ( ! empty( $predefine_reason ) ) {
									?>
									<option value="<?php echo $predefine_reason?>"><?php echo $predefine_reason?></option>
									<?php 
								}
							}
							?>
							<option value=""><?php _e('Other', 'woocommerce-rma-for-wcfm');?></option>
						</select>
					</div>
				<?php 
				}
				?>
			</p>
			<p class="form-row form-row form-row-wide">	
				<input type="text" name="mwb_wrffm_exchange_request_subject" id="mwb_wrffm_exchange_request_subject_text" class="input-text mwb_wrffm_exchange_request_subject" placeholder="<?php _e('Write your reason subject','woocommerce-rma-for-wcfm');?>">
			</p>
			<?php 
			$predefined_exchange_desc = get_option('mwb_wrffm_exchange_request_description', false);
			if(isset($predefined_exchange_desc))
			{	
				if($predefined_exchange_desc == 'yes')
				{
					?>
					<p class="form-row form-row form-row-wide">
						<label>
							<b>
							<?php 
								$reason_exchange_request = __('Reason of Exchange Request', 'woocommerce-rma-for-wcfm');
								echo apply_filters('mwb_wrffm_exchange_request_reason', $reason_exchange_request);
							?>
							</b>
						</label>
						<?php $placeholder = get_option( 'mwb_wrffm_exchange_placeholder_text' , 'Reason for Exchange Request' );
						if ($placeholder == '') {
						 	$placeholder = __('Reason for the Exchange Request','woocommerce-rma-for-wcfm');
						 } ?>
						<textarea name="mwb_wrffm_exchange_request_reason" cols="40" style="height: 222px;" class="mwb_wrffm_exchange_request_reason form-control" placeholder="<?php echo $placeholder; ?>"><?php echo $reason;?></textarea>
					</p>
					
					<?php 
				}
				else
				{
					?>
					<input type="hidden" name="mwb_wrffm_exchange_request_reason" class="mwb_wrffm_exchange_request_reason form-control" value="<?php _e('No Reason Enter','woocommerce-rma-for-wcfm')?>">
					<?php 				
				}	
			}
			else
			{
				?>
				<input type="hidden" name="mwb_wrffm_exchange_request_reason" class="mwb_wrffm_exchange_request_reason form-control" value="<?php _e('No Reason Enter','woocommerce-rma-for-wcfm')?>">
				<?php 				
			}
			?>
			
			<p class="form-row form-row form-row-wide">
				<input type="submit" class="button btn mwb_wrffm_exchange_request_submit" name="mwb_wrffm_exchange_request_submit" value="<?php _e('Submit Request','woocommerce-rma-for-wcfm')?>" class="input-text">
				<div class="mwb_wrffm_exchange_notification"><img src="<?php echo MWB_WRFFM_URL?>/assets/images/loading.gif" width="40px"></div>
			</p>
			<br/>
			<p class="form-row form-row form-row-wide">
				<?php do_action('mwb_wrffm_exchange_after_submit_button'); ?>
			</p>
			<div class="mwb-wrffm_customer_detail">
				<?php wc_get_template( 'order/order-details-customer.php', array( 'order' =>  $order ) ); ?>
			</div>
		</div>
	</div>
<?php 
}
else
{
	 $exchange_request_not_send = __('Exchange Request can\'t be send. ', 'woocommerce-rma-for-wcfm');
	 echo apply_filters('mwb_wrffm_exchange_request_not_send', $exchange_request_not_send);
	 echo $reason;
}

/**
 * woocommerce_sidebar hook.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
$mwb_wrffm_show_sidebar_on_form = get_option('mwb_wrffm_show_sidebar_on_form','no');	
if($mwb_wrffm_show_sidebar_on_form == 'yes')
{
	/**
	 * woocommerce_after_main_content hook.
	 *
	 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
	 */
	do_action( 'woocommerce_after_main_content' );
} else{
	?></div><?php
}

get_footer( 'shop' );
?>