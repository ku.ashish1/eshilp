<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
$user_id = get_current_user_id();

$mwb_main_wrapper_class = get_option('mwb_wrffm_return_exchange_class');
$mwb_child_wrapper_class = get_option('mwb_wrffm_return_exchange_child_class');

$mwb_wrffm_current_user_is_vendor = true;
$mwb_wrffm_usr_cap = get_user_meta( $user_id, 'wp_capabilities', true );
foreach ( $mwb_wrffm_usr_cap as $ckey => $cvalue ) {
    if ( 'wcfm_vendor' == $ckey || 'administrator' == $ckey) {
        if ( $cvalue == 1 ) {
            $mwb_wrffm_current_user_is_vendor = true;
        } else {
            $mwb_wrffm_current_user_is_vendor = false;
        }
    } else {
        $mwb_wrffm_current_user_is_vendor = false;
    }
}

if($mwb_wrffm_current_user_is_vendor)
{
    ?>
    <div class="woocommerce woocommerce-account <?php echo $mwb_main_wrapper_class;?>">
        <div class=" <?php echo $mwb_child_wrapper_class;?>">
            <div class="wcfm-page-headig">
                <span class="wcfmfa fa-retweet"></span>
                <span class="wcfm-page-heading-text">  <?php _e( 'RMA Request', 'woocommerce-rma-for-wcfm' ); ?></span>
                <?php do_action( 'wcfm_page_heading' ); ?>
            </div>
            <div id="mwb_wrffm_vendor_form_wrapper">
              
                <?php 
                $mwb_wrffm_orderid = isset($_GET['orderid']) ? sanitize_text_field($_GET['orderid']) : 0;
                if($mwb_wrffm_orderid > 0)
                {
                    $vendor_product_ids = mwb_wrffm_get_vendor_product();
                    $order = wc_get_order($mwb_wrffm_orderid);
                    $mwb_wrffm_order_status = $order->get_status();
                    if( WC()->version < "3.0.0" )
                    {
                        $order_id=$order->id;
                    }
                    else
                    {
                        $order_id=$order->get_id();
                    }
                    $mwb_wrffm_request_approve = get_post_meta($order_id, 'mwb_wrffm_request_approve'.$user_id ,true);

                    $mwb_wrffm_request_rejected = get_post_meta($order_id, 'mwb_wrffm_request_rejected'.$user_id,true);
                    $return_datas = get_post_meta($order_id, 'mwb_wrffm_return_product', true);
                    $line_items  = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );
                    if(is_array($line_items) && !empty($line_items)){
                        update_post_meta($order_id,'woocommerce_admin_order_new_2_item_types',$line_items);
                    }
                    $line_items = get_post_meta($order_id,'woocommerce_admin_order_new_2_item_types',true);
                    $mwb_wrffm_enable_vendor_addon=get_option('mwb_wrffm_enable_vendor_addon','no');
                    $vendor_fee_enable = get_option('mwb_wrffm_enable_vendor_ship_fee', false);
                    ?>
                    <div class="mwb_wcffm_top_element_container">
                        <h2><?php _e( 'RMA Request Manage', 'woocommerce-rma-for-wcfm' ); ?></h2>
                    </div>
                    <div class="mwb_wrffm_refund_wrapper_for_vendor"> 
                        <?php echo '<h2>'.__('Refund Request','woocommerce-rma-for-wcfm').'</h2>'; ?>
                        <div class="mwb_wrffm_refund_main_container">
                            <ul style="display: none;" class="woocommerce-error" id="mwb-return-alert">
                            </ul><?php 
                        if(isset($return_datas) && !empty($return_datas))
                        {
                            foreach($return_datas as $key=>$return_data)
                            {
                                $date=date_create($key);
                                $date_format = get_option('date_format');
                                $date=date_format($date,$date_format);
                                ?>
                                <p><?php _e( 'Following product refund request made on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $date?>.</b></p>
                                <div>
                                    <div id="mwb_wrffm_return_wrapper">
                                        <table class="shop_table order_details">
                                            <thead>
                                                <tr>
                                                    <th><?php _e( 'Item', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                    <th><?php _e( 'Name', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                    <th><?php _e( 'Cost', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                    <th><?php _e( 'Qty', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                    <th><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                $total = 0;

                                                $return_products = $return_data['products'];

                                                foreach ( $line_items as $item_id => $item ) 
                                                {
                                                    foreach($return_products as $return_products_key => $return_product)
                                                    {

                                                        if($item_id == $return_product['item_id'])
                                                        {
                                                            foreach ($return_product as $return_product_key => $return_product_value){

                                                                if($return_product_key == 'product_id'){

                                                                    $_product = wc_get_product( $return_product_value );
                                                                    if(in_array($return_product_value, $vendor_product_ids))
                                                                    {
                                                                        if(isset($return_product['approve_date']))
                                                                        {
                                                                            $approve_date=date_create($return_product['approve_date']);
                                                                        }
                                                                        $thumbnail     = $_product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $_product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : '';
                                                                        ?>
                                                                        <tr>
                                                                            <td class="thumb">
                                                                                <?php
                                                                                echo '<div class="wc-order-item-thumbnail">' . wp_kses_post( $thumbnail ) . '</div>';
                                                                                ?>
                                                                            </td>
                                                                            <td class="name">
                                                                                <?php
                                                                                echo esc_html( $_product->get_name());
                                                                                if ( $_product && $_product->get_sku() ) {
                                                                                    echo '<div class="wc-order-item-sku"><strong>' . __( 'SKU:', 'woocommerce-rma-for-wcfm' ) . '</strong> ' . esc_html( $_product->get_sku() ) . '</div>';
                                                                                }
                                                                                if ( ! empty( $return_product['variation_id'] ) ) {
                                                                                    echo '<div class="wc-order-item-variation"><strong>' . __( 'Variation ID:', 'woocommerce-rma-for-wcfm' ) . '</strong> ';

                                                                                    echo esc_html(  $return_product['variation_id']);
                                                                                } elseif ( ! empty(  $return_product['variation_id']) ) {
                                                                                    echo esc_html(  $return_product['variation_id']) . ' (' . __( 'No longer exists', 'woocommerce-rma-for-wcfm' ) . ')';
                                                                                    
                                                                                    echo '</div>';
                                                                                }
                                                                                if( WC()->version < "3.1.0" )
                                                                                {
                                                                                    $item_meta      = new WC_Order_Item_Meta( $item, $_product );
                                                                                    $item_meta->display();
                                                                                }
                                                                                else
                                                                                {
                                                                                    $item_meta      = new WC_Order_Item_Product( $item, $_product );
                                                                                    wc_display_item_meta($item_meta);
                                                                                }
                                                                                ?>
                                                                            </td>
                                                                            <td><?php echo mwb_wrffm_format_price($return_product['price']);?></td>
                                                                            <td><?php echo $return_product['qty'];?></td>
                                                                            <td><?php echo mwb_wrffm_format_price($return_product['price']*$return_product['qty']);?></td>
                                                                        </tr>
                                                                        <?php 
                                                                        $total += $return_product['price']*$return_product['qty'];
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }       
                                                }
                                                ?>
                                                <tr>
                                                    <th colspan="4"><?php _e('Total Amount', 'woocommerce-rma-for-wcfm');?></th>
                                                    <th><?php echo mwb_wrffm_format_price($total);?></th>
                                                </tr>
                                            </tbody>
                                        </table>    
                                    </div>
                                    <div class="mwb_wrffm_extra_reason mwb_wrffm_extra_reason_for_refund">
                                        <?php 

                                        $fee_enable = get_option('mwb_wrffm_return_shipcost_enable', false);
                                        if($fee_enable == 'yes' && $vendor_fee_enable == 'on' )
                                        {
                                            $disable = "";
                                            if($return_data['status'] != 'pending' || $mwb_wrffm_request_approve == '1')
                                            {
                                                $disable = 'readonly';
                                            }
                                            else 
                                            {
                                                ?>
                                                <div id="mwb_wrffm_add_fee">
                                                    <?php 
                                                }

                                                $added_fees = get_post_meta($order_id, 'mwb_wrffm_return_added_fee'.$user_id, true);

                                                if(isset($added_fees) && !empty($added_fees))
                                                {
                                                    if(is_array($added_fees))
                                                    {
                                                        ?>
                                                        <p><?php _e('Fees amount is deducted from Refund amount', 'woocommerce-rma-for-wcfm');?></p>
                                                        <?php 
                                                        foreach($added_fees as $da=>$added_fee)
                                                        {
                                                            if($da == $key)
                                                            {
                                                                if(is_array($added_fee))
                                                                {
                                                                    foreach($added_fee as $fee)
                                                                    {
                                                                        $return_data['amount'] -= $fee['val']; 
                                                                        $total -= $fee['val'];
                                                                        if($return_data['status'] == 'pending' && $mwb_wrffm_request_approve != '1')
                                                                        {
                                                                            ?>
                                                                            <div class="mwb_wrffm_add_fee">

                                                                                <?php 
                                                                            }
                                                                            ?>
                                                                            
                                                                            <input type="text" placeholder="<?php _e('Fee Name','woocommerce-rma-for-wcfm')?>" <?php echo $disable?> value="<?php echo $fee['text'];?>" name="mwb_return_fee_txt[]" class="mwb_return_fee_txt">
                                                                            <input type="text" name="" placeholder="0" <?php echo $disable?> value="<?php echo $fee['val'];?>" class="mwb_return_fee_value wc_input_price">
                                                                            <?php 
                                                                            if($return_data['status'] == 'pending' && $mwb_wrffm_request_approve != '1')
                                                                                {   ?>
                                                                                    <input type="button" value="<?php _e('Remove','woocommerce-rma-for-wcfm')?>" class="button mwb_wrffm_remove-return-product-fee">
                                                                                    <?php 
                                                                                }

                                                                                ?>
                                                                            </div>
                                                                            <?php   
                                                                            
                                                                        }
                                                                    }
                                                                    break;
                                                                }
                                                            }   
                                                        }
                                                    }   
                                                    if($return_data['status'] == 'pending' && $mwb_wrffm_request_approve != '1')
                                                    { 
                                                        if(isset($mwb_wrffm_enable_vendor_addon) && $mwb_wrffm_enable_vendor_addon == 'on'){
                                                            ?>
                                                        </div>
                                                        <button class="button mwb_wrffm_add-return-product-fee" type="button"><?php _e('Add Fee', 'woocommerce-rma-for-wcfm');?></button>
                                                        <button class="button button-primary mwb_wrffm_save-return-product-fee" type="button" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>"><?php _e('Save', 'woocommerce-rma-for-wcfm');?></button>
                                                        <?php 
                                                    }
                                                }

                                            }

                                            if($return_data['status'] == 'pending' && $mwb_wrffm_request_approve != '1')
                                            {
                                                ?>
                                                <input type="hidden" value="<?php echo $return_data['amount']?>" id="mwb_wrffm_refund_amount">
                                                <input type="hidden" value="<?php echo $return_data['subject']?>" id="mwb_wrffm_refund_reason">
                                                <?php
                                            }
                                            ?>
                                            <p><strong>

                                                <?php _e('Refund Amount', 'woocommerce-rma-for-wcfm');?> :</strong> <?php echo mwb_wrffm_format_price(mwb_wrffm_refund_policy_price_deduction($total));update_post_meta($order_id,'refundable_amount',number_format($total,2));?> <input type="hidden" name="mwb_wrffm_total_amount_for_refund" class="mwb_wrffm_total_amount_for_refund" value="<?php echo mwb_wrffm_refund_policy_price_deduction($total) ; ?>"></p>
                                                <div class="mwb_wrffm_reason">   
                                                    <p><strong><?php _e('Subject', 'woocommerce-rma-for-wcfm');?> :</strong><i> <?php echo $return_data['subject']?></i></p></p>
                                                    <p><b><?php _e('Reason', 'woocommerce-rma-for-wcfm');?> :</b></p>
                                                    <p><?php echo $return_data['reason']?></p>
                                                    <?php 
                                                    $req_attachments = get_post_meta($order_id, 'mwb_wrffm_return_attachment', true);

                                                    if(isset($req_attachments) && !empty($req_attachments))
                                                    {   
                                                        ?>
                                                        <p><b><?php _e('Attachment', 'woocommerce-rma-for-wcfm');?> :</b></p>
                                                        <?php
                                                        if(is_array($req_attachments))
                                                        {
                                                            foreach($req_attachments as $da=>$attachments)
                                                            {
                                                                if($da == $key)
                                                                {
                                                                    $count = 1;
                                                                    foreach($attachments['files'] as $attachment)
                                                                    {
                                                                        if($attachment != $order_id.'-')
                                                                        {
                                                                            ?>
                                                                            <a href="<?php echo home_url()?>/wp-content/attachment/<?php echo $attachment?>" target="_blank"><?php _e('Attachment','woocommerce-rma-for-wcfm');?>-<?php echo $count;?></a>
                                                                            <?php 
                                                                            $count++;
                                                                        }
                                                                    }   
                                                                    break;
                                                                }
                                                            }       
                                                        }   
                                                    }
                                                    if($return_data['status'] == 'pending' && $mwb_wrffm_request_approve != '1' && $mwb_wrffm_request_rejected != '1' ){
                                                        if(isset($mwb_wrffm_enable_vendor_addon) && $mwb_wrffm_enable_vendor_addon == 'on'){

                                                         ?>
                                                         <p id="mwb_wrffm_return_package">
                                                            <input type="button" value="<?php _e('Accept Request','woocommerce-rma-for-wcfm');?>" class="button" id="mwb_wrffm_accept_return_vendor" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>">
                                                            <input type="button" value="<?php _e('Cancel Request','woocommerce-rma-for-wcfm');?>" class="button" id="mwb_wrffm_cancel_return_request_vendor" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>">
                                                        </p>
                                                        <?php 
                                                    }
                                                    else{
                                                        ?><p class="mwb_wrffm_vendor_permission"><?php _e('Vendor does not have permission to Manage Refund/Exchange Request.','woocommerce-rma-for-wcfm')?></p>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="mwb_wrffm_return_loader" style="display: none;">
                                                <img src="<?php echo home_url();?>/wp-admin/images/spinner-2x.gif">
                                            </div>
                                        </div>  
                                        <p>
                                            <?php 
                                            if( $mwb_wrffm_request_approve == '1' && $mwb_wrffm_request_rejected != '1')
                                            {
                                                ?>
                                                <input type="hidden" value="<?php echo mwb_wrffm_currency_seprator(mwb_wrffm_refund_policy_price_deduction($total)) ?>" id="mwb_wrffm_refund_amount">
                                                <input type="hidden" value="<?php echo $return_data['subject']?>" id="mwb_wrffm_refund_reason">
                                                <?php
                                                $refundable_amount = 0;
                                                $refundable_amount = get_post_meta($order_id,'refundable_amount_'.$user_id,true);
                                                $date_format = get_option('date_format');
                                                if( isset( $approve_date) && ! empty( $approve_date ) ) {
                                                    $approve_date = date_format($approve_date,$date_format);
                                                } else {
                                                    $approve_date = '';
                                                }
                                                $mwb_wrffm_enable_vendor_refund = get_option( 'mwb_wrffm_enable_vendor_refund', 'no' );
                                                if($refundable_amount > 0 && $mwb_wrffm_enable_vendor_refund == 'on')
                                                {
                                                    _e( 'Following product refund request is approved on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b><input type="button" name="mwb_wrffm_left_amount" class="button button-primary" data-orderid="<?php echo $order_id; ?>" id="mwb_wrffm_left_amount" Value="<?php _e('Refund Amount','woocommerce-rma-for-wcfm');?>" > <?php
                                                }
                                                else{
                                                    _e( 'Following product refund request is approved on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b>
                                                    <?php
                                                }

                                                $mwb_wrffm_manage_stock_for_return = get_post_meta($order_id,'mwb_wrffm_manage_stock_for_return_'.$user_id,true);

                                                if($mwb_wrffm_manage_stock_for_return == '')
                                                {
                                                    $mwb_wrffm_manage_stock_for_return = 'yes';
                                                }
                                                $manage_stock = get_option('mwb_wrffm_return_request_manage_stock');
                                                $mwb_wrffm_enable_vendor_manage_stock = get_option( 'mwb_wrffm_enable_vendor_manage_stock', 'no' );
                                                if($manage_stock == "yes" && $mwb_wrffm_manage_stock_for_return == 'yes' && $mwb_wrffm_enable_vendor_manage_stock == 'on')
                                                {
                                                    ?> <div><?php _e( 'When Product Back in stock then for stock management click on ', 'woocommerce-rma-for-wcfm' ); ?> <input type="button" name="mwb_wrffm_manage_stock" class="button button-primary" id="mwb_wrffm_manage_stock" data-type="mwb_wrffm_return_stock" data-orderid="<?php echo $order_id; ?>" Value="<?php _e('Manage Stock','woocommerce-rma-for-wcfm');?>" ></div> <?php
                                                }
                                            }
                                            if($mwb_wrffm_request_rejected == '1')
                                            {
                                                _e( 'Following product refund request is cancelled by you.', 'woocommerce-rma-for-wcfm' ); ?>
                                                <?php
                                            }
                                            ?>
                                        </p>
                                        <hr/>
                                    </div>
                                    <?php 
                                }
                            } 
                            else 
                            {   
                                ?>
                                <p>
                                    <div class="mwb-wrffm-alert">
                                        <?php _e('No request', 'woocommerce-rma-for-wcfm');?>
                                    </div>
                                </p>
                                <?php 
                            }
                            ?></div>
                            <div class="mwb_rma_exchange_wrapper_for_vendor">
                                <?php echo '<h2>'.__('Exchange Request','woocommerce-rma-for-wcfm').'</h2>';
                                $exchange_details = get_post_meta($order_id, 'mwb_wrffm_exchange_product', true);
                                $mwb_wrffm_exchange_rejected = get_post_meta($order_id, 'mwb_wrffm_exchange_rejected'.$user_id,true);

                                $line_items  = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );
                                if(is_array($line_items) && !empty($line_items)){
                                    update_post_meta($order_id,'woocommerce_admin_order_new_ex_item_types',$line_items);
                                }
                                $line_items = get_post_meta($order_id,'woocommerce_admin_order_new_ex_item_types',true);
                                ?><div class="mwb_wrffm_exchange_main_container">
                                    <ul style="display: none;" class="woocommerce-error" id="mwb-return-alert">
                                                    </ul><?php 
                                if(isset($exchange_details) && !empty($exchange_details))
                                {
                                    foreach($exchange_details as $date=>$exchange_detail)
                                    {
                                        if(isset($exchange_details[$date]['subject']) && $exchange_details[$date]['reason'])
                                        {
                                            $approve_date = date_create($date);
                                            $date_format = get_option('date_format');
                                            if( isset( $approve_date) && ! empty( $approve_date ) ) {
                                                $approve_date=date_format($approve_date,$date_format);
                                            } else {
                                                $approve_date = '';
                                            }

                                            $pending_date = '';
                                            if($exchange_detail['status'] == 'pending')
                                            {
                                                $pending_date = $date;
                                            }
                                            $subject = $exchange_details[$date]['subject'];
                                            $reason = $exchange_details[$date]['reason'];
                                            if(isset($exchange_detail['from']))
                                            {
                                                $exchange_products = $exchange_detail['from'];
                                            }
                                            else
                                            {
                                                $exchange_products = array();
                                            }
                                            if(isset($exchange_detail['to']))
                                            {
                                                $exchange_to_products = $exchange_detail['to'];
                                            }
                                            else
                                            {
                                                $exchange_to_products = array();
                                            }

                                            if(isset($exchange_detail['fee']))
                                            {
                                                $exchange_fees = $exchange_detail['fee'];
                                            }
                                            else
                                            {
                                                $exchange_fees = array();
                                            }

                                            $exchange_status = $exchange_detail['status'];
                                            $exchange_reason = $exchange_detail['reason'];
                                            $exchange_subject = $exchange_detail['subject'];

                                            _e( 'Following product exchange request is made on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b>

                                            <div>
                                                <div id="mwb_wrffm_exchange_wrapper">
                                                    <p><b><?php _e('Exchanged Product', 'woocommerce-rma-for-wcfm' ); ?></b></p>
                                                    <table class="shop_table order_details">
                                                        <thead>
                                                            <tr>
                                                                <th><?php _e( 'Item', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Name', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Cost', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Qty', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                            if(isset($exchange_products) && !empty($exchange_products))
                                                            {
                                                                $selected_total_price = 0;
                                                                foreach ( $line_items as $item_id => $item )
                                                                {
                                                                    foreach($exchange_products as $key=>$exchanged_product)
                                                                    {
                                                                        if($item_id == $exchanged_product['item_id'])
                                                                        {
                                                                            foreach ($exchanged_product as $exchanged_product_key => $exchanged_product_value){

                                                                                if($exchanged_product_key == 'product_id'){
                                                                                    $_product  = wc_get_product( $exchanged_product_value );
                                                                                    $item_meta = wc_get_order_item_meta( $item_id,$key );
                                                                                    $thumbnail     = $_product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $_product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : '';
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td class="thumb">
                                                                                            <?php
                                                                                            echo '<div class="wc-order-item-thumbnail">' . wp_kses_post( $thumbnail ) . '</div>';
                                                                                            ?>
                                                                                        </td>
                                                                                        <td class="name">
                                                                                            <?php
                                                                                            echo esc_html( $_product->get_name() );
                                                                                            if ( $_product && $_product->get_sku() ) {
                                                                                                echo '<div class="wc-order-item-sku"><strong>' . __( 'SKU:', 'woocommerce-rma-for-wcfm' ) . '</strong> ' . esc_html( $_product->get_sku() ) . '</div>';
                                                                                            }
                                                                                            if ( ! empty( $exchanged_product['variation_id'] ) ) {
                                                                                                echo '<div class="wc-order-item-variation"><strong>' . __( 'Variation ID:', 'woocommerce-rma-for-wcfm' ) . '</strong> ';

                                                                                                echo esc_html( $exchanged_product['variation_id'] );
                                                                                            } elseif ( ! empty( $exchanged_product['variation_id'] ) ) {
                                                                                                echo esc_html( $exchanged_product['variation_id'] ) . ' (' . __( 'No longer exists', 'woocommerce-rma-for-wcfm' ) . ')';
                                                                                            }
                                                                                            echo '</div>';

                                                                                            if( WC()->version < "3.1.0" )
                                                                                            {
                                                                                                $item_meta      = new WC_Order_Item_Meta( $item, $_product );
                                                                                                $item_meta->display();
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                $item_meta      = new WC_Order_Item_Product( $item, $_product );
                                                                                                wc_display_item_meta($item_meta);
                                                                                            }
                                                                                            ?>
                                                                                        </td>
                                                                                        <td><?php echo mwb_wrffm_format_price($exchanged_product['price']);?></td>
                                                                                        <td><?php echo $exchanged_product['qty'];?></td>
                                                                                        <td><?php echo mwb_wrffm_format_price($exchanged_product['price']*$exchanged_product['qty']);?></td>
                                                                                    </tr>
                                                                                    <?php 
                                                                                    $selected_total_price += $exchanged_product['price']*$exchanged_product['qty'];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }       
                                                            }
                                                            ?>
                                                            <tr>
                                                                <th colspan="4"><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php echo mwb_wrffm_format_price($selected_total_price); ?></th>
                                                            </tr>
                                                        </tbody>
                                                    </table>    
                                                </div>
                                                <div id="mwb_wrffm_exchange_wrapper">
                                                    <p><b><?php _e('Requested Product', 'woocommerce-rma-for-wcfm' ); ?></b></p>
                                                    <table class="shop_table order_details">
                                                        <thead>
                                                            <tr>
                                                                <th><?php _e( 'Item', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Name', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Cost', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Qty', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                            $mwb_woo_tax_enable_setting = get_option('woocommerce_calc_taxes');
                                                            $mwb_woo_tax_display_shop_setting = get_option('woocommerce_tax_display_shop');
                                                            $mwb_wrffm_tax_test = false;

                                                            if(isset($exchange_to_products) && !empty($exchange_to_products))
                                                            {
                                                                $total_price = 0;
                                                                foreach($exchange_to_products as $key=>$exchange_to_product)
                                                                {
                                                                    $variation_attributes = array();

                                                            //Variable Product
                                                                    if(isset($exchange_to_product['variation_id']))
                                                                    {
                                                                        if($exchange_to_product['variation_id'])
                                                                        {
                                                                            $variation_product = wc_get_product($exchange_to_product['variation_id']);
                                                                            $variation_attributes = $variation_product->get_variation_attributes();
                                                                            $variation_labels = array();
                                                                            foreach ($variation_attributes as $label => $value){
                                                                                if(is_null($value) || $value == ''){
                                                                                    $variation_labels[] = $label;
                                                                                }
                                                                            }

                                                                            if(isset($exchange_to_product['variations']) && !empty($exchange_to_product['variations']))
                                                                            {
                                                                                $variation_attributes = $exchange_to_product['variations'];
                                                                            }
                                                                            if($mwb_woo_tax_enable_setting == 'yes')
                                                                            {   
                                                                                $mwb_wrffm_tax_test = true;
                                                                                if(isset($exchange_to_product['price'])){
                                                                                    $exchange_to_product_price = $exchange_to_product['price'];
                                                                                }else{
                                                                                    $exchange_to_product_price = wc_get_price_including_tax($variation_product);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                $exchange_to_product_price = $exchange_to_product['price'];
                                                                            }
                                                                        } $product = wc_get_product($exchange_to_product['variation_id']);  
                                                                    }
                                                                    else
                                                                    {
                                                                        $product = wc_get_product($exchange_to_product['id']);

                                                                        if($mwb_woo_tax_enable_setting == 'yes')
                                                                        {   
                                                                            $mwb_wrffm_tax_test = true;
                                                                            if(isset($exchange_to_product['price'])){
                                                                                $exchange_to_product_price = $exchange_to_product['price'];
                                                                            }else{
                                                                                $exchange_to_product_price = wc_get_price_including_tax($product);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            $exchange_to_product_price = $exchange_to_product['price'];
                                                                        }

                                                                    }
                                                                    if(isset($exchange_to_product['p_id']))
                                                                    {
                                                                        if($exchange_to_product['p_id'])
                                                                        {
                                                                            $grouped_product = new WC_Product_Grouped($exchange_to_product['p_id']);
                                                                            $grouped_product_title = $grouped_product->get_title();
                                                                        }
                                                                    }

                                                                    $pro_price = $exchange_to_product['qty']*$exchange_to_product_price;
                                                                    $total_price += $pro_price;
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php 
                                                                            if(isset($exchange_to_product['p_id']))
                                                                            {
                                                                                echo $grouped_product->get_image();
                                                                            }
                                                                            elseif(isset($variation_attributes) && !empty($variation_attributes))
                                                                            {
                                                                                echo $variation_product->get_image();
                                                                            }   
                                                                            else 
                                                                            {
                                                                                echo $product->get_image();
                                                                            }   
                                                                            ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php 
                                                                            if(isset($exchange_to_product['p_id']))
                                                                            {
                                                                                echo $grouped_product_title.' -> ';
                                                                            }
                                                                            echo $product->get_title(); 
                                                                            if ( $product && $product->get_sku() ) {
                                                                                echo '<div class="wc-order-item-sku"><strong>' . __( 'SKU:', 'woocommerce-rma-for-wcfm' ) . '</strong> ' . esc_html( $product->get_sku() ) . '</div>';
                                                                            }
                                                                            if(isset($variation_attributes) && !empty($variation_attributes))
                                                                            {

                                                                                echo wc_get_formatted_variation( $variation_attributes );
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <td><?php echo mwb_wrffm_format_price($exchange_to_product_price); ?></td>
                                                                        <td><?php echo $exchange_to_product['qty']; ?></td>
                                                                        <td><?php echo mwb_wrffm_format_price($pro_price);?></td>
                                                                    </tr>
                                                                    <?php 
                                                                }
                                                            }
                                                            ?>
                                                            <tr>
                                                                <th colspan="4"><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
                                                                <th><?php echo mwb_wrffm_format_price($total_price); ?></th>
                                                            </tr>
                                                        </tbody>
                                                    </table>    
                                                </div>
                                                <div class="mwb_wrffm_extra_reason mwb_wrffm_extra_reason_for_exchange">
                                                    <?php 
                                                    $fee_enable = get_option('mwb_wrffm_exchange_shipcost_enable', false);
                                                    $vendor_fee_enable = get_option('mwb_wrffm_enable_vendor_ship_fee', false);
                                                    if( $fee_enable == 'yes' && $vendor_fee_enable == 'on')
                                                    {
                                                        $readonly = "";
                                                        if($exchange_status == 'complete')
                                                        {
                                                            $readonly = 'readonly="readonly"';
                                                        }   
                                                        else 
                                                        {
                                                            ?>
                                                            <div id="mwb_wrffm_exchange_add_fee">
                                                                <?php   
                                                            }   

                                                            if(isset($exchange_fees) && !empty($exchange_fees))
                                                            {
                                                                if(is_array($exchange_fees))
                                                                {
                                                                    ?>
                                                                    <p><?php _e('Fees amount is added to Paid amount', 'woocommerce-rma-for-wcfm');?></p>
                                                                    <?php 
                                                                    foreach($exchange_fees as $fee)
                                                                    {
                                                                        $total_price += $fee['val']; 
                                                                        if($exchange_status == 'pending')
                                                                        {
                                                                            ?>
                                                                            <div class="mwb_wrffm_exchange_add_fee">
                                                                                <?php 
                                                                            }
                                                                            ?>
                                                                            <input type="text" placeholder="<?php _e('Fee Name','woocommerce-rma-for-wcfm');?>" value="<?php echo $fee['text'];?>" name="mwb_exchange_fee_txt[]" class="mwb_exchange_fee_txt" <?php echo $readonly;?>>
                                                                            <input type="text" name="" placeholder="0" value="<?php echo $fee['val'];?>" class="mwb_exchange_fee_value wc_input_price" <?php echo $readonly;?>>
                                                                            <?php 
                                                                            if($exchange_status == 'pending')
                                                                            {
                                                                                ?>
                                                                                <input type="button" value="<?php _e('Remove','woocommerce-rma-for-wcfm');?>" class="button mwb_wrffm_remove-exchange-product-fee">
                                                                            </div>
                                                                            <?php 
                                                                        }   
                                                                    }   
                                                                }
                                                            }   
                                                            if($exchange_status == 'pending')
                                                            {
                                                                if(isset($mwb_wrffm_enable_vendor_addon) && $mwb_wrffm_enable_vendor_addon == 'on' && isset($vendor_fee_enable) && $vendor_fee_enable == 'on' ){
                                                                    if(!isset($_GET['cropi']) || $_GET['cropi'] != 1)
                                                                    {
                                                                        ?>
                                                                    </div>
                                                                    <button class="button mwb_wrffm_add-exchange-product-fee" type="button"><?php _e('Add fee', 'woocommerce-rma-for-wcfm');?></button>
                                                                    <button class="button button-primary mwb_wrffm_save-exchange-product-fee" type="button" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $date;?>"><?php _e('Save', 'woocommerce-rma-for-wcfm');?></button>
                                                                    <?php 
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $mwb_cpn_used = get_post_meta( $order_id ,'mwb_wrffm_status_exchanged', true );
                                                    if( $mwb_cpn_used )
                                                    {
                                                        $mwb_dis_tot = $mwb_cpn_used;
                                                    }
                                                    else
                                                    {
                                                        $mwb_cpn_dis = $order->get_discount_total();
                                                        $mwb_cpn_tax = $order->get_discount_tax();
                                                        $mwb_dis_tot = $mwb_cpn_dis + $mwb_cpn_tax;
                                                    }
                                                    $mwb_dis_tot = 0;
                                                    if( $total_price - ( $selected_total_price + $mwb_dis_tot ) > 0)
                                                        {?>
                                                            <p><strong><?php _e('Extra Amount Paid', 'woocommerce-rma-for-wcfm');?> : <?php echo mwb_wrffm_format_price( $total_price-( $selected_total_price + $mwb_dis_tot ) );?></strong></p>
                                                            <?php
                                                        }
                                                        else
                                                        {
                                                            if( $mwb_dis_tot > $total_price )
                                                            {
                                                                $total_price = 0;
                                                            }
                                                            else
                                                            {
                                                                $total_price = $total_price - $mwb_dis_tot;
                                                            }
                                                            ?>
                                                            <p><strong><i><?php _e('Left Amount After Exchange', 'woocommerce-rma-for-wcfm');?></i> : <?php echo mwb_wrffm_format_price( $selected_total_price-$total_price ); update_post_meta($order_id,'refundable_amount',number_format( $selected_total_price-$total_price,2))?></strong>
                                                                <input type="hidden" name="mwb_wrffm_left_amount_for_refund" class="mwb_wrffm_left_amount_for_refund" value="<?php echo($selected_total_price-$total_price) ; ?>">
                                                            </p>
                                                            <?php
                                                        }
                                                        ?>
                                                        <div class="mwb_wrffm_reason">   
                                                            <p><strong><?php _e('Subject', 'woocommerce-rma-for-wcfm');?> :</strong><i> <?php echo $exchange_subject;?></i></p>
                                                            <p><b><?php _e('Reason', 'woocommerce-rma-for-wcfm');?> :</b></p>
                                                            <p><?php echo $exchange_reason;?></p>
                                                            <?php 

                                                            if(isset($_GET['cropi']) && $_GET['cropi'] == 1)
                                                            {
                                                                ?><p id="mwb_wrffm_return_package"><b>
                                                                    <?php _e('You are not able to approve this request because another vendor product is also available in this request.(Shop admin will handle this order)','woocommerce-rma-for-wcfm'); ?>
                                                                    </b></p><?php
                                                                }
                                                                else
                                                                {
                                                                    if($exchange_status == 'pending' && $mwb_wrffm_exchange_rejected != '1')
                                                                    {
                                                                        if(isset($mwb_wrffm_enable_vendor_addon) && $mwb_wrffm_enable_vendor_addon == 'on'){

                                                                            ?>  
                                                                            <p>
                                                                                <input type="button" value="<?php _e('Accept Request','woocommerce-rma-for-wcfm'); ?>" class="button" id="mwb_wrffm_accept_exchange" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $date;?>">
                                                                                <input type="button" value="<?php _e('Cancel Request','woocommerce-rma-for-wcfm'); ?>" class="button" id="mwb_wrffm_cancel_exchange" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $date;?>">
                                                                            </p>
                                                                            <?php 
                                                                        }
                                                                        else{
                                                                            ?><p class="mwb_wrffm_vendor_permission"><?php _e('Vendor does not have permission to Manage Refund/Exchange Request.','woocommerce-rma-for-wcfm')?></p>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>  
                                                            </div>
                                                            <div class="mwb_wrffm_exchange_loader" style="display: none">
                                                                <img src="<?php echo home_url();?>/wp-admin/images/spinner-2x.gif">
                                                            </div>
                                                        </div>  
                                                    </div>
                                                    <p>
                                                        <?php 

                                                        if($exchange_detail['status'] == 'complete' )
                                                        {
                                                            $left_amount = get_post_meta($order_id,"mwb_wrffm_left_amount",true);
                                                            $vendor_left_amount = get_post_meta($order_id,"mwb_wrffm_left_amount_".$user_id,true);
                                                            $mwb_wrffm_enable_vendor_refund=get_option('mwb_wrffm_enable_vendor_refund','');
                                                            if(isset($left_amount) && $left_amount != null && $left_amount > 0 && $vendor_left_amount == '')
                                                            {

                                                                ?><p><strong><?php _e( 'Refundable Amount of this order is ', 'woocommerce-rma-for-wcfm' );
                                                                echo $left_amount.". ";
                                                                ?></strong>
                                                                <?php if(isset($mwb_wrffm_enable_vendor_refund) && $mwb_wrffm_enable_vendor_refund=='on'){?><input type="button" name="mwb_wrffm_left_amount" class="button button-primary" id="mwb_wrffm_left_amount" data-orderid="<?php echo $order_id; ?>"  Value="<?php _e('Refund Amount','woocommerce-rma-for-wcfm');?>" ></p>
                                                            <?php } ?>
                                                            <input type="hidden" name="left_amount" id="left_amount" value="<?php echo $left_amount; ?>"><?php  

                                                        }
                                                        $approve_date= date_create($exchange_detail['approve']);
                                                        $date_format = get_option('date_format');
                                                        $approve_date=date_format($approve_date,$date_format);

                                                        _e( 'Above product exchange request is approved on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b>
                                                        <?php
                                                        $exhanged_order_id = get_post_meta($order_id, "date-$date", true);
                                                        ?></p><p><?php _e( 'A new order is generated for your exchange request ', 'woocommerce-rma-for-wcfm' );?>
                                                        <b>Order #<?php echo $exhanged_order_id;?></b>
                                                        <?php 
                                                        $mwb_wrffm_manage_stock_for_exchange = get_post_meta($order_id,'mwb_wrffm_manage_stock_for_exchange_'.$user_id,true);
                                                        if($mwb_wrffm_manage_stock_for_exchange == '')
                                                        {
                                                            $mwb_wrffm_manage_stock_for_exchange = 'yes';
                                                        }
                                                        $manage_stock = get_option('mwb_wrffm_exchange_request_manage_stock');
                                                        if($manage_stock == "yes" && $mwb_wrffm_manage_stock_for_exchange == 'yes')
                                                        {
                                                            ?> <div><?php _e( 'When Product Back in stock then for stock management click on ', 'woocommerce-rma-for-wcfm' ); ?> <input type="button" name="mwb_wrffm_manage_stock" class="button button-primary" id="mwb_wrffm_manage_stock" data-type="mwb_wrffm_exchange_stock" data-orderid="<?php echo $order_id; ?>" Value="<?php _e('Manage Stock','woocommerce-rma-for-wcfm');?>" ></div> <?php
                                                        }
                                                    }

                                                    if($exchange_detail['status'] == 'cancel' || $mwb_wrffm_exchange_rejected == '1')
                                                    {

                                                        $approve_date=date_create($exchange_detail['cancel_date']);
                                                        $approve_date=date_format($approve_date,"F d, Y");
                                                        ?></p><p><?php
                                                        _e( 'Above product exchange request is cancelled on ', 'woocommerce-rma-for-wcfm' ); ?><b><?php echo $approve_date?>.</b>
                                                        <?php
                                                    }
                                                    ?>
                                                </p>
                                                <hr/>
                                                <?php
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                        <p>
                                            <div class="mwb-wrffm-alert">
                                                <?php _e('No request', 'woocommerce-rma-for-wcfm');?>
                                            </div>
                                        </p>
                                        <?php 
                                    }
                                    ?>  
                                </div></div>
                                <?php
                            }
                            else
                            {
                                ?>
                                <div id="mwb_wrffm_vendor_table_wapper_class">
                                    <h2><?php _e('Refund/Exchange Requests','woocommerce-rma-for-wcfm'); ?></h2>
                                    <?php
                                    $orders = mwb_wrffm_get_vendor_order();
                                    $product_ids = mwb_wrffm_get_vendor_product();
                                    if(is_array($orders) && !empty($orders))
                                    {
                                        ?>
                                        <table id="mwb_wrffm_rma_request_list_table" class="mwb_wrffm_rma_vendor_table shop_table order_details" >
                                            <thead>
                                                <th><?php _e('Order ID','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('Order Date','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('Order Status','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('View Request','woocommerce-rma-for-wcfm') ?></th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($orders as $order_id) {

                                                    $mwb_wrffm_link = get_wcfm_rma_request_url( $order_id );
                                                    $order = wc_get_order($order_id);
                                                    $mwb_wrffm_request_made = get_post_meta($order_id,'mwb_wrffm_request_made',true);
                                                    $mwb_wrffm_request_made = true;

                                                    if($mwb_wrffm_request_made)
                                                    {
                                                        $line_items  = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );
                                                        $order_date = date_i18n( get_option( 'date_format' ), strtotime( $order->get_date_created()  ) );
                                                        $mwb_wrffm_other_product_incl = 0 ;
                                                        if(isset($line_items) && !empty($line_items))
                                                        {
                                                            foreach ($line_items as $item) {
                                                                if($item->get_variation_id() > 0)
                                                                {
                                                                    if(!in_array($item->get_variation_id(), $product_ids))
                                                                    {
                                                                        $mwb_wrffm_other_product_incl = 1;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if(!in_array($item->get_product_id(), $product_ids))
                                                                    {
                                                                        $mwb_wrffm_other_product_incl = 1;
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        if($mwb_wrffm_other_product_incl){
                                                            $mwb_wrffm_link = add_query_arg( 'cropi',$mwb_wrffm_other_product_incl,$mwb_wrffm_link );
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $order_id; ?></td>
                                                            <td><?php echo $order_date; ?></td>
                                                            <td><?php echo wc_get_order_status_name($order->get_status());?></td>
                                                            <td><a class="woocommerce-button button view mwb_request_view" href="<?php echo $mwb_wrffm_link; ?>"><?php echo __('Click Here to View','woocommerce-rma-for-wcfm'); ?></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } ?>
                                            </tbody>
                                            <thead>
                                                <th><?php _e('Order ID','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('Order Date','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('Order Status','woocommerce-rma-for-wcfm') ?></th>
                                                <th><?php _e('View Request','woocommerce-rma-for-wcfm') ?></th>
                                            </thead>
                                        </table> 
                                        <?php
                                    }
                                    else
                                        {?>
                                            <div class="mwb-wrffm-alert">
                                                <?php _e('No Requests is available to show !!', 'woocommerce-rma-for-wcfm'); ?>
                                                </div><?php
                                            }
                                            ?></div> <?php
                                        }
                                        ?>  
                                    </div>
                                </div>
                            </div>
                            <?php 
                        }
                        else
                            { ?>
                                <div class="mwb-wrffm-alert">
                                    <strong><?php _e( 'Error!', 'woocommerce-rma-for-wcfm' ); ?></strong>
                                    <?php _e( 'Your account is not enabled for selling, please contact the admin', 'woocommerce-rma-for-wcfm' ); ?>
                                </div>
                                <?php
                            }
