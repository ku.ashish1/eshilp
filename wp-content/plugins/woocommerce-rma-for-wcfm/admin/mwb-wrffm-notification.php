<?php 
/**
 * Exit if accessed directly
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
//Notification setting page for WooCommerce RMA  Product on admin side

$tab = "basic";
if(isset($_GET['tab']))
{
	$tab = $_GET['tab'];
}
$refund_active = "";
$exchange_active = "";
$basic_active = "";
$return_ship_label_setting_active = "";
$mwb_wrffm_addon_section = "";
$mwb_wrffm_license_section = "";
if($tab == "refund")
{
	$refund_active = "nav-tab-active";
}	
elseif($tab == "exchange")
{
	$exchange_active = "nav-tab-active";
}
elseif($tab == "return_ship_label_setting") {
	$return_ship_label_setting_active = "nav-tab-active";	
}
elseif ($tab == "mwb_wrffm_license_section") {
	$mwb_wrffm_license_section = 	"nav-tab-active";	
}	
else if( $tab != "wrffm_tab")
{
	$basic_active = "nav-tab-active";
}	

if(isset($_POST['mwb_wrffm_noti_save_basic']))
{
	?>
	<div class="notice notice-success is-dismissible">
		<p><strong><?php _e('Settings saved.','woocommerce-rma-for-wcfm'); ?></strong></p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text"><?php _e('Dismiss this notices.','woocommerce-rma-for-wcfm'); ?></span>
		</button>
	</div><?php
	unset($_POST['mwb_wrffm_noti_save_basic']);
	$post = $_POST;
	foreach($post as $k=>$val)
	{
		if(is_array($val))
		{
			foreach($val as $a=>$b)
			{
				if(empty($b) & $b != 0)
				{
					unset($val[$a]);
				}	
			}	
		}	
		update_option($k, $val);
	}
	if (!isset($post['mwb_wrffm_enable_time_policy'])) {
		update_option( 'mwb_wrffm_enable_time_policy' , 'no' );
	}
	if (!isset($post['mwb_wrffm_enable_price_policy'])) {
		update_option( 'mwb_wrffm_enable_price_policy' , 'no' );
	}
	if (!isset($post['mwb_wrffm_show_refund_policy_on_product_page'])) {
		update_option( 'mwb_wrffm_show_refund_policy_on_product_page' , 'no' );
	}


}	
$wrffm_price_acc_to_days = get_option( 'mwb_wrffm_price_redumwb' , array() );
if(isset($_POST['mwb_wrffm_noti_save_return']))
{
	?>
	<div class="notice notice-success is-dismissible">
		<p><strong><?php _e('Settings saved.','woocommerce-rma-for-wcfm'); ?></strong></p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text"><?php _e('Dismiss this notices.','woocommerce-rma-for-wcfm'); ?></span>
		</button>
	</div><?php
	unset($_POST['mwb_wrffm_noti_save_return']);
	$post = $_POST;
	foreach($post as $k=>$val)
	{
		update_option($k, $val);
	}
	if(!isset($post['mwb_wrffm_notification_return_cancel_template']))
	{
		update_option( 'mwb_wrffm_notification_return_cancel_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_return_approve_wallet_template']))
	{
		update_option( 'mwb_wrffm_notification_return_approve_wallet_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_return_approve_template']))
	{
		update_option( 'mwb_wrffm_notification_return_approve_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_return_template']))
	{
		update_option( 'mwb_wrffm_notification_return_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_auto_accept_return_template']))
	{
		update_option( 'mwb_wrffm_notification_auto_accept_return_template' , 'no' );
	}
}
if(isset($_POST['mwb_wrffm_noti_save_exchange']))
{
	?>
	<div class="notice notice-success is-dismissible">
		<p><strong><?php _e('Settings saved.','woocommerce-rma-for-wcfm'); ?></strong></p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text"><?php _e('Dismiss this notices.','woocommerce-rma-for-wcfm'); ?></span>
		</button>
	</div><?php
	unset($_POST['mwb_wrffm_noti_save_exchange']);
	$post = $_POST;
	foreach($post as $k=>$val)
	{
		update_option($k, $val);
	}
	if(!isset($post['mwb_notification_exchange_template']))
	{
		update_option( 'mwb_notification_exchange_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_exchange_approve_template']))
	{
		update_option( 'mwb_wrffm_notification_exchange_approve_template' , 'no' );
	}
	if(!isset($post['mwb_wrffm_notification_exchange_cancel_template']))
	{
		update_option( 'mwb_wrffm_notification_exchange_cancel_template' , 'no' );
	}
}
if(isset($_POST['mwb_wrffm_noti_save_return_slip']))
{
	?>
	<div class="notice notice-success is-dismissible">
		<p><strong><?php _e('Settings saved.','woocommerce-rma-for-wcfm'); ?></strong></p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text"><?php _e('Dismiss this notices.','woocommerce-rma-for-wcfm'); ?></span>
		</button>
	</div><?php
	unset($_POST['mwb_wrffm_noti_save_return_slip']);
	$post = $_POST;
	foreach($post as $k=>$val)
	{
		update_option($k, $val);
	}
	if(!isset($post['mwb_wrffm_enable_return_ship_label']))
	{
		update_option( 'mwb_wrffm_enable_return_ship_label' , 'no' );
	}
}

$mwb_wrffm_license_status = get_option('mwb_wrffm_license_status');
$today_date = current_time('timestamp');
$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
$mwb_wrffm_days = $today_date - $mwb_wrffm_activation_date;
$mwb_wrffm_day_diff = floor($mwb_wrffm_days/(60*60*24));
if( $mwb_wrffm_license_status || $mwb_wrffm_day_diff <= 30 )
{ ?>
	<div class="wrap mwb_wrffm_notification">
		<h2><?php _e('Notification Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
		
		<nav class="nav-tab-wrapper woo-nav-tab-wrapper">
			<a class="nav-tab <?php echo $basic_active;?>" href="<?php echo admin_url()?>admin.php?page=mwb-wrffm-notification&amp;tab=basic"><?php _e('Basic', 'woocommerce-rma-for-wcfm' ); ?></a>
			<a class="nav-tab <?php echo $refund_active;?>" href="<?php echo admin_url()?>admin.php?page=mwb-wrffm-notification&amp;tab=refund"><?php _e('Refund', 'woocommerce-rma-for-wcfm' ); ?></a>
			<a class="nav-tab <?php echo $exchange_active;?>" href="<?php echo admin_url()?>admin.php?page=mwb-wrffm-notification&amp;tab=exchange"><?php _e('Exchange', 'woocommerce-rma-for-wcfm' ); ?></a>
			<a class="nav-tab <?php echo $return_ship_label_setting_active;?>" href="<?php echo admin_url()?>admin.php?page=mwb-wrffm-notification&amp;tab=return_ship_label_setting"><?php _e('Return Slip Label', 'woocommerce-rma-for-wcfm' ); ?></a>
			<?php
			if(mwb_wrffm_wcfm_activated())
			{
			 	 do_action('mwb_wrffm_notification_setting_tab_before_addon_section');
			}
			?>
			<?php do_action('mwb_wrffm_notification_setting_tab'); ?>
			<?php if(! $mwb_wrffm_license_status)
			{ ?>
				<a class="nav-tab <?php echo $mwb_wrffm_license_section;?>" href="<?php echo admin_url()?>admin.php?page=mwb-wrffm-notification&amp;tab=mwb_wrffm_license_section"><?php _e('License Verification', 'woocommerce-rma-for-wcfm' ); ?></a>
			<?php } ?>
		</nav>
		<a href="<?php echo home_url('/wp-admin/admin.php?page=wc-settings&tab=mwb_wrffm_setting')?>"><input type="button" value="<?php _e('GO TO SETTING', 'woocommerce-rma-for-wcfm');?>" class="mwb-wrffm-save-button button button-primary" style="float:right;"></a></div>
		<div class="">
		<?php 
		do_action('mwb_wrffm_custom_setting_section_for_wcfm');
	//Basic Tab of Notification setting

		if($tab == "basic")
		{
			$predefined_return_reason = get_option('mwb_wrffm_return_predefined_reason', false);
			$predefined_exchange_reason = get_option('mwb_wrffm_exchange_predefined_reason', false);
			
			?>
			<div class="mwb_wrffm_sidebar_hide ">
				<form enctype="multipart/form-data" action="" id="mainform" method="post">
					<h2 id="wrffm_mail_setting" class="mwb_wrffm_basic_setting mwb_wrffm_slide_active"><?php _e('Mail Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
					<div id="wrffm_mail_setting_wrapper">
						<table class="form-table mwb_wrffm_notification_section">
							<tbody>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('From Name', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td class="forminp forminp-text">
										<?php 

										$admin_name = get_option('blogname');
										$fname = get_option('mwb_wrffm_notification_from_name', false);
										if(empty($fname))
										{
											$fname = $admin_name;
										}
										?>
										<input type="text" placeholder="" class="input-text" value="<?php echo $fname;?>" style="" id="mwb_wrffm_notification_from_name" name="mwb_wrffm_notification_from_name">
									</td>
								</tr>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_mail"><?php _e('From Email', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td class="forminp forminp-email">
										<?php 
										$admin_email = get_option('admin_email');
										$email = get_option('mwb_wrffm_notification_from_mail', false);
										if(empty($email))
										{
											$email = $admin_email;
										}
										?>
										<input type="email" placeholder="" class="input-text" value="<?php echo $email;?>" id="mwb_wrffm_notification_from_mail" name="mwb_wrffm_notification_from_mail">
									</td>
								</tr>

								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Mail Header', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td class="forminp forminp-textarea">
										<?php 
										$content = stripslashes(get_option('mwb_wrffm_notification_mail_header', false));
										$editor_id = 'mwb_wrffm_notification_mail_header';
										$settings = array(
											'media_buttons'    => true,
											'drag_drop_upload' => true,
											'dfw'              => true,
											'teeny'            => true,
											'editor_height'    => 200,
											'editor_class'	   => '',
											'textarea_name'    => "mwb_wrffm_notification_mail_header"
											);
										wp_editor( $content, $editor_id, $settings );
										?>
									</td>
								</tr>

								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Mail Footer', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td class="forminp forminp-textarea">
										<?php 
										$content = stripslashes(get_option('mwb_wrffm_notification_mail_footer', false));
										$editor_id = 'mwb_wrffm_notification_mail_footer';
										$settings = array(
											'media_buttons'    => true,
											'drag_drop_upload' => true,
											'dfw'              => true,
											'teeny'            => true,
											'editor_height'    => 200,
											'editor_class'	   => '',
											'textarea_name'    => "mwb_wrffm_notification_mail_footer"
											);
										wp_editor( $content, $editor_id, $settings );
										?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>	
					<h2 id="wrffm_return_reason" class="mwb_wrffm_basic_setting"><?php _e('Predefined Refund Reason', 'woocommerce-rma-for-wcfm' ); ?></h2>
					<div id="wrffm_return_reason_wrapper" class="mwb_wrffm_basic_wrapper">
						<table class="form-table mwb_wrffm_notification_section">
							<tbody>
								<tr valign="top">
									<td class="titledesc" scope="row" colspan="2">
										<div id="mwb_wrffm_return_predefined_reason_wrapper">
											<?php 
											$count1 = 0;
											if(isset($predefined_return_reason) && !empty($predefined_return_reason))
											{
												foreach($predefined_return_reason as $reason_key => $predefine_reason)
												{
													if(!empty($predefine_reason))
													{	
														?>
														<div class="mwb_wrffm_return_reason_section">
															<input type="text" class="input-text" value="<?php echo $predefine_reason;?>" class="mwb_wrffm_return_predefined_reason" name="mwb_wrffm_return_predefined_reason[]">
															<?php if($reason_key != 0){?>
																	<a class="mwb_wrffm_remove_return" href="#" ><?php _e('Remove' , 'woocommerce-rma-for-wcfm')?></a>
															<?php }?>
														</div>
														<?php 
													} else {
														if( $count1 == 0 ) {
															?>
															<div class="mwb_wrffm_return_reason_section">
																<input type="text" class="input-text" class="mwb_wrffm_return_predefined_reason" name="mwb_wrffm_return_predefined_reason[]">
															</div>
														<?php
														}
														$count1++;
													}
												}	
											}
											else
											{		
												?>
												<div class="mwb_wrffm_return_reason_section">
													<input type="text" class="input-text" class="mwb_wrffm_return_predefined_reason" name="mwb_wrffm_return_predefined_reason[]">
												</div>
												<?php 
											}
											?>
										</div>
										<input type="button" value="<?php _e('ADD MORE', 'woocommerce-rma-for-wcfm' ); ?>" class="button" id="mwb_wrffm_return_predefined_reason_add">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<h2 id="wrffm_exchange_reason" class="mwb_wrffm_basic_setting"><?php _e('Predefined Exchange Reason', 'woocommerce-rma-for-wcfm' ); ?></h2>
					<div id="wrffm_exchange_reason_wrapper" class="mwb_wrffm_basic_wrapper">
						<table class="form-table mwb_wrffm_notification_section">
							<tbody>
								<tr valign="top">
									<td class="titledesc" scope="row" colspan="2">
										<div id="mwb_wrffm_exchange_predefined_reason_wrapper">
											<?php 
											$count = 0;
											if(isset($predefined_exchange_reason) && !empty($predefined_exchange_reason))
											{
												foreach($predefined_exchange_reason as $ex_key => $predefine_reason)
												{
													if(!empty($predefine_reason))
													{	
														?>
														<div id="mwb_wrffm_exchange_reason_section">
															<input type="text" class="input-text" value="<?php echo $predefine_reason;?>" class="mwb_wrffm_exchange_predefined_reason" name="mwb_wrffm_exchange_predefined_reason[]">
															<?php if($ex_key != 0){?>
																	<a href="#" class="mwb_wrffm_remove_return"><?php _e('Remove' , 'woocommerce-rma-for-wcfm')?></a>
															<?php }?>
														</div>
														<?php 
													} else {
														if( $count == 0 ) {
															?>
														<div id="mwb_wrffm_rnx_exchange_reason_section">
															<input type="text" class="input-text" value="<?php echo $predefine_reason;?>" class="mwb_wrffm_exchange_predefined_reason" name="mwb_wrffm_exchange_predefined_reason[]">
														</div>
														<?php
														}
														$count++;
													}
												}	
											}
											else
											{		
												?>
												<div id="mwb_wrffm_exchange_reason_section">
													<input type="text" class="input-text" class="mwb_wrffm_exchange_predefined_reason" name="mwb_wrffm_exchange_predefined_reason[]">
												</div>
												<?php 
											}
											?>
										</div>
										<input type="button" value="<?php _e('ADD MORE', 'woocommerce-rma-for-wcfm' ); ?>" class="button" id="mwb_wrffm_exchange_predefined_reason_add">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<h2 id="wrffm_refund_rules" class="mwb_wrffm_basic_setting"><?php _e('Refund Policy', 'woocommerce-rma-for-wcfm' ); ?></h2>
					<div id="wrffm_refund_rules_wrapper" class="mwb_wrffm_basic_wrapper">
						<?php do_action( 'mwb_wrffm_add_return_policy_settings_before' ); ?>
						<h2><?php /*_e( 'Price Based Policy', 'woocommerce-rma-for-wcfm' );*/ ?></h2>
						<?php $mwb_wrffm_enable_price_policy = get_option( 'mwb_wrffm_enable_price_policy', 'no' );
						$mwb_wrffm_show_refund_policy_on_product_page = get_option( 'mwb_wrffm_show_refund_policy_on_product_page', 'no' );
						?>
						<table class="form-table mwb_wrffm_notification_section">
							<tbody>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('Enable Price Based Policy', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td>
										<span class="mwb_wrffm_checkbox_wrrapper">
											<input type="checkbox" id="mwb_wrffm_enable_price_policy" name="mwb_wrffm_enable_price_policy" <?php if ($mwb_wrffm_enable_price_policy == 'on') {
												?>checked="checked"<?php
											} ?>></input><label for="mwb_wrffm_enable_price_policy"></label>
											<label for="mwb_wrffm_enable_price_policy"></label>
										</span>
										<span class="description"><?php _e( 'Enable to add price based Refund policy rules', 'woocommerce-rma-for-wcfm' ); ?></span>
									</td>
								</tr>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_show_refund_policy_on_product_page"><?php _e('Show Refund Policy On Product Page', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td>
										<span class="mwb_wrffm_checkbox_wrrapper">
											<input type="checkbox" id="mwb_wrffm_show_refund_policy_on_product_page" name="mwb_wrffm_show_refund_policy_on_product_page" <?php if ($mwb_wrffm_show_refund_policy_on_product_page == 'on') {
												?>checked="checked"<?php
											} ?>></input>
											<label for="mwb_wrffm_show_refund_policy_on_product_page"></label>
										</span>
										<span class="description"><?php _e( 'Enable to add Refund policy tab in product page. Also you can use <strong>[mwb_wrffm_refund_policy]</strong> shortcode for show Refund Policy Table.', 'woocommerce-rma-for-wcfm' ); ?></span>
									</td>
								</tr>
							</tbody>
						</table>
						<table class="form-table mwb_wrffm_notification_section" id="mwb_wrffm_price_based_policy">
							<tbody>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('Number of days to Refund', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e( 'Percentage Price Reduce', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e( 'Action', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
								</tr>
								<?php 
								$wrffm_number_of_days = get_option( 'mwb_wrffm_number_of_days' , array() );
								$wrffm_price_acc_to_days = get_option( 'mwb_wrffm_price_redumwb' , array() );
								if ( is_array($wrffm_number_of_days) && !empty( $wrffm_number_of_days ) ) {
									foreach ($wrffm_number_of_days as $key => $value) {
										?>
										<tr valign="top" class="price_row">
											<th class="titledesc" scope="row">
												<input type="number" name="mwb_wrffm_number_of_days[]" class="mwb_wrffm_number_of_days" value="<?php echo $value; ?>" min="0"></input>
											</th>
											<th class="titledesc" scope="row">
												<input type="text" name="mwb_wrffm_price_redumwb[]" class="mwb_wrffm_price_redumwb" value="<?php echo $wrffm_price_acc_to_days[$key] ; ?>" placeholder="<?php _e('Enter % Price to be reduced' , 'woocommerce-rma-for-wcfm'); ?>"></input>
											</th>
											<th class="titledesc" scope="row">
												<input type="button" class="mwb_wrffm_add_price_row button" value="<?php _e( 'Add','woocommerce-rma-for-wcfm' ) ?>"></input>
												<?php if( $key > 0 ){
													?>
													<input type="button" class="mwb_wrffm_remove_price_row button" value="<?php _e( 'Remove','woocommerce-rma-for-wcfm' ) ?>"></input>
													<?php
												} ?>
											</th>
										</tr>
										<?php
									} 
								}else{
									?>
									<tr valign="top" class="price_row">
										<th class="titledesc" scope="row">
											<input type="number" name="mwb_wrffm_number_of_days[]" class="mwb_wrffm_number_of_days" min="0"></input>
										</th>
										<th class="titledesc" scope="row">
											<input type="text" name="mwb_wrffm_price_redumwb[]" class="mwb_wrffm_price_redumwb" placeholder="<?php _e('Enter % Price to be reduced' , 'woocommerce-rma-for-wcfm'); ?>"></input>
										</th>
										<th class="titledesc" scope="row">
											<input type="button" class="mwb_wrffm_add_price_row button" value="<?php _e( 'Add','woocommerce-rma-for-wcfm' ) ?>"></input>
										</th>
									</tr>
									<?php
								}?>
								<?php $text = get_option( 'mwb_wrffm_price_deduct_message', '' ); ?>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('Price Deduction Message', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<th class="titledesc" scope="row">
										<input type="text" name="mwb_wrffm_price_deduct_message" id="mwb_wrffm_price_deduct_message" value="<?php echo $text; ?>" placeholder="Enter message to be shown"></input>
									</th>
								</tr>
							</tbody>
						</table>
						<?php $mwb_wrffm_enable_time_policy = get_option( 'mwb_wrffm_enable_time_policy', 'no' ); ?>
						<table class="form-table mwb_wrffm_notification_section">
							<tbody>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('Enable Time Based Policy', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td>
										<span class="mwb_wrffm_checkbox_wrrapper">
											<input type="checkbox" id="mwb_wrffm_enable_time_policy" name="mwb_wrffm_enable_time_policy" <?php if ($mwb_wrffm_enable_time_policy == 'on') {
												?>checked="checked"<?php
											} ?>></input>
											<label for="mwb_wrffm_enable_time_policy"></label>
										</span>
										<span class="description"><?php _e( 'Enable to add time based Refund policy rules', 'woocommerce-rma-for-wcfm' ); ?></span>
									</td>
								</tr>
							</tbody>
						</table>
						<?php $mwb_wrffm_from_time = get_option( 'mwb_wrffm_return_from_time', '' ); ?>
						<?php $mwb_wrffm_to_time = get_option( 'mwb_wrffm_return_to_time', '' ); ?>
						<table class="form-table mwb_wrffm_notification_section" id="mwb_wrffm_time_based_policy">
							<tbody>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_notification_from_name"><?php _e('Allow Refund Request Between', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<th class="titledesc" scope="row">
										<input type="text" value="<?php echo $mwb_wrffm_from_time ; ?>" class="mwb_wrffm_date_time_picker" id="mwb_wrffm_return_from_time" placeholder="hh:mm AM" name="mwb_wrffm_return_from_time"></input>
									</th>
									<th class="titledesc" scope="row">
										<input type="text" value="<?php echo $mwb_wrffm_to_time ; ?>" class="mwb_wrffm_date_time_picker" id="mwb_wrffm_return_to_time" placeholder="hh:mm PM" name="mwb_wrffm_return_to_time"></input>
									</th>
								</tr>
							</tbody>
						</table>
						<?php do_action( 'mwb_wrffm_add_return_policy_settings_after' ); ?>
					</div>	
					<p class="submit">
						<input type="submit" value="<?php _e('Save changes', 'woocommerce-rma-for-wcfm' ); ?>" class="button-primary woocommerce-save-button mwb-wrffm-save-button" name="mwb_wrffm_noti_save_basic"> 
					</p>
				</form>
			</div>
			<?php 
		}

	//Refund Tab of Notification setting

		if($tab == "refund")
		{
			?>
			<div class="mwb_wrffm_table">
				<form enctype="multipart/form-data" action="" id="mainform" method="post">

					<div id="mwb_wrffm_accordion">
						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Short-Codes ', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec mwb_wrffm_notification_section">
								<p><h3><?php _e("These are order shortcode that you can use in EMAIL MESSESGES. It will be changed with order's dynamic values.", 'woocommerce-rma-for-wcfm');?></h3></p>
								<p><?php echo sprintf(__('%s Note :%s Use %s [order] %s for Order Number, %s [siteurl] %s for home page url and %s [username] %s for user name.','woocommerce-rma-for-wcfm'),'<b>','</b>','<b>','</b>','<b>','</b>','<b>','</b>');?></p>
								<p><?php echo '<strong> [_order_total] , [formatted_shipping_address] , [formatted_billing_address] , [_billing_company] , [_billing_email] , [_billing_phone] , [_billing_country] , [_billing_address_1] , [_billing_address_2] , [_billing_state] , [_billing_postcode] , [_shipping_first_name] , [_shipping_last_name] , [_shipping_company] , [_shipping_country] , [_shipping_address_1] , [_shipping_address_2] , [_shipping_city] , [_shipping_state] , [_shipping_postcode] , [_payment_method_tittle] , [_order_shipping] , [_refundable_amount]</strong>'?></p>

							</div>
						</div>
					</div>
					<div id="mwb_wrffm_accordion">
						<div class="mwb_wrffm_accord_sec_wrap">
							<h2 class="mwb_wrffm_slide_active"><?php _e('Merchant Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec mwb_wrffm_notification_sec_active">
								<table class="form-table mwb_wrffm_notification_section">
									<tbody>

										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_merchant_return_subject"><?php _e('Merchant Refund Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-text">
												<?php 
												$merchant_subject = get_option('mwb_wrffm_notification_merchant_return_subject', false);
												?>
												<input type="text" placeholder="" class="input-text" value="<?php echo $merchant_subject;?>" style="" id="mwb_wrffm_notification_merchant_return_subject" name="mwb_wrffm_notification_merchant_return_subject">
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>

						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Auto Accept Refund Request', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec">
								<table class="form-table mwb_wrffm_notification_section">
									<tbody>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_subject"><?php _e('Auto Accept Refund Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-text">
												<?php 
												$return_accept_subject = get_option('mwb_wrffm_notification_auto_accept_return_subject', false);
												?>
												<input type="text" placeholder="" class="input-text" value="<?php echo $return_accept_subject;?>" style="" id="mwb_wrffm_notification_auto_accept_return_subject" name="mwb_wrffm_notification_auto_accept_return_subject">
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php $mwb_wrffm_notification_auto_accept_return_template =get_option('mwb_wrffm_notification_auto_accept_return_template', 'no'); ?>
												<span class="mwb_wrffm_checkbox_wrrapper">
													<input type="checkbox" id="mwb_wrffm_notification_auto_accept_return_template" name="mwb_wrffm_notification_auto_accept_return_template" <?php if ($mwb_wrffm_notification_auto_accept_return_template == 'on') {
														?>checked="checked"<?php
													}  ?>><label for="mwb_wrffm_notification_auto_accept_return_template"></label>
												</span>
												<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Auto Accept Refund Request Message', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php 
												$content = stripslashes(get_option('mwb_wrffm_notification_auto_accept_return_rcv', false));
												$editor_id = 'mwb_wrffm_notification_auto_accept_return_rcv';
												$settings = array(
													'media_buttons'    => false,
													'drag_drop_upload' => true,
													'dfw'              => true,
													'teeny'            => true,
													'editor_height'    => 200,
													'editor_class'	   => '',
													'textarea_name'    => "mwb_wrffm_notification_auto_accept_return_rcv"
													);
												wp_editor( $content, $editor_id, $settings );
												?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Refund Request', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec">
								<table class="form-table mwb_wrffm_notification_section">
									<tbody>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_subject"><?php _e('Refund Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-text">
												<?php 
												$return_cancel_subject = get_option('mwb_wrffm_notification_return_subject', false);
												?>
												<input type="text" placeholder="" class="input-text" value="<?php echo $return_cancel_subject;?>" style="" id="mwb_wrffm_notification_return_subject" name="mwb_wrffm_notification_return_subject">
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php $mwb_wrffm_notification_return_template =get_option('mwb_wrffm_notification_return_template', 'no'); ?>
												<span class="mwb_wrffm_checkbox_wrrapper">
													<input type="checkbox" id="mwb_wrffm_notification_return_template" name="mwb_wrffm_notification_return_template" <?php if ($mwb_wrffm_notification_return_template == 'on') {
														?>checked="checked"<?php
													}  ?>><label for="mwb_wrffm_notification_return_template"></label>
												</span>
												<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_rcv"><?php _e('Recieved Refund Request Message', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php 
												$content = stripslashes(get_option('mwb_wrffm_notification_return_rcv', false));
												$editor_id = 'mwb_wrffm_notification_return_rcv';
												$settings = array(
													'media_buttons'    => false,
													'drag_drop_upload' => true,
													'dfw'              => true,
													'teeny'            => true,
													'editor_height'    => 200,
													'editor_class'	   => '',
													'textarea_name'    => "mwb_wrffm_notification_return_rcv"
													);
												wp_editor( $content, $editor_id, $settings );
												?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Refund Approved', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec">
								<table class="form-table mwb_wrffm_notification_section">
									<tbody>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_approve_subject"><?php _e('Approved Refund Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-text">
												<?php 
												$return_subject = get_option('mwb_wrffm_notification_return_approve_subject', false);
												?>
												<input type="text" placeholder="" class="input-text" value="<?php echo $return_subject;?>" style="" id="mwb_wrffm_notification_return_approve_subject" name="mwb_wrffm_notification_return_approve_subject">
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php $mwb_wrffm_notification_return_approve_template =get_option('mwb_wrffm_notification_return_approve_template', 'no'); ?>
												<span class="mwb_wrffm_checkbox_wrrapper">
													<input type="checkbox" id="mwb_wrffm_notification_return_approve_template" name="mwb_wrffm_notification_return_approve_template" <?php if ($mwb_wrffm_notification_return_approve_template == 'on') {
														?>checked="checked"<?php
													}  ?>><label for="mwb_wrffm_notification_return_approve_template"></label>
												</span>
												<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_approve"><?php _e('Approved Refund Request Message', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php 
												$content = stripslashes(get_option('mwb_wrffm_notification_return_approve', false));
												$editor_id = 'mwb_wrffm_notification_return_approve';
												$settings = array(
													'media_buttons'    => false,
													'drag_drop_upload' => true,
													'dfw'              => true,
													'teeny'            => true,
													'editor_height'    => 200,
													'editor_class'	   => '',
													'textarea_name'    => "mwb_wrffm_notification_return_approve"
													);
												wp_editor( $content, $editor_id, $settings );
												?>
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php $mwb_wrffm_notification_return_approve_wallet_template =get_option('mwb_wrffm_notification_return_approve_wallet_template', 'no'); ?>
												<span class="mwb_wrffm_checkbox_wrrapper">
													<input type="checkbox" id="mwb_wrffm_notification_return_approve_wallet_template" name="mwb_wrffm_notification_return_approve_wallet_template" <?php if ($mwb_wrffm_notification_return_approve_wallet_template == 'on') {
														?>checked="checked"<?php
													}  ?>><label for="mwb_wrffm_notification_return_approve_wallet_template"></label>
												</span>
												<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
											</td>
										</tr>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_approve_wallet"><?php _e('Approved Refund Request Message (Wallet Feature enabled)', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-textarea">
												<?php 
												$content = stripslashes(get_option('mwb_wrffm_notification_return_approve_wallet', false));
												$editor_id = 'mwb_wrffm_notification_return_approve_wallet';
												$settings = array(
													'media_buttons'    => false,
													'drag_drop_upload' => true,
													'dfw'              => true,
													'teeny'            => true,
													'editor_height'    => 200,
													'editor_class'	   => '',
													'textarea_name'    => "mwb_wrffm_notification_return_approve_wallet"
													);
												wp_editor( $content, $editor_id, $settings );
												?>
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Refund Cancel', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec ">
								<table class="form-table mwb_wrffm_notification_section ">
									<tbody>
										<tr valign="top">
											<th class="titledesc" scope="row">
												<label for="mwb_wrffm_notification_return_cancel_subject"><?php _e('Cancelled Refund Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
											</th>
											<td class="forminp forminp-text">
												<?php 
												$return_subject = get_option('mwb_wrffm_notification_return_cancel_subject', false);
												?>
												<input type="text" placeholder="" class="input-text" value="<?php echo $return_subject?>" style="" id="mwb_wrffm_notification_return_cancel_subject" name="mwb_wrffm_notification_return_cancel_subject">
											</td>
										</tr>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-textarea">
											<?php $mwb_wrffm_notification_return_cancel_template =get_option('mwb_wrffm_notification_return_cancel_template', 'no'); ?>
											<span class="mwb_wrffm_checkbox_wrrapper">
												<input type="checkbox" id="mwb_wrffm_notification_return_approve_wallet_template" name="mwb_wrffm_notification_return_cancel_template" <?php if ($mwb_wrffm_notification_return_cancel_template == 'on') {
													?>checked="checked"<?php
												}  ?>><label for="mwb_wrffm_notification_return_approve_wallet_template"></label>
											</span>
											<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_return_cancel"><?php _e('Cancelled Refund Request Message', 'woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-textarea">
											<?php 
											$content = stripslashes(get_option('mwb_wrffm_notification_return_cancel', false));
											$editor_id = 'mwb_wrffm_notification_return_cancel';
											$settings = array(
												'media_buttons'    => false,
												'drag_drop_upload' => true,
												'dfw'              => true,
												'teeny'            => true,
												'editor_height'    => 200,
												'editor_class'	   => '',
												'textarea_name'    => "mwb_wrffm_notification_return_cancel"
												);
											wp_editor( $content, $editor_id, $settings );
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>		
				<p class="submit">
					<input type="submit" value="<?php _e('Save
					Settings', 'woocommerce-rma-for-wcfm' ); ?>" class="mwb-wrffm-save-button button-primary woocommerce-save-button" name="mwb_wrffm_noti_save_return"> 
				</p>
			</form>
		</div>
		<?php 
	}

	//Exchange Tab of Notification setting

	if($tab == "exchange")
	{
		?>
		<div class="mwb_wrffm_table">
			<form enctype="multipart/form-data" action="" id="mainform" method="post">
				<div id="mwb_wrffm_accordion">
					<div class="mwb_wrffm_accord_sec_wrap">
						<h2><?php _e('Short-Codes ', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div class="mwb_wrffm_content_sec mwb_wrffm_notification_section">
							<p><?php _e('These are order shortcode that you can use in EMAIL MESSESGE', 'woocommerce-rma-for-wcfm');?></p>
							<p><?php echo sprintf(__('%s Note :%s Use %s [order] %s for Order Number, %s [siteurl] %s for home page url and %s [username] %s for user name.','woocommerce-rma-for-wcfm'),'<b>','</b>','<b>','</b>','<b>','</b>','<b>','</b>');?></p>
							<p><?php echo '<strong> [_order_total] , [formatted_shipping_address] , [formatted_billing_address] , [_billing_company] , [_billing_email] , [_billing_phone] , [_billing_country] , [_billing_address_1] , [_billing_address_2] , [_billing_state] , [_billing_postcode] , [_shipping_first_name] , [_shipping_last_name] , [_shipping_company] , [_shipping_country] , [_shipping_address_1] , [_shipping_address_2] , [_shipping_city] , [_shipping_state] , [_shipping_postcode] , [_payment_method_tittle] , [_order_shipping] , [_refundable_amount]</strong>'?></p>

						</div>
					</div>
				</div>
				<div id="mwb_wrffm_accordion">
					<div class="mwb_wrffm_accord_sec_wrap">
						<h2 class="mwb_wrffm_slide_active"><?php _e('Merchant Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div class="mwb_wrffm_content_sec mwb_wrffm_notification_sec_active">
							<table class="form-table mwb_wrffm_notification_section">
								<tbody>

									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_merchant_exchange_subject"><?php _e('Merchant Exchange Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-text">
											<?php 
											$merchant_subject = get_option('mwb_wrffm_notification_merchant_exchange_subject', false);
											?>
											<input type="text" placeholder="" class="input-text" value="<?php echo $merchant_subject;?>" style="" id="mwb_wrffm_notification_merchant_exchange_subject" name="mwb_wrffm_notification_merchant_exchange_subject">
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>	
					<!-- Exchange request received -->

					<div class="mwb_wrffm_accord_sec_wrap">
						<h2><?php _e('Exchange Request', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div class="mwb_wrffm_content_sec ">
							<table class="form-table mwb_wrffm_notification_section">
								<tbody>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_subject"><?php _e('Exchange Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-text">
											<?php 
											$exchange_subject = get_option('mwb_notification_exchange_subject', false);
											?>
											<input type="text" placeholder=""class="input-text" value="<?php echo $exchange_subject;?>" style="" id="mwb_notification_exchange_subject" name="mwb_notification_exchange_subject">
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-textarea">
											<?php $mwb_notification_exchange_template =get_option('mwb_notification_exchange_template', 'no'); ?>
											<span class="mwb_wrffm_checkbox_wrrapper">
												<input type="checkbox" id="mwb_notification_exchange_template" name="mwb_notification_exchange_template" <?php if ($mwb_notification_exchange_template == 'on') {
													?>checked="checked"<?php
												}  ?>><label for="mwb_notification_exchange_template"></label>
											</span>
											<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_rcv"><?php _e('Recieved Exchange Request Message', 'woocommerce-rma-for-wcfm' ); ?></label> 
										</th>
										<td class="forminp forminp-textarea">
											<?php 
											$content = stripslashes(get_option('mwb_notification_exchange_rcv', false));
											$editor_id = 'mwb_notification_exchange_rcv';
											$settings = array(
												'media_buttons'    => false,
												'drag_drop_upload' => true,
												'dfw'              => true,
												'teeny'            => true,
												'editor_height'    => 200,
												'editor_class'	   => '',
												'textarea_name'    => "mwb_notification_exchange_rcv"
												);
											wp_editor( $content, $editor_id, $settings );
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>	
					<!-- Exchange request accepted -->

					<div class="mwb_wrffm_accord_sec_wrap">
						<h2><?php _e('Exchange Approved', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div class="mwb_wrffm_content_sec ">
							<table class="form-table mwb_wrffm_notification_section">
								<tbody>	
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_subject"><?php _e('Approve Exchange Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-text">
											<?php 
											$exchange_subject = get_option('mwb_wrffm_notification_exchange_approve_subject', false);
											?>
											<input type="text" placeholder=""class="input-text" value="<?php echo $exchange_subject;?>" style="" id="mwb_wrffm_notification_exchange_approve_subject" name="mwb_wrffm_notification_exchange_approve_subject">
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-textarea">
											<?php $mwb_wrffm_notification_exchange_approve_template =get_option('mwb_wrffm_notification_exchange_approve_template', 'no'); ?>
											<span class="mwb_wrffm_checkbox_wrrapper">
												<input type="checkbox" id="mwb_wrffm_notification_exchange_approve_template" name="mwb_wrffm_notification_exchange_approve_template" <?php if ($mwb_wrffm_notification_exchange_approve_template == 'on') {
													?>checked="checked"<?php
												}  ?>><label for="mwb_wrffm_notification_exchange_approve_template"></label>
											</span>
											<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_rcv"><?php _e('Recieved Exchange Request Message', 'woocommerce-rma-for-wcfm' ); ?></label> 
										</th>
										<td class="forminp forminp-textarea">
											<?php 
											$content = stripslashes(get_option('mwb_wrffm_notification_exchange_approve', false));
											$editor_id = 'mwb_wrffm_notification_exchange_approve';
											$settings = array(
												'media_buttons'    => false,
												'drag_drop_upload' => true,
												'dfw'              => true,
												'teeny'            => true,
												'editor_height'    => 200,
												'editor_class'	   => '',
												'textarea_name'    => "mwb_wrffm_notification_exchange_approve"
												);
											wp_editor( $content, $editor_id, $settings );
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Exchange request cancel -->

					<div class="mwb_wrffm_accord_sec_wrap">
						<h2><?php _e('Exchange Cancel', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div class="mwb_wrffm_content_sec ">
							<table class="form-table mwb_wrffm_notification_section">
								<tbody>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_subject"><?php _e('Cancel Exchange Request Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-text">
											<?php 
											$exchange_subject = get_option('mwb_wrffm_notification_exchange_cancel_subject', false);
											?>
											<input type="text" placeholder=""class="input-text" value="<?php echo $exchange_subject;?>" style="" id="mwb_wrffm_notification_exchange_cancel_subject" name="mwb_wrffm_notification_exchange_cancel_subject">
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_wrffm_notification_auto_accept_return_rcv"><?php _e('Enable for custom email template','woocommerce-rma-for-wcfm' ); ?></label>
										</th>
										<td class="forminp forminp-textarea">
											<?php $mwb_wrffm_notification_exchange_cancel_template =get_option('mwb_wrffm_notification_exchange_cancel_template', 'no'); ?>
											<span class="mwb_wrffm_checkbox_wrrapper">
												<input type="checkbox" id="mwb_wrffm_notification_exchange_cancel_template" name="mwb_wrffm_notification_exchange_cancel_template" <?php if ($mwb_wrffm_notification_exchange_cancel_template == 'on') {
													?>checked="checked"<?php
												}  ?>><label for="mwb_wrffm_notification_exchange_cancel_template"></label>
											</span>
											<?php _e('Enable, if you want to put custom email template in editor & Put your email template under text tab of editor. ','woocommerce-rma-for-wcfm' ); ?>
										</td>
									</tr>
									<tr valign="top">
										<th class="titledesc" scope="row">
											<label for="mwb_notification_exchange_rcv"><?php _e('Recieved Exchange Request Message', 'woocommerce-rma-for-wcfm' ); ?></label> 
										</th>
										<td class="forminp forminp-textarea">

											<?php

											$content = stripslashes(get_option('mwb_wrffm_notification_exchange_cancel', false));
											$editor_id = 'mwb_wrffm_notification_exchange_cancel';
											$settings = array(
												'media_buttons'    => false,
												'drag_drop_upload' => true,
												'dfw'              => true,
												'teeny'            => true,
												'editor_height'    => 200,
												'editor_class'	   => '',
												'textarea_name'    => "mwb_wrffm_notification_exchange_cancel"
												);
											wp_editor( $content, $editor_id, $settings );
											?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<p class="submit">
					<input type="submit" value="<?php _e('Save changes', 'woocommerce-rma-for-wcfm' ); ?>" class="mwb-wrffm-save-button button-primary woocommerce-save-button" name="mwb_wrffm_noti_save_exchange"> 
				</p>
			</form>
		</div>
		<?php 
	}

	if($tab == 'return_ship_label_setting')
	{
		?>
		<div class="mwb_wrffm_table">
			<form enctype="multipart/form-data" action="" id="mainform" method="post">
				<div id="mwb_wrffm_accordion">
					<div class="mwb_wrffm_accord_sec_wrap">
						<h2 id="wrffm_mail_setting" class="mwb_wrffm_basic_setting mwb_wrffm_slide_active"><?php _e('Return Slip Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
						<div id="wrffm_mail_setting_wrapper">
							<table class="form-table mwb_wrffm_notification_section">
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_enable_return_ship_label"><?php _e('Enable Shiping Label', 'woocommerce-rma-for-wcfm' ); ?></label> 
									</th>
									<td class="forminp forminp-textarea">
										<?php $mwb_wrffm_enable_return_ship_label =get_option('mwb_wrffm_enable_return_ship_label', 'no'); 
										?>
										<span class="mwb_wrffm_checkbox_wrrapper">
											<input type="checkbox" id="mwb_wrffm_enable_return_ship_label" name="mwb_wrffm_enable_return_ship_label"<?php if ($mwb_wrffm_enable_return_ship_label == 'on') {
												?>checked="checked"<?php
											} ?>></input><label for="mwb_wrffm_enable_return_ship_label"></label>
										</span>
										<span class="description"><?php _e( 'Enable this to send a return Slip Label to customer for sending return Product Back.', 'woocommerce-rma-for-wcfm' ); ?></span>
									</td>
								</tr>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_return_slip_mail_subject"><?php _e('Return Slip Mail Subject', 'woocommerce-rma-for-wcfm' ); ?></label>
									</th>
									<td class="forminp forminp-text">
										<?php 
										$mwb_wrffm_return_slip_mail_subject = get_option('mwb_wrffm_return_slip_mail_subject', false);
										?>
										<input type="text" placeholder=""class="input-text" value="<?php echo $mwb_wrffm_return_slip_mail_subject;?>" style="" id="mwb_wrffm_return_slip_mail_subject" name="mwb_wrffm_return_slip_mail_subject">
									</td>
								</tr>
								<tr valign="top">
									<th class="titledesc" scope="row">
										<label for="mwb_wrffm_return_ship_template"><?php _e('Return Slip template', 'woocommerce-rma-for-wcfm' ); ?></label> 
									</th>
									<td class="forminp forminp-textarea">
										<p><?php _e(' Use ', 'woocommerce-rma-for-wcfm'); ?><b>[Tracking_Id]</b><?php _e(' Shortcode in Place of Tracking Id. Here tracking id represent order Id.', 'woocommerce-rma-for-wcfm') ?>
											<br><?php _e(' Use ', 'woocommerce-rma-for-wcfm'); ?><b>[Order_shipping_address]</b><?php _e(' Shortcode in place of return label "from" column.  ', 'woocommerce-rma-for-wcfm') ?><br><?php _e(' Use ', 'woocommerce-rma-for-wcfm'); ?><b>[siteurl]</b><?php _e(' Shortcode in place of your site url.', 'woocommerce-rma-for-wcfm') ?><br><?php _e(' Use ', 'woocommerce-rma-for-wcfm'); ?><b>[username]</b><?php _e(' Shortcode in place of Customer name.', 'woocommerce-rma-for-wcfm') ?></p>
											<?php 
											$content = stripslashes(get_option('mwb_wrffm_return_ship_template', false));
											$editor_id = 'mwb_wrffm_return_ship_template';
											$settings = array(
												'media_buttons'    => false,
												'drag_drop_upload' => true,
												'dfw'              => true,
												'teeny'            => true,
												'editor_height'    => 200,
												'editor_class'	   => '',
												'textarea_name'    => "mwb_wrffm_return_ship_template"
												);
											wp_editor( $content, $editor_id, $settings );
											?>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div id="mwb_wrffm_accordion">
						<div class="mwb_wrffm_accord_sec_wrap">
							<h2><?php _e('Terms & condition Setting', 'woocommerce-rma-for-wcfm' ); ?></h2>
							<div class="mwb_wrffm_content_sec ">
								<table class="form-table mwb_wrffm_notification_section">
									<tr>
										<td><br>
											<?php _e('<b>1-</b> You need to create a terms & Condition page and put your all terms and conditions as content of page. <a href="'. home_url().'/wp-admin/post-new.php?post_type=page">Click Here</a> to create terms & conditions Page.<br><br>
											<b>2-</b> After page setup you need to go with WooCommerce->Settings->checkout and select terms and conditions page under Terms and conditions setting. <a href="'. home_url().'/wp-admin/admin.php?page=wc-settings&tab=checkout">Click Here</a> to start terms & conditions Policy.','woocommerce-rma-for-wcfm'); ?>
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>
					<p class="submit">
						<input type="submit" value="<?php _e('Save changes', 'woocommerce-rma-for-wcfm' ); ?>" class="mwb-wrffm-save-button button-primary woocommerce-save-button" name="mwb_wrffm_noti_save_return_slip"> 
					</p>
				</form>
			</div>		
			<?php
		}

		do_action('mwb_wrffm_custom_setting_section');

		if($tab == 'mwb_wrffm_license_section')
		{
			?><div class="mwb_wrffm_table"><?php
			include_once MWB_WRFFM_DIRPATH.'template/mwb-wrffm-license-template.php';
			?></div><?php
		}
		?>
	</div>
	<?php
}else{
	include_once MWB_WRFFM_DIRPATH.'template/mwb-wrffm-license-template.php';
}