<?php
/**
 * Exit if accessed directly
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Show Return Product detail on Order Page on admin Side

if ( ! is_int( $thepostid ) ) {
	$thepostid = $post->ID;
}
if ( ! is_object( $theorder ) ) {
	$theorder = wc_get_order( $thepostid );
}

$order = $theorder;
if( WC()->version < "3.0.0" )
{
	$order_id=$order->id;
}
else
{
	$order_id=$order->get_id();
}
$return_datas = get_post_meta($order_id, 'mwb_wrffm_return_product', true);
$line_items  = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );

if(isset($return_datas) && !empty($return_datas))
{
	foreach($return_datas as $key=>$return_data)
	{
		$date=date_create($key);
		$date_format = get_option('date_format');
		$date=date_format($date,$date_format);
		?>
		<p><?php _e( 'Following product refund request made on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $date?>.</b></p>
		<div>
		<div id="mwb_wrffm_return_wrapper">
			<table>
				<thead>
					<tr>
						<th><?php _e( 'Item', 'woocommerce-rma-for-wcfm' ); ?></th>
						<th><?php _e( 'Name', 'woocommerce-rma-for-wcfm' ); ?></th>
						<th><?php _e( 'Cost', 'woocommerce-rma-for-wcfm' ); ?></th>
						<th><?php _e( 'Qty', 'woocommerce-rma-for-wcfm' ); ?></th>
						<th><?php _e( 'Total', 'woocommerce-rma-for-wcfm' ); ?></th>
						<?php if(mwb_wrffm_wcfm_activated()) : ?>
						<th><?php _e( 'Approved By Vendor', 'woocommerce-rma-for-wcfm' ); ?></th>
						<?php endif; ?>
					</tr>
				</thead>
				<tbody>
				<?php 
				$total = 0;
				$return_products = $return_data['products'];

				$mwb_vendor_id[]='';
				foreach ($return_products as $pr_key => $pr_value) {
					if(mwb_wrffm_wcfm_activated()){
						$author = get_post_field( 'post_author',$pr_value['product_id'] );
						if ( user_can( $author, 'wcfm_vendor' ) || user_can( $author, 'administrator' )   ) {
							$is_seller = true;
						}
						else{
							$is_seller = false;
						}
						if($is_seller) {
							$seller_data = get_userdata($author);	
								$mwb_vendor_id[$pr_value['item_id']] = $seller_data->ID;
						}

					}
				}
				$vendor_id_arr = [];
				foreach ( $line_items as $item_id => $item ) 
				{
					foreach($return_products as $return_product)
					{
						if($item_id == $return_product['item_id'])
						{
							$_product  = $order->get_product_from_item( $item );
							$product = get_post( $_product->get_id() );
							$vendor_id = $product->post_author;
							if( ! in_array( $vendor_id, $vendor_id_arr ) ) {
								array_push( $vendor_id_arr, $vendor_id );
							}
							$item_meta =wc_get_order_item_meta( $item_id,$key );
							$thumbnail     = $_product ? apply_filters( 'woocommerce_admin_order_item_thumbnail', $_product->get_image( 'thumbnail', array( 'title' => '' ), false ), $item_id, $item ) : '';
							?>
							<tr>
								<td class="thumb">
								<?php
									echo '<div class="wc-order-item-thumbnail">' . wp_kses_post( $thumbnail ) . '</div>';
								?>
								</td>
								<td class="name">
								<?php
									echo esc_html( $item['name'] );
									if ( $_product && $_product->get_sku() ) {
										echo '<div class="wc-order-item-sku"><strong>' . __( 'SKU:', 'woocommerce-rma-for-wcfm' ) . '</strong> ' . esc_html( $_product->get_sku() ) . '</div>';
									}
									if ( ! empty( $item['variation_id'] ) ) {
										echo '<div class="wc-order-item-variation"><strong>' . __( 'Variation ID:', 'woocommerce-rma-for-wcfm' ) . '</strong> ';
										if ( ! empty( $item['variation_id'] ) && 'product_variation' === get_post_type( $item['variation_id'] ) ) {
											echo esc_html( $item['variation_id'] );
										} elseif ( ! empty( $item['variation_id'] ) ) {
											echo esc_html( $item['variation_id'] ) . ' (' . __( 'No longer exists', 'woocommerce-rma-for-wcfm' ) . ')';
										}
										echo '</div>';
									}
									if( WC()->version < "3.1.0" )
									{
										$item_meta      = new WC_Order_Item_Meta( $item, $_product );
										$item_meta->display();
									}
									else
									{
										$item_meta      = new WC_Order_Item_Product( $item, $_product );
										wc_display_item_meta($item_meta);
									}
									?>
								</td>
								<td><?php echo mwb_wrffm_format_price($return_product['price']);?></td>
								<td><?php echo $return_product['qty'];?></td>
								<td><?php echo mwb_wrffm_format_price($return_product['price']*$return_product['qty']);?></td>
								<?php if(mwb_wrffm_wcfm_activated()) : ?>
								<td><?php
									$mwb_wrffm_wcv_approved = isset($return_product['approved'])? $return_product['approved'] : 0 ;
									if($mwb_wrffm_wcv_approved)
									{
										_e('Yes','woocommerce-rma-for-wcfm');
									}
									else
									{
										_e('No','woocommerce-rma-for-wcfm');
									}
								 ?></td>
								<?php endif; ?>
							</tr>
							<?php 
							$total += $return_product['price']*$return_product['qty'];
						}
					}		
				}
				?>
					<tr>
						<th colspan="4"><?php _e('Total Amount', 'woocommerce-rma-for-wcfm');?></th>
						<th><?php echo mwb_wrffm_format_price($total);?></th>
					</tr>
				</tbody>
			</table>	
		</div>
		<div class="mwb_wrffm_extra_reason mwb_wrffm_extra_reason_for_refund">
		<?php 
		
		$fee_enable = get_option('mwb_wrffm_return_shipcost_enable', false);
		
		if($fee_enable == 'yes')
		{
			?>
			<p><?php _e('Fees amount is deducted from Refund amount', 'woocommerce-rma-for-wcfm');?></p>
			<?php 
			$disable = "";
			$vendor_fee_enable = get_option('mwb_wrffm_enable_vendor_ship_fee', false);
			$vendor_enable = get_option('mwb_wrffm_enable_vendor_addon', false);
			$vendor_fee_flag = false;
			if( $vendor_fee_enable == 'on' && $vendor_enable == 'on' ) {
				$vendor_fee_flag = true;
			}
			if( $return_data['status'] != 'pending' || $vendor_fee_flag == true )
			{
				$disable = 'readonly';
			}
			else 
			{
			?>
			<div id="mwb_wrffm_add_fee">
			<?php 
			}
			$ship_fee_arr = [];
			$added_fees = [];
			if( $vendor_fee_flag == true ) {
				if( isset( $vendor_id_arr ) && is_array( $vendor_id_arr ) && ! empty( $vendor_id_arr ) ) {
					foreach ($vendor_id_arr as $vkey => $vval) {
						$temp_fee_arr = get_post_meta($order_id, 'mwb_wrffm_return_added_fee'.$vval, true);
						if ( ! empty( $temp_fee_arr ) && is_array( $temp_fee_arr ) ) {
							foreach ( $temp_fee_arr as $tkey => $tvalue ) {
								foreach ($tvalue as $fkey => $fvalue) {
									array_push( $ship_fee_arr, $fvalue);
								}
							}
						}
					}
					if( isset( $ship_fee_arr ) && ! empty( $ship_fee_arr ) ) {
						$added_fees[$tkey] = $ship_fee_arr;	
					}
				}
			} else {
				$added_fees = get_post_meta($order_id, 'mwb_wrffm_return_added_fee', true);
			}
			$ref_flag = false;
			if(isset($added_fees) && !empty($added_fees))
			{
				if(is_array($added_fees))
				{
					foreach($added_fees as $da=>$added_fee)
					{
						if($da == $key)
						{
							if(is_array($added_fee))
							{
								foreach($added_fee as $fee)
								{
									foreach ($mwb_vendor_id as $vendor_key => $vendor_value) {
			 							$refund_initiate_vendor = get_post_meta( $order_id,'refund_amount_by_vendor'.$vendor_value,true); 
			 							if( $refund_initiate_vendor == 1 ){
			 								$ref_flag = true;
			 							}
			 						}
			 						if( ! $ref_flag ) {
										$return_data['amount'] -= $fee['val']; 
			 						}
									if($return_data['status'] == 'pending')
									{
									?>
									<div class="mwb_wrffm_add_fee">
									
									<?php 
									}
									?>
										
										<input type="text" placeholder="<?php _e('Fee Name','woocommerce-rma-for-wcfm')?>" <?php echo $disable?> value="<?php echo $fee['text'];?>" name="mwb_return_fee_txt[]" class="mwb_return_fee_txt">
										<input type="text" name="" placeholder="0" <?php echo $disable?> value="<?php echo $fee['val'];?>" class="mwb_return_fee_value wc_input_price">
										<?php 
										if($return_data['status'] == 'pending' && $vendor_fee_flag == false )
										{	?>
										<input type="button" value="<?php _e('Remove','woocommerce-rma-for-wcfm')?>" class="button mwb_wrffm_remove-return-product-fee">
										<?php 
										}
									if($return_data['status'] == 'pending')
									{
									?>
									</div>
									<?php 	
									}
								}
							}
							break;
						}
					}	
				}
			}	
			if($return_data['status'] == 'pending' && $vendor_fee_flag == false)
			{
			  	?>
				</div>
				<button class="button mwb_wrffm_add-return-product-fee" type="button"><?php _e('Add Fee', 'woocommerce-rma-for-wcfm');?></button>
				<button class="button button-primary mwb_wrffm_save-return-product-fee" type="button" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>"><?php _e('Save', 'woocommerce-rma-for-wcfm');?></button>
				<?php 
			}

		}
		if($return_data['status'] == 'pending')
		{
			?>
			<input type="hidden" value="<?php echo $return_data['amount']?>" id="mwb_wrffm_refund_amount">
			<input type="hidden" value="<?php echo $return_data['subject']?>" id="mwb_wrffm_refund_reason">
			<?php
		}
		?>
		<p><strong>
			<?php
				foreach ($mwb_vendor_id as $vendor_key => $vendor_value) {
			 		$refund_initiate_vendor = get_post_meta($order_id,'refund_amount_by_vendor'.$vendor_value,true);
					$refund_amount_by_vendor=get_post_meta($order_id,'refund_amount_vendor'.$vendor_value.$vendor_key,true);
					if(isset($refund_amount_by_vendor) && !empty($refund_amount_by_vendor) && isset($refund_initiate_vendor) && !empty($refund_initiate_vendor)){
						$return_data['amount']=$return_data['amount']-$refund_amount_by_vendor;
						
					}
				}
			?>
		<?php _e('Refund Amount', 'woocommerce-rma-for-wcfm');?> :</strong> <?php echo mwb_wrffm_format_price($return_data['amount'])?> <input type="hidden" name="mwb_wrffm_total_amount_for_refund" class="mwb_wrffm_total_amount_for_refund" value="<?php echo $return_data['amount'] ; ?>"></p>
		<div class="mwb_wrffm_reason">	
			<p><strong><?php _e('Subject', 'woocommerce-rma-for-wcfm');?> :</strong><i> <?php echo $return_data['subject']?></i></p></p>
			<p><b><?php _e('Reason', 'woocommerce-rma-for-wcfm');?> :</b></p>
			<p><?php echo $return_data['reason']?></p>
			<?php 
			$req_attachments = get_post_meta($order_id, 'mwb_wrffm_return_attachment', true);
			
			if(isset($req_attachments) && !empty($req_attachments))
			{	
				?>
				<p><b><?php _e('Attachment', 'woocommerce-rma-for-wcfm');?> :</b></p>
				<?php
				if(is_array($req_attachments))
				{
					foreach($req_attachments as $da=>$attachments)
					{
						if($da == $key)
						{
							$count = 1;
							foreach($attachments['files'] as $attachment)
							{
								if($attachment != $order_id.'-')
								{
									?>
									<a href="<?php echo home_url()?>/wp-content/attachment/<?php echo $attachment?>" target="_blank"><?php _e('Attachment','woocommerce-rma-for-wcfm');?>-<?php echo $count;?></a>
									<?php 
									$count++;
								}
							}	
							break;
						}
					}		
				}	
			}
		if($return_data['status'] == 'pending')
		{
			$mwb_wrffm_enable_return_ship_label =get_option('mwb_wrffm_enable_return_ship_label', 'no');
			if($mwb_wrffm_enable_return_ship_label == 'on')
			{ ?>
		        <p><b><?php _e('Return Slip Label Attachment :','woocommerce-rma-for-wcfm');?></b></p> 
		        <?php
		        $mwb_wrffm_return_upload_button = __('Upload','woocommerce-rma-for-wcfm');
				$return_label_name = get_post_meta($order_id,'mwb_return_label_attachment_name',true);
				if($return_label_name != ''){
					?>
					<span><b>Uploaded Attachment :</b></span>
					<a href="<?php echo content_url().'/return-label-attachments/'.$return_label_name ?>" target="_blank"><?php _e('Return Label Attachment','woocommerce-rma-for-wcfm'); ?></a><br><br>
					<?php
					$mwb_wrffm_return_upload_button = __('Change Attachment','woocommerce-rma-for-wcfm');
				}
				?>

				<div id="mwb_wrffm_return_ship_attchment_div">      
				    <p class="description"><?php _e('Upload your Return label attachment here.','woocommerce-rma-for-wcfm');?></p>
				    <input type="file" id="mwb_return_label_attachment" name="mwb_return_label_attachment" value="" size="25" />
	                <input type="submit" name="save" class="button save_order button-primary" value="<?php echo $mwb_wrffm_return_upload_button; ?>" id='mwb_wrffm_save_button'> 
                </div>
            <?php	
			}
	    }		

			if($return_data['status'] == 'pending')
			{	
				?>
				<p id="mwb_wrffm_return_package">
				<input type="button" value="<?php _e('Accept Request','woocommerce-rma-for-wcfm');?>" class="button" id="mwb_wrffm_accept_return" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>">
				<input type="button" value="<?php _e('Cancel Request','woocommerce-rma-for-wcfm');?>" class="button" id="mwb_wrffm_cancel_return" data-orderid="<?php echo $order_id;?>" data-date="<?php echo $key;?>">
				</p>
				<?php 
			}
			?>
		</div>
		<div class="mwb_wrffm_return_loader">
			<img src="<?php echo home_url();?>/wp-admin/images/spinner-2x.gif">
		</div>
		</div>	
		</div>
		<p>
		<?php 
		if($return_data['status'] == 'complete')
		{
			?>
			<input type="hidden" value="<?php echo mwb_wrffm_currency_seprator($return_data['amount']) ?>" id="mwb_wrffm_refund_amount">
			<input type="hidden" value="<?php echo $return_data['subject']?>" id="mwb_wrffm_refund_reason">
			<?php
			$refundable_amount = 0;
			$refundable_amount = get_post_meta($order_id,'refundable_amount',true);
			$approve_date=date_create($return_data['approve_date']);
			$date_format = get_option('date_format');
			$approve_date=date_format($approve_date,$date_format);
         
			if($refundable_amount > 0)
			{
				_e( 'Following product refund request is approved on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b><input type="button" name="mwb_wrffm_left_amount" class="button button-primary" data-orderid="<?php echo $order_id; ?>" id="mwb_wrffm_refund_left_amount" Value="<?php _e('Refund Amount','woocommerce-rma-for-wcfm'); ?>" > <?php
			}
			else{
				_e( 'Following product refund request is approved on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b>
			<?php
			}
			$mwb_wrffm_manage_stock_for_return = get_post_meta($order_id,'mwb_wrffm_manage_stock_for_return',true);
			if($mwb_wrffm_manage_stock_for_return == '')
			{
				$mwb_wrffm_manage_stock_for_return = 'yes';
			}
			$manage_stock = get_option('mwb_wrffm_return_request_manage_stock');
			if($manage_stock == "yes" && $mwb_wrffm_manage_stock_for_return == 'yes')
			{
				?> <div id="mwb_wrffm_stock_button_wrapper"><?php _e( 'When Product Back in stock then for stock management click on ', 'woocommerce-rma-for-wcfm' ); ?> <input type="button" name="mwb_wrffm_stock_back" class="button button-primary" id="mwb_wrffm_stock_back" data-type="mwb_wrffm_return" data-orderid="<?php echo $order_id; ?>" Value="Manage Stock" ></div> <?php
			}
		}
		if($return_data['status'] == 'cancel')
		{
			$approve_date=date_create($return_data['cancel_date']);
			$approve_date=date_format($approve_date,"F d, Y");
				
			_e( 'Following product refund request is cancelled on', 'woocommerce-rma-for-wcfm' ); ?> <b><?php echo $approve_date?>.</b>
		<?php
		}
		?>
		</p>
		<hr/>
		<?php 
	}
} 
else 
{	
	$mwb_wrffm_pages= get_option('mwb_wrffm_pages');
	$page_id = $mwb_wrffm_pages['pages']['mwb_return_from'];
	$return_url = get_permalink($page_id);
	$order_id = $order->get_id();
	$mwb_wrffm_return_url = add_query_arg('order_id',$order_id,$return_url);
?>
<p><?php _e('No request from customer', 'woocommerce-rma-for-wcfm');?></p>
<a target="_blank" href="<?php echo $mwb_wrffm_return_url; ?>" class="button-primary button"><b><?php _e('Initiate Refund Request','woocommerce-rma-for-wcfm'); ?></b></a>
<?php 
}
?>