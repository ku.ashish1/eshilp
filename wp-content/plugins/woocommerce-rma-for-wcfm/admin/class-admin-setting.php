<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main Plugin class for managing admin interfaces.
 *
 * @class    MWB_wrffm_admin_interface
 *
 * @version  1.0.0
 * @package  return-and-exchange/admin
 * @category Class
 * @author   makewebbetter <webmaster@makewebbetter.com>
 */
	
if( !class_exists( 'MWB_wrffm_admin_interface' ) ){

	class MWB_wrffm_admin_interface{
		
		/**
		 * This is construct of class
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		
		public function __construct(){
			$this->id = 'mwb_wrffm_setting';
			$this->add_admin_menu_for_rma_configuration();

			$mwb_wrffm_license_status = get_option('mwb_wrffm_license_status',false);
			$today_date = current_time('timestamp');
			$mwb_wrffm_activation_date = get_option('mwb_wrffm_activation_date',false);
			$mwb_wrffm_days = $today_date - $mwb_wrffm_activation_date;
			$mwb_wrffm_day_diff = floor($mwb_wrffm_days/(60*60*24));
			if( $mwb_wrffm_license_status || $mwb_wrffm_day_diff <= 30 )
			{
				add_filter( 'woocommerce_settings_tabs_array', array($this,'mwb_wrffm_add_settings_tab'), 50 );
				add_action( 'woocommerce_settings_tabs_' . $this->id, array($this,'mwb_wrffm_settings_tab') );
				add_action( 'woocommerce_sections_' . $this->id, array( $this, 'mwb_wrffm_output_sections' ) );
				add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
				add_filter( 'manage_users_columns', array($this,'mwb_wrffm_add_coupon_column' ));
				add_filter( 'manage_users_custom_column', array($this,'mwb_wrffm_add_coupon_column_row'), 10, 3 );
				add_action( 'edit_user_profile', array( $this , 'mwb_wrffm_add_customer_wallet_price_field' ) );
				add_action( 'show_user_profile', array( $this , 'mwb_wrffm_add_customer_wallet_price_field' ) );
				add_action( 'admin_init', array( $this , 'mwb_wrffm_save_catalog_settengs' ) );
			}
		}

		/**
		 * This function is used for creating admin menu for rm.
		 * 
		 * @name mwb_wrffm_save_catalog_settengs
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public function add_admin_menu_for_rma_configuration()
		{		
		   add_action('admin_menu', array( $this, 'mwb_wrffm_notification_menu_for_woo'));		
		}
		/**
		 * This function is used for saving catalog settings.
		 * 
		 * @name mwb_wrffm_save_catalog_settengs
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public function mwb_wrffm_save_catalog_settengs()
		{
			if(isset($_GET['section']))
			{	
				if($_GET['section']=='catalog_setting')
				{
					if(isset($_POST['save']))
					{	
						$count=get_option('catalog_count');
						$Catalog=array();
						if(isset($count) && $count!=null && $count>0)
						{
							for($i=1;$i<=$count;$i++)
							{
								if( !empty($_POST["mwb_wrffm_products$i"]) && trim($_POST["mwb_wrffm_catalog_name$i"])!=null && ($_POST["mwb_wrffm_catalog_refund_days$i"]!=null || $_POST["mwb_wrffm_catalog_exchange_days$i"]!=null))
								{
									$Catalog['Catalog'.$i]['name']=$_POST["mwb_wrffm_catalog_name$i"];
									$Catalog['Catalog'.$i]['products']=$_POST["mwb_wrffm_products$i"];
									$Catalog['Catalog'.$i]['refund']=$_POST["mwb_wrffm_catalog_refund_days$i"];
									$Catalog['Catalog'.$i]['exchange']=$_POST["mwb_wrffm_catalog_exchange_days$i"];
								}
							}
						}
						else{
							if( !empty($_POST["mwb_wrffm_products1"]) && trim($_POST["mwb_wrffm_catalog_name1"])!=null && ($_POST["mwb_wrffm_catalog_refund_days1"]!=null || $_POST["mwb_wrffm_catalog_exchange_days1"]!=null))
							{
								$Catalog['Catalog1']['name']=$_POST["mwb_wrffm_catalog_name1"];
								$Catalog['Catalog1']['products']=$_POST["mwb_wrffm_products1"];
								$Catalog['Catalog1']['refund']=$_POST["mwb_wrffm_catalog_refund_days1"];
								$Catalog['Catalog1']['exchange']=$_POST["mwb_wrffm_catalog_exchange_days1"];
							}
						}	
						update_option('catalog',$Catalog,'yes');
					}
				}
			}
		}
		
		/**
		 * Add wallet Coupon column on user list page.
		 * 
		 * @name mwb_wrffm_add_coupon_column
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_add_coupon_column( $column )
		{
			$column['mwb_wrffm_coupon_column'] = __('User Wallet' , 'woocommerce-rma-for-wcfm');
			return $column;
		}

		/**
		 * Add Wallet Coupon amount change field on user edit page .
		 * 
		 * @name mwb_wrffm_add_customer_wallet_price_field
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_add_customer_wallet_price_field( $user ){
			
			$coupon_code = get_user_meta( $user->ID, 'mwb_wrffm_refund_wallet_coupon' , TRUE );
			$the_coupon = new WC_Coupon( $coupon_code );
			if( WC()->version < "3.0.0" )
			{
				$coupon_id=$the_coupon->id;
			}
			else
			{
				$coupon_id=$the_coupon->get_id();	
			}
			if(isset($coupon_id) && $coupon_id!= '')
			{
				$customer_coupon_id = $coupon_id;
				$amount = get_post_meta( $customer_coupon_id, 'coupon_amount', true );
				?>
			  	<h3><?php _e("Add amount to Customer Wallet", "woocommerce-rma-for-wcfm"); ?></h3>
			 	<table class="form-table">
			    	<tr>
			      		<th><label for="mwb_wrffm_customer_wallet_price"><?php echo $coupon_code; ?></label></th>
			      		<td>
			        		<input type="text" id="mwb_wrffm_customer_wallet_price" class="regular-text" 
			            value="<?php echo $amount ?>" /><br />
			        		<span class="description"><?php _e("Provide Coupon Amount (Enter only number and decimal amount)" , 'woocommerce-rma-for-wcfm'); ?></span>
			    		</td>
			    		<td>
			    			<input type="button" class="button button-primary mwb_wrffm_change_customer_wallet_amount" id="mwb_wrffm_change_customer_wallet_amount" data-id = "<?php echo $user->ID ?>" data-couponcode = "<?php echo $coupon_code ?>" value="<?php _e('Change Coupon Amount' , 'woocommerce-rma-for-wcfm') ?>"></input>
			    			<img class="regenerate_coupon_code_image" src = '<?php echo MWB_WRFFM_URL."assets/images/loading.gif" ?>' width="20px" style="display:none;">
			    		</td>
			   		</tr>
			  </table>
				<?php
			}
		}

		/**
		 * Add user wallet data to the custom column created.
		 * 
		 * @name mwb_wrffm_add_coupon_column_row
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_add_coupon_column_row( $val, $column_name, $user_id ) {
			switch ($column_name)
			{
				case 'mwb_wrffm_coupon_column' :
					$coupon_code = get_user_meta( $user_id, 'mwb_wrffm_refund_wallet_coupon' , TRUE );
					$the_coupon = new WC_Coupon( $coupon_code );
					if( WC()->version < "3.0.0" )
					{
						$coupon_id=$the_coupon->id;
					}
					else
					{
						$coupon_id=$the_coupon->get_id();
					}
					if(isset($coupon_id) && $coupon_id != '')
					{
						$coupon_amount = get_post_meta( $coupon_id, 'coupon_amount', true );
						$coupon_amount = wc_price($coupon_amount);
						$val = $coupon_code.'<br><b>( '.$coupon_amount.' )</b>';
						return $val;
					}else{
						$val = '<div id="user'.$user_id.'"><input type="button" class="button button-primary mwb_wrffm_add_customer_wallet" data-id = "'.$user_id.'" id="mwb_wrffm_add_customer_wallet-'.$user_id.'" value="Create Wallet"><img id="regenerate_coupon_code_image-'.$user_id.'" src = '.MWB_WRFFM_URL.'assets/images/loading.gif width="20px" style="display:none;"></div>';
					}
					break;
				default:
			}
			return $val;
		}

		/**
		 * Add notification submenu in woocommerce 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_notification_menu_for_woo()
		{
			add_submenu_page( 'woocommerce', __('RMA Configuration','woocommerce-rma-for-wcfm'), __('RMA Configuration','woocommerce-rma-for-wcfm'), 'manage_options', 'mwb-wrffm-notification', array( $this, 'mwb_wrffm_notification_callback' ));
		}
		
		/**
		 * Add notification submenu in woocommerce
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_notification_callback()
		{
			include_once MWB_WRFFM_DIRPATH.'admin/mwb-wrffm-notification.php';
		}
		
		/**
		 * Add new tab to woocommerce setting
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public static function mwb_wrffm_add_settings_tab( $settings_tabs ) {
			$settings_tabs['mwb_wrffm_setting'] = __( 'RMA Setting', 'woocommerce-rma-for-wcfm' );
			return $settings_tabs;
		}
		
		/**
		 * Save section setting 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		function mwb_wrffm_settings_tab() 
		{
			global $current_section; ?>
			<div class="mwb_wrffm_table"> 
			<?php 
			woocommerce_admin_fields( self::mwb_wrffm_get_settings($current_section) );
			?>
			</div>
			<!-- <div class="mwb_table">  -->
			<?php 
			if(isset($_GET['section']))
			{
				if($_GET['section']=='catalog_setting')
				{ 
					?><div class="mwb_wrffm_error_notice">
					</div>
       				<?php
			
					$mwb_wrffm_catalog=get_option('catalog',array());
					$products= array();
					$args = array( 'post_type' => 'product','posts_per_page' => -1);
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); 
					global $product; 
					$products[$product->get_id()]=$product->get_title();
					endwhile; 
					wp_reset_query();
					$counter=0;
					?><div class="mwb_wrffm_catalog_wrapper_section">
						<?php
						if(isset($products) && !empty($products) ) 
						{
							$mwb_count=0;
							 foreach ($products as $key => $value) 
							 {
							?><input type="hidden" class="mwb_product_id<?php echo $mwb_count ?>" value="<?php echo $key ?>" >
							<input type="hidden" id="mwb_products_count" value="<?php echo count($products); ?>"	>
							<input type="hidden" class="mwb_products<?php echo $mwb_count ?>" value="<?php echo $value ?>" >
							<?php $mwb_count++; 
							 }
						}
					if(isset($mwb_wrffm_catalog) && !empty($mwb_wrffm_catalog) ) 
					{	
						foreach ($mwb_wrffm_catalog as $key => $value) 
						{ 
							$counter++;
						?>
							<div class="mwb_wrffm_catalog_dropdwn" data-counter = <?php echo $counter ?> >
								<div class="mwb_wrffm_catalog_wrapper" >
										<div class="mwb_wrffm_catalog_name_text"><strong><?php echo $value['name'] ?></strong></div>
										<a class="mwb_wrffm_catalog_delete" data-counter="<?php echo $counter; ?>" href="javascript:; "><strong><?php _e('-','woocommerce-rma-for-wcfm' ); ?></strong></a>
										<a class="mwb_wrffm_catalog_add" data-counter="<?php echo $counter; ?>" href="javascript:;"><strong><?php _e('+','woocommerce-rma-for-wcfm' ); ?></strong></a>
									</div>
									<div class="mwb_wrffm_catalog_toggle" >
										<table id="mwb_wrffm_catalog_table">
											<tr>	
												<th><label><strong><?php _e('Catalog Name:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
												<td><input type="text" name="mwb_wrffm_catalog_name<?php echo $counter ?>" class="mwb_wrffm_catalog_name" placeholder="<?php _e('Enter Catalog Name','woocommerce-rma-for-wcfm' ); ?>" value="<?php echo $value['name'] ?>" ></td>
											</tr>
											<tr>	
												<th ><label><strong><?php _e('Select Catalog Products:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
												<td><select  name="mwb_wrffm_products<?php echo $counter ?>[]" class="mwb_wrffm_products" multiple id="product">
													<?php  
													$args = array( 'post_type' => 'product', 'posts_per_page' => -1);
													$loop = new WP_Query( $args );
													while ( $loop->have_posts() ) : $loop->the_post(); 
													global $product; 
													?>
													<option value="<?php echo $id=get_the_ID(); ?>" <?php if(is_array($value['products'])){ if(in_array($id,$value['products'])){ echo 'selected'; } } ?>><?php echo get_the_title(); ?></option>
													<?php
													endwhile; 
													wp_reset_query(); 
													?>
												</select></td>
											</tr><tr>	
												<th><label><strong><?php _e('Maximum Refund Days:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
												<td><input type="number" min="0" placeholder="<?php _e('Enter Refund Days','woocommerce-rma-for-wcfm' ); ?>"  value="<?php echo $value['refund'] ?>" name="mwb_wrffm_catalog_refund_days<?php echo $counter ?>" class="mwb_wrffm_catalog_refund_days"><span><?php _e('[If value is 0 then catalog will not work.]', 'woocommerce-rma-for-wcfm' ); ?></span></td>
											</tr><tr>
												<th><label><strong><?php _e('Maximum Exchange Days:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
												<td><input type="number" min="0" placeholder="<?php _e('Enter Exchange Days','woocommerce-rma-for-wcfm' ); ?>" value="<?php echo $value['exchange'] ?>" name="mwb_wrffm_catalog_exchange_days<?php echo $counter ?>" class="mwb_wrffm_catalog_exchange_days"><span><?php _e('[If value is 0 then catalog will not work.]', 'woocommerce-rma-for-wcfm' ); ?></span></td>
											</tr>
										</table>
									</div>
								</div>		
							<?php
						}
					}
					else
					{ ?>
						<div class="mwb_wrffm_catalog_dropdwn" data-counter = 1 >
							<div class="mwb_wrffm_catalog_wrapper" >
								<div class="mwb_wrffm_catalog_name_text"><strong><?php _e('Default Catalog', 'woocommerce-rma-for-wcfm' ); ?></strong></div>
								<a class="mwb_wrffm_catalog_delete" data-counter="1" href="javascript:; "><strong><?php _e('-','woocommerce-rma-for-wcfm' ); ?></strong></a>
								<a class="mwb_wrffm_catalog_add" data-counter="1" href="javascript:;"><strong><?php _e('+','woocommerce-rma-for-wcfm' ); ?></strong></a>
							</div>
							<div class="mwb_wrffm_catalog_toggle" >
								<table id="mwb_wrffm_catalog_table">
									<tr>	
										<th><label><strong><?php _e('Catalog Name:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
										<td><input type="text" name="mwb_wrffm_catalog_name1" class="mwb_wrffm_catalog_name" placeholder="<?php _e('Enter Catalog Name','woocommerce-rma-for-wcfm' ); ?>"  ></td>
									</tr>
									<tr>	
										<th ><label><strong><?php _e('Select Catalog Products:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
										<td><select name="mwb_wrffm_products1[]" id="mwb_wrffm_products" class="mwb_wrffm_products" multiple>
											<?php  
											$args = array( 'post_type' => 'product', 'posts_per_page' => -1);
											$loop = new WP_Query( $args );
											while ( $loop->have_posts() ) : $loop->the_post(); 
											global $product; ?>
											<option value="<?php echo get_the_ID(); ?>" ><?php echo get_the_title(); ?></option>
											<?php
											endwhile; 
											wp_reset_query(); 
											?>
										</select></td>
									</tr><tr>	
										<th><label><strong><?php _e('Maximum Refund Days:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
										<td><input type="number" min="0" placeholder="<?php _e('Enter Refund Days','woocommerce-rma-for-wcfm' ); ?>"   name="mwb_wrffm_catalog_refund_days1" class="mwb_wrffm_catalog_refund_days"><span><?php _e('[If value is 0 then catalog will not work.]', 'woocommerce-rma-for-wcfm' ); ?></span></td>
									</tr><tr>
										<th><label><strong><?php _e('Maximum Exchange Days:', 'woocommerce-rma-for-wcfm' ); ?></strong></label></th>
										<td><input type="number" min="0" placeholder="<?php _e('Enter Exchange Days','woocommerce-rma-for-wcfm' ); ?>"   name="mwb_wrffm_catalog_exchange_days1" class="mwb_wrffm_catalog_exchange_days"><span><?php _e('[If value is 0 then catalog will not work.]', 'woocommerce-rma-for-wcfm' ); ?></span></td>
									</tr>
								</table>
							</div>
						</div>
					<?php 
					}
					?>
					</div><?php
					}
				}
				 ?>
				<?php  
			}
		
		/**
		 * Output of section setting 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public function mwb_wrffm_output_sections() {
				
			global $current_section;
			$sections = $this->mwb_wrffm_get_sections();
			if ( empty( $sections ) || 1 === sizeof( $sections ) ) {
				return;
			}
		
			echo '<ul class="subsubsub">';
		
			$array_keys = array_keys( $sections );
		
			foreach ( $sections as $id => $label ) {
				echo '<li><a href="' . admin_url( 'admin.php?page=wc-settings&tab=' . $this->id . '&section=' . sanitize_title( $id ) ) . '" class="' . ( $current_section == $id ? 'current' : '' ) . '">' . $label . '</a> ' . ( end( $array_keys ) == $id ? '' : '|' ) . ' </li>';
			}
		
			echo '<li> | <a href="'.home_url().'/wp-admin/admin.php?page=mwb-wrffm-notification">'.__('Mail Configuration','woocommerce-rma-for-wcfm').'</a></li>';			
			echo '</ul><br class="clear mwb_wrffm_clear"/>';
		}
		
		/**
		 * Create section setting 
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		public function mwb_wrffm_get_sections() {
		
			$sections = array(
					''             	=>  __( 'Refund Products', 'woocommerce-rma-for-wcfm' ),
					'exchange'     	=>  __( 'Exchange Products', 'woocommerce-rma-for-wcfm' ),
					'other'     	=>  __( 'Common Setting', 'woocommerce-rma-for-wcfm' ),
					'cancel'	   	=>  __( 'Cancel Order', 'woocommerce-rma-for-wcfm' ),
					'wallet'	   	=>  __( 'Wallet Settings', 'woocommerce-rma-for-wcfm' ),	
					'text_setting'  =>  __( 'Text Settings' , 'woocommerce-rma-for-wcfm' ),
					'catalog_setting'=> __('Catalog Settings', 'woocommerce-rma-for-wcfm'),
			);
		
			return apply_filters( 'mwb_wrffm_get_sections_' . $this->id, $sections );
		}
		
		/**
		 * Section setting
		 * @author makewebbetter<webmaster@makewebbetter.com>
		 * @link http://www.makewebbetter.com/
		 */
		 function mwb_wrffm_get_settings($current_section) {
	    	
		 	/* get woocommerce categories */
		 		
		 	$all_cat = get_terms('product_cat',array('hide_empty'=>0));
		 	$cat_name = array();
		 	if($all_cat){
		 		foreach ($all_cat as $cat){
		 	
		 			$cat_name[$cat->term_id] = $cat->name;
		 	
		 		}
		 	}
		 	
		 	$statuses = wc_get_order_statuses();
		 	$status=$statuses;
		 	
		 	if ( 'exchange' == $current_section ) 
	    	{   
	    		$settings = array(
	    							array(
										'title' => __( 'Exchange Products Setting', 'woocommerce-rma-for-wcfm' ),
										'type' 	=> 'title',
									),
									array(
				    					'title'         => __( 'Enable', 'woocommerce-rma-for-wcfm' ),
				    					'desc'          => __( 'Enable Exchange Request', 'woocommerce-rma-for-wcfm' ),
				    					'default'       => 'no',
				    					'type'          => 'checkbox',
					    				'id' 		=> 'mwb_wrffm_exchange_enable'
									),
					    			array(
				    					'title'         => __( 'Enable Exchange Request With Same Product or its Variations', 'woocommerce-rma-for-wcfm' ),
				    					'desc'          => __( 'Enable Exchange Request only for Exchange with same Product or its Variations.', 'woocommerce-rma-for-wcfm' ),
				    					'default'       => 'no',
				    					'type'          => 'checkbox',
					    				'id' 		=> 'mwb_wrffm_exchange_variation_enable'
									),
				    				array(
			    						'title'         => __( 'Sale Items', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Enable Exchange Request for Sale Items', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_sale_enable'
				    				),
	    				
				    				array(
			    						'title'         => __( 'Include Tax', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Include Tax with Product Exchange Request.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_tax_enable'
				    				),
	    				 
				    				array(
			    						'title'         => __( 'Add Shipping Fee', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Add Shipping fee to Exchange amount.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_shipcost_enable'
				    				),
				    				array(
			    						'title'         => __( 'Enable Exchange Note on Product Page', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Enable to show the note on product page.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_note_enable'
				    				),
				    				 
				    				array(
			    						'title'         => __( 'Exchange Note on Product Page', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'This note is shown on product detail page.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'This Product is not exchangable',
			    						'type'          => 'textarea',
			    						'desc_tip' =>  true,
			    						'id' 		=> 'mwb_wrffm_exchange_note_message'
				    				),
				    				array(
				    					'title'         => __( 'Maximum Number of Days', 'woocommerce-rma-for-wcfm' ),
				    					'desc'          => __( 'If days exceeds from the day of order delivered then Exchange Request will not be send. If value is 0 or blank then Exchange button will not visible at order detail page.', 'woocommerce-rma-for-wcfm' ),
				    					'type'          => 'number',
				    					'custom_attributes'   => array('min'=>'0'),
				    					'id' 		=> 'mwb_wrffm_exchange_days'
					    			),
				    				array(
			    						'title'         => __( 'Minimum Order Amount', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Minimum Order amount must be greater or equal to this amount. Keep blank to enable exchange for all Order.', 'woocommerce-rma-for-wcfm' ),
			    						'type'          => 'number',
			    						'custom_attributes'   => array('min'=>'0'),
			    						'desc_tip' =>  true,
			    						'id' 		=> 'mwb_wrffm_exchange_minimum_amount'
				    				),
				    				array(
			    						'title'    => __( 'Exclude Categories', 'woocommerce-rma-for-wcfm' ),
			    						'desc'     => __( 'Select those categories for which products you don\'t want to exchange.', 'woocommerce-rma-for-wcfm' ),
			    						'class'    => 'wc-enhanced-select',
			    						'css'      => 'min-width:300px;',
			    						'default'  => '',
			    						'type'     => 'multiselect',
			    						'options'  => $cat_name,
			    						'desc_tip' =>  true,
			    						'id' 		=> 'mwb_wrffm_exchange_ex_cats'
				    				),
				    				array(
			    						'title'         => __( 'Show Add To Cart button on time of Exchange', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Enable to show Add To Cart button on time exchange session is enable.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_add_to_cart_enable'
				    				),
				    				array(
			    						'title'         => __( 'Enable Exchange Reason Description', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Enable this for user to send the detail description of exchange request.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_request_description'
				    				),
				    				array(
			    						'title'         => __( 'Enable Manage Stock', 'woocommerce-rma-for-wcfm' ),
			    						'desc'          => __( 'Enable this to increase product stock when exhange request is accepted.', 'woocommerce-rma-for-wcfm' ),
			    						'default'       => 'no',
			    						'type'          => 'checkbox',
			    						'id' 		=> 'mwb_wrffm_exchange_request_manage_stock'
				    				),
				    				array(
			    						'title'    => __( 'Select the order status in which the order can be exchanged', 'woocommerce-rma-for-wcfm' ),
			    						'desc'     => __( 'Select Order status on which you want exchange request user can submit.', 'woocommerce-rma-for-wcfm' ),
			    						'class'    => 'wc-enhanced-select',
			    						'css'      => 'min-width:300px;',
			    						'default'  => '',
			    						'type'     => 'multiselect',
			    						'options'  => $statuses,
			    						'desc_tip' =>  true,
			    						'id' 		=> 'mwb_wrffm_exchange_order_status'
				    				),
	    							array(
										'type' 	=> 'sectionend',
									)
								);
	    		return apply_filters( 'mwb_wrffm_get_settings_return' . $this->id, $settings );
	    	}
	    	else
	    	{
	    		if ( 'other' == $current_section ) 
	    		{
	    			$settings = array(
				    					array(
				    							'title' => __( 'Common Setting', 'woocommerce-rma-for-wcfm' ),
				    							'type' 	=> 'title',
				    					),
		    							array(
					    					'title'         => __( 'Enable', 'woocommerce-rma-for-wcfm' ),
					    					'desc'          => sprintf(__( 'Enable Single Refund/Exchange Request per order. %s ( %s If any one Refund/Exchange request is done with an order then Refund/Exchange request is disable for that order. %s )', 'woocommerce-rma-for-wcfm' ), '<br/>', '<i>', '</i>'),
					    					'default'       => 'no',
					    					'type'          => 'checkbox',
		    								'id' 		=> 'mwb_wrffm_return_exchange_enable'
										),

										array(
				    						'title'         => __( 'Enable Refund & Exchange for exchange approved order', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable refund & exchange feature for exchange approved order.When exchange approved order goes in selected order status then order is available for refund & exchange feature.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_exchange_approved_enable'
					    				),
					    				array(
				    						'title'         => __( 'Show Sidebar in Refund & Exchange Request Form', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable this if you want to show sidebar on refund and exchange request form..', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_show_sidebar_on_form'
					    				),
				    					array(
			    							'title'         => __( 'Main Wrapper Class of Theme', 'woocommerce-rma-for-wcfm' ),
			    							'desc'          => sprintf(__( 'Write the main wrapper class of your theme if some design issue arises.','woocommerce-rma-for-wcfm'  ), '<br/>', '<i>', '</i>'),
			    							'type'          => 'text',
			    							'id' 		=> 'mwb_wrffm_return_exchange_class'
				    					),
				    					array(
			    							'title'         => __( 'Child Wrapper Class of Theme', 'woocommerce-rma-for-wcfm' ),
			    							'desc'          => sprintf(__( 'Write the child wrapper class of your theme if some design issue arises.','woocommerce-rma-for-wcfm' ), '<br/>', '<i>', '</i>'),
			    							'type'          => 'text',
			    							'id' 		=> 'mwb_wrffm_return_exchange_child_class'
				    					),
				    					array(
			    							'title'         => __( 'Refund form Custom CSS', 'woocommerce-rma-for-wcfm' ),
			    							'desc'          => sprintf(__( 'Write the custom css for Refund form.' ,'woocommerce-rma-for-wcfm' ), '<br/>', '<i>', '</i>'),
			    							'type'          => 'textarea',
			    							'id' 		=> 'mwb_wrffm_return_custom_css'
				    					),
				    					array(
			    							'title'         => __( 'Exchange form Custom CSS', 'woocommerce-rma-for-wcfm' ),
			    							'desc'          => sprintf(__( 'Write the custom css for exchange form.','woocommerce-rma-for-wcfm'  ), '<br/>', '<i>', '</i>'),
			    							'type'          => 'textarea',
			    							'id' 		=> 'mwb_wrffm_exchange_custom_css'
				    					),
				    					array(
		    								'title' 		=> __( 'Shortcode for Wallet', 'woocommerce-rma-for-wcfm' ),
		    								'desc'    		=> __( 'Copy and  Paste this Shortcode on any page for the customer wallet to be displayed.', 'woocommerce-rma-for-wcfm' ),
		    								'desc_tip'		=> true,
		    								'type'			=> 'textarea',
		    								'default'       => '[mwb_wrffm_customer_wallet]',
		    								'id'			=> 'mwb_wrffm_customer_wallet_shortcode',
	    								), 
	    								array(
				    							'type' 	=> 'sectionend',
				    					)
	    						);
	    			return apply_filters( 'mwb_wrffm_get_settings_other' . $this->id, $settings );
	    		}
	    		else if( 'cancel' == $current_section )
	    		{
	    			$settings = array(
	    								array(
	    									'title' 		=> __( 'Cancel Order Setting' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'title',
    									),
    									array(
	    									'title' 		=> __( 'Enable' , 'woocommerce-rma-for-wcfm' ),
	    									'desc'          => __( 'Enable Cancel Order', 'woocommerce-rma-for-wcfm' ),
	    									'default'       => 'no',
	    									'type' 			=> 'checkbox',
	    									'id' 			=> 'mwb_wrffm_cancel_enable'
    									),
    									array(
	    									'title' 		=> __( "Enable Order's Product Cancel " , 'woocommerce-rma-for-wcfm' ),
	    									'desc'          => __( "Enable Cancel Order's Product", 'woocommerce-rma-for-wcfm' ),
	    									'default'       => 'no',
	    									'type' 			=> 'checkbox',
	    									'id' 			=> 'mwb_wrffm_cancel_order_product_enable'
    									),
	    								array(
				    						'title'    => __( 'Select the order status in which the order can be cancelled', 'woocommerce-rma-for-wcfm' ),
					    					'desc'     => __( 'Select Order status on which you want to cancel the order by the customer.', 'woocommerce-rma-for-wcfm' ),
					    					'class'    => 'wc-enhanced-select',
					    					'css'      => 'min-width:300px;',
					    					'default'  => '',
					    					'type'     => 'multiselect',
					    					'options'  => $statuses,
					    					'desc_tip' =>  true,
					    					'id' 		=> 'mwb_wrffm_cancel_order_status'
					    				),
					    				array(
					    						'type' 	=> 'sectionend',
					    				),
	    							);
	    			return apply_filters( 'mwb_wrffm_get_settings_cancel' . $this->id, $settings );
	    		}
	    		else if( 'text_setting' == $current_section )
	    		{
	    			$settings = array(
	    								array(
	    									'title' 		=> __( 'Modify Text on Frontend' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'title',
    									),
    									array(
	    									'title' 		=> __( 'Guest Refund/Exchange Form Text ' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'default'		=> 'Refund/Exchange Request Form',
	    									'id'			=> 'mwb_wrffm_return_exchange_page_heading_text',
	    									'desc'			=> __( 'Change heading for guest Refund exchange request page' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Exchange Button Text' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_exchange_button_text',
	    									'default'		=> 'Exchange',
	    									'desc'			=> __( 'Change exchange button text on frontend' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Refund Button Text' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_return_button_text',
	    									'default'		=> 'Refund',
	    									'desc'			=> __( 'Change Refund button text on frontend' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Placeholder text for Refund reason field' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_return_placeholder_text',
	    									'default'		=> 'Reason for Refund',
	    									'desc'			=> __( 'Add Placeholder text for Refund reason' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Placeholder text for Exchange reason field' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_exchange_placeholder_text',
	    									'default'		=> 'Reason for Exchange',
	    									'desc'			=> __( 'Add Placeholder text for Exchange reason' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Exchange with same product form text' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_exchnage_with_same_product_text',
	    									'default'		=> 'Click on product(s) to exchange with selected product(s) or its variation(s). ',
	    									'desc'			=> __( "Add text to display on Exchange form to Exchanging with same product(s) and it's variation(s). ", "woocommerce-rma-for-wcfm"),
    									),
    									array(
	    									'title' 		=> __( 'Order Cancel Button Text' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_order_cancel_text',
	    									'default'		=> __('Cancel Order','woocommerce-rma-for-wcfm'),
	    									'desc'			=> __( 'Change Cancel Order button text on frontend' , 'woocommerce-rma-for-wcfm' ),
    									),
    									array(
	    									'title' 		=> __( 'Cancel Product Button Text' , 'woocommerce-rma-for-wcfm' ),
	    									'type'  		=> 'text',
	    									'id'			=> 'mwb_wrffm_product_cancel_text',
	    									'default'		=> __('Cancel Product','woocommerce-rma-for-wcfm'),
	    									'desc'			=> __( 'Change Cancel Product button text on frontend' , 'woocommerce-rma-for-wcfm' ),
    									),
					    				array(
					    						'type' 	=> 'sectionend',
					    				),
	    							);
	    			return apply_filters( 'mwb_wrffm_get_text_change_settings' . $this->id, $settings );
	    		}
	    		else if( 'catalog_setting' == $current_section )
	    		{
	    			$settings = array(
	    				array(
	    					'class'	=> 'mwb_wrffm_catalog_submit',
	    					'type' => 'sectionend',
	    					),);
	    								
	    			return apply_filters( 'mwb_wrffm_get_catalog_settings' . $this->id, $settings );
	    			
	    		
	    		}
	    		else if( 'wallet' == $current_section )
	    		{
	    			$settings = array(
	    					array(
								'title' 		=> __( 'Wallet settings' , 'woocommerce-rma-for-wcfm' ),
								'type'  		=> 'title',
							),
	    					array(
	    						'title'         => __( 'Enable Wallet', 'woocommerce-rma-for-wcfm' ),
	    						'desc'          => __( 'Enable this for add the refund amount to customer wallet', 'woocommerce-rma-for-wcfm' ),
	    						'default'       => 'no',
	    						'type'          => 'checkbox',
	    						'id' 		=> 'mwb_wrffm_return_wallet_enable'
		    				),
		    				array(
	    						'title'         => __( 'Enable to Select Refund Method to Customer', 'woocommerce-rma-for-wcfm' ),
	    						'desc'          => __( 'Enable this to select the refund method to customer.(If wallet is enable then it will work) ', 'woocommerce-rma-for-wcfm' ),
	    						'default'       => 'no',
	    						'type'          => 'checkbox',
	    						'id' 		=> 'mwb_wrffm_select_refund_method_enable'
		    				),
		    				array(
	    						'title'         => __( 'Cancel Order Amount to Wallet', 'woocommerce-rma-for-wcfm' ),
	    						'desc'          => __( 'Enable this for add the Order amount with coupon discount to customer wallet for those order which is paid and having status Processing and Completed and going to be cancelled due to some reason.', 'woocommerce-rma-for-wcfm' ),
	    						'default'       => 'no',
	    						'type'          => 'checkbox',
	    						'id' 		=> 'mwb_wrffm_return_wallet_cancelled'
		    				),
		    				
		    				array(
	    						'title'         => __( 'Wallet Coupon Prefix', 'woocommerce-rma-for-wcfm' ),
	    						'desc'          => __( ' Prefix for using wallet amount using coupon', 'woocommerce-rma-for-wcfm' ),
	    						'default'       => '',
	    						'type'          => 'text',
	    						'id' 		=> 'mwb_wrffm_return_coupon_prefeix'
		    				),
		    				array(
		    						'type' 	=> 'sectionend',
		    				),
	    				);
	    								
	    			return apply_filters( 'mwb_wrffm_get_wallet_settings' . $this->id, $settings );
	    			
	    		
	    		}
	    		else 
		    	{	
		    		$settings = array(
		    				
										array(
											'title' => __( 'Refund Products Setting', 'woocommerce-rma-for-wcfm' ),
											'type' 	=> 'title',
										),
										
						    			array(
					    					'title'         => __( 'Enable', 'woocommerce-rma-for-wcfm' ),
					    					'desc'          => __( 'Enable Refund Request', 'woocommerce-rma-for-wcfm' ),
					    					'default'       => 'no',
					    					'type'          => 'checkbox',
						    				'id' 		=> 'mwb_wrffm_return_enable'
										),
						    				
					    				array(
					    					'title'         => __( 'Sale Items', 'woocommerce-rma-for-wcfm' ),
					    					'desc'          => __( 'Enable Refund Request for Sale Items', 'woocommerce-rma-for-wcfm' ),
					    					'default'       => 'no',
					    					'type'          => 'checkbox',
					    					'id' 		=> 'mwb_wrffm_return_sale_enable'
					    				),
		    				
					    				array(
				    						'title'         => __( 'Include Tax', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Include Tax with Product Refund Request.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_tax_enable'
					    				),
						    				
					    				array(
					    					'title'         => __( 'Exclude Shipping Fee', 'woocommerce-rma-for-wcfm' ),
					    					'desc'          => __( 'Exclude Shipping Cost from Refunded amount.', 'woocommerce-rma-for-wcfm' ),
					    					'default'       => 'no',
					    					'type'          => 'checkbox',
					    					'id' 		=> 'mwb_wrffm_return_shipcost_enable'
					    				),
					    				
		    							array(
				    						'title'         => __( 'Enable Refund Note on Product Page', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable to show the note on product page.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_note_enable'
					    				),
		    				
					    				array(
				    						'title'         => __( 'Enable Auto Accept Product Refund Request', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable to Auto Accept Product Refund Request.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_autoaccept_enable'
					    				),
		    				
					    				array(
					    					
				    						'title'         => __( 'Maximum Number of Days for Auto Accept', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'If Refund Request submitted within selected number of days then Refund Request is auto approved. If value is 0 or blank then automatic accept request function is not work.', 'woocommerce-rma-for-wcfm' ),
				    						'type'          => 'number',
				    						'custom_attributes'   => array('min'=>'0'),
				    						'id' 		=> 'mwb_wrffm_auto_return_days'
					    				),
		    				
					    				array(
				    						'title'         => __( 'Refund Note on Product Page', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'This note is shown on product detail page.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'This Product is not refundable',
				    						'type'          => 'textarea',
				    						'desc_tip' =>  true,
				    						'id' 		=> 'mwb_wrffm_return_note_message'
					    				),
						    				 
						    			array(
					    					'title'         => __( 'Maximum Number of Days', 'woocommerce-rma-for-wcfm' ),
					    					'desc'          => __( 'If days exceeds from the day of order delivered then Refund Request will not be send. If value is 0 or blank then Refund button will not visible at order detail page.', 'woocommerce-rma-for-wcfm' ),
					    					'type'          => 'number',
					    					'custom_attributes'   => array('min'=>'0'),
					    					'id' 		=> 'mwb_wrffm_return_days'
						    			),
		    				
					    				array(
				    						'title'         => __( 'Minimum Order Amount', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Minimum Order amount must be greater or equal to this amount. Keep blank to enable Refund for all Order.', 'woocommerce-rma-for-wcfm' ),
				    						'type'          => 'number',
				    						'custom_attributes'   => array('min'=>'0'),
				    						'desc_tip' =>  true,
				    						'id' 		=> 'mwb_wrffm_return_minimum_amount'
					    				),
					
					    				array(
					    					'title'    => __( 'Exclude Categories', 'woocommerce-rma-for-wcfm' ),
					    					'desc'     => __( 'Select those categories for which products you don\'t want to Refund.', 'woocommerce-rma-for-wcfm' ),
					    					'class'    => 'wc-enhanced-select',
					    					'css'      => 'min-width:300px;',
					    					'default'  => '',
					    					'type'     => 'multiselect',
					    					'options'  => $cat_name,
					    					'desc_tip' =>  true,
					    					'id' 		=> 'mwb_wrffm_return_ex_cats'
					    				),
					    				array(
				    						'title'         => __( 'Enable Attachment on Request Form', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable this for user to send the attachment. User can attach <i>.png, .jpg, .jpeg</i> type files.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_attach_enable'
					    				),
					    				array(
				    						'title'         => __( 'Enable Refund Reason Description', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable this for user to send the detail description of Refund request.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_request_description'
					    				),
					    				array(
				    						'title'         => __( 'Enable Manage Stock', 'woocommerce-rma-for-wcfm' ),
				    						'desc'          => __( 'Enable this to increase product stock when Refund request is accepted.', 'woocommerce-rma-for-wcfm' ),
				    						'default'       => 'no',
				    						'type'          => 'checkbox',
				    						'id' 		=> 'mwb_wrffm_return_request_manage_stock'
					    				),
					    				array(
				    						'title'    => __( 'Select the orderstatus in which the order can be Refunded', 'woocommerce-rma-for-wcfm' ),
					    					'desc'     => __( 'Select Order status on which you want Refund request user can submit.', 'woocommerce-rma-for-wcfm' ),
					    					'class'    => 'wc-enhanced-select ',
					    					'css'      => 'min-width:300px;',
					    					'default'  => '',
					    					'type'     => 'multiselect',
					    					'options'  => $status,
					    					'desc_tip' =>  true,
					    					'id' 		=> 'mwb_wrffm_return_order_status'
					    				),
					    				array(
					    						'type' 	=> 'sectionend',
					    				),
		    				
		    						);
		    		
		    		return apply_filters( 'mwb_wrffm_get_settings_exchange' . $this->id, $settings );
		    	} 
	    	}
	    }
	    
	    /**
	     * Save setting
	     * @author makewebbetter<webmaster@makewebbetter.com>
	     * @link http://www.makewebbetter.com/
	     */
	    public function save() {
	    	global $current_section;
	    	$settings = $this->mwb_wrffm_get_settings( $current_section );
	    	WC_Admin_Settings::save_fields( $settings );
	    }
	}
	new MWB_wrffm_admin_interface();
}
?>