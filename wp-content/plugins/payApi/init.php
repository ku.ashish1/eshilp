<?php
/**
 * Plugin Name: Dic Custom Api
 * Plugin URI: https://github.com/Paytm-Payments/
 * Description: This plugin allow you to accept payments using Paytm. This plugin will add a Paytm Payment option on WooCommerce checkout page, when user choses Paytm as Payment Method, he will redirected to Paytm website to complete his transaction and on completion his payment, paytm will send that user back to your website along with transactions details. This plugin uses server-to-server verification to add additional security layer for validating transactions. Admin can also see payment status for orders by navigating to WooCommerce > Orders from menu in admin.
 * Version: 2.0
 * Author: Paytm
 * Author URI: https://developer.paytm.com/
 * Tags: Paytm, Paytm Payments, PayWithPaytm, Paytm WooCommerce, Paytm Plugin, Paytm Payment Gateway
 * Requires at least: 4.0.1
 * Tested up to: 5.0
 * Requires PHP: 5.6
 * Text Domain: Paytm Payments
 * WC requires at least: 2.0.0
 * WC tested up to: 4.0
 */
    add_action( 'rest_api_init','paytm_checksume' );
    add_action( 'rest_api_init','check_paytm_checksume');
    add_action( 'rest_api_init','lang_hi' );
    add_action( 'rest_api_init','lang_feeding' );
    add_action( 'rest_api_init','get_attributes_by_category' );
    function check_paytm_checksume()
    {
        register_rest_route('wpptm/v1','/validate-checksum', array(
                'methods' => 'GET',
                'callback' => 'ValidateCheckSum',
                'args'     => array(
                    "order_id"=>"",
                    "checksum"=>"",
                ),
            )
        );
    }
    function ValidateCheckSum($data)
    {
        if(empty($data['order_id']) && empty($data['checksum'])){
            echo(json_encode(["error"=>"Checksum mismatch with Order_id"]));
        }else{
            /* import checksum generation utility */
            require_once(plugin_dir_path( __FILE__ )."../paytm-payments/includes/PaytmChecksum.php");

            /* string we need to verify against checksum */  
            $body = '{"MID":"Digita62153518081968","ORDER_ID":$data["order_id"]}';

            /* checksum that we need to verify */
            $paytmChecksum = $data['checksum'];

            /**
            * Verify checksum
             */
            $isVerifySignature = PaytmChecksum::verifySignature($body, 'CWe2twgn@RY8oeTY', $paytmChecksum);
            if($isVerifySignature) {
                echo(json_encode(["result"=>"Checksum Matched"]));
            } else {
                echo(json_encode(["result"=>"Checksum Mismatched"]));
            }
        }
    }
    function paytm_checksume() {
        register_rest_route('wpptm/v1','/create-checksum', array(
                        'methods' => 'GET',
                        'callback' => 'createCheckSum',
                        'args'     => array(
                            "order_id"=>"",
                        ),
                    )
                );
    }
    function createCheckSum($data)
    {
        if(empty($data['order_id'])){
            echo(json_encode(["error"=>"Order_id is required"]));
        }else{
            $is_order = validate_order_id($data['order_id']);
            // return $is_order;
            if($is_order){
                /* import checksum generation utility */
                require_once(plugin_dir_path( __FILE__ )."../paytm-payments/includes/PaytmChecksum.php");

                /* initialize an array */
                $paytmParams = array();
                // $paytmParams["body"] =[
                //     "requestType"   => "Payment",
                //     "mid"           => "Digita62153518081968",
                //     "websiteName"   => "APPSTAGING",
                //     "channel_Id"    =>"WAP",
                //     "orderId"       => $data['order_id'],
                //     "callbackUrl"   => "https://designs.digitalindiacorporation.in/new/digi/wp-json/wpptm/v1/validate-checksum",
                //     "txnAmount"     => array(
                //         "value"     => "1.00",
                //         "currency"  => "INR",
                //     ),
                //     "userInfo"      => array(
                //         "custId"    => "CUST_001",
                //     ),
                // ];
                /* add parameters in Array */
                $paytmParams["REQUEST_TYPE"] = "Payment";
                $paytmParams["MID"] = "Digita62153518081968";
                $paytmParams["ORDER_ID"] = $data['order_id'];
                $paytmParams["CHANNEL_ID"]="WAP";
                $paytmParams["WEBSITE"]="APPSTAGING";
                $paytmParams["INDUSTRY_TYPE_ID"]="Retail";
                

                // $paytmChecksum = PaytmChecksum::generateSignature(json_encode($paytmParams["body"], JSON_UNESCAPED_SLASHES), "CWe2twgn@RY8oeTY");
                // $paytmParams["head"] = array(
                //     "signature"    => $checksum
                // );
                // $post_data = json_encode($paytmParams, JSON_UNESCAPED_SLASHES);
                /* for Staging */
                // $url = "https://securegw-stage.paytm.in/theia/api/v1/initiateTransaction?mid=Digita62153518081968&orderId="+$data['order_id'];
                // $ch = curl_init($url);
                // curl_setopt($ch, CURLOPT_POST, 1);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                // curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
                // $response = curl_exec($ch);
                // echo  $response;
                /**
                * Generate checksum by parameters we have
                */
                $paytmChecksum = PaytmChecksum::generateSignature($paytmParams, 'CWe2twgn@RY8oeTY');
                echo(json_encode(["key"=>$paytmChecksum,"callback"=>"https://designs.digitalindiacorporation.in/new/digi/wp-json/wpptm/v1/validate-checksum"]));
            }else{
                echo(json_encode(["error"=>"orderId is not valid"]));
            }
        }
    }
    function validate_order_id($order_id)
    {
        if(get_post_type($order_id) == "shop_order"){
            return true;
        }else{
            return false;
        }
    }
    function lang_hi() {
        register_rest_route('lang/v1','/lang', array(
                        'methods' => 'GET',
                        'callback' => 'languages',
                        'args'     => array(
                            "lang"=>"",
                            "all"=>"",
                        ),
                    )
                );
    }
    function languages($data)
    {
        if(empty($data['lang'])){
            echo(json_encode(["error"=>"lang is required"]));
        }else{
            global $wpdb;
            $status="where status=2";
            if($data['all']==1){
                $status="where translated=''";
            }
            $getData= $wpdb->get_results( "SELECT id,original,translated FROM {$wpdb->prefix}trp_dictionary_en_us_{$data['lang']} $status", OBJECT); 
            if(!empty($getData)){
                echo(json_encode(["success"=>$getData]));
            }else{
                echo(json_encode(["success"=>[]]));
            }
        }
    }
    function lang_feeding() {
        register_rest_route('lang/v1','/feeding', array(
                        'methods' => 'POST',
                        'callback' => 'language_feeding',
                        'args'     => array(
                            "id"=>"",
                            "translate"=>"",
                            "lang"=>"",
                        ),
                    )
                );
    }
    function language_feeding($data){
        global $wpdb;
        // if($data['language']=="hi_in")
        $response=$wpdb->query("UPDATE {$wpdb->prefix}trp_dictionary_en_us_{$data['lang']} set `translated`='".$data['translate']."',status=2 where `id`=".$data['id']);   
        echo(json_encode($response)); 
    }
    function get_attributes_by_category() {
        register_rest_route('custom/v1','/attributes_by_category', array(
                        'methods' => 'GET',
                        'callback' => 'get_attributes',
                        'args'     => array(
                            "cat_id"=>""
                        ),
                    )
                );
    }
    function get_attributes($data){
        global $wpdb;
        if($data['cat_id']){
            $getData= $wpdb->get_row("SELECT * FROM {$wpdb->prefix}options WHERE option_name='wcfm_category_attributes_mapping' limit 1");
            if(getData){
                $un_data = unserialize($getData->option_value);
                $search_cat=$data['cat_id'];
                $filter_data=array_filter($un_data,  function ($key) use ($search_cat) {
                                       return in_array($key, [(int)$search_cat]);
                                    },ARRAY_FILTER_USE_KEY );
                if(count($filter_data[$search_cat])){
                    $attribute_list=[];
                    foreach ($filter_data[$search_cat] as $key => $value) {
                        $attribute=$wpdb->get_row("SELECT * FROM {$wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name='".substr($value,3)."' limit 1");
                        $attribute->attribute_details=get_terms( array(
                            'taxonomy' => $value,
                            'hide_empty' => false,
                        ) );
                        $attribute_list[]=$attribute;
                    }
                    echo(json_encode(["success"=>$attribute_list]));
                }else{
                    echo(json_encode(["success"=>[]]));
                }
            }else{
                echo(json_encode(["success"=>[]]));
            }
        }else{
            echo(json_encode(["error"=>"category id is required"]));
        } 
    }
?>