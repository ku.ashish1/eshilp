<?php
/**
 * WCFM_CAS install
 *
 * @package  WCFM_CAS/core
 */
defined( 'ABSPATH' ) || exit;

class WCFM_CAS_Install {

    private static $backup_dir = WCFM_CAS_PLUGIN_DIR . 'backup';

    public function __construct() {
        add_action( 'admin_notices', array( $this, 'fail_notice' ) );
    }

    /**
     * Install WCFM CAS
     */
    public static function install() {
        if ( ! get_option( 'wcfm_cas_installed' ) ) {
            if ( self::check_for_writable_backup_dir( self::$backup_dir ) ) {
                self::modify_wpconfig();
                self::modify_htaccess();
            }
            update_option( 'wcfm_cas_installed', 1 );
            flush_rewrite_rules();
        }
    }

    private static function check_for_writable_backup_dir( $backup_dir ) {

        if ( ! file_exists( $backup_dir ) ) {
            @mkdir( $backup_dir, 0775, true );
            if ( ! file_exists( $backup_dir ) ) {
                return false;
            }
        }

        // If we still cannot write, bail.
        if ( ! is_writable( $backup_dir ) ) {
            return false;
        }

        // Create an index.php in there for security reasons
        $idx_file = rtrim( $backup_dir, '/\\' ) . '/index.php';
        if ( ! is_file( $idx_file ) ) {
            @file_put_contents( $idx_file, implode( PHP_EOL . PHP_EOL, array( '<?php', '//Nothing to see here', "header( 'HTTP/1.0 403 Forbidden' );" ) ) );
        }

        return true;
    }

    private static function modify_wpconfig() {
        $file_name = 'wp-config-copy.php';
        $file_path = '';

        if ( file_exists( ABSPATH . $file_name ) ) {
            $file_path = ABSPATH . $file_name;
        } elseif ( @file_exists( dirname( ABSPATH ) . "/{$file_name}" ) ) {
            $file_path = dirname( ABSPATH ) . "/{$file_name}";
        }
        $success = false;
        if ( $file_path && @copy( $file_path, self::$backup_dir . "/{$file_name}" ) ) {
            $rule_name = 'JWT_AUTH_SECRET_KEY';
            $rule_val = SECURE_AUTH_KEY;
            $define_rule = "define( '{$rule_name}', '{$rule_val}' );" . PHP_EOL;
            $lines = array();
            foreach ( file( $file_path ) as $line ) {
                if ( strpos( $line, $rule_name ) !== false )
                    continue; //if previously present just skip it as we are adding it anyway!

                array_push( $lines, $line );

                if ( strpos( $line, 'NONCE_SALT' ) !== false ) {
                    array_push( $lines, $define_rule );
                }
            }
            $success = @file_put_contents( $file_path, $lines );
        }

        if ( ! $success ) {
            set_transient( 'cas-wpconfig-writing-failure', true, 5 );
        }
    }

    private static function modify_htaccess() {
        $file_name = '.htaccess';
        $file_path = ABSPATH . $file_name;
        if ( file_exists( $file_path ) && ! self::htaccess_modification_exists( $file_path ) && @copy( $file_path, self::$backup_dir . "/{$file_name}" ) ) {
            header( 'Content-Type: text/plain' );
            $content = implode( PHP_EOL, array(
                    '# JWT Auth - WCFM',
                    '# Enabling HTTP Authorization Header',
                    '<IfModule mod_rewrite.c>',
                    'RewriteEngine on',
                    'RewriteCond %{HTTP:Authorization} ^(.*)',
                    'RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]',
                    '</IfModule>'
                ) ) . PHP_EOL . file_get_contents( $file_path );

            @file_put_contents( $file_path, $content );
        } else {
            set_transient( 'cas-htaccess-writing-failure', true, 5 );
        }
    }

    private static function htaccess_modification_exists( $file_path ) {
        foreach ( file( $file_path ) as $line ) {
            if ( strpos( $line, '[E=HTTP_AUTHORIZATION:%1]' ) !== false ) {
                set_transient( 'cas-htaccess-writing-exists', true, 5 );
                return true;
            }
        }
        return false;
    }

    public static function fail_notice() {
        if ( get_transient( 'cas-wpconfig-writing-failure' ) ) {
            ?>
            <div id="message" class="error settings-error notice is-dismissible">
                <p><?php printf( __( 'Unable to write to wp-config.php. Please do it manually!', 'wcfm-cas' ) ); ?></p>
            </div>
            <?php
            delete_transient( 'cas-wpconfig-writing-failure' );
        }
        if ( get_transient( 'cas-htaccess-writing-failure' ) && !get_transient( 'cas-htaccess-writing-exists' ) ) {
            ?>
            <div id="message" class="error settings-error notice is-dismissible">
                <p><?php printf( __( 'Unable to write to .htaccess file. Please do it manually!', 'wcfm-cas' ) ); ?></p>
            </div>
            <?php
            delete_transient( 'cas-htaccess-writing-failure' );
        }
        delete_transient( 'cas-htaccess-writing-exists' );
    }

    /**
     * Uninstall WCFM CAS
     */
    public static function uninstall() {
        delete_option( 'wcfm_cas_installed' );
        flush_rewrite_rules();
    }

}
