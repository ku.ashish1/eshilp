<?php
defined( 'ABSPATH' ) || exit;
class WCFM_CAS_Frontend {
    public function __construct() {
        add_filter( 'wcfm_query_vars', array( &$this, 'wcfm_cas_query_vars' ), 90 );
        add_filter( 'wcfm_endpoint_wcfm-cas_title', array( &$this, 'wcfm_cas_endpoint_title' ), 90, 2 );
        add_action( 'init', array( &$this, 'wcfm_cas_init' ), 90 );

        add_filter( 'wcfm_endpoints_slug', array( $this, 'wcfm_cas_endpoints_slug' ) );

        // WCFM Menu Filter
        add_filter( 'wcfm_menus', array( &$this, 'wcfm_cas_menus' ), 300 );
        add_filter( 'wcfm_menu_dependancy_map', array( &$this, 'wcfm_cas_menu_dependancy_map' ) );

        add_action( 'wcfm_load_scripts', array( &$this, 'wcfm_cas_load_scripts' ) );
        add_action( 'wcfm_load_styles', array( &$this, 'wcfm_cas_load_styles' ) );
        add_action( 'wcfm_load_views', array( &$this, 'wcfm_cas_load_views' ) );

        //disable product popup in cas_setting page
        add_filter( 'wcfm_blocked_product_popup_views', array( &$this, 'wcfm_cas_block_popup' ) );
        
        // WC Order Payment page <-- changes for WebView element
        add_action( 'after_setup_theme', array( &$this, 'wcfm_cas_auto_login_from_token' ) );
        add_action( 'wp_enqueue_scripts', array( &$this, 'wcfm_cas_no_distraction_order_pay' ) );
        add_filter( 'woocommerce_get_order_item_totals', array( &$this, 'wcfm_cas_remove_payment_method_row' ) );
    }

    public function wcfm_cas_query_vars( $query_vars ) {
        $wcfm_modified_endpoints = wcfm_get_option( 'wcfm_endpoints', array() );
        $cas_qry = array( 'wcfm-cas' => ! empty( $wcfm_modified_endpoints['wcfm-cas'] ) ? $wcfm_modified_endpoints['wcfm-cas'] : 'cas' );
        return array_merge( $query_vars, $cas_qry );
    }

    public function wcfm_cas_endpoint_title( $endpoint ) {
        return __( 'Customer App Settings', 'wcfm-cas' );
    }

    /**
     * WCFM CAS Endpoint Intialize
     */
    public function wcfm_cas_init() {
        global $WCFM_Query;
        // Intialize WCFM Endpoints
        $WCFM_Query->init_query_vars();
        $WCFM_Query->add_endpoints();

        if ( ! get_option( 'wcfm_updated_end_point_wcfm_cas' ) ) {
            // Flush rules after endpoint update
            flush_rewrite_rules();
            update_option( 'wcfm_updated_end_point_wcfm_cas', 1 );
        }
    }

    /**
     * WCFM CAS Endpoint Edit
     */
    function wcfm_cas_endpoints_slug( $endpoints ) {
        $wcfm_cas_endpoints = array( 'wcfm-cas' => 'cas' );
        $endpoints = array_merge( $endpoints, $wcfm_cas_endpoints );
        return $endpoints;
    }

    /**
     * WCFM CAS Menu
     */
    function wcfm_cas_menus( $menus ) {
        global $WCFM;
        if ( current_user_can( 'administrator' ) ) {
            $menus = array_slice( $menus, 0, 3, true ) +
                array( 'wcfm-cas' => array( 'label' => __( 'Customer App', 'wcfm-cas' ),
                        'url' => get_wcfm_cas_url(),
                        'icon' => 'mobile',
                        'priority' => 73
                    ) ) +
                array_slice( $menus, 3, count( $menus ) - 3, true );
        }
        return $menus;
    }

    /**
     * WCFM CAS Menu Dependency
     */
    function wcfm_cas_menu_dependancy_map( $menu_dependency_mapping ) {
        if ( current_user_can( 'administrator' ) ) {
            $menu_dependency_mapping['wcfm-cas'] = 'wcfm-cas';
        }
        return $menu_dependency_mapping;
    }

    public function wcfm_cas_load_scripts( $endpoint ) {
        global $WCFM;
        if ( $endpoint == 'wcfm-cas' ) {
            $WCFM->library->load_collapsible_lib();
            $WCFM->library->load_select2_lib();
            $WCFM->library->load_upload_lib();
            $WCFM->library->load_multiinput_lib();
            wp_enqueue_script( 'wcfm_settings_js', $WCFM->library->js_lib_url . 'settings/wcfm-script-settings.js', array( 'jquery' ), $WCFM->version, true );
            wp_localize_script( 'wcfm_settings_js', 'wcfm_setting_options', array( 'default_tab' => apply_filters( 'wcfm_setting_default_tab', 'wcfm_cas_general_head' ) ) );
            wp_enqueue_script( 'wcfm_cas_js', WCFM_CAS_PLUGIN_URL . 'assets/js/' . 'wcfm-cas-script.js', array( 'jquery' ), CAS()->version, true );
        }
    }

    public function wcfm_cas_load_styles( $endpoint ) {
        global $WCFM;
        if ( $endpoint == 'wcfm-cas' ) {
            wp_enqueue_style( 'wcfm_settings_css', $WCFM->plugin_url . 'assets/css/settings/wcfm-style-settings.css', array(), $WCFM->version );
        }
    }

    public function wcfm_cas_load_views( $endpoint ) {
        global $WCFM_CAS;
        if ( $endpoint == 'wcfm-cas' )
            $WCFM_CAS->template->get_template( 'wcfm-cas-view-settings.php' );
    }

    public function wcfm_cas_block_popup( $blocked_endpoints ) {
        $blocked_endpoints[] = 'wcfm-cas';
        return $blocked_endpoints;
    }
    
    public function wcfm_cas_no_distraction_order_pay() {
        if ( isset( $_SERVER['HTTP_NO_DISTRACTION_MODE'] ) && absint( get_query_var( 'order-pay' ) ) ) {
            wp_enqueue_style( 'wcfm_no_distraction_css', WCFM_CAS_PLUGIN_URL . 'assets/css/wcfm-no-distraction-style.css', array(), WCFM_CAS_VERSION );
        }
    }

    public function wcfm_cas_remove_payment_method_row($total_rows) {
        if ( isset( $_SERVER['HTTP_NO_DISTRACTION_MODE'] ) && absint( get_query_var( 'order-pay' ) ) && isset( $total_rows['payment_method'] ) ) {
            unset( $total_rows['payment_method'] );
        }
        return $total_rows;
    }

    public function wcfm_cas_auto_login_from_token() {
        if ( isset( $_SERVER['HTTP_NO_DISTRACTION_MODE'] ) && class_exists( 'Jwt_Auth' ) && class_exists( 'Jwt_Auth_Public' ) ) {
            $jwt_auth = new Jwt_Auth();
            $jwt_auth_pub = new Jwt_Auth_Public( $jwt_auth->get_plugin_name(), $jwt_auth->get_version() );
            $result = $jwt_auth_pub->validate_token( false );
            if ( !is_wp_error( $result ) && isset( $result->data->user->id ) ) {
                $user_id = $result->data->user->id;
                $user = get_user_by( 'id', intval($user_id) );
                if ( ! is_wp_error( $user ) && isset( $user->ID ) ) {
                    clean_user_cache( $user->ID );
                    wp_clear_auth_cookie();
                    wp_set_current_user( $user->ID, $user->user_login );
                    wp_set_auth_cookie( $user->ID, true, true );
                    update_user_caches( $user );
                }
            }
        }
    }
}