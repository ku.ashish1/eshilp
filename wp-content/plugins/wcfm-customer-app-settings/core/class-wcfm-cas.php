<?php

/**
 * WCFM_CUSTOMER_APP_SETTINGS setup
 *
 * @package  WCFM_CAS/core
 * @since    0.0.1
 */
defined( 'ABSPATH' ) || exit;

/**
 * Main WCFM_CUSTOMER_APP_SETTINGS Class.
 *
 * @class WCFM_CUSTOMER_APP_SETTINGS
 */
final class WCFM_CUSTOMER_APP_SETTINGS {

    /**
     * WCFM_CUSTOMER_APP_SETTINGS version.
     * @var string
     */
    public $version = '0.0.1';

    /**
     * The single instance of the class.
     *
     * @var WCFM_CUSTOMER_APP_SETTINGS
     */
    protected static $_instance = null;

    /**
     * Holds instance of Dependencies class.
     *
     * @access public
     * @var Object Instance of WCFM_CAS_Dependencies class.
     */
    public $dependencies = null;

    /**
     * Third Party Integrations instance.
     *
     * @var WCFM_CAS_Integrations
     */
    public $integrations = null;

    /**
     * User capabilities instance.
     *
     * @var WCFM_CAS_Capabilities
     */
    public $capabilities = null;

    /**
     * The library instance.
     *
     * @var WCFM_CAS_Library
     */
    public $library = null;

    /**
     * The admin instance.
     *
     * @var WCFM_CAS_Admin
     */
    public $admin = null;

    /**
     * The frontend instance.
     *
     * @var WCFM_CAS_Frontend
     */
    public $frontend = null;
    
    public $settings = null;

    /**
     * The endpoints instance.
     *
     * @var WCFM_CAS_Endpoints
     */
    public $endpoints = null;

    /**
     * The template instance.
     *
     * @var WCFM_CAS_Template
     */
    public $template = null;

    /**
     * The ajax instance.
     *
     * @var WCFM_CAS_Ajax
     */
    public $ajax = null;


    /**
     * Main WCFM_CUSTOMER_APP_SETTINGS Instance.
     *
     * Ensures only one instance of WCFM_CUSTOMER_APP_SETTINGS is loaded or can be loaded.
     *
     * @static
     * @see CAS()
     *
     * @return object The WCFM_CUSTOMER_APP_SETTINGS object.
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Primary class constructor.
     *
     * @access public
     */
    public function __construct() {

        if ( ! class_exists( 'WCFM_CAS_Dependencies' ) ) {
            require_once plugin_dir_path( WCFM_CAS_PLUGIN_FILE ) . 'helpers/class-wcfm-cas-dependencies.php';
        }
        $this->dependencies = new WCFM_CAS_Dependencies();
        if ( ! $this->dependencies->can_plugin_activate() ) {
            return;
        }

        $this->define_constants();
        $this->includes();
        $this->init_hooks();

        do_action( 'wcfm_cas_loaded' );
    }

    /**
     * Cloning is forbidden.
     */
    public function __clone() {
        _doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'wcfm-cas' ), '0.0.1' );
    }

    /**
     * Unserializing instances of this class is forbidden.
     */
    public function __wakeup() {
        _doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'wcfm-cas' ), '0.0.1' );
    }

    private function define_constants() {

        if ( ! defined( 'WCFM_CAS_VERSION' ) ) {
            define( 'WCFM_CAS_VERSION', $this->version );
        }

        if ( ! defined( 'WCFM_CAS_PLUGIN_TOKEN' ) ) {
            define( 'WCFM_CAS_PLUGIN_TOKEN', 'wcfm-cas' );
        }

        if ( ! defined( 'WCFM_CAS_CLASS_PREFIX' ) ) {
            define( 'WCFM_CAS_CLASS_PREFIX', 'WCFM_CAS_' );
        }

        if ( ! defined( 'WCFM_CAS_TEXT_DOMAIN' ) ) {
            define( 'WCFM_CAS_TEXT_DOMAIN', 'wcfm-cas' );
        }

        if ( ! defined( 'WCFM_CAS_SERVER_URL' ) ) {
            define( 'WCFM_CAS_SERVER_URL', 'https://wclovers.com' );
        }

        if ( ! defined( 'WCFM_CAS_PLUGIN_DIR' ) ) {
            define( 'WCFM_CAS_PLUGIN_DIR', plugin_dir_path( WCFM_CAS_PLUGIN_FILE ) );
        }

        if ( ! defined( 'WCFM_CAS_PLUGIN_URL' ) ) {
            define( 'WCFM_CAS_PLUGIN_URL', plugin_dir_url( WCFM_CAS_PLUGIN_FILE ) );
        }
    }

    /**
     * What type of request is this?
     *
     * @param  string $type admin, ajax, cron or frontend.
     * @return bool
     */
    private function is_request( $type ) {
        switch ( $type ) {
            case 'admin':
                return is_admin();
            case 'ajax':
                return defined( 'DOING_AJAX' );
            case 'cron':
                return defined( 'DOING_CRON' );
            case 'frontend':
                return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
        }
    }

    private function includes() {
        if ( $this->is_request( 'admin' ) ) { //Admin pages
            $this->load_class( 'install' );
            $CAS_Install = new WCFM_CAS_Install();
        }
    }

    private function init_hooks() {
        register_activation_hook( WCFM_CAS_PLUGIN_FILE, array( 'WCFM_CAS_Install', 'install' ) );
        register_deactivation_hook( WCFM_CAS_PLUGIN_FILE, array( 'WCFM_CAS_Install', 'uninstall' ) );

        add_action( 'wcfm_init', array( &$this, 'init' ) );
    }

    /**
     * initilize plugin on WP init
     */
    function init() {
        global $WCFMmp;
        
        // Init Text Domain
        $this->load_plugin_textdomain();

        //include core functionality
        require_once plugin_dir_path( WCFM_CAS_PLUGIN_FILE ) . 'helpers/wcfm-cas-core-functions.php';
        // Init ajax
        if ( $this->is_request( 'ajax' ) ) { // DOING_AJAX is available in init
            $this->load_class( 'ajax' );
            $this->ajax = new WCFM_CAS_Ajax();
        }
        
        if( $this->is_request( 'frontend' ) ) {
//            $this->load_class( 'settings' );
//            $this->settings = new WCFM_CAS_Settings();
            $this->load_class( 'frontend' );
            $this->frontend = new WCFM_CAS_Frontend();
            $this->load_class( 'template' );
            $this->template = new WCFM_CAS_Template();
            
        }
    }

    /**
     * Load Localization files.
     *
     * Note: the first-loaded translation file overrides any following ones if the same translation is present
     *
     * @access public
     * @return void
     */
    public function load_plugin_textdomain() {
        $locale = is_admin() && function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
        $locale = apply_filters( 'plugin_locale', $locale, 'wcfm-cas' );

        unload_textdomain( 'wcfm-cas' );
        load_textdomain( 'wcfm-cas', WP_LANG_DIR . "/wcfm-customer-app-settings/wcfm-customer-app-settings-$locale.mo" );
        load_textdomain( 'wcfm-cas', false, dirname( plugin_basename( WCFM_CAS_PLUGIN_FILE ) ) . '/languages/' );
    }

    public function load_class( $class_name = '' ) {
        if ( '' != $class_name && '' != WCFM_CAS_PLUGIN_TOKEN ) {
            require_once ('class-' . esc_attr( WCFM_CAS_PLUGIN_TOKEN ) . '-' . esc_attr( $class_name ) . '.php');
        }
    }

}
