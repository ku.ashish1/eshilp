<?php
defined( 'ABSPATH' ) || exit;

class WCFM_CAS_Settings {

    public function __construct() {
        add_action( 'end_wcfm_marketplace_settings', array( &$this, 'add_omnichannel_settings_tab_callback' ) );
    }

    function add_omnichannel_settings_tab_callback( $user_id ) {
        global $WCFM;
        $user_data = get_user_meta( $user_id, 'wcfmmp_profile_settings', true );
        if ( ! $user_data )
            $user_data = array();

        $wcfm_shopify_domain = isset( $user_data['wcfm_omni_shopify_domain'] ) ? $user_data['wcfm_omni_shopify_domain'] : '';
        $wcfm_omni_shopify_version = isset( $user_data['wcfm_omni_shopify_version'] ) ? $user_data['wcfm_omni_shopify_version'] : '';
        $wcfm_shopify_key = isset( $user_data['wcfm_omni_shopify_key'] ) ? $user_data['wcfm_omni_shopify_key'] : '';
        $wcfm_shopify_code = isset( $user_data['wcfm_omni_shopify_code'] ) ? $user_data['wcfm_omni_shopify_code'] : '';
        ob_start();
        ?>
        <?php if ( apply_filters( 'wcfm_is_pref_vendor_omnichannel', true ) ) { ?>
            <!-- collapsible -->
            <div class="page_collapsible" id="wcfm_settings_form_omni_head">
                <label class="wcfmfa fa-handshake"></label>
                <?php _e( 'Omnichannel', 'wcfm-omni' ); ?><span></span>
            </div>
            <div class="wcfm-container">
                <div id="wcfm_settings_form_omni_expander" class="wcfm-content">
                    <?php if ( apply_filters( 'wcfm_is_pref_vendor_omnichannel', true ) ) { ?>
                        <div class="wcfm-omni-shopify">
                            <h3>Shopify</h3>
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_vendors_settings_fields_omni', array(
                                "wcfm_omni_shopify_domain"  => array( 'label' => __( 'Domain', 'wcfm-omni' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele omni_shopify_ele', 'label_class' => 'wcfm_title wcfm_ele omni_shopify_ele', 'value' => $wcfm_shopify_domain ),
                                "wcfm_omni_shopify_version" => array( 'label' => __( 'API version', 'wcfm-omni' ), 'type' => 'select', 'class' => 'wcfm-select wcfm_ele omni_shopify_ele', 'label_class' => 'wcfm_title wcfm_ele omni_shopify_ele', 'options' => array( '2019-10' => '2019-10 (Latest)', '2020-01' => '2020-01 (Release candidate)' ), 'value' => $wcfm_omni_shopify_version ),
                                "wcfm_omni_shopify_key"     => array( 'label' => __( 'API key', 'wcfm-omni' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele omni_shopify_ele', 'label_class' => 'wcfm_title wcfm_ele omni_shopify_ele', 'value' => $wcfm_shopify_key ),
                                "wcfm_omni_shopify_code"    => array( 'label' => __( 'Password', 'wcfm-omni' ), 'type' => 'password', 'class' => 'wcfm-text wcfm_ele omni_shopify_ele', 'label_class' => 'wcfm_title wcfm_ele omni_shopify_ele', 'value' => $wcfm_shopify_code ),
                                    ), $user_id ) );
                            ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="wcfm_clearfix"></div>
            <!-- end collapsible -->
            <?php
        }
        ob_end_flush();
    }

}