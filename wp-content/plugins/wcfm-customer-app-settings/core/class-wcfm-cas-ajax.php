<?php

/**
 * WCFM_CAS ajax class
 *
 * @package  WCFM_CAS/core
 * @since    0.0.1
 */
defined( 'ABSPATH' ) || exit;

class WCFM_CAS_Ajax {

    public $controllers_path;

    public function __construct() {
        $this->controllers_path = WCFM_CAS_PLUGIN_DIR . 'controllers/';
        add_action( 'after_wcfm_ajax_controller', array( &$this, 'cas_ajax_controller_callback' ) );
    }

    public function cas_ajax_controller_callback() {
        if ( isset( $_POST['controller'] ) && $_POST['controller'] === 'wcfm-cas-settings' ) {
            include_once( $this->controllers_path . 'wcfm-cas-controller-settings.php' );
            new WCFM_CAS_Settings_Controller();
        }
        return;
    }

}
