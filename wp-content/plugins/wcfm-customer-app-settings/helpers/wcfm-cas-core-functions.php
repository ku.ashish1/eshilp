<?php

if ( ! function_exists( 'get_wcfm_cas_url' ) ) {

    function get_wcfm_cas_url() {
        global $WCFM;
        $wcfm_page = get_wcfm_page();
        $wcfmg_cas_url = wcfm_get_endpoint_url( 'wcfm-cas', '', $wcfm_page );
        return $wcfmg_cas_url;
    }

}

if ( ! function_exists( 'check_for_writable_backup_dir' ) ) {

    function check_for_writable_backup_dir() {
        $backup_dir = WCFM_CAS_PLUGIN_DIR . 'backup';
        if ( ! file_exists( $backup_dir ) ) {
            @mkdir( $backup_dir, 0775, true );
            if ( ! file_exists( $backup_dir ) ) {
                return false;
            }
        }

        // If we still cannot write, bail.
        if ( ! is_writable( $backup_dir ) ) {
            return false;
        }

        // Create an index.php in there for security reasons
        $idx_file = rtrim( $backup_dir, '/\\' ) . '/index.php';
        if ( ! is_file( $idx_file ) ) {
            @file_put_contents( $idx_file, implode( PHP_EOL, array( '<?php', '//Nothing to see here', "header( 'HTTP/1.0 403 Forbidden' );" ) ) );
        }

        return true;
    }

}

if ( ! function_exists( 'format_products_array_for_select2' ) ) {

    function format_products_array_for_select2( $product_ids = array() ) {
        $products_array = array();
        if ( ! empty( $product_ids ) ) {
            foreach ( $product_ids as $product_id ) {
                if ( get_post_status( $product_id ) ) {
                    $products_array[$product_id] = get_post( absint( $product_id ) )->post_title;
                }
            }
        }
        return $products_array;
    }

}