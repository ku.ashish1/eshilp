<?php

/**
 * WC Dependency Checker
 *
 */
class WCFM_CAS_Dependencies {

    /**
     * Activated plugin list
     * 
     * @var ARRAY_A
     */
    protected $wp_plugins;

    /**
     * Prerequisite plugins
     * 
     * @var ARRAY_N | ARRAY_A based on single site or multisite
     */
    protected $required_plugins;

    /**
     * Activation fail reason
     * 
     * @var STRING 
     */
    protected $failed_check;

    public function __construct() {
        $this->wp_plugins = (array) get_option( 'active_plugins', array() );
        if ( is_multisite() ) {
            $this->wp_plugins = array_merge( $this->wp_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
        }
        $this->required_plugins = $this->set_required_plugins();
    }

    public function check_prerequisites() {
        foreach ( $this->required_plugins as $plugin => $path ) {
            if ( ! $this->is_active( $path ) ) {
                $this->failed_check = $plugin;
                add_action( 'admin_notices', array( $this, 'prerequisite_fail_notice' ) );
                return false;
            }
        }
        return true;
    }

    public function wcfmmp_version_check() {
        global $WCFMmp;
        if ( ! version_compare( $WCFMmp->version, '3.2.0', '>=' ) ) {
            $this->failed_check = 'version';
            add_action( 'admin_notices', array( $this, 'prerequisite_fail_notice' ) );
            return false;
        }
        return true;
    }

    public function php_version_check() {
        if ( version_compare( PHP_VERSION, '5.5.16', '<' ) ) {
            $this->failed_check = 'phpversion';
            add_action( 'admin_notices', array( $this, 'prerequisite_fail_notice' ) );
            return false;
        }
        return true;
    }

    public function can_plugin_activate() {
        return $this->check_prerequisites() && $this->wcfmmp_version_check() && $this->php_version_check();
    }

    function prerequisite_fail_notice() {
        if ( $this->failed_check ) {
            if ( $this->failed_check === 'woocommerce' ) {
                ?>
                <div id="message" class="error">
                    <p><?php printf( __( '%sWCFM Customer App Settings is inactive.%s The %sWooCommerce plugin%s must be active for the WCFM Customer App Settings to work. Please %sinstall & activate WooCommerce%s', 'wcfm-cas' ), '<strong>', '</strong>', '<a target="_blank" href="http://wordpress.org/extend/plugins/woocommerce/">', '</a>', '<a href="' . admin_url( 'plugin-install.php?tab=search&s=woocommerce' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
                </div>
                <?php
            } elseif ( $this->failed_check === 'wcfmmp' ) {
                ?>
                <div id="message" class="error">
                    <p><?php printf( __( '%sWCFM Customer App Settings is inactive.%s The %sWCFM Marketplace%s must be active for the WCFM Customer App Settings to work. Please %sinstall, activate & make sure WCFM Marketplace%s is updated', 'wcfm-cas' ), '<strong>', '</strong>', '<a target="_blank" href="https://wordpress.org/plugins/wc-multivendor-marketplace/">', '</a>', '<a href="' . admin_url( 'plugin-install.php?tab=search&s=wcfm+marketplace' ) . '">', '&nbsp;&raquo;</a>' ); ?></p>
                </div>
                <?php
            } elseif ( $this->failed_check === 'version' ) {
                ?>
                <div id="message" class="error">
                    <p><?php printf( __( 'Warning! This version of WCFM CAS is not compatible with older version of WCFM Marketplace. Please update to WCFM 3.2 or later.', 'wcfm-cas' ) ); ?></p>
                </div>
                <?php
            } elseif ( $this->failed_check === 'phpversion' ) {
                ?>
                <div id="message" class="error">
                    <p><?php printf( __( '%sWCFM Customer App Settings%s requires PHP 5.5.16 or greater. We recommend upgrading to PHP 7.2 or greater.', 'wcfm-cas' ), '<strong>', '</strong>' ); ?></p>
                </div>
                <?php
            }
        }
    }

    /**
     * Set plugin prerequisites
     * All these plugins needs to be activated before activating WCFM CAS
     * 
     * @return ARRAY_A
     */
    private function set_required_plugins() {
        return array(
            'woocommerce' => 'woocommerce/woocommerce.php',
            'wcfmmp'        => 'wc-multivendor-marketplace/wc-multivendor-marketplace.php',
        );
    }

    /**
     * Check if a plugin is activated
     * @param string $path
     * @return BOOLEAN
     */
    private function is_active( $path ) {
        return ( in_array( $path, $this->wp_plugins ) || array_key_exists( $path, $this->wp_plugins ) ) ? true : false;
    }

}