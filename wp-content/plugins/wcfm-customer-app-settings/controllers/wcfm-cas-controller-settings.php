<?php

/**
 * WCFM plugin controllers
 *
 * CAS Plugin Setings Controller
 *
 * @author 		WC Lovers
 * @package 	WCFM_CAS/controllers
 * @version     0.0.1
 */
class WCFM_CAS_Settings_Controller {

    public function __construct() {
        global $WCFM;

        $this->processing();
    }

    public function processing() {
        global $WCFM;

        $wcfm_cas_form_data = array();
        parse_str( $_POST['wcfm_cas_form'], $wcfm_cas_form_data );

        if ( ! defined( 'WCFM_REST_API_CALL' ) ) {
            if ( isset( $wcfm_cas_form_data['wcfm_nonce'] ) && ! wp_verify_nonce( $wcfm_cas_form_data['wcfm_nonce'], 'wcfm_cas' ) ) {
                echo '{"status": false, "message": "' . __( 'Invalid nonce! Refresh your page and try again.', 'wc-frontend-manager' ) . '"}';
                die;
            }
        }

        if ( isset( $wcfm_cas_form_data['logo'] ) ) {
            if ( ! empty( $wcfm_cas_form_data['logo'] ) ) {
                $wcfm_cas_form_data['logo'] = $WCFM->wcfm_get_attachment_id( $wcfm_cas_form_data['logo'] );
            } else {
                $wcfm_cas_form_data['logo'] = '';
            }
        }

        if ( isset( $wcfm_cas_form_data['hero_img'] ) ) {
            if ( ! empty( $wcfm_cas_form_data['hero_img'] ) ) {
                $wcfm_cas_form_data['hero_img'] = $WCFM->wcfm_get_attachment_id( $wcfm_cas_form_data['hero_img'] );
            } else {
                $wcfm_cas_form_data['hero_img'] = '';
            }
        }

        wcfm_update_option( 'wcfm_cas', $wcfm_cas_form_data );

        do_action( 'wcfm_cas_options_updated', $wcfm_cas_form_data );

        echo '{"status": true, "message": "' . __( 'Profile saved successfully', 'wc-frontend-manager' ) . '"}';
        die;
    }

}
