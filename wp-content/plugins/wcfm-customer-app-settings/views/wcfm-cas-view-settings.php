<?php
/**
 * WCFM plugin view
 *
 * WCFM Profile View
 *
 * @author 		WC Lovers
 * @package 	wcfm/view
 * @version   2.2.5
 */
global $WCFM, $wpdb, $blog_id, $wp;

$wcfm_cas_options = wcfm_get_option( 'wcfm_cas', array() );

$logo_url = isset( $wcfm_cas_options['logo'] ) ? $wcfm_cas_options['logo'] : '';
$banner_slider = isset( $wcfm_cas_options['banner_slider'] ) ? $wcfm_cas_options['banner_slider'] : array();
$attr_limit = apply_filters( 'wcfm_cas_sections_item_count', 6 );
$ps1_label = isset( $wcfm_cas_options['label_ps1'] ) ? $wcfm_cas_options['label_ps1'] : array();
$ps1_ids = isset( $wcfm_cas_options['items_ps1'] ) ? $wcfm_cas_options['items_ps1'] : array();
$ps1_product_arr = format_products_array_for_select2($ps1_ids);
$hero_img_url = isset( $wcfm_cas_options['hero_img'] ) ? $wcfm_cas_options['hero_img'] : '';
$ps2_label = isset( $wcfm_cas_options['label_ps2'] ) ? $wcfm_cas_options['label_ps2'] : array();
$ps2_ids = isset( $wcfm_cas_options['items_ps2'] ) ? $wcfm_cas_options['items_ps2'] : array();
$ps2_product_arr = format_products_array_for_select2($ps2_ids);
$vs_label = isset( $wcfm_cas_options['label_vs'] ) ? $wcfm_cas_options['label_vs'] : array();
$vs_ids =  isset( $wcfm_cas_options['stores_vs'] ) ? $wcfm_cas_options['stores_vs'] : array();
$name = isset( $wcfm_cas_options['name'] ) ? $wcfm_cas_options['name'] : '';
$store_banner_width = 250;
$store_banner_height = 150;
$banner_help_text = sprintf(
    __( 'Top slider of Apps home screen. Image size is (%sx%s) pixels.', 'wcfm-cas' ), $store_banner_width, $store_banner_height
);
?>

<div class="collapse wcfm-collapse">
    <div class="wcfm-page-headig">
        <span class="wcfmfa fa-mobile"></span>
        <span class="wcfm-page-heading-text"><?php _e( 'Customer App', 'wcfm-cas' ); ?></span>
        <?php do_action( 'wcfm_page_heading' ); ?>
    </div>
    <div class="wcfm-collapse-content">
        <div id="wcfm_page_load"></div>

        <div class="wcfm-container wcfm-top-element-container">
            <h2><?php _e( 'Customer App Settings', 'wcfm-cas' ); ?></h2>
            <div class="wcfm-clearfix"></div>
        </div>
        <div class="wcfm-clearfix"></div><br />

        <?php do_action( 'before_wcfm_cas_settings' ); ?>

        <form id="wcfm_cas_form" class="wcfm">

            <?php do_action( 'begin_wcfm_cas_settings_form' ); ?>

            <div class="wcfm-tabWrap">

                <!-- collapsible -->
                <div class="page_collapsible" id="wcfm_cas_general_head">
                    <label class="wcfmfa fa-image"></label>
                    <?php _e( 'General', 'wcfm-cas' ); ?><span></span>
                </div>
                <div class="wcfm-container">
                    <div id="wcfm_cas_general_expander" class="wcfm-content">
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_brand_heading"><h2><?php _e( 'Brand', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_brand">
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_logo', array(
                                "logo" => array( 'label' => __( 'App logo', 'wcfm-cas' ), 'type' => 'upload', 'class' => 'wcfm-text banner_type_upload banner_type_field banner_type_single_img wcfm-banner-uploads', 'label_class' => 'wcfm_title  banner_type_field banner_type_single_img', 'prwidth' => 200, 'value' => $logo_url, 'hints' => __( 'Preferred  size is (125x125) pixels.', 'wcfm-cas' ) ),
                            ) ) );
                            ?>
                        </div>
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_top_slider_heading"><h2><?php _e( 'Top Slider', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_top_slider">
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_slider', array(
                                "banner_slider" => array( 'label'       => __( 'Slider', 'wcfm-cas' ), 'type'        => 'multiinput', 'class'       => 'wcfm-text banner_type_upload banner_type_field banner_type_slider wcfm_non_sortable', 'label_class' => 'wcfm_title banner_type_field banner_type_slider', 'value'       => $banner_slider, 'hints'       => $banner_help_text, 'options'     => array(
                                        "image" => array( 'type' => 'upload', 'class' => 'wcfm_gallery_upload banner_type_upload wcfm-banner-uploads', 'prwidth' => 75 ),
                                        "link"  => array( 'type' => 'text', 'class' => 'wcfm-text banner_type_slilder_link', 'placeholder' => __( 'Slider Hyperlink', 'wc-frontend-manager' ) ),
                                    ) ),
                            ) ) );
                            ?>
                        </div>
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_ps1_heading"><h2><?php _e( 'Product Section: 1', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_ps1">
                            <?php
                            $attr_limit1 = apply_filters( 'wcfm_cas_ps1_items_count', $attr_limit );
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_ps1', array(
                                "label_ps1" => array( 'label' => __( 'Label', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'value' => $ps1_label, 'placeholder' => __( 'Featured Products', 'wcfm-cas' ) ),
                                "items_ps1" => array( 'label' => __( 'Choose products', 'wcfm-cas' ), 'type' => 'select', 'attributes' => array( 'multiple' => 'multiple', 'style' => 'width: 60%;' ), 'class' => 'wcfm-select wcfm-cas-ps', 'label_class' => 'wcfm_title', 'options' => $ps1_product_arr, 'value' => $ps1_ids, 'custom_attributes' => array( 'attrlimit' => $attr_limit1 ) ),
                            ) ) );
                            ?>
                        </div>
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_hero_image_heading"><h2><?php _e( 'Hero Image', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_hero_image">
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_her_img', array(
                                "hero_img" => array( 'label' => __( 'Hero Image', 'wcfm-cas' ), 'type' => 'upload', 'class' => 'wcfm-text banner_type_upload banner_type_field banner_type_single_img wcfm-banner-uploads', 'label_class' => 'wcfm_title banner_type_field banner_type_single_img', 'prwidth' => 200, 'value' => $hero_img_url, 'hints' => __( 'Preferred  size is (150x250) pixels.', 'wcfm-cas' ) ),
                            ) ) );
                            ?>
                        </div>
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_ps1_heading"><h2><?php _e( 'Product Section: 2', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_ps1">
                            <?php
                            $attr_limit2 = apply_filters( 'wcfm_cas_ps2_items_count', $attr_limit );
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_ps1', array(
                                "label_ps2" => array( 'label' => __( 'Label', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'value' => $ps2_label, 'placeholder' => __( 'Top rated Products', 'wcfm-cas' ) ),
                                "items_ps2" => array( 'label' => __( 'Choose products', 'wcfm-cas' ), 'type' => 'select', 'attributes' => array( 'multiple' => 'multiple', 'style' => 'width: 60%;' ), 'class' => 'wcfm-select wcfm-cas-ps', 'label_class' => 'wcfm_title', 'options' => $ps2_product_arr, 'value' => $ps2_ids, 'custom_attributes' => array( 'attrlimit' => $attr_limit2 ) ),
                            ) ) );
                            ?>
                        </div>
                        <div class="wcfm_clearfix"></div><br />
                        <div class="wcfm_cas_vs_heading"><h2><?php _e( 'Vendors Section', 'wcfm-cas' ); ?></h2></div>
                        <div class="wcfm_clearfix"></div>
                        <div class="wcfm_cas_vs">
                            <?php
                            $vendor_arr = $WCFM->wcfm_vendor_support->wcfm_get_vendor_list();
                            $attr_limit_vs = apply_filters( 'wcfm_cas_vs_stores_count', $attr_limit );
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_cas_settings_fields_general_vs', array(
                                "label_vs" => array( 'label' => __( 'Label', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title', 'value' => $vs_label, 'placeholder' => __( 'Top rated Products', 'wcfm-cas' ) ),
                                "stores_vs" => array( 'label' => __( 'Choose vendors', 'wcfm-cas' ), 'type' => 'select', 'attributes' => array( 'multiple' => 'multiple', 'style' => 'width: 60%;' ), 'class' => 'wcfm-select wcfm-cas-vs', 'label_class' => 'wcfm_title', 'options' => $vendor_arr, 'value' => $vs_ids, 'custom_attributes' => array( 'attrlimit' => $attr_limit_vs ) ),
                            ) ) );
                            ?>
                        </div>
                        <?php
//                        $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_phone', array(
//                            "phone" => array( 'label' => __( 'Phone', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $phone ),
//                                ), $user_id ) );
//
//                        if ( apply_filters( 'wcfm_is_allow_update_password', true ) ) {
//                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_password', array(
//                                "password"          => array( 'label' => __( 'Password', 'wcfm-cas' ), 'type' => 'password', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => '', 'placeholder' => __( 'Set New Password – Leave blank to retain current password', 'wcfm-cas' ) ),
//                                "password_strength" => array( 'type' => 'html', 'value' => '<div id="password-strength-status"></div>' )
//                                    ), $user_id ) );
//                        }
//
//                        // User Locale Support - 3.0.0
//                        include_once( ABSPATH . 'wp-admin/includes/translation-install.php' );
//                        $translations = wp_get_available_translations();
//                        $languages = get_available_languages();
//
//                        if ( $languages ) {
//                            if ( 'en_US' === $locale || 'en' === $locale ) {
//                                $locale = 'en';
//                            } elseif ( '' === $locale || ! in_array( $locale, $languages, true ) ) {
//                                $locale = 'site-default';
//                            }
//                            $list_laguages = array( 'site-default' => __( 'Site Default', 'wcfm-cas' ), 'en' => 'English (United States)' );
//                            foreach ( $languages as $language ) {
//                                if ( isset( $translations[$language] ) ) {
//                                    $list_laguages[$language] = $translations[$language]['native_name'];
//                                }
//                            }
//
//                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_language', array(
//                                "locale" => array( 'label' => __( 'Language', 'wcfm-cas' ), 'type' => 'select', 'options' => $list_laguages, 'class' => 'wcfm-select wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $locale )
//                                    ), $user_id ) );
//                        }
//
//                        $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_about', array(
//                            "about" => array( 'label' => __( 'About', 'wcfm-cas' ), 'type' => $wpeditor, 'class' => 'wcfm-textarea wcfm_ele wcfm_full_ele ' . $rich_editor, 'label_class' => 'wcfm_title wcfm_full_ele', 'value' => $about ),
//                                ), $user_id ) );
                        ?>
                    </div>
                </div>
                <div class="wcfm_clearfix"></div>
                <!-- end collapsible -->

                <!-- collapsible -->
                <?php if ( $wcfm_is_allow_address_profile = apply_filters( 'wcfm_is_allow_address_profile', true ) ) { ?>
                    <div class="page_collapsible" id="wcfm_profile_address_head">
                        <label class="wcfmfa fa-address-card"></label>
                        <?php _e( 'Address', 'wcfm-cas' ); ?><span></span>
                    </div>
                    <div class="wcfm-container">
                        <div id="wcfm_profile_address_expander" class="wcfm-content">
                            <div class="wcfm_profile_heading"><h3><?php _e( 'Billing', 'wcfm-cas' ); ?></h3></div>
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_billing', array(
                                "bfirst_name" => array( 'label' => __( 'First Name', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text', 'label_class' => 'wcfm_title wcfm_ele', 'value' => '' ),
//                                "blast_name"  => array( 'label' => __( 'Last Name', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $blast_name ),
//                                "baddr_1"     => array( 'label' => __( 'Address 1', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $baddr_1 ),
//                                "baddr_2"     => array( 'label' => __( 'Address 2', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $baddr_2 ),
//                                "bcountry"    => array( 'label' => __( 'Country', 'wcfm-cas' ), 'type' => 'country', 'class' => 'wcfm-select wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'attributes' => array( 'style' => 'width: 60%;' ), 'value' => $bcountry ),
//                                "bcity"       => array( 'label' => __( 'City/Town', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $bcity ),
//                                "bstate"      => array( 'label' => __( 'State/County', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $bstate ),
//                                "bzip"        => array( 'label' => __( 'Postcode/Zip', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $bzip ),
                            ) ) );
                            ?>

                            <div class="wcfm_clearfix"></div>
                            <div class="wcfm_profile_heading"><h3><?php _e( 'Shipping', 'wcfm-cas' ); ?></h3></div>
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_shipping', array(
//                                "same_as_billing" => array( 'label' => __( 'Same as Billing', 'wcfm-cas' ), 'type' => 'checkbox', 'class' => 'wcfm-checkbox wcfm_ele', 'label_class' => 'wcfm_title checkbox_title wcfm_ele', 'value' => 'yes', 'dfvalue' => $same_as_billing ),
//                                "sfirst_name"     => array( 'label' => __( 'First Name', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $sfirst_name ),
//                                "slast_name"      => array( 'label' => __( 'Last Name', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $slast_name ),
//                                "saddr_1"         => array( 'label' => __( 'Address 1', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $saddr_1 ),
//                                "saddr_2"         => array( 'label' => __( 'Address 2', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $saddr_2 ),
//                                "scountry"        => array( 'label' => __( 'Country', 'wcfm-cas' ), 'type' => 'country', 'class' => 'wcfm-select wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'attributes' => array( 'style' => 'width: 60%;' ), 'value' => $scountry ),
//                                "scity"           => array( 'label' => __( 'City/Town', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $scity ),
//                                "sstate"          => array( 'label' => __( 'State/County', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $sstate ),
//                                "szip"            => array( 'label' => __( 'Postcode/Zip', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele same_as_billing_ele', 'label_class' => 'wcfm_title wcfm_ele same_as_billing_ele', 'value' => $szip ),
                            ) ) );
                            ?>
                        </div>
                    </div>
                    <div class="wcfm_clearfix"></div>
                <?php } ?>
                <!-- end collapsible -->

                <!-- collapsible -->
                <?php if ( $wcfm_is_allow_social_profile = apply_filters( 'wcfm_is_allow_social_profile', true ) ) { ?>
                    <div class="page_collapsible" id="sm_profile_form_social_head">
                        <label class="wcfmfa fa-users"></label>
                        <?php _e( 'Social', 'wcfm-cas' ); ?><span></span>
                    </div>
                    <div class="wcfm-container">
                        <div id="wcfm_profile_form_social_expander" class="wcfm-content">
                            <?php
                            $WCFM->wcfm_fields->wcfm_generate_form_field( apply_filters( 'wcfm_profile_fields_social', array(
//                                "twitter"     => array( 'label' => __( 'Twitter', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $twitter ),
//                                "facebook"    => array( 'label' => __( 'Facebook', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $facebook ),
//                                "instagram"   => array( 'label' => __( 'Instagram', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $instagram ),
//                                "youtube"     => array( 'label' => __( 'Youtube', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $youtube ),
//                                "linkdin"     => array( 'label' => __( 'Linkedin', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $linkdin ),
//                                "google_plus" => array( 'label' => __( 'Google Plus', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $google_plus ),
//                                "snapchat"    => array( 'label' => __( 'Snapchat', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $snapchat ),
//                                "pinterest"   => array( 'label' => __( 'Pinterest', 'wcfm-cas' ), 'type' => 'text', 'class' => 'wcfm-text wcfm_ele', 'label_class' => 'wcfm_title wcfm_ele', 'value' => $pinterest ),
                            ) ) );
                            ?>
                        </div>
                    </div>
                    <div class="wcfm_clearfix"></div>
                <?php } ?>
                <!-- end collapsible -->

                <?php do_action( 'end_wcfm_cas_settings_form' ); ?>

            </div>

            <div id="wcfm_profile_submit" class="wcfm_form_simple_submit_wrapper">
                <div class="wcfm-message" tabindex="-1"></div>

                <input type="submit" name="save-data" value="<?php _e( 'Save', 'wcfm-cas' ); ?>" id="wcfm_cas_save_button" class="wcfm_submit_button" />
            </div>
            <input type="hidden" name="wcfm_nonce" value="<?php echo wp_create_nonce( 'wcfm_cas' ); ?>" />
        </form>
        <?php
        do_action( 'after_wcfm_cas_settings' );
        ?>
    </div>
</div>