<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wcfm' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'S=C$|,t^8/lsnWg_Xe+zic]{]-vFX*?YhP_7t b(/N`*aj:7h*OSxvTuajlGgfv>' );
define( 'SECURE_AUTH_KEY',  '59 8!!!0h2H!<@Q<1cT6W/9T? ){6jK<fYrP_muS> L0f] eF&*X30;pNNk_kbhE' );
define( 'LOGGED_IN_KEY',    '}iX[UMB1S~qr]1vR5Cv~(`;,7n7/c]Tmt=VeUuR]xXa7R-OE|$|4W)kRk`1c(RnG' );
define( 'NONCE_KEY',        'IsqiK;wnWS|Nz>KmI51{P%Dnz!^?(&TQ<Id Hh@`3fmr#y+JZwenxGd%vP+{18n~' );
define( 'AUTH_SALT',        'O0/>Omg/x,j02$TSyN ]}7Mw1z|[GU7mVeU}6iHJ_3No,+,1;[c11uJsp:f-]^(&' );
define( 'SECURE_AUTH_SALT', 'y,{g,hp|9dv_C202bE/WN-W{!tA*^78*N7U,oHNIJNH6TzEY^+`!HXuwpv#[iU<[' );
define( 'LOGGED_IN_SALT',   'rs^#3w@1WQ-kCuPV5Gl$_CQ?jO=F25fs~-Ime4`&JGux(5,`HzGG##n@M/N9cO{V' );
define( 'NONCE_SALT',       '1uZqpzLc.S)!e4,M@:W|lmeFR9i{DVWP{|i,7qyVFSR,%xVQ+Ijb EK0DK43?s^O' );
define( 'JWT_AUTH_SECRET_KEY', '59 8!!!0h2H!<@Q<1cT6W/9T? ){6jK<fYrP_muS> L0f] eF&*X30;pNNk_kbhE' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
