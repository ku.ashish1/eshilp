<?php
/**
 * Plugin Name: WCFM Customer App Settings
 * Plugin URI: https://wclovers.com/product/wcfm-customer-app-settings/
 * Description: Settings page for Customer App
 * Author: WC Lovers
 * Version: 0.0.1 Beta
 * Author URI: https://wclovers.com
 *
 * Text Domain: wcfm-cas
 * Domain Path: /i18n/languages/
 *
 * @package  WCFM_CAS
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

// Define WCFM_CAS_PLUGIN_FILE.
if ( ! defined( 'WCFM_CAS_PLUGIN_FILE' ) ) {
	define( 'WCFM_CAS_PLUGIN_FILE', __FILE__ );
}

// Include the main Omnichannel class.
if ( ! class_exists( 'WCFM_CUSTOMER_APP_SETTINGS' ) ) {
	include_once plugin_dir_path( __FILE__ ) . 'core/class-wcfm-cas.php';
}

/**
 * Main instance of WCFM CAS.
 *
 * Returns the main instance of WCFM_CUSTOMER_APP_SETTINGS to prevent the need to use globals.
 *
 * @return WCFM_CUSTOMER_APP_SETTINGS
 */

function CAS() {
    return WCFM_CUSTOMER_APP_SETTINGS::instance();
}

$GLOBALS['WCFM_CAS'] = CAS();