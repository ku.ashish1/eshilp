'use strict';
var app_settings_manager = ( function ( $, D ) {
  var $body, $form, $generalWrapper, $ps1, $ps2, $vs, $submitBtn, $msgWrapper, $customCSS;
  var _pvt = {
    cacheDom: function cacheDom( ) {
      $body = $( D.body );
      $form = $( '#wcfm_cas_form' );
      $generalWrapper = $form.find( '#wcfm_cas_general_expander' );
      $ps1 = $form.find( '#items_ps1' ); //Product section 1 select2
      $ps2 = $form.find( '#items_ps2' ); //Product section 2 select2
      $vs = $form.find( '#stores_vs' ); //Vendors section select2
      $submitBtn = $form.find( '#wcfm_cas_save_button' );
      $msgWrapper = $form.find( '.wcfm-message' );

      $customCSS = $( '#wcfm_custom_css-css' );
      return this;
    },
    bindEvents: function bindEvents() {
      $generalWrapper.on( 'click', '#banner_slider .add_multi_input_block', _pvt.resetBlockHeight );
      $ps1.on('change', _pvt.resetBlockHeight );
      $ps2.on('change', _pvt.resetBlockHeight );
      $vs.on('change', _pvt.resetBlockHeight );
      $submitBtn.on( 'click', this.saveFormData );
      return this;
    },
    saveFormData: function saveFormData( event ) {
      event.preventDefault();
      $msgWrapper.html( '' ).removeClass( 'wcfm-error wcfm-success' ).slideUp();
      $form.block( {
        message: null,
        overlayCSS: {
          background: '#fff',
          opacity: 0.6
        }
      } );
      var data = {
        action: 'wcfm_ajax_controller',
        controller: 'wcfm-cas-settings',
        wcfm_cas_form: $form.serialize()
      }
      $.post( wcfm_params.ajax_url, data, function ( response ) {
        if ( response ) {
          var responseJSON = $.parseJSON( response );
          wcfm_notification_sound.play();
          if ( responseJSON.status ) {
            $msgWrapper.html( '<span class="wcicon-status-completed"></span>' + responseJSON.message ).addClass( 'wcfm-success' ).slideDown();
            if ( responseJSON.file )
              $customCSS.attr( 'href', responseJSON.file );
          } else {
            $msgWrapper.html( '<span class="wcicon-status-cancelled"></span>' + responseJSON.message ).addClass( 'wcfm-error' ).slideDown();
          }
          _pvt.wcfmMessageHide();
          $form.unblock();
        }
      } );
    },
    wcfmMessageHide: function wcfmMessageHide() {
      var messageCloseTimer;
      clearTimeout( messageCloseTimer );
      messageCloseTimer = setTimeout( function () {
        $msgWrapper.slideUp( "slow", function () {
          $msgWrapper.html( '' ).removeClass( 'wcfm-success wcfm-error' );
        } );
      }, 10000 );
    },
    resetBlockHeight: function resetBlockHeight() {
      resetCollapsHeight( $generalWrapper );
    },
    startup: function startup() {
      $ps1.select2( _pvt.getSelectConfig( $ps1, $wcfm_product_select_args ) );
      $ps2.select2( _pvt.getSelectConfig( $ps2, $wcfm_product_select_args ) );
      $vs.select2( _pvt.getSelectConfig( $vs, { placeholder: "Choose Vendors ..." } ) );
      return this;
    },
    getSelectConfig: function getSelectConfig( $el, args ) {
      var limit = $el.data( 'attrlimit' ) || 6;
      return Object.assign( args, { maximumSelectionLength: limit } );
    }
  };
  var _pub = {
    init: function init() {
      _pvt.cacheDom( ).bindEvents( ).startup( );
    }
  };
  return _pub;
} )( jQuery, document );

jQuery( app_settings_manager.init.bind( app_settings_manager ) );