<?php
/**
 * Enqueue script and styles for child theme
 */
function woodmart_child_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'woodmart-style' ), woodmart_get_theme_info( 'Version' ) );
}
add_action( 'wp_enqueue_scripts', 'woodmart_child_enqueue_styles', 10010 );

add_filter( 'wcfmmp_is_allow_archive_product_sold_by', '__return_false' );

add_action( 'woocommerce_before_shop_loop', 'woodmart_show_sidebar_btn', 25 );

	function woodmart_show_sidebar_btn() {
		if ( wc_get_loop_prop( 'is_shortcode' ) || ! wc_get_loop_prop( 'is_paginated' ) || ! woocommerce_products_will_display() ) return;
		?>
			<div class="woodmart-show-sidebar-btn">
				<span class="fas fa-filter"></span>
				<span><?php esc_html_e('&nbsp; Filter Options', 'woodmart'); ?></span>
			</div>
		<?php

	} 	
/*	
add_filter( 'register_url', 'custom_register_url' );
function custom_register_url( $register_url )
{
    $register_url = get_permalink( $register_page_id = "/new/digi/buyer-registration/" );
    return $register_url;
}
*/
add_filter( 'wcfm_is_force_category_attributes_mapping', '__return_true' );

add_filter( 'wcfm_is_allow_product_category', '__return_false' );

add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_for_menus', 10, 2);

function wc_reg_for_menus( $register, $name = '' ) {
    //  if ( $name == 'pa_brand' ) $register = true;
     return true;
}

//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);
/**
 * AShish Agarwal
 * code for create category gst,or GST TYPE
 */
add_action('product_cat_add_form_fields', 'register_add_gst_type', 10, 1);
add_action('product_cat_edit_form_fields', 'wh_taxonomy_edit_meta_field', 10, 1);
//Product Cat Create page
function register_add_gst_type() {
    ?>   
    <div class="form-field">
        <label for="wh_gst_type"><?php _e('Gst Type', 'wh'); ?></label>
        <select name="wh_gst_type" id="wh_gst_type">
            <option value="0" selected>---Select Gst Category--</option>
            <option value="handicarft">Handicarft</option>
            <option value="handloom">Handloom</option>
        </select>
        <p class="description"><?php _e('Select GST Type', 'wh'); ?></p>
    </div>
    <div class="form-field">
        <label for="wh_gst"><?php _e('GST (in %)', 'wh'); ?></label>
        <input type="number" name="wh_gst" id="wh_gst">
        <p class="description"><?php _e('Enter a GST', 'wh'); ?></p>
    </div>
    <?php
}

//Product Cat Edit page
function wh_taxonomy_edit_meta_field($term) {

    //getting term ID
    $term_id = $term->term_id;

    // retrieve the existing value(s) for this meta field.
    $wh_gst_type = get_term_meta($term_id, 'wh_gst_type', true);
    $wh_gst = get_term_meta($term_id, 'wh_gst', true);
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="wh_gst_type"><?php _e('Select GST Type', 'wh'); ?></label></th>
        <td>
            <select name="wh_gst_type" id="wh_gst_type">
            <?php
             $handicarft_selected='';
             if(esc_attr($wh_gst_type)=="handicarft"){
                 $handicarft_selected="selected";
                 }
            $handloom_selected='';
             if(esc_attr($wh_gst_type)=="handloom"){
                 $handloom_selected="selected";
                 }
            ?>
                <option value="0" selected>---Select Gst Category--</option>
                <option value="handicarft" <?php echo $handicarft_selected; ?>>Handicarft</option>
                <option value="handloom" <?php echo $handloom_selected; ?> >Handloom</option>
            </select>
            <p class="description"><?php _e('Select GST Type', 'wh'); ?></p>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="wh_gst"><?php _e('GST(in %)', 'wh'); ?></label></th>
        <td>
        <input type="number" name="wh_gst" id="wh_gst" value=<?php echo esc_attr($wh_gst) ? esc_attr($wh_gst) : ''; ?>>
        <p class="description"><?php _e('Enter a GST', 'wh'); ?></p>
        </td>
    </tr>
    <?php
}
add_action('edited_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);
add_action('create_product_cat', 'wh_save_taxonomy_custom_meta', 10, 1);

// Save extra taxonomy fields callback function.
function wh_save_taxonomy_custom_meta($term_id) {

    $wh_gst_type = filter_input(INPUT_POST, 'wh_gst_type');
    $wh_gst = filter_input(INPUT_POST, 'wh_gst');

    update_term_meta($term_id, 'wh_gst_type', $wh_gst_type);
    update_term_meta($term_id, 'wh_gst', $wh_gst);
}

//Displaying Additional Columns
add_filter( 'manage_edit-product_cat_columns', 'wh_customFieldsListTitle' ); //Register Function
function wh_customFieldsListTitle( $columns ) {
    $columns['pro_gst_type'] = __( 'GST Type', 'woocommerce' );
    $columns['pro_gst'] = __( 'GST', 'woocommerce' );
    return $columns;
}
add_action( 'manage_product_cat_custom_column', 'wh_customFieldsListDisplay' , 10, 3); //Populating the Columns
function wh_customFieldsListDisplay( $columns, $column, $id ) {
    if ( 'pro_gst_type' == $column ) {
        $columns = esc_html( get_term_meta($id, 'wh_gst_type', true) );
    }
    elseif ( 'pro_gst' == $column ) {
        $columns = esc_html( get_term_meta($id, 'wh_gst', true) );
    }
    return $columns;
}
include 'second_functions.php';
