<?php

add_filter( 'woocommerce_checkout_fields' , 'add_custom_fileld_for_address' );
function add_custom_fileld_for_address( $fields ) {
    $userDetails=get_user_details();
    $billing_district=getDistrictList($userDetails['billing_state_id']);
    $billing_subDistrictList=get_sub_district($userDetails['billing_district_id']);
    $shipping_district=getDistrictList($userDetails['shipping_state_id']);
    $shipping_subDistrictList=get_sub_district($userDetails['shipping_district_id']);
    //echo "<pre>";print_r($userDetails); echo "</pre>";

    $arr1=array(
        ''=>__( 'Select District', 'woocommerce' )
    );
    if($billing_district['status']==true){
        foreach($billing_district['districtList'] as $key=>$value){
            $arr1[$value['district_id']]=__( $value['district_name'], 'woocommerce' );
        }

    }
    $arr2=array(
        ''=>__( 'Select Sub District', 'woocommerce' )
    );
    if($billing_subDistrictList['status']==true){
        foreach($billing_subDistrictList['subDistrictList'] as $key=>$value){
            $arr2[$value['sub_district_id']]=__( $value['sub_district_name'], 'woocommerce' );
        }
    }
    $arr3=array(
        ''=>__( 'Select District', 'woocommerce' )
    );
    if($shipping_district['status']==true){
        foreach($shipping_district['districtList'] as $key=>$value){
            $arr3[$value['district_id']]=__( $value['district_name'], 'woocommerce' );
        }

    }
    $arr4=array(
        ''=>__( 'Select Sub District', 'woocommerce' )
    );
    if($shipping_subDistrictList['status']==true){
        foreach($shipping_subDistrictList['subDistrictList'] as $key=>$value){
            $arr4[$value['sub_district_id']]=__( $value['sub_district_name'], 'woocommerce' );
        }
    }
// echo "<pre>"; print_r($arr3);echo "<pre>";
	/*$fields['billing']['billing_state'] = array(
		'label'     => __('State', 'woocommerce')
	);
	$fields['shipping']['shipping_state'] = array(
		'label'     => __('State', 'woocommerce')
	);*/
    $fields['billing']['billing_district'] = array(
        'label'     => __('District', 'woocommerce'),
        'placeholder'   => _x('District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>81,
        'default'   =>$userDetails['billing_district_id'],
        'options'     => $arr1
    );
    $fields['billing']['billing_sub_district'] = array(
        'label'     => __('Sub District', 'woocommerce'),
        'placeholder'   => _x('Sub District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>82,
        'default'   =>$userDetails['billing_sub_district_id'],
        'options'     => $arr2
    );
    $fields['shipping']['shipping_district'] = array(
        'label'     => __('District', 'woocommerce'),
        'placeholder'   => _x('District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>81,
        'default'   =>$userDetails['shipping_district_id'],
        'options'     => $arr3
    );
    $fields['shipping']['shipping_sub_district'] = array(
        'label'     => __('Sub District', 'woocommerce'),
        'placeholder'   => _x('Sub District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>82,
        'default'   =>$userDetails['shipping_sub_district_id'],
        'options'     => $arr4
    );
    return $fields;
}

//add_action( 'woocommerce_after_edit_account_address_form', 'misha_add_field_edit_account_form' );
function misha_add_field_edit_account_form() {
    $userDetails=get_user_details();
    $district=getDistrictList($userDetails['state_id']);
    $subDistrictList=get_sub_district($userDetails['district_id']);

    $arr1=array(
        ''=>__( 'Select District', 'woocommerce' )
    );
    if($district['status']==true){
        foreach($district['districtList'] as $key=>$value){
            $arr1[$value['district_id']]=__( $value['district_name'], 'woocommerce' );
        }

    }
	woocommerce_form_field(
		'billing_district',
		array(
            'label'     => __('District', 'woocommerce'),
            'placeholder'   => _x('District', 'placeholder', 'woocommerce'),
            'required'  => true,
            'class'     => array('district_select'),
            'clear'     => true,
            'type'      => 'select',
            'priority'  =>81,
            'options'     => $arr1
		),
		get_user_meta( get_current_user_id(), 'billing_district', true )
    );
    
    $arr2=array(
        ''=>__( 'Select Sub District', 'woocommerce' )
    );
    if($subDistrictList['status']==true){
        foreach($subDistrictList['subDistrictList'] as $key=>$value){
            $arr2[$value['sub_district_id']]=__( $value['sub_district_name'], 'woocommerce' );
        }
    }
	woocommerce_form_field(
		'billing_sub_district',
		array(
            'label'     => __('Sub District', 'woocommerce'),
            'placeholder'   => _x('Sub District', 'placeholder', 'woocommerce'),
            'required'  => true,
            'class'     => array('sub_district_select'),
            'clear'     => true,
            'type'      => 'select',
            'priority'  =>82,
            'options'     => $arr2
		),
		get_user_meta( get_current_user_id(), 'billing_sub_district', true )
	);
 
}

add_action('woocommerce_checkout_process', 'add_on_field_validate');

function add_on_field_validate() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['billing_district'] ){
        wc_add_notice( __( 'Please select district' ), 'error' );
    }
    if ( ! $_POST['billing_sub_district'] ){
        wc_add_notice( __( 'Please select sub district' ), 'error' );
    }
    if ( ! $_POST['shipping_district'] ){
        wc_add_notice( __( 'Please select district' ), 'error' );
    }
    if ( ! $_POST['shipping_sub_district'] ){
        wc_add_notice( __( 'Please select sub district' ), 'error' );
    }
}


add_action( 'wp_ajax_nopriv_get_user_details_ajax', 'get_user_details_ajax' );
add_action( 'wp_ajax_get_user_details_ajax', 'get_user_details_ajax' );

function get_user_details_ajax(){
    $userData=get_user_details();
    echo json_encode($userData);
	wp_die();
}

function get_user_details(){
    $userData=wp_get_current_user();
    $userAllData = get_user_meta($userData->ID>0?$userData->ID:0);
    $getaddressfromwcfm = unserialize($userAllData['wcfmvm_static_infos'][0])['address'];
   
    $billing_state_id=(isset($userAllData['billing_state']) && (!empty($userAllData['billing_state'])))?$userAllData['billing_state'][0]:(isset($getaddressfromwcfm['state'])?$getaddressfromwcfm['state']:0);
    $billing_district_id=(isset($userAllData['billing_district']) && (!empty($userAllData['billing_district'])))?$userAllData['billing_district'][0]:(isset($getaddressfromwcfm['district'])?$getaddressfromwcfm['district']:0);
    $billing_sub_district_id=(isset($userAllData['billing_sub_district']) && (!empty($userAllData['billing_sub_district'])))?$userAllData['billing_sub_district'][0]:(isset($getaddressfromwcfm['sub_district'])?$getaddressfromwcfm['sub_district']:0);
    
    $shipping_state_id=(isset($userAllData['shipping_state']) && (!empty($userAllData['shipping_state'])))?$userAllData['shipping_state'][0]:(isset($getaddressfromwcfm['state'])?$getaddressfromwcfm['state']:0);
    $shipping_district_id=(isset($userAllData['shipping_district']) && (!empty($userAllData['shipping_district'])))?$userAllData['shipping_district'][0]:(isset($getaddressfromwcfm['district'])?$getaddressfromwcfm['district']:0);
    $shipping_sub_district_id=(isset($userAllData['shipping_sub_district']) && (!empty($userAllData['shipping_sub_district'])))?$userAllData['shipping_sub_district'][0]:(isset($getaddressfromwcfm['sub_district'])?$getaddressfromwcfm['sub_district']:0);
    
	if($shipping_state_id == 0){
		$shipping_state_id = $billing_state_id;
	}
	if($shipping_district_id == 0){
		$shipping_district_id = $billing_district_id;
	}

	if($shipping_sub_district_id == 0){
		$shipping_sub_district_id = $billing_sub_district_id;
	}
	return array(
        'billing_state_id'=>$billing_state_id,
        'billing_district_id'=>$billing_district_id,
        'billing_sub_district_id'=>$billing_sub_district_id,
        'shipping_state_id'=>$shipping_state_id,
        'shipping_district_id'=>$shipping_district_id,
        'shipping_sub_district_id'=>$shipping_sub_district_id,
		'user_data'=>$userAllData,
		'meta'=>$userData
    );
}



//load_state_district_subDistrict($state_id,$district_id,$sub_district_id);

add_shortcode(  'custom_shipment_track_view_html',  'custom_shipment_track_view' );

function custom_shipment_track_view(){ 
    $html = '<div class="container px-1 px-md-4 py-5 mx-auto">
        <div class="card">
            <div class="row d-flex justify-content-between px-3 top">
                <div class="d-flex">
                    <h5>ORDER <span class="text-primary font-weight-bold">#Y34XDHR</span></h5>
                </div>
                <div class="d-flex flex-column text-sm-right">
                    <p class="mb-0">Expected Arrival <span>01/12/19</span></p>
                    <p>USPS <span class="font-weight-bold">234094567242423422898</span></p>
                </div>
            </div> 
            <div class="row d-flex justify-content-center">
                <div class="col-12">
                    <ul id="progressbar" class="text-center">
                        <li class="active step0"></li>
                        <li class="active step0"></li>
                        <li class="active step0"></li>
                        <li class="step0"></li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-between top">
                <div class="row d-flex icon-content"> <img class="icon" src="https://i.imgur.com/9nnc9Et.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Order<br>Processed</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="https://i.imgur.com/u1AzR7w.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Order<br>Shipped</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="https://i.imgur.com/TkPm63y.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Order<br>En Route</p>
                    </div>
                </div>
                <div class="row d-flex icon-content"> <img class="icon" src="https://i.imgur.com/HdsziHP.png">
                    <div class="d-flex flex-column">
                        <p class="font-weight-bold">Order<br>Arrived</p>
                    </div>
                </div>
            </div>
        </div>
    </div>';

return $html;

}

// Add the custom field "favorite_color"
add_action( 'woocommerce_edit_account_form', 'add_favorite_color_to_edit_account_form' );
function add_favorite_color_to_edit_billing_address_form() {
    $user = wp_get_current_user();

    // First Field
    ?>

    <?php
}

/*
add_action( 'woocommerce_save_account_details', 'save_favorite_color_account_details', 12, 1 );
function save_favorite_color_account_details( $user_id ) {
    // For Favorite color
    if( isset( $_POST['favorite_color'] ) )
        update_user_meta( $user_id, 'favorite_color', sanitize_text_field( $_POST['favorite_color'] ) );

    // For Favorite color 2
    if( isset( $_POST['favorite_color2'] ) )
        update_user_meta( $user_id, 'favorite_color2', sanitize_text_field( $_POST['favorite_color2'] ) );


}
*/

add_filter('woocommerce_shipping_fields', 'custom_woocommerce_shipping_fields');

function custom_woocommerce_shipping_fields($fields)
{
   
	$userDetails=get_user_details();
    $shipping_district=getDistrictList($userDetails['shipping_state_id']);
    $shipping_subDistrictList=get_sub_district($userDetails['shipping_district_id']);
    //echo "<pre>";print_r($userDetails); echo "</pre>";

    $arr3=array(
        ''=>__( 'Select District', 'woocommerce' )
    );
    if($shipping_district['status']==true){
        foreach($shipping_district['districtList'] as $key=>$value){
            $arr3[$value['district_id']]=__( $value['district_name'], 'woocommerce' );
        }

    }
    $arr4=array(
        ''=>__( 'Select Sub District', 'woocommerce' )
    );
    if($shipping_subDistrictList['status']==true){
        foreach($shipping_subDistrictList['subDistrictList'] as $key=>$value){
            $arr4[$value['sub_district_id']]=__( $value['sub_district_name'], 'woocommerce' );
        }
    }
	// echo "<pre>"; print_r($arr3);echo "<pre>";
   
    $fields['shipping_district'] = array(
        'label'     => __('District', 'woocommerce'),
        'placeholder'   => _x('District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>81,
        'default'   =>$userDetails['shipping_district_id'],
        'options'     => $arr3
    );
    $fields['shipping_sub_district'] = array(
        'label'     => __('Sub District', 'woocommerce'),
        'placeholder'   => _x('Sub District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>82,
        'default'   =>$userDetails['shipping_sub_district_id'],
        'options'     => $arr4
    );
    return $fields;
}
add_filter('woocommerce_billing_fields', 'custom_woocommerce_billing_fields');

function custom_woocommerce_billing_fields($fields)
{
   
  /*  $fields['billing_options'] = array(
        'label' => __('NIF', 'woocommerce'), // Add custom field label
        'placeholder' => _x('Your NIF here....', 'placeholder', 'woocommerce'), // Add custom field placeholder
        'required' => false, // if field is required or not
        'clear' => false, // add clear or not
        'type' => 'text', // add field type
        'class' => array('my-css')    // add class name
    );*/
	$userDetails=get_user_details();
    $billing_district=getDistrictList($userDetails['billing_state_id']);
    $billing_subDistrictList=get_sub_district($userDetails['billing_district_id']);

    $arr1=array(
        ''=>__( 'Select District', 'woocommerce' )
    );
    if($billing_district['status']==true){
        foreach($billing_district['districtList'] as $key=>$value){
            $arr1[$value['district_id']]=__( $value['district_name'], 'woocommerce' );
        }

    }
    $arr2=array(
        ''=>__( 'Select Sub District', 'woocommerce' )
    );
    if($billing_subDistrictList['status']==true){
        foreach($billing_subDistrictList['subDistrictList'] as $key=>$value){
            $arr2[$value['sub_district_id']]=__( $value['sub_district_name'], 'woocommerce' );
        }
    }

	$fields['billing_district'] = array(
        'label'     => __('District', 'woocommerce'),
        'placeholder'   => _x('District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>81,
        'default'   =>(!empty($userDetails['billing_district_id']))?$userDetails['billing_district_id']:'',
        'options'     => $arr1
    );
    $fields['billing_sub_district'] = array(
        'label'     => __('Sub District', 'woocommerce'),
        'placeholder'   => _x('Sub District', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('state_select'),
        'clear'     => true,
        'type'      => 'select',
        'priority'  =>82,
        'default'   =>(!empty($userDetails['billing_sub_district_id']))?$userDetails['billing_sub_district_id']:'',
        'options'     => $arr2
    );
   
    return $fields;
}
add_filter( 'woocommerce_default_address_fields' , 'bbloomer_rename_state_province', 9999 );
 
function bbloomer_rename_state_province( $fields ) {
    $fields['state']['label'] = 'State';
    return $fields;
}